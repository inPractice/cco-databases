﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_Session_IPA_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN
	SELECT 
	 [SiteCode]
	,[MemberGUId] 		
	,[SessionGUId] 		
	,[URL]              
	,[SessionStartDttm] 	
	,[SessionYear] 		
	,[SessionMonth] 	
	,[SessionWeek]
	FROM
	(
		SELECT [SiteCode]		= 'IPA'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionUniqueId]
		,	[DeviceTypeName]    = ''
		,	[OSVersionName]     = ''
		,	[URL]               = sd.[Referrer]
		,	[SessionStartDttm] 	= s.[Created]
		,	[SessionYear] 		= YEAR(s.[Created])
		,	[SessionMonth] 		= MONTH(s.[Created])
		,	[SessionWeek]		= DATEPART(wk ,s.[Created])
		FROM [dbo].[Session] s
		LEFT JOIN [dbo].[SessionDetails] sd
			ON sd.[SessionId] = s.[SessionId]
		WHERE s.[Created] >= @Dttm
	) ss
	;
END
;