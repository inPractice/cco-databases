﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_PageView_IPA_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
	 [SiteCode]		
	,[MemberGUId] 	
	,[SessionGUId] 	
	,[PageViewGUID]	
	,[PageItemGUID]	
	,[URL]			
	,[ActivityStartDttm] 
	,[SessionStartDttm]
	,[ActivityYear] 	
	,[ActivityMonth] 
	,[ActivityWeek]
	FROM
	(

		SELECT DISTINCT
		 [SiteCode]			= 'IPA'
		,[MemberGUId] 		= s.[MemberID]
		,[SessionGUId] 		= s.[SessionUniqueID]
		,[PageViewGUID]		= pv.[PropertyID]
		,[PageItemGUID]		= pv.[PropertyID]
		,[URL]				= ''
		,[ActivityStartDttm] 	= pv.[Created]
		,[SessionStartDttm] 	= s.[Created]
		,[ActivityYear] 		= YEAR(pv.[Created])
		,[ActivityMonth] 		= MONTH(pv.[Created])
		,[ActivityWeek]		= DATEPART(wk ,pv.[Created])
		FROM [dbo].[Session] s
		JOIN [dbo].[PageView] pv
			ON pv.[SessionUniqueID] = s.[SessionUniqueID]
			AND pv.[PropertyID] IS NOT NULL
		WHERE s.[Created] >= @Dttm
			AND pv.[PropertyID] IS NOT NULL
	) rpv

END
;