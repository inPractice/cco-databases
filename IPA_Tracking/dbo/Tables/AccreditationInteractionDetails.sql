﻿CREATE TABLE [dbo].[AccreditationInteractionDetails] (
    [AccreditationInteractionDetailsId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccreditationInteractionDetailsUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [AccreditationInteractionId]              INT              NOT NULL,
    [Status]                                  VARCHAR (50)     NULL,
    [HoursEngaged]                            FLOAT (53)       NULL,
    [ProviderId]                              UNIQUEIDENTIFIER NULL,
    [CountryId]                               INT              NULL,
    [AccreditingOrganization]                 VARCHAR (50)     NULL,
    [CertificateType]                         VARCHAR (100)    NULL,
    [DeliveryMethod]                          VARCHAR (100)    NULL,
    [PassedTest]                              BIT              NULL,
    [UserScore]                               FLOAT (53)       NULL,
    [InteractionDetailsCreatedOn]             DATETIME         NOT NULL,
    [InteractionDetailsCompleteOn]            DATETIME         NULL,
    [InteractionDetailsUpdated]               DATETIME         NULL,
    [InsertedOn]                              DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]                              VARCHAR (10)     NULL,
    [CertificateUrl]                          VARCHAR (500)    NULL,
    CONSTRAINT [PK_AccreditationInteractionDetails] PRIMARY KEY CLUSTERED ([AccreditationInteractionDetailsId] ASC),
    CONSTRAINT [FK_AccreditationInteractionDetails_AccreditationInteraction] FOREIGN KEY ([AccreditationInteractionId]) REFERENCES [dbo].[AccreditationInteraction] ([AccreditationInteractionId]) ON DELETE CASCADE,
    CONSTRAINT [UNQ__AccreditationInteractionDetails__AccreditationInteractionDetailsUniqueId] UNIQUE NONCLUSTERED ([AccreditationInteractionDetailsUniqueId] ASC)
);

