﻿CREATE TABLE [dbo].[UrlLog] (
    [UrlLogId]  INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Url]       VARCHAR (MAX) NULL,
    [OtherInfo] VARCHAR (MAX) NULL,
    [CreatedOn] DATETIME      NOT NULL,
    CONSTRAINT [PK_UrlLog] PRIMARY KEY CLUSTERED ([UrlLogId] ASC)
);

