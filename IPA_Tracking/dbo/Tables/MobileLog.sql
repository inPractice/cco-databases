﻿CREATE TABLE [dbo].[MobileLog] (
    [MobileLogId]      INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LocalUserId]      VARCHAR (100)    NOT NULL,
    [MemberId]         UNIQUEIDENTIFIER NULL,
    [UTCTime]          DATETIME         NOT NULL,
    [TimeOffset]       VARCHAR (10)     NOT NULL,
    [Operation]        VARCHAR (100)    NOT NULL,
    [Status]           BIT              NOT NULL,
    [MessageSent]      BIT              NOT NULL,
    [FileName]         VARCHAR (500)    NOT NULL,
    [Parameters]       VARCHAR (MAX)    NULL,
    [EncodeParameters] VARCHAR (MAX)    NULL,
    [Message]          VARCHAR (4000)   NULL,
    [InsertedOn]       DATETIME         NOT NULL,
    [logGuid]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MobileLog] PRIMARY KEY CLUSTERED ([MobileLogId] ASC)
);

