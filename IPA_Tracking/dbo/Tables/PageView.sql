﻿CREATE TABLE [dbo].[PageView] (
    [PageViewId]             INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PageViewUniqueId]       UNIQUEIDENTIFIER NOT NULL,
    [PropertyId]             UNIQUEIDENTIFIER NOT NULL,
    [SessionUniqueId]        UNIQUEIDENTIFIER NOT NULL,
    [SearchCriteriaUniqueId] UNIQUEIDENTIFIER NULL,
    [Created]                DATETIME         NOT NULL,
    [Anonymous]              BIT              NOT NULL,
    [InsertedOn]             DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]             VARCHAR (10)     NULL,
    CONSTRAINT [PK_PageView] PRIMARY KEY CLUSTERED ([PageViewId] ASC),
    CONSTRAINT [UNQ__PageView__PageViewUniqueId] UNIQUE NONCLUSTERED ([PageViewUniqueId] ASC)
);

