﻿CREATE TABLE [dbo].[PageClick] (
    [PageClickId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PageClickUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [PageViewId]        INT              NOT NULL,
    [ActionId]          INT              NOT NULL,
    [CreatedOn]         DATETIME         NOT NULL,
    [InsertedOn]        DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]        VARCHAR (10)     NULL,
    CONSTRAINT [PK_PageClick] PRIMARY KEY CLUSTERED ([PageClickId] ASC),
    CONSTRAINT [FK_PageClick_PageView] FOREIGN KEY ([PageViewId]) REFERENCES [dbo].[PageView] ([PageViewId])
);

