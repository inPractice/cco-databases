﻿CREATE TABLE [dbo].[Curriculum] (
    [CurriculumId]        INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CurriculumUniqueId]  UNIQUEIDENTIFIER NOT NULL,
    [CurriculumName]      VARCHAR (500)    NOT NULL,
    [ProgramId]           UNIQUEIDENTIFIER NULL,
    [CurriculumCreatedOn] DATETIME         NOT NULL,
    CONSTRAINT [PK_Curriculum] PRIMARY KEY CLUSTERED ([CurriculumId] ASC)
);

