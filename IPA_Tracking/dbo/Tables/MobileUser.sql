﻿CREATE TABLE [dbo].[MobileUser] (
    [MobileUserId]        INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MobileLocalId]       VARCHAR (100)    NOT NULL,
    [MemberId]            UNIQUEIDENTIFIER NULL,
    [NurseManager]        BIT              CONSTRAINT [DF_MobileUser_NurseManager] DEFAULT ((0)) NOT NULL,
    [UserName]            VARCHAR (100)    NULL,
    [FirstName]           VARCHAR (100)    NULL,
    [LastName]            VARCHAR (100)    NULL,
    [Email]               VARCHAR (100)    NULL,
    [MobileUserCreatedOn] DATETIME         NOT NULL,
    [InsertedOn]          DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]          VARCHAR (10)     NULL,
    [ExpiryDate]          DATETIME         NULL,
    CONSTRAINT [PK_MobileUser] PRIMARY KEY CLUSTERED ([MobileUserId] ASC)
);

