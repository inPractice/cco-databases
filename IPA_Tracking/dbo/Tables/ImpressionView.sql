﻿CREATE TABLE [dbo].[ImpressionView] (
    [ImpressionViewId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ImpressionViewUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [PropertyId]             UNIQUEIDENTIFIER NOT NULL,
    [SessionUniqueId]        UNIQUEIDENTIFIER NOT NULL,
    [ImpressionId]           UNIQUEIDENTIFIER NULL,
    [Created]                DATETIME         NOT NULL,
    [InsertedOn]             DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]             VARCHAR (10)     NULL,
    CONSTRAINT [PK_ImpressionView] PRIMARY KEY CLUSTERED ([ImpressionViewId] ASC),
    CONSTRAINT [UNQ__Impression__ImpressionViewUniqueId] UNIQUE NONCLUSTERED ([ImpressionViewUniqueId] ASC)
);

