﻿CREATE TABLE [dbo].[Bookmark] (
    [BookmarkId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [BookmarkUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [MemberId]         UNIQUEIDENTIFIER NULL,
    [MobileLocalId]    VARCHAR (100)    NOT NULL,
    [PropertyId]       UNIQUEIDENTIFIER NOT NULL,
    [Created]          DATETIME         NOT NULL,
    [Updated]          DATETIME         NULL,
    [IsActive]         BIT              NOT NULL,
    [InsertedOn]       DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]       VARCHAR (10)     NULL,
    CONSTRAINT [PK_Bookmark] PRIMARY KEY CLUSTERED ([BookmarkId] ASC),
    CONSTRAINT [UNQ__Bookmark__BookmarkUniqueId] UNIQUE NONCLUSTERED ([BookmarkUniqueId] ASC)
);

