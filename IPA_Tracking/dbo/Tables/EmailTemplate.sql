﻿CREATE TABLE [dbo].[EmailTemplate] (
    [EmailTemplateId] UNIQUEIDENTIFIER NOT NULL,
    [EmailBody]       NVARCHAR (MAX)   NOT NULL,
    [EmailSubject]    VARCHAR (1000)   NULL,
    [FromAddress]     VARCHAR (100)    NULL,
    [ToAddress]       VARCHAR (100)    NULL,
    [FromName]        VARCHAR (100)    NULL,
    [Created]         DATETIME         NULL,
    [InsertedOn]      DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]      VARCHAR (10)     NULL,
    CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED ([EmailTemplateId] ASC)
);

