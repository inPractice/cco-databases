﻿CREATE TABLE [dbo].[ChapterView] (
    [ChapterViewId]        INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ChapterViewUniqueId]  UNIQUEIDENTIFIER NOT NULL,
    [ChapterPageId]        UNIQUEIDENTIFIER NOT NULL,
    [AccreditationId]      UNIQUEIDENTIFIER NOT NULL,
    [MemberId]             UNIQUEIDENTIFIER NULL,
    [MobileLocalId]        VARCHAR (100)    NOT NULL,
    [ChapterViewCreatedOn] DATETIME         NOT NULL,
    [InsertedOn]           DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]           VARCHAR (10)     NULL,
    CONSTRAINT [PK_ChapterView] PRIMARY KEY CLUSTERED ([ChapterViewId] ASC),
    CONSTRAINT [UNQ__ChapterView__ChapterViewUniqueId] UNIQUE NONCLUSTERED ([ChapterViewUniqueId] ASC)
);

