﻿CREATE TABLE [dbo].[MobileUserEmailAColleague] (
    [MobileUserEmailACollegueId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MobileUserId]               INT              NOT NULL,
    [MemberId]                   UNIQUEIDENTIFIER NOT NULL,
    [pageId]                     UNIQUEIDENTIFIER NOT NULL,
    [Email]                      VARCHAR (100)    NOT NULL,
    [Message]                    VARCHAR (4000)   NOT NULL,
    [CreatedOn]                  DATETIME         NOT NULL,
    [InsertedOn]                 DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]                 VARCHAR (10)     NOT NULL,
    CONSTRAINT [PK_MobileUserEmailACollegueId] PRIMARY KEY CLUSTERED ([MobileUserEmailACollegueId] ASC),
    CONSTRAINT [FK_MobileUserEmailAColleague_MobileUser] FOREIGN KEY ([MobileUserId]) REFERENCES [dbo].[MobileUser] ([MobileUserId])
);

