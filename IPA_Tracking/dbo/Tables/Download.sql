﻿CREATE TABLE [dbo].[Download] (
    [DownloadId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PageViewId]       INT              NOT NULL,
    [DownloadUniqueID] UNIQUEIDENTIFIER NOT NULL,
    [PageId]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME         NOT NULL,
    [InsertedOn]       DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]       VARCHAR (10)     NULL,
    CONSTRAINT [PK_Download] PRIMARY KEY CLUSTERED ([DownloadId] ASC),
    CONSTRAINT [FK_Download_PageView] FOREIGN KEY ([PageViewId]) REFERENCES [dbo].[PageView] ([PageViewId])
);

