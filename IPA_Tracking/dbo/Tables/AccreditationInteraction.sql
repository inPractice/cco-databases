﻿CREATE TABLE [dbo].[AccreditationInteraction] (
    [AccreditationInteractionId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccreditationInteractionUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [AccreditationId]                  UNIQUEIDENTIFIER NOT NULL,
    [QuestionnaireId]                  UNIQUEIDENTIFIER NOT NULL,
    [QuestionnaireTypeId]              INT              NOT NULL,
    [AllowMultipleAttempts]            BIT              NOT NULL,
    [MemberId]                         UNIQUEIDENTIFIER NOT NULL,
    [SessionUniqueId]                  UNIQUEIDENTIFIER NOT NULL,
    [InteractionCreatedOn]             DATETIME         NOT NULL,
    [InsertedOn]                       DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]                       VARCHAR (10)     NULL,
    CONSTRAINT [PK_AccreditationInteraction] PRIMARY KEY CLUSTERED ([AccreditationInteractionId] ASC),
    CONSTRAINT [FK_AccreditationInteraction_QuestionnaireType] FOREIGN KEY ([QuestionnaireTypeId]) REFERENCES [dbo].[QuestionnaireType] ([QuestionnaireTypeId]),
    CONSTRAINT [UNQ__AccreditationInteraction__AccreditationInteractionUniqueId] UNIQUE NONCLUSTERED ([AccreditationInteractionUniqueId] ASC)
);

