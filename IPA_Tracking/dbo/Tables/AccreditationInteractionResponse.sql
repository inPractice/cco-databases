﻿CREATE TABLE [dbo].[AccreditationInteractionResponse] (
    [AccreditationInteractionResponseId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccreditationInteractionResponseUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [AccreditationInteractionId]               INT              NOT NULL,
    [QuestionId]                               UNIQUEIDENTIFIER NOT NULL,
    [QuestionVersion]                          INT              NOT NULL,
    [FreeFormResponse]                         VARCHAR (2000)   NULL,
    [Weight]                                   FLOAT (53)       NULL,
    [ResponseCreatedOn]                        DATETIME         NOT NULL,
    [InsertedOn]                               DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]                               VARCHAR (10)     NULL,
    CONSTRAINT [PK_AccreditationInteractionResponse] PRIMARY KEY CLUSTERED ([AccreditationInteractionResponseId] ASC),
    CONSTRAINT [FK_AccreditationInteractionResponse_AccreditationInteraction] FOREIGN KEY ([AccreditationInteractionId]) REFERENCES [dbo].[AccreditationInteraction] ([AccreditationInteractionId]) ON DELETE CASCADE,
    CONSTRAINT [UNQ__AccreditationInteractionResponse__AccreditationInteractionResponseUniqueId] UNIQUE NONCLUSTERED ([AccreditationInteractionResponseUniqueId] ASC)
);

