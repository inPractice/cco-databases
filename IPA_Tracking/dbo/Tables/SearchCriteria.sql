﻿CREATE TABLE [dbo].[SearchCriteria] (
    [SearchCriteriaId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SearchCriteriaUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [SessionUniqueId]        UNIQUEIDENTIFIER NOT NULL,
    [MemberId]               UNIQUEIDENTIFIER NULL,
    [MobileLocalId]          VARCHAR (100)    NOT NULL,
    [SearchTerm]             VARCHAR (255)    NOT NULL,
    [Parameters]             VARCHAR (255)    NULL,
    [StartIndex]             INT              NULL,
    [Collection]             VARCHAR (255)    NULL,
    [Source]                 VARCHAR (50)     NULL,
    [Created]                DATETIME         NOT NULL,
    [InsertedOn]             DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]             VARCHAR (10)     NULL,
    CONSTRAINT [PK_SearchCriteria] PRIMARY KEY CLUSTERED ([SearchCriteriaId] ASC),
    CONSTRAINT [UNQ__SearchCriteria__SearchCriteriaUniqueId] UNIQUE NONCLUSTERED ([SearchCriteriaUniqueId] ASC)
);

