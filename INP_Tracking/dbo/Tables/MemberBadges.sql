﻿CREATE TABLE [dbo].[MemberBadges] (
    [MemberBadgeId] INT      IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberTestId]  BIGINT   NOT NULL,
    [BadgeId]       INT      NOT NULL,
    [CreatedDttm]   DATETIME DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]  DATETIME NULL,
    CONSTRAINT [PK_MemberBadge] PRIMARY KEY CLUSTERED ([MemberBadgeId] ASC),
    CONSTRAINT [FK_BadgeMemberBadge] FOREIGN KEY ([BadgeId]) REFERENCES [dbo].[Badges] ([BadgeId]),
    CONSTRAINT [FK_MemberTestMemberBadge] FOREIGN KEY ([MemberTestId]) REFERENCES [dbo].[MemberTest] ([MemberTestId])
);

