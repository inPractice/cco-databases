﻿CREATE TABLE [dbo].[PianoFile] (
    [PianoFileId]    INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FileName]       VARCHAR (200)    NOT NULL,
    [IsSuccess]      BIT              NOT NULL,
    [StartedOn]      DATETIME         NOT NULL,
    [CompletedOn]    DATETIME         NOT NULL,
    [Message]        VARCHAR (MAX)    NULL,
    [Localuserid]    VARCHAR (100)    NULL,
    [MemberId]       UNIQUEIDENTIFIER NULL,
    [ErrorMessage]   VARCHAR (MAX)    NULL,
    [AdditionalInfo] VARCHAR (MAX)    NULL,
    CONSTRAINT [PK_PianoFile] PRIMARY KEY CLUSTERED ([PianoFileId] ASC)
);

