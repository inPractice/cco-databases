﻿CREATE TABLE [dbo].[ProgressStatus] (
    [ProgressId]              INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProgressStatusId]        INT           NOT NULL,
    [ProgressStatusType]      VARCHAR (500) NULL,
    [ProgressStatusText]      VARCHAR (500) NULL,
    [ProgressStatusToolTip]   VARCHAR (500) NULL,
    [ProgressStatusCreatedOn] DATETIME      NOT NULL,
    CONSTRAINT [PK_ProgressStatus] PRIMARY KEY CLUSTERED ([ProgressId] ASC)
);

