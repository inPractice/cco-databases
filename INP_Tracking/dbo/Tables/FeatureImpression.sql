﻿CREATE TABLE [dbo].[FeatureImpression] (
    [FeatureImpressionId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SessionUniqueId]           UNIQUEIDENTIFIER NOT NULL,
    [FeatureId]                 UNIQUEIDENTIFIER NOT NULL,
    [PageViewId]                INT              NOT NULL,
    [PromotedTrackedPropertyId] UNIQUEIDENTIFIER NULL,
    [Created]                   DATETIME         NOT NULL,
    [InsertedOn]                DATETIME         NOT NULL,
    [TimeOffset]                VARCHAR (10)     NOT NULL,
    CONSTRAINT [PK_FeatureImpression] PRIMARY KEY CLUSTERED ([FeatureImpressionId] ASC),
    CONSTRAINT [FK_FeatureImpression_Feature] FOREIGN KEY ([FeatureId]) REFERENCES [dbo].[Feature] ([FeatureId]),
    CONSTRAINT [FK_FeatureImpression_PageView] FOREIGN KEY ([PageViewId]) REFERENCES [dbo].[PageView] ([PageViewId]),
    CONSTRAINT [FK_FeatureImpression_Session] FOREIGN KEY ([SessionUniqueId]) REFERENCES [dbo].[Session] ([SessionUniqueId])
);

