﻿CREATE TABLE [dbo].[InPracticeMemberPreferences] (
    [MemberPreferenceId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberId]           UNIQUEIDENTIFIER NOT NULL,
    [PreferenceId]       INT              NOT NULL,
    [Preference]         VARCHAR (250)    NOT NULL,
    [Active]             BIT              NOT NULL,
    [Created]            DATETIME         NOT NULL,
    [Updated]            DATETIME         NULL,
    CONSTRAINT [PK_MemberPreferenceId] PRIMARY KEY CLUSTERED ([MemberPreferenceId] ASC),
    CONSTRAINT [FK_InPracticeMemberPreferences_InPracticePreferences] FOREIGN KEY ([PreferenceId]) REFERENCES [dbo].[InPracticePreferences] ([PreferenceId])
);

