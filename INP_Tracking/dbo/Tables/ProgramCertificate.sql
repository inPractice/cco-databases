﻿CREATE TABLE [dbo].[ProgramCertificate] (
    [ProgramCertificateId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProgramId]            UNIQUEIDENTIFIER NOT NULL,
    [MemberId]             UNIQUEIDENTIFIER NOT NULL,
    [Status]               INT              NOT NULL,
    [CreatedOn]            DATETIME         NOT NULL,
    [UpdatedOn]            DATETIME         NOT NULL,
    CONSTRAINT [PK_ProgramCertificate] PRIMARY KEY CLUSTERED ([ProgramCertificateId] ASC)
);

