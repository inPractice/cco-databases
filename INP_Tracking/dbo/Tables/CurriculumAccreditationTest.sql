﻿CREATE TABLE [dbo].[CurriculumAccreditationTest] (
    [CurriculumAccreditationQuestionnaireId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CurriculumAccreditationId]              INT              NOT NULL,
    [Questionnaireid]                        UNIQUEIDENTIFIER NOT NULL,
    [AllowedRetakes]                         INT              NOT NULL,
    [CreatedOn]                              DATETIME         NOT NULL,
    CONSTRAINT [PK_CurriculumAccreditationTest] PRIMARY KEY CLUSTERED ([CurriculumAccreditationQuestionnaireId] ASC),
    CONSTRAINT [FK_CurriculumAccreditationTest_CurriculumAccreditation] FOREIGN KEY ([CurriculumAccreditationId]) REFERENCES [dbo].[CurriculumAccreditation] ([CurriculumAccreditationId])
);

