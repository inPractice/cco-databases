﻿CREATE TABLE [dbo].[EulaAccept] (
    [EulaAcceptId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberId]     UNIQUEIDENTIFIER NOT NULL,
    [ActivityId]   UNIQUEIDENTIFIER NOT NULL,
    [SessionId]    UNIQUEIDENTIFIER NOT NULL,
    [Created]      DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]   VARCHAR (10)     NOT NULL,
    [Updated]      DATETIME         NOT NULL,
    [RF_ProgramId] BIGINT           NULL,
    CONSTRAINT [PK_EulaAccept] PRIMARY KEY NONCLUSTERED ([EulaAcceptId] ASC)
);

