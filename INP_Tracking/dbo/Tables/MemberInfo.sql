﻿CREATE TABLE [dbo].[MemberInfo] (
    [MemberId]     BIGINT         NOT NULL,
    [FirstName]    NVARCHAR (100) NOT NULL,
    [LastName]     NVARCHAR (100) NOT NULL,
    [CreatedDttm]  DATETIME       DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm] DATETIME       NULL,
    CONSTRAINT [PK_MemberInfo] PRIMARY KEY CLUSTERED ([MemberId] ASC)
);

