﻿CREATE TABLE [dbo].[AccreditationInteractionResponseAnswer] (
    [AccreditationInteractionResponseAnswerId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccreditationInteractionResponseAnswerUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [AccreditationInteractionResponseId]             INT              NOT NULL,
    [AnswerId]                                       UNIQUEIDENTIFIER NOT NULL,
    [AnswerVersion]                                  INT              NOT NULL,
    [FreeFormResponse]                               VARCHAR (2000)   NULL,
    [Weight]                                         FLOAT (53)       NULL,
    [AnswerCreatedOn]                                DATETIME         NOT NULL,
    [InsertedOn]                                     DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]                                     VARCHAR (10)     NULL,
    CONSTRAINT [PK_AccreditationInteractionResponseAnswer] PRIMARY KEY CLUSTERED ([AccreditationInteractionResponseAnswerId] ASC),
    CONSTRAINT [FK_AccreditationInteractionResponseAnswer_AccreditationInteractionResponse] FOREIGN KEY ([AccreditationInteractionResponseId]) REFERENCES [dbo].[AccreditationInteractionResponse] ([AccreditationInteractionResponseId]) ON DELETE CASCADE,
    CONSTRAINT [UNQ__AccreditationInteractionResponseAnswer__AccreditationInteractionResponseAnswerUniqueId] UNIQUE NONCLUSTERED ([AccreditationInteractionResponseAnswerUniqueId] ASC)
);

