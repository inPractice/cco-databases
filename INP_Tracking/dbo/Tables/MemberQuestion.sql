﻿CREATE TABLE [dbo].[MemberQuestion] (
    [MemberQuestionId]  BIGINT           IDENTITY (1, 1) NOT NULL,
    [SessionUniqueId]   UNIQUEIDENTIFIER NOT NULL,
    [MemberTestId]      BIGINT           NOT NULL,
    [QuestionId]        INT              NOT NULL,
    [PresentedCount]    INT              NOT NULL,
    [IgnoreTestResults] BIT              DEFAULT ((0)) NOT NULL,
    [SecondsRemaining]  INT              NOT NULL,
    [AnsweredCorrectly] BIT              DEFAULT ((0)) NOT NULL,
    [Deferred]          BIT              DEFAULT ((0)) NOT NULL,
    [CreatedDttm]       DATETIME         DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]      DATETIME         NULL,
    CONSTRAINT [PK_MemberQuestion] PRIMARY KEY CLUSTERED ([MemberQuestionId] ASC),
    CONSTRAINT [FK_MemberQuestion_MemberTest] FOREIGN KEY ([MemberTestId]) REFERENCES [dbo].[MemberTest] ([MemberTestId])
);


GO
CREATE NONCLUSTERED INDEX [INP_Tracking_MemberTestId_Session]
    ON [dbo].[MemberQuestion]([MemberTestId] ASC)
    INCLUDE([SessionUniqueId]);

