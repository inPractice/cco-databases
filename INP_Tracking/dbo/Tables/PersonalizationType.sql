﻿CREATE TABLE [dbo].[PersonalizationType] (
    [PersonalizationTypeId] INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TypeName]              NVARCHAR (100) NOT NULL,
    [CreatedOn]             DATETIME       NOT NULL,
    [TimeOffset]            VARCHAR (10)   NOT NULL,
    CONSTRAINT [PK_PersonalizationType] PRIMARY KEY CLUSTERED ([PersonalizationTypeId] ASC)
);

