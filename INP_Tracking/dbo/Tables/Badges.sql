﻿CREATE TABLE [dbo].[Badges] (
    [BadgeId]         INT            IDENTITY (1, 1) NOT NULL,
    [BadgeTypeId]     INT            NOT NULL,
    [ImageLocation]   NVARCHAR (500) NOT NULL,
    [Name]            NVARCHAR (200) NOT NULL,
    [Description]     NVARCHAR (255) NOT NULL,
    [OrdinalPosition] INT            NOT NULL,
    [Points]          INT            NOT NULL,
    [Condition]       INT            NOT NULL,
    [Operand]         NVARCHAR (2)   NULL,
    [Factor]          INT            NULL,
    CONSTRAINT [PK_Badges] PRIMARY KEY CLUSTERED ([BadgeId] ASC),
    CONSTRAINT [FK_Badges_BadgeType] FOREIGN KEY ([BadgeTypeId]) REFERENCES [dbo].[BadgeType] ([BadgeTypeId])
);

