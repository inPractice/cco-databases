﻿CREATE TABLE [dbo].[CurriculumCertificate] (
    [CurriculumCertificateId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CurriculumCertificateUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [AccreditationId]               UNIQUEIDENTIFIER NULL,
    [ProgramID]                     UNIQUEIDENTIFIER NULL,
    [SessionUniqueId]               UNIQUEIDENTIFIER NOT NULL,
    [MemberId]                      UNIQUEIDENTIFIER NOT NULL,
    [Created]                       DATETIME         NOT NULL,
    [InsertedOn]                    DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]                    VARCHAR (10)     NULL,
    [CurriculumId]                  INT              NULL,
    CONSTRAINT [PK_CurriculumCertificate] PRIMARY KEY CLUSTERED ([CurriculumCertificateId] ASC),
    CONSTRAINT [UNQ__EmailCertificate__EmailCertificateUniqueId] UNIQUE NONCLUSTERED ([CurriculumCertificateUniqueId] ASC)
);

