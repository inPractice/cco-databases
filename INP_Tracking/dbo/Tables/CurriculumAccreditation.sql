﻿CREATE TABLE [dbo].[CurriculumAccreditation] (
    [CurriculumAccreditationId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CurriculumId]              INT              NULL,
    [AccreditationId]           UNIQUEIDENTIFIER NOT NULL,
    [ModuleId]                  UNIQUEIDENTIFIER NOT NULL,
    [Role]                      VARCHAR (2400)   NULL,
    [AllowedRetakes]            INT              NOT NULL,
    [CreatedOn]                 DATETIME         NOT NULL,
    CONSTRAINT [PK_CurriculumAccreditation] PRIMARY KEY CLUSTERED ([CurriculumAccreditationId] ASC),
    CONSTRAINT [FK_CurriculumAccreditation_Curriculum] FOREIGN KEY ([CurriculumId]) REFERENCES [dbo].[Curriculum] ([CurriculumId])
);

