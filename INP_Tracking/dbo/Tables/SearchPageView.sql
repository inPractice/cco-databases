﻿CREATE TABLE [dbo].[SearchPageView] (
    [SearchPageViewId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SearchPageViewUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [PageViewId]             INT              NOT NULL,
    [SearchCriteriaUniqueId] UNIQUEIDENTIFIER NULL,
    [Created]                DATETIME         NOT NULL,
    [Updated]                DATETIME         NULL,
    [Relevant]               BIT              NOT NULL,
    [InsertedOn]             DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]             VARCHAR (10)     NULL,
    CONSTRAINT [PK_SearchPageView] PRIMARY KEY CLUSTERED ([SearchPageViewId] ASC),
    CONSTRAINT [FK_SearchPageView_PageView] FOREIGN KEY ([PageViewId]) REFERENCES [dbo].[PageView] ([PageViewId]),
    CONSTRAINT [FK_SearchPageView_SearchCriteria] FOREIGN KEY ([SearchCriteriaUniqueId]) REFERENCES [dbo].[SearchCriteria] ([SearchCriteriaUniqueId]),
    CONSTRAINT [UNQ__SearchPageView__SearchPageViewUniqueId] UNIQUE NONCLUSTERED ([SearchPageViewUniqueId] ASC)
);

