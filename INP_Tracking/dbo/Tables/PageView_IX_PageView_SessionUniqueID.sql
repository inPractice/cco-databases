/*
Missing Index Details from SQLQuery85.sql - 614060-SQLCLUS1.INP_Tracking (CLINICALOPTIONS\kdanielsekani (228))
The Query Processor estimates that implementing the following index could improve the query cost by 86.8878%.
*/

USE [INP_Tracking]
GO
CREATE NONCLUSTERED INDEX [IX_PageView_SessionUniqueID]
ON [dbo].[PageView] ([SessionUniqueId])
INCLUDE ([PageViewUniqueId],[Created],[TimeOffset])
GO

