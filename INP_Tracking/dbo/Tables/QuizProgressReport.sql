﻿CREATE TABLE [dbo].[QuizProgressReport] (
    [QuizProgressReportId] BIGINT   IDENTITY (1, 1) NOT NULL,
    [MemberId]             BIGINT   NOT NULL,
    [ParticipantId]        BIGINT   NOT NULL,
    [QuizStatusId]         INT      DEFAULT ((1)) NOT NULL,
    [ChapterId]            INT      NOT NULL,
    [QuestionId]           INT      NOT NULL,
    [TestId]               INT      NOT NULL,
    [TextBookId]           INT      DEFAULT ((0)) NULL,
    [SectionId]            INT      DEFAULT ((0)) NULL,
    [MemberTestId]         BIGINT   NOT NULL,
    [ProgramId]            BIGINT   NOT NULL,
    [GroupId]              BIGINT   DEFAULT ((0)) NOT NULL,
    [AnsweredCorrectly]    BIT      DEFAULT ((0)) NOT NULL,
    [CreatedDttm]          DATETIME DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]         DATETIME NULL,
    CONSTRAINT [PK_QuizProgressReportId] PRIMARY KEY CLUSTERED ([QuizProgressReportId] ASC)
);

