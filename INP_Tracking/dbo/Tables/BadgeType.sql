﻿CREATE TABLE [dbo].[BadgeType] (
    [BadgeTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NULL,
    [Description] NVARCHAR (500) NULL,
    CONSTRAINT [PK_BadgeType] PRIMARY KEY CLUSTERED ([BadgeTypeId] ASC)
);

