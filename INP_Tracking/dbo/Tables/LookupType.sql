﻿CREATE TABLE [dbo].[LookupType] (
    [LookupTypeId]      INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LookupType]        VARCHAR (50) NOT NULL,
    [LookupTypeCreated] DATETIME     NOT NULL,
    CONSTRAINT [PK_LookupType] PRIMARY KEY CLUSTERED ([LookupTypeId] ASC)
);

