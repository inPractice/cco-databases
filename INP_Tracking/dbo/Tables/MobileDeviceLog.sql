﻿CREATE TABLE [dbo].[MobileDeviceLog] (
    [MobileDeviceLogId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberId]          VARCHAR (50)  NOT NULL,
    [DeviceType]        VARCHAR (50)  NULL,
    [DeviceModel]       VARCHAR (50)  NULL,
    [OsVersion]         VARCHAR (50)  NULL,
    [CreatedOn]         DATETIME      CONSTRAINT [DF_MobileDeviceLog_CreatedOn] DEFAULT (getdate()) NULL,
    [LocalUserId]       VARCHAR (100) NULL,
    [SessionId]         VARCHAR (100) NULL,
    CONSTRAINT [PK_MobileDeviceLog] PRIMARY KEY CLUSTERED ([MobileDeviceLogId] ASC)
);

