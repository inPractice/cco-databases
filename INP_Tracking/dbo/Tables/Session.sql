﻿CREATE TABLE [dbo].[Session] (
    [SessionId]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SessionUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [MemberId]        UNIQUEIDENTIFIER NOT NULL,
    [MobileLocalId]   VARCHAR (100)    NOT NULL,
    [Created]         DATETIME         NOT NULL,
    [Updated]         DATETIME         NOT NULL,
    [Anonymous]       BIT              NULL,
    [InsertedOn]      DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]      VARCHAR (10)     NULL,
    CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED ([SessionId] ASC),
    CONSTRAINT [UNQ__Session__SessionUniqueId] UNIQUE NONCLUSTERED ([SessionUniqueId] ASC)
);

