﻿CREATE TABLE [dbo].[QuestionnaireType] (
    [QuestionnaireTypeId]        INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [QuestionnaireUniqueId]      UNIQUEIDENTIFIER NOT NULL,
    [TypeName]                   VARCHAR (100)    NULL,
    [QuestionnaireTypeCreatedOn] DATETIME         NOT NULL,
    [InsertedOn]                 DATETIME         DEFAULT (getdate()) NOT NULL,
    [TimeOffset]                 VARCHAR (10)     NULL,
    CONSTRAINT [PK_QuestionnaireType] PRIMARY KEY CLUSTERED ([QuestionnaireTypeId] ASC),
    CONSTRAINT [UNQ__QuestionnaireType__QuestionnaireUniqueId] UNIQUE NONCLUSTERED ([QuestionnaireUniqueId] ASC)
);

