﻿CREATE TABLE [dbo].[ModuleProgressReport] (
    [ModuleProgressReportId] BIGINT   IDENTITY (1, 1) NOT NULL,
    [MemberId]               BIGINT   NOT NULL,
    [ParticipantId]          BIGINT   NOT NULL,
    [AssignmentStatusId]     INT      DEFAULT ((1)) NOT NULL,
    [TestStatusId]           INT      DEFAULT ((1)) NOT NULL,
    [ChapterId]              INT      NOT NULL,
    [TestId]                 INT      NOT NULL,
    [TextBookId]             INT      DEFAULT ((0)) NULL,
    [SectionId]              INT      DEFAULT ((0)) NULL,
    [TestTypeId]             INT      NOT NULL,
    [AssignmentId]           BIGINT   DEFAULT ((0)) NOT NULL,
    [ProgramId]              BIGINT   NOT NULL,
    [MemberTestId]           BIGINT   NOT NULL,
    [GroupId]                BIGINT   DEFAULT ((0)) NOT NULL,
    [TotalAnsweredCorrectly] INT      DEFAULT ((0)) NOT NULL,
    [TotalQuestions]         INT      DEFAULT ((0)) NOT NULL,
    [CreatedDttm]            DATETIME DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]           DATETIME NULL,
    CONSTRAINT [PK_ModuleProgressReportId] PRIMARY KEY CLUSTERED ([ModuleProgressReportId] ASC)
);

