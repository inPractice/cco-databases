﻿CREATE TABLE [dbo].[FeatureClick] (
    [FeatureClickId]      INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [FeatureImpressionId] INT            NOT NULL,
    [Target]              VARCHAR (1024) NOT NULL,
    [ClickAction]         VARCHAR (50)   NOT NULL,
    [Created]             DATETIME       NOT NULL,
    [InsertedOn]          DATETIME       NOT NULL,
    [TimeOffset]          VARCHAR (10)   NOT NULL,
    CONSTRAINT [PK_FeatureClick] PRIMARY KEY CLUSTERED ([FeatureClickId] ASC),
    CONSTRAINT [FK_FeatureClick_FeatureImpression] FOREIGN KEY ([FeatureImpressionId]) REFERENCES [dbo].[FeatureImpression] ([FeatureImpressionId])
);

