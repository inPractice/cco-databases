﻿CREATE TABLE [dbo].[Legacy Guids Map] (
    [Legacy GUID]  UNIQUEIDENTIFIER NULL,
    [Current GUID] UNIQUEIDENTIFIER NULL,
    [Current Path] VARCHAR (255)    NULL
);

