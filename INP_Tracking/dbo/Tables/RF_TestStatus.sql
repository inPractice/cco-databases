﻿CREATE TABLE [dbo].[RF_TestStatus] (
    [TestStatusId]    INT           IDENTITY (1, 1) NOT NULL,
    [TestStatusValue] NVARCHAR (50) NULL,
    CONSTRAINT [PK_STestStatusId] PRIMARY KEY CLUSTERED ([TestStatusId] ASC)
);

