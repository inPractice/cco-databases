﻿CREATE TABLE [dbo].[Personalization] (
    [PersonalizationId]   UNIQUEIDENTIFIER NOT NULL,
    [MemberID]            UNIQUEIDENTIFIER NOT NULL,
    [AssetID]             UNIQUEIDENTIFIER NOT NULL,
    [PersonalizationType] INT              NOT NULL,
    [Created]             DATETIME         NOT NULL,
    [Updated]             DATETIME         NOT NULL,
    CONSTRAINT [PK_Personalization] PRIMARY KEY CLUSTERED ([PersonalizationId] ASC)
);

