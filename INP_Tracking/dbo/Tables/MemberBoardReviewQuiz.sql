﻿CREATE TABLE [dbo].[MemberBoardReviewQuiz] (
    [MemberBoardReviewQuizId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [MemberTestId]            BIGINT           NOT NULL,
    [MemberId]                BIGINT           NOT NULL,
    [SessionUniqueId]         UNIQUEIDENTIFIER NOT NULL,
    [RankingId]               INT              NOT NULL,
    [TestId]                  INT              NOT NULL,
    [Points]                  INT              NULL,
    [StreakPoints]            INT              NULL,
    [CertificateUrl]          NVARCHAR (500)   NULL,
    [HoursEngaged]            FLOAT (53)       NULL,
    [CertificateId]           UNIQUEIDENTIFIER NULL,
    [AccreditationId]         UNIQUEIDENTIFIER NULL,
    [ProviderId]              UNIQUEIDENTIFIER NULL,
    [AccreditingOrganization] UNIQUEIDENTIFIER NULL,
    [CertificateType]         UNIQUEIDENTIFIER NULL,
    [CertificateTitle]        NVARCHAR (500)   NULL,
    [CreatedDttm]             DATETIME         DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]            DATETIME         NULL,
    CONSTRAINT [PK_MemberBoardReviewQuiz] PRIMARY KEY CLUSTERED ([MemberBoardReviewQuizId] ASC),
    CONSTRAINT [FK_MemberBoardReviewQuiz_MemberTest] FOREIGN KEY ([MemberTestId]) REFERENCES [dbo].[MemberTest] ([MemberTestId])
);

