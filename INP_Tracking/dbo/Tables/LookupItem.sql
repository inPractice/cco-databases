﻿CREATE TABLE [dbo].[LookupItem] (
    [LookupItemId]      INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LookupTypeId]      INT           NOT NULL,
    [LookupItemName]    VARCHAR (500) NULL,
    [LookupItemCreated] DATETIME      NOT NULL,
    CONSTRAINT [PK_LookupItem] PRIMARY KEY CLUSTERED ([LookupItemId] ASC),
    CONSTRAINT [FK_LookupItem_LookupType] FOREIGN KEY ([LookupTypeId]) REFERENCES [dbo].[LookupType] ([LookupTypeId])
);

