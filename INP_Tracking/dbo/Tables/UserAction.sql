﻿CREATE TABLE [dbo].[UserAction] (
    [UserActionId]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [UserActionName] VARCHAR (100) NULL,
    [CreatedOn]      DATETIME      NOT NULL,
    CONSTRAINT [PK_ImpressionLookup] PRIMARY KEY CLUSTERED ([UserActionId] ASC)
);

