﻿CREATE TABLE [dbo].[Rankings] (
    [RankingId]    INT            IDENTITY (1, 1) NOT NULL,
    [RankingValue] NVARCHAR (50)  NULL,
    [Description]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Rankings] PRIMARY KEY CLUSTERED ([RankingId] ASC)
);

