﻿CREATE TABLE [dbo].[InPracticePreferences] (
    [PreferenceId]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [PreferenceName] VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_PreferenceId] PRIMARY KEY CLUSTERED ([PreferenceId] ASC)
);

