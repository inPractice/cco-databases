﻿CREATE TABLE [dbo].[Feature] (
    [FeatureId]   UNIQUEIDENTIFIER NOT NULL,
    [Description] VARCHAR (200)    NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    CONSTRAINT [PK_Feature] PRIMARY KEY CLUSTERED ([FeatureId] ASC)
);

