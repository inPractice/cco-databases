﻿CREATE TABLE [dbo].[BookmarkExport] (
    [BookmarkUniqueId] UNIQUEIDENTIFIER NOT NULL,
    [MemberId]         UNIQUEIDENTIFIER NULL,
    [MobileLocalId]    VARCHAR (100)    NOT NULL,
    [PropertyId]       UNIQUEIDENTIFIER NOT NULL,
    [Created]          DATETIME         NOT NULL,
    [Updated]          DATETIME         NULL,
    [IsActive]         BIT              NOT NULL,
    [InsertedOn]       DATETIME         NOT NULL,
    [TimeOffset]       VARCHAR (10)     NULL,
    [PropertyName]     VARCHAR (255)    NOT NULL,
    [ClusterID]        UNIQUEIDENTIFIER NOT NULL,
    [TemplateName]     VARCHAR (255)    NOT NULL
);

