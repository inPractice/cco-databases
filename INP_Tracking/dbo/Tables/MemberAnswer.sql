﻿CREATE TABLE [dbo].[MemberAnswer] (
    [MemberAnswerId]    BIGINT   IDENTITY (1, 1) NOT NULL,
    [MemberQuestionId]  BIGINT   NOT NULL,
    [AnswerId]          INT      NOT NULL,
    [QuestionId]        INT      NOT NULL,
    [IgnoreTestResults] BIT      DEFAULT ((0)) NOT NULL,
    [AnsweredCorrectly] BIT      DEFAULT ((0)) NOT NULL,
    [AnswerCount]       INT      DEFAULT ((0)) NOT NULL,
    [CreatedDttm]       DATETIME DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]      DATETIME NULL,
    CONSTRAINT [PK_MemberTestResponse] PRIMARY KEY CLUSTERED ([MemberAnswerId] ASC),
    CONSTRAINT [FK_MemberAnswer_MemberQuestion] FOREIGN KEY ([MemberQuestionId]) REFERENCES [dbo].[MemberQuestion] ([MemberQuestionId])
);

