﻿CREATE TABLE [dbo].[SiteCoreContent] (
    [SiteCoreContentId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Guid]              UNIQUEIDENTIFIER NOT NULL,
    [ParentGuid]        UNIQUEIDENTIFIER NOT NULL,
    [Name]              VARCHAR (MAX)    NOT NULL,
    [ContentCategory]   VARCHAR (100)    NOT NULL,
    [TrackingCategory]  VARCHAR (100)    NOT NULL,
    [CreatedOn]         DATETIME         NOT NULL,
    CONSTRAINT [PK_SiteCoreContentId] PRIMARY KEY CLUSTERED ([SiteCoreContentId] ASC)
);

