﻿CREATE TABLE [dbo].[MemberTest] (
    [MemberTestId]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [MemberId]          BIGINT           NOT NULL,
    [SessionUniqueId]   UNIQUEIDENTIFIER NOT NULL,
    [TestId]            INT              NOT NULL,
    [GroupId]           BIGINT           DEFAULT ((0)) NOT NULL,
    [ClassRoomId]       BIGINT           DEFAULT ((0)) NOT NULL,
    [ProgramId]         BIGINT           DEFAULT ((0)) NOT NULL,
    [AssignmentId]      BIGINT           DEFAULT ((0)) NOT NULL,
    [PassedTest]        BIT              NULL,
    [Completed]         BIT              DEFAULT ((0)) NOT NULL,
    [IgnoreTestResults] BIT              DEFAULT ((0)) NOT NULL,
    [UserScore]         FLOAT (53)       DEFAULT ((0)) NOT NULL,
    [CreatedDttm]       DATETIME         DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]      DATETIME         NULL,
    [CompletedDttm]     DATETIME         NULL,
    CONSTRAINT [PK_MemberQuizs] PRIMARY KEY CLUSTERED ([MemberTestId] ASC)
);

