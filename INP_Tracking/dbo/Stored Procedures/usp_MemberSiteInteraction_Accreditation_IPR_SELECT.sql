﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_Accreditation_IPR_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
	 [SiteCode]						
	,[SourceCode]
	,[MemberGUID]		
	,[SRC_CreditGUID]	
	,[SRC_CreditID]		
	,[CreditsClaimed]	
	,[AccreditationDttm]			= rpv.[DateClaimed]		
	,[CountryCode3]		
	,[SRC_SurveyGUID_Evaluation]	
	,[CMEAccreditingBodyGUID]		= rpv.[SRC_AccreditingBody]		
	,[SRC_CertificateTypeGUID]	
	,[SRC_SearchGUID]			
	,[SearchTerm]
	,[InsertedDttm]
	FROM
	(
		SELECT	DISTINCT 
		 [SiteCode]					= 'IPR'
		,[SourceCode]				= 'IPR'
		,[MemberGUID]				= AI.MemberId
		,[SRC_CreditGUID]			= AI.AccreditationId
		,[SRC_CreditID]				= CAST(NULL AS int) 
		,[CreditsClaimed]			= CAST(AID.HoursEngaged as DECIMAL(6,2))
		,[DateClaimed]				= AID.InteractionDetailsCompleteOn
		,[CountryCode3]				= co.[Abbreviation]
		,[SRC_SurveyGUID_Evaluation]	= AI.AccreditationInteractionUniqueId 
		,[SRC_AccreditingBody]		= CAST(AID.AccreditingOrganization AS uniqueidentifier) 
		,[SRC_CertificateTypeGUID]	= CAST(AID.CertificateType AS uniqueidentifier) 
		,[SRC_SearchGUID]			= SC.SearchCriteriaUniqueId 
		,[SearchTerm]				= CAST(PoCTerm AS nvarchar(100))
		,[InsertedDttm]				= ai.InteractionCreatedOn
		FROM dbo.[AccreditationInteraction] AI
		JOIN dbo.[AccreditationInteractionDetails] AID 
			ON AI.AccreditationInteractionId = AID.AccreditationInteractionId 
			AND AI.QuestionnaireTypeID = 4
		LEFT JOIN dbo.[SearchCriteria] SC 
			ON SC.SearchCriteriaUniqueId = (SELECT TOP 1 SearchCriteriaUniqueId FROM SearchCriteria WHERE AccreditationInteractionUniqueId = AI.AccreditationInteractionUniqueId)
		LEFT JOIN [MemberCentralProd].[dbo].[Country] co
			ON co.[CountryID] = aid.[CountryId]
		WHERE 1=1
		AND ai.[InteractionCreatedOn] > @Dttm

		UNION

		SELECT	DISTINCT 
		 [SiteCode]					= 'IPR'
		,[SourceCode]				= 'IPR'
		,[MemberGUID]				= AIE.MemberId
		,[SRC_CreditGUID]			= AIE.AccreditationId
		,[SRC_CreditID]				= CAST(NULL AS int) 
		,[CreditsClaimed]			= CAST(AID.HoursEngaged as DECIMAL(6,2))
		,[DateClaimed]				= AID.[InteractionDetailsCompleteOn]
		,[CountryCode3]				= co.[Abbreviation]
		,[SRC_SurveyGUID_Evaluation]	= AIE.AccreditationInteractionUniqueId 
		,[SRC_AccreditingBody]		= CAST(AID.AccreditingOrganization AS uniqueidentifier) 
		,[SRC_CertificateTypeGUID]	= CAST(AID.CertificateType AS uniqueidentifier) 
		,[SRC_SearchGUID]			= CAST(NULL AS uniqueidentifier)
		,[SearchTerm]				= CAST(NULL AS nvarchar(100))
		,[InsertedDttm]				= aie.[InteractionCreatedOn]
		FROM dbo.AccreditationInteraction AIT
		INNER JOIN dbo.AccreditationInteractionDetails AID 
			ON AIT.AccreditationInteractionId = AID.AccreditationInteractionId 
		INNER JOIN dbo.AccreditationInteraction AIE 
			ON AIT.AccreditationId = AIE.AccreditationId 
			AND AIT.MemberId = AIE.MemberId 
			AND AIE.QuestionnaireTypeID = 2
		LEFT JOIN [MemberCentralProd].[dbo].[Country] co
			ON co.[CountryID] = aid.[CountryId]
		WHERE AIT.QuestionnaireTypeID = 1
			AND AID.PassedTest = 1
			AND AID.CertificateUrl IS NOT NULL
			AND aie.[InteractionCreatedOn] > @Dttm
	) rpv

END
;