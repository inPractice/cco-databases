﻿
CREATE  PROCEDURE [dbo].[usp_MemberLogin_UPDATE]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ProcessName varchar(64)		= 'usp_MemberLogin_UPDATE'
	DECLARE @ProcessTypeFlg varchar(12)		= 'U'
	DECLARE @AffectedTableName varchar(64)	= 'MemberLogin'
	DECLARE @StartDttm datetime				= getdate()
	DECLARE @EndDttm datetime				= getdate()
	DECLARE @RowsInsertedCount int			= 0
	DECLARE @RowsUpdatedCount int			= 0
	DECLARE @RowsDeletedCount int			= 0
	DECLARE @ProcessCompleteInd bit			= 0
	DECLARE @ProcessSuccessfulInd bit		= 0
	DECLARE @ServerName varchar(64)			= @@SERVERNAME
	DECLARE @LastLoginDttm Datetime
	
	SET @LastLoginDttm = (
		SELECT LastStartDttm = max(CASE WHEN [StartDttm] = '1900-01-01 00:00:00.000' THEN [EndDttm] ELSE [StartDttm] END)
		  FROM [IPRDW_SRC].[dbo].[tblAuditTracking] au
		  WHERE 
			  [ServerName] = @ServerName
		  AND [AffectedTableName] = @AffectedTableName
		  AND au.[ProcessTypeFlg] = @ProcessTypeFlg
		AND [ProcessCompleteInd] = 1
		AND [ProcessSuccessfulInd] = 1
	)

	UPDATE mp
	SET 
	   mp.[LastLoginDttm] 		= ml.[LastLoginDttm]
	  ,mp.[VisitCount] 			= mp.[VisitCount] + ISNULL(mv.[VisitCount], 0)
	  ,mp.[ModifiedDttm]			= GetDate()
	FROM [MemberCentralProd].[dbo].[MemberLogin] mp
	JOIN 
	(
		SELECT 
		   [MemberGUID] 	= s.MemberId
		, [LastLoginDttm] 	= MAX(s.[Created])
		FROM 
			(
				SELECT ss.MemberId
				, Created = ss.[InsertedOn]
				FROM [dbo].[Session] ss (nolock)
				WHERE 1=1 
					AND ss.[InsertedOn] >= @LastLoginDttm
					AND CONVERT(date, ss.[InsertedOn]) = CONVERT(date, GETDATE())
			) s		
		JOIN [MemberCentralProd].[dbo].[MemberLogin] ml (nolock)
			ON ml.[MemberGUID] = s.[MemberId]
		WHERE 1=1
		AND s.[Created] >= @LastLoginDttm
		GROUP BY s.MemberId
	) ml
		ON ml.MemberGUID = mp.MemberGUID
	JOIN 
	(
		SELECT v.[MemberGUID]
		, [VisitCount] 	= SUM(v.[SRow])
		FROM
		(
			SELECT DISTINCT
			  [SRow] 		= 1   
			, [MemberGUID] 	= s.MemberId
			, [SessionVisit] 	= CONVERT(date,CAST(s.[Created] as varchar(17)))
			FROM 
			(
				SELECT DISTINCT ss.MemberId
				, Created = CONVERT(date,CAST(ss.[InsertedOn] as varchar(17)))
				FROM [dbo].[Session] ss (nolock)
				WHERE 1=1 
					AND ss.[Created] >= @LastLoginDttm
					AND CONVERT(date, ss.[InsertedOn]) = CONVERT(date, GETDATE())
			) s
			JOIN [MemberCentralProd].[dbo].[MemberLogin] ml (nolock)
				ON ml.[MemberGUID] = s.[MemberId]
			WHERE 1=1	
		) v
		GROUP BY v.[MemberGUID]
	) mv
		ON mv.MemberGUID = mp.MemberGUID
	;	
	SET @RowsUpdatedCount = @@ROWCOUNT
	SET @ProcessCompleteInd = 1
	SET @ProcessSuccessfulInd = 1
	SET @EndDttm = GetDate()
--	EXEC IPRDW_SRC.dbo.usp_tblAuditTracking_Insert @ProcessName, @ProcessTypeFlg, @AffectedTableName, @StartDttm, @EndDttm, @RowsInsertedCount, @RowsUpdatedCount, @RowsDeletedCount, @ProcessCompleteInd, @ProcessSuccessfulInd, @ServerName;
END



