﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_Accreditation_RF_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
	 [SiteCode]						
	,[SourceCode]
	,[MemberGUID]		
	,[CMEAccreditingBodyGUID]		= rpv.[SRC_AccreditingBody]		
	,[SRC_CertificateTypeGUID]	
	,[SRC_CreditGUID]	
	,[SRC_CreditID]		
	,[SurveyGUID]					= [SRC_SurveyGUID_Evaluation]	
	,[AccreditationDttm]			= rpv.[DateClaimed]		
	,[CreditsClaimed]	
	,[CountryCode]		
	,[SRC_SearchGUID]			
	,[SearchTerm]
	,[InsertedDttm]
	FROM
	(
		SELECT	DISTINCT 
		 [SiteCode]					= 'IPR'
		,[SourceCode]				= 'R&F'
		,[MemberGUID]				= AI.MemberId
		,[SRC_CreditGUID]			= AI.AccreditationId
		,[SRC_CreditID]				= CAST(NULL AS int) 
		,[CreditsClaimed]			= CAST(AID.HoursEngaged as DECIMAL(6,2))
		,[DateClaimed]				= AID.InteractionDetailsCompleteOn
		,[CountryCode]				= co.[Abbreviation]
		,[SRC_SurveyGUID_Evaluation]	= AI.AccreditationInteractionUniqueId 
		,[SRC_AccreditingBody]		= CAST(AID.AccreditingOrganization AS uniqueidentifier) 
		,[SRC_CertificateTypeGUID]	= CAST(AID.CertificateType AS uniqueidentifier) 
		,[SRC_SearchGUID]			= SC.SearchCriteriaUniqueId 
		,[SearchTerm]				= CAST(PoCTerm AS nvarchar(100))
		,[InsertedDttm]				= ai.InteractionCreatedOn
		FROM dbo.[AccreditationInteraction] AI
		JOIN [SiteAccess].[dbo].[RF.Participant] p
			ON p.[MemberID] = ai.[MemberId]
		JOIN dbo.[AccreditationInteractionDetails] AID 
			ON AI.AccreditationInteractionId = AID.AccreditationInteractionId 
			AND AI.QuestionnaireTypeID = 4
		LEFT JOIN dbo.[SearchCriteria] SC 
			ON SC.SearchCriteriaUniqueId = (SELECT TOP 1 SearchCriteriaUniqueId FROM SearchCriteria WHERE AccreditationInteractionUniqueId = AI.AccreditationInteractionUniqueId)
		LEFT JOIN [MemberCentralProd].[dbo].[Country] co
			ON co.[CountryID] = aid.[CountryId]
		WHERE 1=1
--			AND ai.[InteractionCreatedOn] >= p.[CreatedDttm]
			AND ai.[InteractionCreatedOn] > @Dttm

		UNION

		SELECT	DISTINCT 
		 [SiteCode]					= 'IPR'
		,[SourceCode]				= 'R&F'
		,[MemberGUID]				= AIE.MemberId
		,[SRC_CreditGUID]			= AIE.AccreditationId
		,[SRC_CreditID]				= CAST(NULL AS int) 
		,[CreditsClaimed]			= CAST(AID.HoursEngaged as DECIMAL(6,2))
		,[DateClaimed]				= AID.[InteractionDetailsCompleteOn]
		,[CountryCode]				= co.[Abbreviation]
		,[SRC_SurveyGUID_Evaluation]	= AIE.AccreditationInteractionUniqueId 
		,[SRC_AccreditingBody]		= CAST(AID.AccreditingOrganization AS uniqueidentifier) 
		,[SRC_CertificateTypeGUID]	= CAST(AID.CertificateType AS uniqueidentifier) 
		,[SRC_SearchGUID]			= CAST(NULL AS uniqueidentifier)
		,[SearchTerm]				= CAST(NULL AS nvarchar(100))
		,[InsertedDttm]				= aie.[InteractionCreatedOn]
		FROM dbo.AccreditationInteraction AIT
		INNER JOIN dbo.AccreditationInteractionDetails AID 
			ON AIT.AccreditationInteractionId = AID.AccreditationInteractionId 
		INNER JOIN dbo.AccreditationInteraction AIE 
			ON AIT.AccreditationId = AIE.AccreditationId 
			AND AIT.MemberId = AIE.MemberId 
			AND AIE.QuestionnaireTypeID = 2
		JOIN [SiteAccess].[dbo].[RF.Participant] p
			ON p.[MemberID] = aie.[MemberId]
		LEFT JOIN [MemberCentralProd].[dbo].[Country] co
			ON co.[CountryID] = aid.[CountryId]
		WHERE AIT.QuestionnaireTypeID = 1
			AND AID.PassedTest = 1
			AND AID.CertificateUrl IS NOT NULL
--			AND aie.[InteractionCreatedOn] >= p.[CreatedDttm]
			AND aie.[InteractionCreatedOn] > @Dttm


		UNION


		SELECT	DISTINCT 
		 [SiteCode]					= 'R&F'
		,[SourceCode]				= 'R&F'
		,[MemberGUID]				= m.MemberId
		,[SRC_CreditGUID]			= acc.[AccreditationSitecoreItemId]
		,[SRC_CreditID]				= CAST(NULL AS int) 
		,[CreditsClaimed]			= CAST(mbq.HoursEngaged as DECIMAL(6,2))
		,[DateClaimed]				= mt.CompletedDttm
		,[CountryCode]				= ISNULL(co.[Abbreviation], 'US')
		,[SRC_SurveyGUID_Evaluation]= imt.[TestSitecoreId]
		,[SRC_AccreditingBody]		= mbq.[AccreditingOrganization]
		,[SRC_CertificateTypeGUID]	= mbq.[CertificateType]
		,[SRC_SearchGUID]			= CAST(NULL AS uniqueidentifier)
		,[SearchTerm]				= CAST(NULL AS nvarchar(100))
		,[InsertedDttm]				= ''
		-- SELECT TOP 100 *
		FROM [SiteAccess].[dbo].[RF.Participant] p
		JOIN [MemberCentralProd].[dbo].[Member] m
			ON m.[MemberId] = p.[MemberID]
		JOIN [MemberCentralProd].[dbo].[MailMember] mm
			ON mm.[MemberId] = p.[MemberID]
		JOIN [dbo].[MemberBoardReviewQuiz] mbq
			ON mbq.[MemberId] = m.[MemberInt]
		JOIN [INP_Manifest].[dbo].[Test] imt
			ON imt.[TestId] = mbq.[TestId]
		JOIN [INP_Manifest].[dbo].[Accreditation] acc
			ON acc.[AccreditationSitecoreItemId] = mbq.[AccreditationId]
		JOIN [dbo].[MemberTest] mt
			ON mt.[MemberTestId] = mbq.[MemberTestId]
		LEFT JOIN [MemberCentralProd].[dbo].[Country] co
			ON co.[CountryID] = mm.[CountryId]
		WHERE 1=1
--			AND ISNULL(mt.[PassedTest], 1) = 1
			ANd mt.[Completed] = 1
			AND mt.[CompletedDttm] > @Dttm
	) rpv

END
;
GO
