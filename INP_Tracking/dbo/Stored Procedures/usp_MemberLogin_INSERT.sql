﻿CREATE  PROCEDURE [dbo].[usp_MemberLogin_INSERT]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ProcessName varchar(64)		= 'usp_MemberLogin_INSERT'
	DECLARE @ProcessTypeFlg varchar(12)		= 'I'
	DECLARE @AffectedTableName varchar(64)	= 'MemberLogin'
	DECLARE @StartDttm datetime				= getdate()
	DECLARE @EndDttm datetime				= getdate()
	DECLARE @RowsInsertedCount int			= 0
	DECLARE @RowsUpdatedCount int			= 0
	DECLARE @RowsDeletedCount int			= 0
	DECLARE @ProcessCompleteInd bit			= 0
	DECLARE @ProcessSuccessfulInd bit		= 0
	DECLARE @ServerName varchar(64)			= @@SERVERNAME
	DECLARE @LastLoginDttm Datetime
	
	SET @LastLoginDttm = (
		SELECT LastStartDttm = max(CASE WHEN [StartDttm] = '1900-01-01 00:00:00.000' THEN [EndDttm] ELSE [StartDttm] END)
		  FROM [IPRDW_SRC].[dbo].[tblAuditTracking] au
		  WHERE 
			  [ServerName] = @ServerName
		  AND [AffectedTableName] = @AffectedTableName
		  AND au.[ProcessTypeFlg] = @ProcessTypeFlg
		AND [ProcessCompleteInd] = 1
		AND [ProcessSuccessfulInd] = 1
	)

	INSERT [MemberCentralProd].[dbo].[MemberLogin]
	(
		   [MemberId]
		  ,[MemberGUId]
		  ,[SourceId]
		  ,[FirstLoginDttm]
		  ,[LastLoginDttm]
		  ,[VisitCount]
		  ,[CreatedDttm]
		  ,[ModifiedDttm]
	)
	SELECT 
		   [MemberId]
		  ,[MemberGUId]
		  ,[SourceId]		= 0 --[SELECT SourceId FROM dbo.Source s WHERE s.[Text] = 'Residents and Fellows']
		  ,[FirstLoginDttm]
		  ,[LastLoginDttm]
		  ,[VisitCount]
		  ,[CreatedDttm] 	= GetDate()
		  ,[ModifiedDttm] 	= GetDate()
	FROM
	(
		SELECT 
		  [MemberID] 		= m.MemberInt
		, [MemberGUID] 		= s.MemberId
		, [FirstLoginDttm] 	= MIN(s.[Created])
		, [LastLoginDttm] 	= MAX(s.[Created])
		, [VisitCount] 		= 1
		FROM [MemberCentralProd].[dbo].[Member] m
		JOIN [MemberCentralProd].[dbo].[Session] s
			ON m.[MemberId] = s.[MemberId]
		LEFT JOIN [MemberCentralProd].[dbo].[MemberLogin] ml
			ON ml.[MemberGUID] = s.[MemberId]
		WHERE ml.[MemberLoginId] IS NULL	
			AND s.[Created] >= @LastLoginDttm
		GROUP BY s.MemberId, m.MemberInt
	) m
	;	
	SET @RowsInsertedCount = @@ROWCOUNT
	SET @EndDttm = GetDate()
	SET @ProcessCompleteInd = 1
	SET @ProcessSuccessfulInd = 1
--	EXEC IPRDW_SRC.dbo.usp_tblAuditTracking_Insert @ProcessName, @ProcessTypeFlg, @AffectedTableName, @StartDttm, @EndDttm, @RowsInsertedCount, @RowsUpdatedCount, @RowsDeletedCount, @ProcessCompleteInd, @ProcessSuccessfulInd, @ServerName;

END

