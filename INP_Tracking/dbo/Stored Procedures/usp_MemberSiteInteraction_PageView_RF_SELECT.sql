﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_PageView_RF_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
	 [SiteCode]		
	,[MemberGUId] 	
	,[SessionGUId] 	
	,[PageViewGUID]	
	,[PageItemGUID]	
	,[URL]			
	,[ActivityStartDttm] 
	,[SessionStartDttm]
	,[ActivityYear] 	
	,[ActivityMonth] 
	,[ActivityWeek]
	FROM
	(
		SELECT DISTINCT
		 [SiteCode]			= 'R&F'
		,[MemberGUId] 		= s.[MemberID]
		,[SessionGUId] 		= pv.[SessionUniqueId]
		,[PageItemGUID]		= pv.[PropertyId]
		,[PageViewGUID]		= pv.[PropertyId]
		,[URL]				= ''
		,[ActivityStartDttm] = DATEADD(hour, CONVERT(float,pv.[TimeOffset]), pv.[Created])
		,[SessionStartDttm]  = s.[Created]
		,[ActivityYear] 		= YEAR(DATEADD(hour, CONVERT(float,pv.[TimeOffset]), pv.[Created]))
		,[ActivityMonth] 		= MONTH(DATEADD(hour, CONVERT(float,pv.[TimeOffset]), pv.[Created]))
		,[ActivityWeek]		= DATEPART(wk ,DATEADD(hour, CONVERT(float,pv.[TimeOffset]), pv.[Created]))
		FROM [dbo].[PageView] pv
		JOIN [dbo].[Session] s
			ON s.[SessionUniqueId] = pv.[SessionUniqueId]
		JOIN [MemberCentralProd].[dbo].[Member] m
			ON s.[MemberId]			= m.[MemberId]
		JOIN [SiteAccess].[dbo].[RF.Participant] pt
			ON pt.[MemberID]			= m.[MemberID]
		WHERE DATEADD(hour, CONVERT(float,pv.[TimeOffset]), pv.[Created]) >= @Dttm
			AND pv.[PageViewUniqueId] IS NOT NULL
	) rpv


END
GO
