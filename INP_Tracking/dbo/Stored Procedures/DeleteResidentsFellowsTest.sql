﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteResidentsFellowsTest]
	-- Add the parameters for the stored procedure here
	@MemberTestId bigint 	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Delete answers
	Delete from MemberAnswer where MemberQuestionId in (Select MemberQuestionId from MemberQuestion where MemberTestId=@MemberTestId)
	--Delete Questions
	Delete from MemberQuestion where MemberTestId=@MemberTestId
	--Delete Badges
	Delete from MemberBadges where MemberTestId=@MemberTestId
	--Delete MemberBoardReviewQuiz
	Delete from MemberBoardReviewQuiz where MemberTestId=@MemberTestId
	--Delete MemberTest
	Delete from MemberTest where MemberTestId=@MemberTestId

	Delete from QuizProgressReport where MemberTestId=@MemberTestId

	Delete from ModuleProgressReport where MemberTestId=@MemberTestId

END
SET QUOTED_IDENTIFIER ON

