﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_Survey_IPR_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
	 [SiteCode] 		
	,[SourceCode]		
	,[SRC_MemberSurveyGUID]			
	,[SRC_MemberSurveyResponseGUID]	
	,[SRC_MemberGUID]				
	,[SRC_SurveyGUID]				
	,[SRC_QuestionGUID] 			
	,[SRC_ResponseGUID] 			
	,[SRC_SessionGUID]				
	,[ResponseText] 				
	,[SurveyType]					
	,[CountryCode]					
	,[ActivityDttm]					
	,[CompletedInd]					
	,[PassedInd]					
	,[InsertedDttm]					
	,[LastModifiedDttm]
	FROM
	(
		SELECT
		 [SiteCode] 					= 'IPR'
		,[SourceCode]					= 'IPR_Sitecore'
		,[SRC_MemberSurveyGUID]			= tr.[QuestionnaireId]
		,[SRC_MemberSurveyResponseGUID]	= trqa.[AccreditationInteractionResponseAnswerUniqueId]
		,[SRC_MemberGUID]				= tr.[MemberID]
		,[SRC_SurveyGUID]				= tr.[AccreditationId]
		,[SRC_QuestionGUID] 			= r.[QuestionId]
		,[SRC_ResponseGUID] 			= trqa.[AnswerId]
		,[SRC_SessionGUID]				= tr.[SessionUniqueId]
		,[ResponseText] 				= CONVERT(nvarchar(1024), ISNULL(trqa.[FreeFormResponse], ''))
		,[SurveyType]					= CONVERT(varchar(36), qt.[TypeName])
		,[CountryCode]					= ''
		,[ActivityDttm]					= DATEADD(hour, CONVERT(float, trqa.[TimeOffset]), trqa.[AnswerCreatedOn])
		,[CompletedInd]					= CONVERT(bit, 1)
		,[PassedInd]					= CONVERT(bit, 1)
		,[InsertedDttm]					= DATEADD(hour, CONVERT(float, tr.[TimeOffset]), tr.[InteractionCreatedOn])
		,[LastModifiedDttm]				= DATEADD(hour, CONVERT(float, tr.[TimeOffset]), tr.[InsertedOn])
		FROM [dbo].[AccreditationInteraction] tr 
		JOIN [dbo].[AccreditationInteractionResponse] r 
			ON r.[AccreditationInteractionId] = tr.[AccreditationInteractionId]
		JOIN [dbo].[AccreditationInteractionResponseAnswer] trqa
			ON trqa.[AccreditationInteractionResponseId] = r.[AccreditationInteractionResponseId]
		JOIN [dbo].[QuestionnaireType] qt
			On qt.[QuestionnaireTypeId] = tr.[QuestionnaireTypeId]
		WHERE 1=1
			AND DATEADD(hour, CONVERT(float, trqa.[TimeOffset]), trqa.[AnswerCreatedOn]) > @Dttm
	) res

END
GO
