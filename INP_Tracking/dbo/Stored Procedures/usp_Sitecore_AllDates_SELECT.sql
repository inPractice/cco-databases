﻿CREATE PROCEDURE [dbo].[usp_Sitecore_AllDates_SELECT]
(
	@Dttm datetime = '2000-01-01 00:00:00'
	'@EndDttm datetime = '2020-12-31 23:59:59'
)
AS
BEGIN


	SELECT DISTINCT 
	  [SiteCode]			= 'IPR'
	, [SourceCode]			= 'SC_IPR'
	, [ContentObjectGUID]	= CONVERT(UNIQUEIDENTIFIER, I.[ID])
	, [ContentObjectName]	= CONVERT(VARCHAR(512), I.[Name]) COLLATE SQL_Latin1_General_CP1_CI_AS
	-- USE TRY_CONVERT ON DATE VALUES 
	, [ContentObjectDate]	= (CASE WHEN CHARINDEX('T', fv.[Value]) > 0 THEN LEFT(fv.[Value], 8) ELSE fv.[Value] END) --CONVERT(DATE,(CASE WHEN CHARINDEX('T', fv.[Value]) > 0 THEN LEFT(fv.[Value], 8) ELSE fv.[Value] END))
	, [DateType]			= fvi.[Name] COLLATE SQL_Latin1_General_CP1_CI_AS
	FROM [INP_Sitecore_Web].[dbo].[Items] i
	JOIN [INP_Sitecore_Web].[dbo].[VersionedFields] fv
	ON fv.[ItemId] = i.[ID]
	JOIN [INP_Sitecore_Web].[dbo].[Items] fvi
		ON fvi.[ID] = fv.[FieldId]
	WHERE  1=1
	--	AND LEN((CASE WHEN CHARINDEX('T', fv.[Value]) > 0 THEN LEFT(fv.[Value], 8) ELSE fv.[Value] END)) <> 8
		AND fvi.[Name] IN ('__Updated','__Created','Date Posted','Date Reviewed','Date Updated','EndDate','Publication Date','Validity End Date','Validity Start Date')
		AND CHARINDEX(' by', fvi.[Name]) = 0
		AND CHARINDEX('Text', fvi.[Name]) = 0
		AND fv.[Value] <> ''
		AND NOT(fvi.[Name] IN ('Update Text'))
		AND NOT(i.[Name] IN ('__Standard Values'))


END
;
GO
