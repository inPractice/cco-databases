﻿



CREATE PROCEDURE [dbo].[LeaderBoard]
@testId int,
@memberId bigint, 
@programId int, 
@groupId int
WITH EXECUTE AS CALLER
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
DECLARE @sql nvarchar(2000),@paramlist nvarchar(max)



--select ROW_NUMBER() OVER(ORDER BY mt.userscore DESC) AS Ranking,FirstName, LastName,mt.UserScore,
--(select top 1 ImageLocation from MemberBadges mb 
--inner join Badges b on b.BadgeId = mb.BadgeId 
--where MemberTestId = mt.MemberTestId and BadgeTypeId = 1
--order by b.BadgeId desc) steaks,
--(select  ',' + ImageLocation from MemberBadges mb 
--inner join Badges b on b.BadgeId = mb.BadgeId 
--where MemberTestId = mt.MemberTestId  and BadgeTypeId = 2
--order by b.BadgeId desc  FOR XML PATH('')) badges,1 LoggedInUserRanking from 
--MemberBoardReviewQuiz mq inner join MemberTest mt on mt.MemberTestId = mq.MemberTestId 
--left join memberinfo mi on mi.memberid = mq.memberid 
--where 1= 1  and mt.testId = @testId  and  mt.MemberId = @memberId
--union all
--select ROW_NUMBER() OVER(ORDER BY mt.userscore DESC) AS Ranking,FirstName, LastName,mt.UserScore,
--(select top 1 ImageLocation from MemberBadges mb 
--inner join Badges b on b.BadgeId = mb.BadgeId 
--where MemberTestId = mt.MemberTestId and BadgeTypeId = 1
--order by b.BadgeId desc) steaks,
--(select  ',' + ImageLocation from MemberBadges mb 
--inner join Badges b on b.BadgeId = mb.BadgeId 
--where MemberTestId = mt.MemberTestId  and BadgeTypeId = 2
--order by b.BadgeId desc  FOR XML PATH('')) badges,0 LoggedInUserRanking from
--MemberBoardReviewQuiz mq inner join MemberTest mt on mt.MemberTestId = mq.MemberTestId 
--left join memberinfo mi on mi.memberid = mq.memberid
--where 1= 1  and mt.testId = @testId  
--AND (@groupId <> 0 and  mt.GroupId = @groupId)
--AND (@programId <> 0 and  mt.ProgramId = @programId)


--select @sql = ' select * from MemberBoardReviewQuiz mq inner join MemberTest mt on mt.MemberTestId = mq.MemberTestId 
--left join memberinfo mi on mi.memberid = mq.memberid
--where 1= 1 '



--select @sql = ' select ROW_NUMBER() OVER(ORDER BY mt.userscore DESC) AS Ranking,FirstName, LastName,mt.UserScore,(select top 1 ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId  where MemberTestId = mt.MemberTestId and BadgeTypeId = 1 order by b.BadgeId desc) steaks, (select  '','' + ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId  where MemberTestId = mt.MemberTestId  and BadgeTypeId = 2 order by b.BadgeId   FOR XML PATH('''')) badges,1 LoggedInUserRanking from MemberBoardReviewQuiz mq inner join MemberTest mt on mt.MemberTestId = mq.MemberTestId left join memberinfo mi on mi.memberid = mq.memberid where 1= 1  and mt.testId = @xtestId  and  mt.MemberId = @xmemberId union all select ROW_NUMBER() OVER(ORDER BY mt.userscore DESC) AS Ranking,FirstName, LastName,mt.UserScore, (select top 1 ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId  where MemberTestId = mt.MemberTestId and BadgeTypeId = 1 order by b.BadgeId desc) steaks, (select  '','' + ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId  where MemberTestId = mt.MemberTestId and BadgeTypeId = 2 order by b.BadgeId  FOR XML PATH('''')) badges,0 LoggedInUserRanking from MemberBoardReviewQuiz mq inner join MemberTest mt on mt.MemberTestId = mq.MemberTestId  left join memberinfo mi on mi.memberid = mq.memberid where 1= 1  and mt.testId = @xtestId  '

select @sql = ' select ROW_NUMBER() OVER(ORDER BY mt.userscore DESC) AS Ranking,FirstName, LastName,mt.UserScore,(select top 1 ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId  where MemberTestId = mt.MemberTestId and BadgeTypeId = 1 order by b.BadgeId desc) steaks, (select  '','' + ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId  where MemberTestId = mt.MemberTestId  and BadgeTypeId = 2 order by b.BadgeId   FOR XML PATH('''')) badges,1 LoggedInUserRanking from MemberBoardReviewQuiz mq inner join MemberTest mt on mt.MemberTestId = mq.MemberTestId  left join memberinfo mi on mi.memberid = mq.memberid where 1= 1   and mt.testId = @xtestId  and  mt.MemberId = @xmemberId  and mt.IgnoreTestResults =0  union all  select top 10 ROW_NUMBER() OVER(ORDER BY mt.userscore DESC) AS Ranking,FirstName, LastName,mt.UserScore,  (select top 1 ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId  where  MemberTestId = mt.MemberTestId and BadgeTypeId = 1 order by b.BadgeId desc) steaks,  (select  '','' + ImageLocation from MemberBadges mb  inner join Badges b on b.BadgeId = mb.BadgeId   where MemberTestId = mt.MemberTestId and BadgeTypeId = 2 order by b.BadgeId   FOR XML PATH('''')) badges,0 LoggedInUserRanking  from MemberBoardReviewQuiz mq inner join MemberTest mt on mt.MemberTestId = mq.MemberTestId   left join memberinfo mi on mi.memberid = mq.memberid where 1= 1  and mt.testId = @xtestId and mt.IgnoreTestResults =0  '
if @programId > 0
select @sql = @sql + ' and mt.programId = @xprogramId '
if @groupId > 0
select @sql = @sql + ' and mt.groupId = @xgroupId '



--PRINT @sql

SELECT @paramlist = '@xtestId int,@xmemberId bigint,@xprogramId int,@xgroupId int'

--insert into dbo.sqllog([text]) values(CONVERT(nvarchar(10), @testId)  + ' ' + CONVERT(nvarchar(10), @memberId)  + ' '  + CONVERT(nvarchar(10), @programId) + ' '  + CONVERT(nvarchar(10), @groupId)  +'---' + @sql)


EXEC sp_executesql @sql, @paramlist,@testId,@memberId,@programId,@groupId

SET @Err = @@Error
RETURN @Err
END





