﻿CREATE PROCEDURE [dbo].[usp_Sitecore_Program_SELECT]
AS
BEGIN

	SELECT DISTINCT 
	  [ProgramGUID]			= CONVERT(UNIQUEIDENTIFIER, I.[ID])
	, [ProgramName]			= CONVERT(VARCHAR(512), I.[Name]) COLLATE SQL_Latin1_General_CP1_CI_AS
	, [ProgramStartDate]	= CONVERT(DATE,(CASE WHEN CHARINDEX('T', fv.[Value]) > 0 THEN LEFT(fv.[Value], 8) ELSE fv.[Value] END))
	--, [SurveyTextType]		= fvi.[Name]
	FROM [INP_Sitecore_Web].[dbo].[Items] i
	JOIN [INP_Sitecore_Web].[dbo].[VersionedFields] fv
	ON fv.[ItemId] = i.[ID]
	JOIN [INP_Sitecore_Web].[dbo].[Items] fvi
		ON fvi.[ID] = fv.[FieldId]
	WHERE  1=1
		AND CHARINDEX('Validity Start Date', fvi.[Name]) > 0 -- IN('StartDate')
		AND CHARINDEX(' by', fvi.[Name]) = 0
		AND CHARINDEX('Text', fvi.[Name]) = 0
		AND fv.[Value] <> ''
		AND NOT(fvi.[Name] IN ('Update Text'))
		AND NOT(i.[Name] IN ('__Standard Values'))
	;

END
;
GO