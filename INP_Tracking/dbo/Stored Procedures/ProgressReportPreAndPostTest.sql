﻿

CREATE PROCEDURE [dbo].[ProgressReportPreAndPostTest]  
@textbookId int,  
@memberId bigint,   
@programId bigint,  
@assignmentIds nvarchar(1000),  
@groupIds nvarchar(1000),  
@participantsIds nvarchar(1000)  
WITH EXECUTE AS CALLER  
AS  
BEGIN  
SET NOCOUNT ON  
DECLARE @Err int  
DECLARE @sql nvarchar(2000),@paramlist nvarchar(max)  
  
if (@assignmentIds !=0)  
 begin  
  select distinct c.chapterid,s.sectionId,  
  (SELECT assignmentstatusid FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) AssignmentId,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPretestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPosttestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid   and assignmentid in (select data from dbo.Split(@assignmentIds, ','))) ProgramPretestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid   and assignmentid in (select data from dbo.Split(@assignmentIds, ','))) ProgramPretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid   and assignmentid in (select data from dbo.Split(@assignmentIds, ','))) ProgramPosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid   and assignmentid in (select data from dbo.Split(@assignmentIds, ','))) ProgramPosttestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid ) NationwidePretestCorrect,   
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid ) NationwidePretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid ) NationwidePosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid ) NationwidePosttestTotal  
  from [RFsection]  s inner join   [RFchapter] c on s.sectionid = c.sectionid 
  --inner join  [RFtest] t on t.chapterid = c.chapterid  
  where s.TextBookId =@textbookId  
  
 end  
else if (@groupIds !='0')  
 begin  
 select distinct c.chapterid,s.sectionId,  
 (SELECT assignmentstatusid FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) AssignmentId,  
 (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPretestCorrect,  
 (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPretestTotal,  
 (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPosttestCorrect,  
 (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPosttestTotal,  
 (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) ProgramPretestCorrect,  
 (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) ProgramPretestTotal,  
 (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) ProgramPosttestCorrect,  
 (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) ProgramPosttestTotal,  
 (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) NationwidePretestCorrect,   
 (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) NationwidePretestTotal,  
 (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) NationwidePosttestCorrect,  
 (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid  and GroupId in (select data from dbo.Split(@groupIds, ','))) NationwidePosttestTotal  
 from [RFsection]  s inner join   [RFchapter] c on s.sectionid = c.sectionid 
 --inner join  [RFtest] t on t.chapterid = c.chapterid  
 where s.TextBookId =@textbookId  
 end  
else if (@participantsIds !='0')  
 begin  
  select distinct c.chapterid,s.sectionId,  
  (SELECT top 1 assignmentstatusid FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and  ParticipantId in (select data from dbo.Split(@participantsIds, ','))) AssignmentId,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and  ParticipantId in (select data from dbo.Split(@participantsIds, ','))) IndividualPretestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and  ParticipantId in (select data from dbo.Split(@participantsIds, ','))) IndividualPretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and  ParticipantId in (select data from dbo.Split(@participantsIds, ','))) IndividualPosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and  ParticipantId in (select data from dbo.Split(@participantsIds, ','))) IndividualPosttestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid ) ProgramPretestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid ) ProgramPretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid ) ProgramPosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid ) ProgramPosttestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid ) NationwidePretestCorrect,   
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid ) NationwidePretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid ) NationwidePosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid ) NationwidePosttestTotal  
  from [RFsection]  s inner join   [RFchapter] c on s.sectionid = c.sectionid 
  ---inner join  [RFtest] t on t.chapterid = c.chapterid  
  where s.TextBookId =@textbookId  
 end  
else  
 begin  
  select distinct c.chapterid,s.sectionId,  
  (SELECT assignmentstatusid FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) AssignmentId,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPretestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid and memberid = @memberId) IndividualPosttestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid ) ProgramPretestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and programid = @programId and chapterid = c.chapterid ) ProgramPretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid ) ProgramPosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and programid = @programId and chapterid = c.chapterid ) ProgramPosttestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid ) NationwidePretestCorrect,   
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=1 and chapterid = c.chapterid ) NationwidePretestTotal,  
  (SELECT sum(totalansweredcorrectly) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid ) NationwidePosttestCorrect,  
  (SELECT sum(totalquestions) FROM [ModuleProgressReport]  where TestTypeid=2 and chapterid = c.chapterid ) NationwidePosttestTotal  
  from [RFsection]  s inner join   [RFchapter] c on s.sectionid = c.sectionid
  -- inner join  [RFtest] t on t.chapterid = c.chapterid  
  where s.TextBookId =@textbookId  
 end  
  
  
  
--(SELECT count(*) FROM [QuizProgressReport]  where   
--programid = @programId and questionid = tq.QuestionId and answeredcorrectly = 1) ProgramPass,  
--(SELECT count(*) FROM [QuizProgressReport]  where    
--programid = @programId and questionid = tq.QuestionId)  ProgramTotal,  
--(SELECT count(*) FROM [QuizProgressReport]  where   
--questionid = tq.QuestionId and answeredcorrectly = 1) NationwidePass,  
--(SELECT count(*) FROM [QuizProgressReport]  where   
--questionid = tq.QuestionId)  as NationwideTotal  
--from [RFsection]  s inner join   [RFchapter] c on s.sectionid = c.sectionid inner join  [RFtest] t on t.chapterid = c.chapterid  
--where s.TextBookId =@textbookId  
  
  
  
SET @Err = @@Error  
RETURN @Err  
END  
  

