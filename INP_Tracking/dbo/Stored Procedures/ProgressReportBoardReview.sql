﻿

CREATE PROCEDURE [dbo].[ProgressReportBoardReview]
@textbookId int,
@memberId bigint, 
@programId bigint,
@assignmentIds nvarchar(1000),
@groupIds nvarchar(1000),
@participantsIds nvarchar(1000)
WITH EXECUTE AS CALLER
AS
BEGIN
SET NOCOUNT ON
DECLARE @Err int
DECLARE @sql nvarchar(2000),@paramlist nvarchar(max)


----if (@assignmentIds !=0)
----	begin
----		select distinct QuestionId, 
----		(select top 1 sectionid from [QuizProgressReport] where programid = @programId and questionid = tq.QuestionId) sectionId,
----		(SELECT answeredcorrectly FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and memberid = @memberId) Individual,
----		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and answeredcorrectly = 1 and assignmentid in (select data from dbo.Split(@assignmentIds, ','))) ProgramPass,
----		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and assignmentid in (select data from dbo.Split(@assignmentIds, ',')))  ProgramTotal,
----		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId and answeredcorrectly = 1) NationwidePass,
----		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId)  as NationwideTotal
----		from [RFTest] t inner join [RFTestQuestion]  tq on t.TestId = tq.TestId where TextBookId =@textbookId and  TestTypeId = 4
----	end
----else 
if (@groupIds !='0')
	begin
		select distinct QuestionId, 
		(select top 1 sectionid FROM [QuizProgressReport] where programid = @programId and questionid = tq.QuestionId) sectionId,
		(SELECT answeredcorrectly FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and memberid = @memberId) Individual,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and answeredcorrectly = 1 and GroupId in (select data from dbo.Split(@groupIds, ','))) ProgramPass,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and GroupId in (select data from dbo.Split(@groupIds, ',')))  ProgramTotal,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId and answeredcorrectly = 1 and GroupId in (select data from dbo.Split(@groupIds, ','))) NationwidePass,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId and GroupId in (select data from dbo.Split(@groupIds, ',')))  as NationwideTotal
		from [RFTest] t inner join [RFTestQuestion]  tq on t.TestId = tq.TestId where TextBookId =@textbookId and  TestTypeId = 4
	end
else if (@participantsIds !='0')
	begin
		select distinct QuestionId, 
		(select top 1 sectionid FROM [QuizProgressReport] where programid = @programId and questionid = tq.QuestionId) sectionId,
		(SELECT answeredcorrectly FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and  ParticipantId
		 in (select data from dbo.Split(@participantsIds, ','))) Individual,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and answeredcorrectly = 1) ProgramPass,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId)  ProgramTotal,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId and answeredcorrectly = 1) NationwidePass,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId)  as NationwideTotal
		from [RFTest] t inner join [RFTestQuestion]  tq on t.TestId = tq.TestId where TextBookId =@textbookId and  TestTypeId = 4
	end
else
	begin
		select distinct QuestionId, 
		(select top 1 sectionid FROM [QuizProgressReport] where programid = @programId and questionid = tq.QuestionId) sectionId,
		(SELECT answeredcorrectly FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and memberid = @memberId) Individual,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId and answeredcorrectly = 1) ProgramPass,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where programid = @programId and questionid = tq.QuestionId)  ProgramTotal,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId and answeredcorrectly = 1) NationwidePass,
		(SELECT count(MemberId) FROM [QuizProgressReport]  where questionid = tq.QuestionId)  as NationwideTotal
		from [RFTest] t inner join [RFTestQuestion]  tq on t.TestId = tq.TestId where TextBookId =@textbookId and  TestTypeId = 4
	end



SET @Err = @@Error
RETURN @Err
END



