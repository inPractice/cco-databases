﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_Survey_RF_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
		 [SiteCode] 					
		,[SourceCode]					
		,[SRC_MemberSurveyGUID]			
		,[SRC_MemberSurveyID]			
		,[SRC_MemberSurveyResponseGUID]	
		,[SRC_MemberSurveyResponseID]	
		,[SRC_MemberGUID]				
		,[SRC_SurveyGUID]				
		,[SRC_QuestionGUID] 			
		,[SRC_ResponseGUID] 			
		,[SRC_SessionGUID]				
		,[ResponseText] 				
		,[SurveyType]					
		,[CountryCode]					
		,[ActivityDttm]					
		,[CompletedDttm]				
		,[CompletedInd]					
		,[PassedInd]					
		,[InsertedDttm]					
		,[LastModifiedDttm]				
	FROM
	(
		SELECT
		 [SiteCode] 					= 'R&F'
		,[SourceCode]					= 'IPR_Sitecore'
		,[SRC_MemberSurveyGUID]			= CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000')
		,[SRC_MemberSurveyID]			= mt.[MemberTestId]
		,[SRC_MemberSurveyResponseGUID]	= CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000')
		,[SRC_MemberSurveyResponseID]	= ma.[MemberAnswerId]
		,[SRC_MemberGUID]				= m.[MemberID]
		,[SRC_SurveyGUID]				= t.[TestSitecoreId]
		,[SRC_QuestionGUID] 			= q.[QuestionSitecoreId]
		,[SRC_ResponseGUID] 			= a.[AnswerSitecoreId]
		,[SRC_SessionGUID]				= mrq.[SessionUniqueId]
		,[ResponseText] 				= CONVERT(nvarchar(1024), '')
		,[SurveyType]					= CONVERT(varchar(36), 'Board Review')
		,[CountryCode]					= ''
		,[ActivityDttm]					= ma.[CreatedDttm]
		,[CompletedDttm]				= mt.[CompletedDttm]
		,[CompletedInd]					= CONVERT(BIT,mt.[Completed])
		,[PassedInd]					= CONVERT(BIT, mt.[PassedTest])
		,[InsertedDttm]					= mt.[CreatedDttm]
		,[LastModifiedDttm]				= mrq.[CreatedDttm]
		FROM [dbo].[MemberTest] mt 
		JOIN [dbo].[MemberQuestion] mq 
			ON mq.[MemberTestId] = mt.[MemberTestId]
		JOIN [dbo].[MemberAnswer] ma
			ON ma.[MemberQuestionId] = mq.[MemberQuestionId]
		JOIN [dbo].[MemberBoardReviewQuiz] mrq
			ON mrq.[MemberTestId] = mt.[MemberTestId]
		JOIN [INP_Manifest].[dbo].[Test] t
			ON t.[TestId] = mt.[TestId]
		JOIN [INP_Manifest].[dbo].[Question] q
			ON q.[QuestionId] = mq.[QuestionId]
		JOIN [INP_Manifest].[dbo].[Answer] a
			ON a.[AnswerId] = ma.[AnswerId]
		JOIN [MemberCentralProd].[dbo].[Member] m
			ON m.[MemberInt] = mt.[MemberId]
		WHERE 1=1
			AND ((ma.[CreatedDttm])) > @Dttm
	) res

END
GO
