﻿



CREATE VIEW [dbo].[vwMemberProductDetail]
AS
SELECT MemberSubscriptionId
, m.MemberName
, m.EmailAddress
, m.UserLogin
, MemberId = ms.[MemberGUID]
, sp.[SubscriptionId]
, sp.[ProductId]
, p.[ProductGUID]
, p.[ProductName]
, s.[SubscriptionName]
, p.[ProductDescription]
, [RegionCountryName] 			= ISNULL(spa.[RegionCountryName], 'Universal')
, [RegionCountryCode]			= ISNULL(spa.[RegionCountryCode], 'ALL')
, ms.[SubscriptionTrialAccessId]
, TrialAccessMessage = ISNULL(sta.AccessMessage, '')
, TrialAccessDuration = CASE WHEN ISNULL(sta.TermDuration, 0) = 0 THEN '' ELSE CAST(sta.TermDuration as varchar(12)) END + ' ' + tut.TermUnitName
, ms.[SubscriptionPaidAccessId]
, PaidAccessMessage = ISNULL(spa.AccessMessage, '')
, PaidAccessDuration = CASE WHEN ISNULL(spa.TermDuration, 0) = 0 THEN '' ELSE CAST(spa.TermDuration as varchar(12)) END + ' ' + tup.TermUnitName
, ms.SubscriptionGroupAccessId
, GroupAccessMessage = ISNULL(sga.AccessMessage, '')
, GroupAccessDuration = CASE WHEN ISNULL(sga.TermDuration, 0) = 0 THEN '' ELSE CAST(sga.TermDuration as varchar(12)) END + ' ' + tug.TermUnitName
, ms.SubscriptionStartDate
, ms.SubscriptionEndDate
, TransactionID = ISNULL(ms.TransactionID, '')
, sga.AccessCode
, sga.PromotionName
FROM dbo.MemberSubscription ms (nolock)
JOIN 
(
	SELECT m.MemberID
	, MemberName = ISNULL(m.FirstName, '') + ' ' + ISNULL(m.LastName, '')
	, EmailAddress = em.Email
	, UserLogin		= sm.UserName
	FROM MemberCentralProd.dbo.Member m
	JOIN MemberCentralProd.dbo.EmailMember em
		ON em.MemberID = m.MemberID
	JOIN MemberCentralProd.dbo.SiteMember sm
		ON sm.MemberID = m.MemberID
) m
	ON m.MemberID = ms.MemberGUID

JOIN dbo.Subscription s (nolock)
	ON s.SubscriptionId = ms.SubscriptionId
JOIN dbo.SubscriptionProduct sp (nolock)
	ON sp.SubscriptionId = ms.SubscriptionId
JOIN dbo.Product p (nolock)
	ON p.ProductId = sp.ProductId
LEFT JOIN dbo.SubscriptionTrialAccess sta (nolock)
	ON sta.SubscriptionTrialAccessId = ms.[SubscriptionTrialAccessId]
LEFT JOIN dbo.TermUnit tut (nolock)
	ON tut.TermUnitId = sta.TermUnitId
LEFT JOIN dbo.SubscriptionPaidAccess spa (nolock)
	ON spa.SubscriptionPaidAccessId = ms.[SubscriptionPaidAccessId]
LEFT JOIN dbo.TermUnit tup (nolock)
	ON tup.TermUnitId = spa.TermUnitId
LEFT JOIN dbo.SubscriptionGroupAccess sga (nolock)
	ON sga.SubscriptionGroupAccessId = ms.[SubscriptionGroupAccessId]
LEFT JOIN dbo.TermUnit tug (nolock) 
	ON tug.TermUnitId = sga.TermUnitId
WHERE 1=1
AND ms.DeletedInd = 0




