﻿

CREATE VIEW [dbo].[vwProductSubscriptionAccessList]
AS 
	SELECT
	acs.ProductName
	, acs.ProductId
	, acs.SubscriptionName
	, acs.SubscriptionId
	, acs.AvailabilityEndDate
	, acs.ExpirationDate
	, SubscriptionTrialAccessId = CASE WHEN acs.TrailSubscriptionName <> '' THEN acs.SubscriptionAccessId ELSE 0 END
	, acs.TrailSubscriptionName
	, SubscriptionPaidAccessId = CASE WHEN acs.PaidSubscriptionName <> '' THEN acs.SubscriptionAccessId ELSE 0 END
	, acs.PaidSubscriptionName
	, SubscriptionGroupAccessId = CASE WHEN acs.GroupSubscriptionName <> '' THEN acs.SubscriptionAccessId ELSE 0 END
	, acs.GroupSubscriptionName
	, acs.OrganizationName
	, acs.GroupName
	, acs.GroupAccessCode
	FROM 
	(
	SELECT DISTINCT 
	  p.ProductName
	, s.SubscriptionName
	, TrailSubscriptionName = LTRIM(
		CASE WHEN sta.SubscriptionTrialAccessId IS NULL THEN ''
					ELSE 
						CASE WHEN sta.TermDuration = 0  THEN '' ELSE CAST(sta.TermDuration as varchar(4)) END 
						+ ' ' 
						+ CASE WHEN sta.TermDuration = 0 
								THEN ''
							WHEN sta.TermDuration = 1 
								THEN Left(ttu.TermUnitName, LEN(ttu.TermUnitName)-1) 
							ELSE ttu.TermUnitName 
							END 
		+ ' ' + sta.AccessMessage
		END)
	, PaidSubscriptionName =''
	, GroupSubscriptionName = ''
	, GroupAccessCode = ''
	, GroupName			= ''
	, OrganizationName  = ''
	, sta.AvailabilityEndDate
	, sta.ExpirationDate
	, sp.[SubscriptionId]
	, sp.[ProductId]
	, [SubscriptionAccessId]	= sta.SubscriptionTrialAccessId
	FROM [SiteAccess].[dbo].[SubscriptionProduct] sp
	JOIN [SiteAccess].[dbo].[Subscription] s
		ON s.[SubscriptionId] = sp.[SubscriptionId]
	JOIN [SiteAccess].[dbo].[Product] p
		ON p.[ProductId] = sp.[ProductId]
	JOIN [SiteAccess].[dbo].[SubscriptionTrialAccess] sta
		ON sta.[SubscriptionId] = sp.[SubscriptionId]
	JOIN [SiteAccess].[dbo].[TermUnit] ttu
		ON ttu.[TermUnitId] = sta.[TermUnitId]
	UNION ALL
	SELECT DISTINCT 
	  p.ProductName
	, s.SubscriptionName
	, TrailSubscriptionName = ''
	, PaidSubscriptionName = ''
	, GroupSubscriptionName = LTRIM(
		CASE WHEN sga.SubscriptionGroupAccessId IS NULL THEN ''
					ELSE 
						CASE WHEN sga.TermDuration = 0  THEN '' ELSE CAST(sga.TermDuration as varchar(4)) END 
						+ ' ' 
						+ CASE WHEN sga.TermDuration = 0 
								THEN ''
							WHEN sga.TermDuration = 1 
								THEN Left(gtu.TermUnitName, LEN(gtu.TermUnitName)-1) 
							ELSE gtu.TermUnitName 
							END 
		+ ' ' + sga.AccessMessage
		END)
	, GroupAccessCode = CASE WHEN sga.SubscriptionGroupAccessId IS NULL THEN '' ELSE sga.AccessCode END
	, GroupName			= CASE WHEN sga.SubscriptionGroupAccessId IS NULL THEN '' ELSE og.OrganizationGroupName END
	, OrganizationName  = CASE WHEN sga.SubscriptionGroupAccessId IS NULL THEN '' ELSE o.OrganizationName END
	, sga.AvailabilityEndDate
	, sga.ExpirationDate
	, sp.[SubscriptionId]
	, sp.[ProductId]
	, [SubscriptionAccessId]	= sga.SubscriptionGroupAccessId
	FROM [SiteAccess].[dbo].[SubscriptionProduct] sp
	JOIN [SiteAccess].[dbo].[Subscription] s
		ON s.[SubscriptionId] = sp.[SubscriptionId]
	JOIN [SiteAccess].[dbo].[Product] p
		ON p.[ProductId] = sp.[ProductId]
	JOIN [SiteAccess].[dbo].[SubscriptionGroupAccess] sga
		ON sga.[SubscriptionId] = sp.[SubscriptionId]
	JOIN [SiteAccess].[dbo].[TermUnit] gtu
		ON gtu.[TermUnitId] = sga.[TermUnitId]
	JOIN [SiteAccess].[dbo].[OrganizationGroup] og
		ON og.[OrganizationGroupId] = sga.[OrganizationGroupId]
	JOIN [SiteAccess].[dbo].[Organization] o
		ON o.[OrganizationId] = og.[OrganizationId]
	UNION ALL
	SELECT DISTINCT 
	  p.ProductName
	, s.SubscriptionName
	, TrailSubscriptionName = ''
	, PaidSubscriptionName = LTRIM(
		CASE WHEN spa.SubscriptionPaidAccessId IS NULL THEN ''
					ELSE 
						CASE WHEN spa.TermDuration = 0  THEN '' ELSE CAST(spa.TermDuration as varchar(4)) END 
						+ ' ' 
						+ CASE WHEN spa.TermDuration = 0 
								THEN ''
							WHEN spa.TermDuration = 1 
								THEN Left(ptu.TermUnitName, LEN(ptu.TermUnitName)-1) 
							ELSE ptu.TermUnitName 
							END 
		+ ' ' + spa.AccessMessage
		END)
	, GroupSubscriptionName = ''
	, GroupAccessCode	= ''
	, GroupName			= ''
	, OrganizationName  = ''
	, spa.AvailabilityEndDate
	, spa.ExpirationDate
	, sp.[SubscriptionId]
	, sp.[ProductId]
	, [SubscriptionAccessId]	= spa.SubscriptionPaidAccessId
	FROM [SiteAccess].[dbo].[SubscriptionProduct] sp
	JOIN [SiteAccess].[dbo].[Subscription] s
		ON s.[SubscriptionId] = sp.[SubscriptionId]
	JOIN [SiteAccess].[dbo].[Product] p
		ON p.[ProductId] = sp.[ProductId]
	JOIN [SiteAccess].[dbo].[SubscriptionPaidAccess] spa
		ON spa.[SubscriptionId] = sp.[SubscriptionId]
	JOIN [SiteAccess].[dbo].[TermUnit] ptu
		ON ptu.[TermUnitId] = spa.[TermUnitId]
	) acs
--	ORDER BY 2,5,4,3,1


