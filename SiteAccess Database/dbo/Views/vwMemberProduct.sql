﻿
CREATE VIEW [dbo].[vwMemberProduct]
AS
SELECT MemberSubscriptionId,
 MemberId = ms.MemberGUID
, sp.SubscriptionId
, sp.ProductId
, p.ProductGUID
, p.ProductName
, s.SubscriptionName
, p.ProductDescription
, [SubscriptionTrialAccessId] = ISNULL(ms.SubscriptionTrialAccessId, 0)
, TrialAccessMessage = ISNULL(sta.AccessMessage, '')
, [SubscriptionPaidAccessId] = ISNULL(ms.[SubscriptionPaidAccessId], 0)
, PaidAccessMessage = ISNULL(spa.AccessMessage, '')
, SubscriptionGroupAccessId = ISNULL(ms.SubscriptionGroupAccessId, 0)
, GroupAccessMessage = ISNULL(sga.AccessMessage, '')
, ms.SubscriptionStartDate
, ms.SubscriptionEndDate
, TransactionID = ISNULL(ms.TransactionID, '')
, AccessCode = ISNULL(sga.AccessCode, '')
, PromotionName = ISNULL(sga.PromotionName,'')
FROM dbo.MemberSubscription ms
JOIN dbo.Subscription s
	ON s.SubscriptionId = ms.SubscriptionId
JOIN dbo.SubscriptionProduct sp
	ON sp.SubscriptionId = ms.SubscriptionId
JOIN dbo.Product p
	ON p.ProductId = sp.ProductId
LEFT JOIN dbo.SubscriptionTrialAccess sta
	ON sta.SubscriptionTrialAccessId = ms.[SubscriptionTrialAccessId]
LEFT JOIN dbo.SubscriptionPaidAccess spa
	ON spa.SubscriptionPaidAccessId = ms.[SubscriptionPaidAccessId]
LEFT JOIN dbo.SubscriptionGroupAccess sga
	ON sga.SubscriptionGroupAccessId = ms.[SubscriptionGroupAccessId]
WHERE 1=1
AND ms.DeletedInd = 0

