﻿

/* 
  fn_IsGUID Check that a String is a valid GUID
  Returns:
   Valid GUID (as uniqueidentifier), or NULL if invalid
 */
CREATE FUNCTION [dbo].[fn_IsGUID]
(
 @strGUID varchar(8000) -- String to be tested - hyphens optional, allows surrounding {...} and trims spaces
)
RETURNS uniqueidentifier -- NULL = Bad GUID encountered, else cleanedup GUID returned
-- WITH ENCRYPTION 
 BEGIN 

DECLARE @uidGUID uniqueidentifier 
SELECT  @strGUID = LTRIM(RTRIM(REPLACE(REPLACE(@strGUID, '{', ''), '}', ''))),            
    @strGUID = CASE WHEN LEN(@strGUID) = 32            
         THEN LEFT(@strGUID, 8)             
          + '-' + SUBSTRING(@strGUID, 9, 4)            
          + '-' + SUBSTRING(@strGUID, 13, 4)            
          + '-' + SUBSTRING(@strGUID, 17, 4)            
          + '-' + SUBSTRING(@strGUID, 21, 12)             
         ELSE @strGUID            
         END,            
    @uidGUID = CASE WHEN @strGUID like             
         '[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]-[0-9A-F][0-9A-F][0-9A-F][0-9A-F]-[0-9A-F][0-9A-F][0-9A-F][0-9A-F]-[0-9A-F][0-9A-F][0-9A-F][0-9A-F]-[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]'             
       THEN CONVERT(uniqueidentifier, @strGUID)            
       ELSE NULL            
       END            
RETURN @uidGUID 
END
