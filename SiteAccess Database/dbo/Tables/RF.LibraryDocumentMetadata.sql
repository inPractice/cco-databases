﻿CREATE TABLE [dbo].[RF.LibraryDocumentMetadata] (
    [DocumentID]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [SectionID]       BIGINT           NULL,
    [DocumentTitle]   NVARCHAR (128)   NULL,
    [DocumentSource]  NVARCHAR (128)   NULL,
    [PublicationDate] DATETIME         NULL,
    [FileSize]        INT              NULL,
    [CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]     DATETIME         NOT NULL,
    [ModifiedDttm]    DATETIME         NOT NULL,
    [DeletedDttm]     DATETIME         NULL,
    [DeletedInd]      BIT              NOT NULL,
    CONSTRAINT [PK_RF.LibraryDocument] PRIMARY KEY CLUSTERED ([DocumentID] ASC),
    CONSTRAINT [FK_RF.LibraryDocument_RF.LibrarySection] FOREIGN KEY ([SectionID]) REFERENCES [dbo].[RF.LibrarySection] ([SectionID])
);

