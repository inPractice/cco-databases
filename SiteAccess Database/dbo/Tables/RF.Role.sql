﻿CREATE TABLE [dbo].[RF.Role] (
    [RoleID]   BIGINT         NOT NULL,
    [RoleName] NVARCHAR (128) NULL,
    CONSTRAINT [PK_RF.Role] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);

