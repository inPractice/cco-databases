﻿CREATE TABLE [dbo].[RF.Assignment] (
    [AssignmentID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ProgramID]    BIGINT           NULL,
    [Name]         NVARCHAR (128)   NULL,
    [AssignDate]   DATETIME         NULL,
    [DueDate]      DATETIME         NULL,
    [Instructions] NVARCHAR (512)   NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]  DATETIME         NOT NULL,
    [ModifiedDttm] DATETIME         NOT NULL,
    [DeletedDttm]  DATETIME         NULL,
    [DeletedInd]   BIT              NOT NULL,
    CONSTRAINT [PK_RF.Assignment] PRIMARY KEY CLUSTERED ([AssignmentID] ASC),
    CONSTRAINT [FK_RF.Assignment_RF.Program] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[RF.Program] ([ProgramID])
);

