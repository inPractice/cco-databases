﻿CREATE TABLE [dbo].[Subscription] (
    [SubscriptionId]          BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SubscriptionGUID]        UNIQUEIDENTIFIER CONSTRAINT [DF_Subscription_SubscriptionGUID] DEFAULT (newid()) NOT NULL,
    [SubscriptionName]        VARCHAR (128)    NOT NULL,
    [SubscriptionDescription] VARCHAR (512)    NOT NULL,
    [ActiveInd]               BIT              CONSTRAINT [DF_Subscription_ActiveInd] DEFAULT ((0)) NOT NULL,
    [LaunchDate]              DATETIME         NULL,
    [ExpirationDate]          DATETIME         NULL,
    [AvailabilityEndDate]     DATETIME         NULL,
    [InsertedDttm]            DATETIME         CONSTRAINT [DF_Subscription_InsertedDttm] DEFAULT (getdate()) NOT NULL,
    [LastModifiedDttm]        DATETIME         NULL,
    [CreatedDttm]             DATETIME         CONSTRAINT [DF_Subscription_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]            DATETIME         NULL,
    [DeletedInd]              BIT              CONSTRAINT [DF_Subscription_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]             DATETIME         NULL,
    CONSTRAINT [PK_Subscription] PRIMARY KEY CLUSTERED ([SubscriptionId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source system record creation date', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'InsertedDttm';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last day subscription available for for purchase', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'AvailabilityEndDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date subscription exipires', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'ExpirationDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date subscription made available to public', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'LaunchDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subscription active and available for purchase', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'ActiveInd';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source system subscription name(s) and description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'SubscriptionDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Reporting subscription name (clean)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'SubscriptionName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies Subscription', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Subscription', @level2type = N'COLUMN', @level2name = N'SubscriptionId';

