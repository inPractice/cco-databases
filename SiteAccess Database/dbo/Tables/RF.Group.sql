﻿CREATE TABLE [dbo].[RF.Group] (
    [GroupID]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [GroupName]    NVARCHAR (128)   NULL,
    [StartDate]    DATETIME         NULL,
    [EndDate]      DATETIME         NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]  DATETIME         NOT NULL,
    [ModifiedDttm] DATETIME         NOT NULL,
    [DeletedDttm]  DATETIME         NULL,
    [DeletedInd]   BIT              NOT NULL,
    CONSTRAINT [PK_RF.Group] PRIMARY KEY CLUSTERED ([GroupID] ASC)
);

