﻿CREATE TABLE [dbo].[RF.LibrarySection] (
    [SectionID]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [ProgramID]       BIGINT           NULL,
    [ProductGUID]     UNIQUEIDENTIFIER NULL,
    [SectionName]     NVARCHAR (128)   NULL,
    [DescriptiveText] NVARCHAR (512)   NULL,
    [LeftNavDisplay]  BIT              NULL,
    [DisplayName]     NVARCHAR (128)   NULL,
    [CreatedBy]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]     DATETIME         NOT NULL,
    [ModifiedDttm]    DATETIME         NOT NULL,
    [DeletedDttm]     DATETIME         NULL,
    [DeletedInd]      BIT              NOT NULL,
    CONSTRAINT [PK_RF.LibrarySection] PRIMARY KEY CLUSTERED ([SectionID] ASC),
    CONSTRAINT [FK_RF.LibrarySection_RF.Program] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[RF.Program] ([ProgramID])
);

