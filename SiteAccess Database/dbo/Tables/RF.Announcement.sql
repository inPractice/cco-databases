﻿CREATE TABLE [dbo].[RF.Announcement] (
    [AnnouncementID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ProgramID]      BIGINT           NULL,
    [Subject]        NVARCHAR (128)   NULL,
    [Message]        NVARCHAR (MAX)   NULL,
    [RoleID]         BIGINT           NULL,
    [DateExpired]    DATETIME         NULL,
    [CreatedBy]      UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]    DATETIME         NOT NULL,
    [ModifiedDttm]   DATETIME         NOT NULL,
    [DeletedDttm]    DATETIME         NULL,
    [DeletedInd]     BIT              NOT NULL,
    CONSTRAINT [PK_Announcement] PRIMARY KEY CLUSTERED ([AnnouncementID] ASC),
    CONSTRAINT [FK_RF.Announcement_RF.Program] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[RF.Program] ([ProgramID]),
    CONSTRAINT [FK_RF.Announcement_RF.Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[RF.Role] ([RoleID])
);

