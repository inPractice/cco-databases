﻿CREATE TABLE [dbo].[RF.Participant] (
    [ParticipantID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [MemberID]      UNIQUEIDENTIFIER NOT NULL,
    [ProgramID]     BIGINT           NOT NULL,
    [GroupID]       BIGINT           NULL,
    [RoleID]        BIGINT           NOT NULL,
    [StatusID]      INT              NOT NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]   DATETIME         NOT NULL,
    [ModifiedDttm]  DATETIME         NOT NULL,
    [DeletedDttm]   DATETIME         NULL,
    [DeletedInd]    BIT              NOT NULL,
    CONSTRAINT [PK_RF.Participant] PRIMARY KEY CLUSTERED ([ParticipantID] ASC),
    CONSTRAINT [FK_RF.Participant_RF.Group] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[RF.Group] ([GroupID]),
    CONSTRAINT [FK_RF.Participant_RF.Program] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[RF.Program] ([ProgramID]),
    CONSTRAINT [FK_RF.Participant_RF.Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[RF.Role] ([RoleID]),
    CONSTRAINT [FK_RF.Participant_RF.Status] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[RF.Status] ([StatusID])
);

