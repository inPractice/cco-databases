﻿CREATE TABLE [dbo].[RF.ForumType] (
    [ForumTypeId]   INT          IDENTITY (1, 1) NOT NULL,
    [ForumTypeName] VARCHAR (50) NULL,
    CONSTRAINT [PK_RF.ForumType] PRIMARY KEY CLUSTERED ([ForumTypeId] ASC)
);

