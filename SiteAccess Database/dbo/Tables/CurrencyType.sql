﻿CREATE TABLE [dbo].[CurrencyType] (
    [CurrencyTypeId]     INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CurrencyTypeName]   VARCHAR (64) NOT NULL,
    [CurrencyTypeCode]   VARCHAR (36) NULL,
    [CurrencyTypeSymbol] NVARCHAR (5) NULL,
    [ActiveInd]          BIT          CONSTRAINT [DF_CurrencyType_ActiveInd] DEFAULT ((1)) NOT NULL,
    [CreatedDttm]        DATETIME     CONSTRAINT [DF_CurrencyType_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]       DATETIME     NULL,
    [DeletedInd]         BIT          CONSTRAINT [DF_CurrencyType_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]        DATETIME     NULL,
    CONSTRAINT [PK_CurrencyType] PRIMARY KEY CLUSTERED ([CurrencyTypeId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Currency type descriptor name (i.e. US-Dollar)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CurrencyType', @level2type = N'COLUMN', @level2name = N'CurrencyTypeName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies CurrencyType', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CurrencyType', @level2type = N'COLUMN', @level2name = N'CurrencyTypeId';

