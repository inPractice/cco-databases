﻿CREATE TABLE [dbo].[RF.Class] (
    [ClassID]      BIGINT           IDENTITY (1, 1) NOT NULL,
    [ProgramID]    BIGINT           NOT NULL,
    [ClassName]    NVARCHAR (128)   NOT NULL,
    [StartDate]    DATETIME         NOT NULL,
    [EndDate]      DATETIME         NOT NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]  DATETIME         NOT NULL,
    [ModifiedDttm] DATETIME         NOT NULL,
    [DeletedDttm]  DATETIME         NULL,
    [DeletedInd]   BIT              NOT NULL,
    CONSTRAINT [PK_RF.Class] PRIMARY KEY CLUSTERED ([ClassID] ASC),
    CONSTRAINT [FK_RF.Class_RF.Program] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[RF.Program] ([ProgramID]),
    CONSTRAINT [FK_RF.Class_RF.Program1] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[RF.Program] ([ProgramID])
);

