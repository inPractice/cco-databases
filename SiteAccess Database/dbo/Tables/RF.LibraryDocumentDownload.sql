﻿CREATE TABLE [dbo].[RF.LibraryDocumentDownload] (
    [DownloadID]    BIGINT   IDENTITY (1, 1) NOT NULL,
    [ParticipantID] BIGINT   NOT NULL,
    [DocumentID]    BIGINT   NOT NULL,
    [CreatedDttm]   DATETIME DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]  DATETIME DEFAULT (getdate()) NOT NULL,
    [DeletedDttm]   DATETIME NULL,
    [DeletedInd]    BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_RF.LibraryDocumentDownload] PRIMARY KEY CLUSTERED ([DownloadID] ASC)
);

