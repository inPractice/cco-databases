﻿CREATE TABLE [dbo].[RF.ForumAccess] (
    [ForumId]      BIGINT        IDENTITY (1, 1) NOT NULL,
    [Title]        VARCHAR (150) NOT NULL,
    [Description]  VARCHAR (500) NULL,
    [Url]          VARCHAR (200) NOT NULL,
    [RF_ProgramId] BIGINT        NULL,
    [ForumTypeId]  INT           NOT NULL,
    [CreatedDttm]  DATETIME      NOT NULL,
    [ModifiedDttm] DATETIME      NOT NULL,
    CONSTRAINT [PK_RF.ForumAccess] PRIMARY KEY CLUSTERED ([ForumId] ASC),
    CONSTRAINT [FK_RF.ForumAccess_RF.ForumType] FOREIGN KEY ([ForumTypeId]) REFERENCES [dbo].[RF.ForumType] ([ForumTypeId]),
    CONSTRAINT [FK_RF.ForumAccess_RF.Program] FOREIGN KEY ([RF_ProgramId]) REFERENCES [dbo].[RF.Program] ([ProgramID])
);

