﻿CREATE TABLE [dbo].[RF.Status] (
    [StatusID]   INT           NOT NULL,
    [StatusName] NVARCHAR (50) NULL,
    CONSTRAINT [PK_RF.Status] PRIMARY KEY CLUSTERED ([StatusID] ASC)
);

