﻿CREATE TABLE [dbo].[SubscriptionLookup] (
    [SubscriptionLookupId]          BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SubscriptionAccessTrialId]     BIGINT           NULL,
    [SubscriptionAccessPaidId]      BIGINT           NULL,
    [SubscriptionAccessGroupId]     BIGINT           NULL,
    [SiteAccessSubscriptionId]      BIGINT           NULL,
    [MemberCentralSubscriptionGUID] UNIQUEIDENTIFIER NULL,
    [MemberCentralSubscriptionName] VARCHAR (128)    NULL,
    [CreatedDttm]                   DATETIME         CONSTRAINT [DF_SubscriptionLookup_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]                  DATETIME         NULL,
    [DeletedInd]                    BIT              CONSTRAINT [DF_SubscriptionLookup_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]                   DATETIME         NULL,
    CONSTRAINT [PK_SubscriptionLookup] PRIMARY KEY CLUSTERED ([SubscriptionLookupId] ASC) WITH (IGNORE_DUP_KEY = ON)
);

