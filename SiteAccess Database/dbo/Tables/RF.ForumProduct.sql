﻿CREATE TABLE [dbo].[RF.ForumProduct] (
    [ForumProductId] BIGINT           IDENTITY (1, 1) NOT NULL,
    [ForumId]        BIGINT           NOT NULL,
    [ProductGuid]    UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_RF.ForumProduct] PRIMARY KEY CLUSTERED ([ForumProductId] ASC),
    CONSTRAINT [FK_RF.ForumProduct_RF.ForumAccess] FOREIGN KEY ([ForumId]) REFERENCES [dbo].[RF.ForumAccess] ([ForumId])
);

