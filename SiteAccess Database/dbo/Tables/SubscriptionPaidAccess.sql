﻿CREATE TABLE [dbo].[SubscriptionPaidAccess] (
    [SubscriptionPaidAccessId] BIGINT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SubscriptionId]           BIGINT          NOT NULL,
    [NetSuiteProductUniqueId]  NVARCHAR (256)  NOT NULL,
    [AccessMessage]            NVARCHAR (256)  NULL,
    [TermUnitId]               INT             CONSTRAINT [DF_SubscriptionPaidAccess_TermUnitId] DEFAULT ((0)) NOT NULL,
    [TermDuration]             INT             NOT NULL,
    [Version]                  VARCHAR (36)    CONSTRAINT [DF_SubscriptionPaidAccess_Version] DEFAULT ((CONVERT([varchar](4),datepart(year,getdate()),(0))+'-')+CONVERT([varchar](2),datepart(month,getdate()),(0))) NOT NULL,
    [Cost]                     DECIMAL (12, 2) NOT NULL,
    [Currency]                 NVARCHAR (64)   NOT NULL,
    [RegionCountryName]        NVARCHAR (128)  NULL,
    [RegionCountryCode]        NVARCHAR (24)   NULL,
    [LaunchDate]               DATETIME        NULL,
    [AvailabilityEndDate]      DATETIME        NULL,
    [ExpirationDate]           DATETIME        NULL,
    [CreatedDttm]              DATETIME        CONSTRAINT [DF_SubscriptionPaidAccess_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]             DATETIME        NULL,
    [DeletedInd]               BIT             CONSTRAINT [DF_SubscriptionPaidAccess_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]              DATETIME        NULL,
    CONSTRAINT [PK_SubscriptionPaidAccess] PRIMARY KEY CLUSTERED ([SubscriptionPaidAccessId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_SubscriptionPaidAccess_Subscription] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([SubscriptionId])
);

