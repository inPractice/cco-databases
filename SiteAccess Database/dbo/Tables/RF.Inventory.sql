﻿CREATE TABLE [dbo].[RF.Inventory] (
    [InventoryID]  BIGINT           IDENTITY (1, 1) NOT NULL,
    [AssignmentID] BIGINT           NOT NULL,
    [SourceTypeID] BIGINT           NOT NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]  DATETIME         NOT NULL,
    [ModifiedDttm] DATETIME         NOT NULL,
    [DeletedDttm]  DATETIME         NULL,
    [DeletedInd]   BIT              NOT NULL,
    [SourceID]     BIGINT           NOT NULL,
    CONSTRAINT [PK_RF.Inventory] PRIMARY KEY CLUSTERED ([InventoryID] ASC),
    FOREIGN KEY ([AssignmentID]) REFERENCES [dbo].[RF.Assignment] ([AssignmentID]),
    FOREIGN KEY ([AssignmentID]) REFERENCES [dbo].[RF.Assignment] ([AssignmentID]),
    FOREIGN KEY ([SourceTypeID]) REFERENCES [dbo].[RF.AssignmentSource] ([SourceTypeID]),
    FOREIGN KEY ([SourceTypeID]) REFERENCES [dbo].[RF.AssignmentSource] ([SourceTypeID])
);

