﻿CREATE TABLE [dbo].[Organization] (
    [OrganizationId]          BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrganizationName]        NVARCHAR (128)   NOT NULL,
    [OrganizationDescription] NVARCHAR (MAX)   NULL,
    [OrganizationGUID]        UNIQUEIDENTIFIER NULL,
    [ParentOrganizationId]    BIGINT           CONSTRAINT [DF_Organization_ParentOrganizationId] DEFAULT ((0)) NULL,
    [ContactName]             NVARCHAR (128)   NULL,
    [ContactMemberId]         UNIQUEIDENTIFIER NULL,
    [WebSite]                 VARCHAR (256)    NULL,
    [Phone]                   VARCHAR (30)     NULL,
    [Fax]                     VARCHAR (30)     NULL,
    [EmailAddress]            VARCHAR (128)    NULL,
    [AddressTo]               NVARCHAR (128)   NULL,
    [Address1]                NVARCHAR (128)   NULL,
    [Address2]                NVARCHAR (128)   NULL,
    [City]                    NVARCHAR (96)    NULL,
    [ZipCode]                 VARCHAR (20)     NULL,
    [ZipPlus]                 VARCHAR (5)      NULL,
    [refStateProvinceId]      INT              NULL,
    [CountryId]               INT              NULL,
    [ActiveInd]               BIT              CONSTRAINT [DF_Organization_ActiveInd] DEFAULT ((1)) NOT NULL,
    [CreatedDttm]             DATETIME         CONSTRAINT [DF_Organization_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]            DATETIME         NULL,
    [DeletedInd]              BIT              CONSTRAINT [DF_Organization_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]             DATETIME         NULL,
    [OrganizationImage]       VARBINARY (MAX)  NULL,
    CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED ([OrganizationId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_Organization_ParentOrganization] FOREIGN KEY ([ParentOrganizationId]) REFERENCES [dbo].[Organization] ([OrganizationId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Organization URL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'COLUMN', @level2name = N'WebSite';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Contact link for existing member', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'COLUMN', @level2name = N'ContactMemberId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Source system org GUID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'COLUMN', @level2name = N'OrganizationGUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Customer organization description', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'COLUMN', @level2name = N'OrganizationDescription';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Customer organization name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Organization', @level2type = N'COLUMN', @level2name = N'OrganizationName';

