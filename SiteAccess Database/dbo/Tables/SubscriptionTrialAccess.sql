﻿CREATE TABLE [dbo].[SubscriptionTrialAccess] (
    [SubscriptionTrialAccessId] BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SubscriptionId]            BIGINT         NOT NULL,
    [TermUnitId]                INT            CONSTRAINT [DF_SubscriptionTrialAccess_TermUnitId] DEFAULT ((0)) NOT NULL,
    [TermDuration]              INT            NOT NULL,
    [Version]                   VARCHAR (36)   CONSTRAINT [DF_SubscriptionTrialAccess_Version] DEFAULT ((CONVERT([varchar](4),datepart(year,getdate()),(0))+'-')+CONVERT([varchar](2),datepart(month,getdate()),(0))) NOT NULL,
    [AccessMessage]             NVARCHAR (256) NULL,
    [LaunchDate]                DATETIME       NULL,
    [AvailabilityEndDate]       DATETIME       NULL,
    [ExpirationDate]            DATETIME       NULL,
    [CreatedDttm]               DATETIME       CONSTRAINT [DF_SubscriptionTrialAccess_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]              DATETIME       NULL,
    [DeletedInd]                BIT            CONSTRAINT [DF_SubscriptionTrialAccess_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]               DATETIME       NULL,
    CONSTRAINT [PK_SubscriptionTrialAccess] PRIMARY KEY CLUSTERED ([SubscriptionTrialAccessId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_SubscriptionTrialAccess_Subscription] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([SubscriptionId])
);

