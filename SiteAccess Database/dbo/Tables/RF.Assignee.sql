﻿CREATE TABLE [dbo].[RF.Assignee] (
    [AssigneeID]    BIGINT           IDENTITY (1, 1) NOT NULL,
    [AssignmentID]  BIGINT           NOT NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]   DATETIME         NOT NULL,
    [ModifiedDttm]  DATETIME         NOT NULL,
    [DeletedDttm]   DATETIME         NULL,
    [DeletedInd]    BIT              NOT NULL,
    [ParticipantID] BIGINT           NOT NULL,
    CONSTRAINT [PK_RF.Assignee] PRIMARY KEY CLUSTERED ([AssigneeID] ASC),
    FOREIGN KEY ([AssignmentID]) REFERENCES [dbo].[RF.Assignment] ([AssignmentID]),
    CONSTRAINT [FK_Assignee_Participant] FOREIGN KEY ([ParticipantID]) REFERENCES [dbo].[RF.Participant] ([ParticipantID])
);

