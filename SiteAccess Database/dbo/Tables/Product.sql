﻿CREATE TABLE [dbo].[Product] (
    [ProductId]           BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductGUID]         UNIQUEIDENTIFIER CONSTRAINT [DF_Product_ProductGUID] DEFAULT (newid()) NOT NULL,
    [ProductName]         VARCHAR (128)    NOT NULL,
    [ProductDescription]  VARCHAR (512)    NOT NULL,
    [ActiveInd]           BIT              CONSTRAINT [DF_Product_ActiveInd] DEFAULT ((1)) NOT NULL,
    [LaunchDate]          DATETIME         NULL,
    [AvailabilityEndDate] DATETIME         NULL,
    [CreatedDttm]         DATETIME         CONSTRAINT [DF_Product_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]        DATETIME         NULL,
    [DeletedInd]          BIT              CONSTRAINT [DF_Product_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]         DATETIME         NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last day product available to public', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'AvailabilityEndDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date product went live', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'LaunchDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Product active and available for use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'ActiveInd';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Distinct Product Name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'ProductName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'MemberCentralProd.dbo.EcommerceMemberSubscription.ProductId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'ProductGUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Distinct product identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Product', @level2type = N'COLUMN', @level2name = N'ProductId';

