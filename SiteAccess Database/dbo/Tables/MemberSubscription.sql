﻿CREATE TABLE [dbo].[MemberSubscription] (
    [MemberSubscriptionId]      BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberGUID]                UNIQUEIDENTIFIER NOT NULL,
    [SubscriptionId]            BIGINT           NOT NULL,
    [SubscriptionTrialAccessId] BIGINT           CONSTRAINT [DF_MemberSubscription_TrialAccessId] DEFAULT ((0)) NOT NULL,
    [SubscriptionPaidAccessId]  BIGINT           CONSTRAINT [DF_MemberSubscription_PaidAccessId] DEFAULT ((0)) NOT NULL,
    [SubscriptionGroupAccessId] BIGINT           CONSTRAINT [DF_MemberSubscription_GroupAccessId] DEFAULT ((0)) NOT NULL,
    [SubscriptionStartDate]     DATETIME         NOT NULL,
    [SubscriptionEndDate]       DATETIME         NULL,
    [TransactionID]             NVARCHAR (128)   NULL,
    [CreatedDttm]               DATETIME         CONSTRAINT [DF_MemberSubscription_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]              DATETIME         NULL,
    [DeletedInd]                BIT              CONSTRAINT [DF_MemberSubscription_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]               DATETIME         NULL,
    CONSTRAINT [PK_MemberSubscription] PRIMARY KEY CLUSTERED ([MemberSubscriptionId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_MemberSubscription_Subscription] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([SubscriptionId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UDX_MemberSubscription]
    ON [dbo].[MemberSubscription]([MemberGUID] ASC, [SubscriptionId] ASC, [SubscriptionTrialAccessId] ASC, [SubscriptionPaidAccessId] ASC, [SubscriptionGroupAccessId] ASC, [SubscriptionStartDate] ASC) WITH (IGNORE_DUP_KEY = ON);

