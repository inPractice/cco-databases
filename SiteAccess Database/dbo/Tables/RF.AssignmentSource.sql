﻿CREATE TABLE [dbo].[RF.AssignmentSource] (
    [SourceTypeID] BIGINT         NOT NULL,
    [SourceName]   NVARCHAR (128) NULL,
    CONSTRAINT [PK_RF.Source] PRIMARY KEY CLUSTERED ([SourceTypeID] ASC)
);

