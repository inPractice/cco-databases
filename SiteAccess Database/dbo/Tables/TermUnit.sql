﻿CREATE TABLE [dbo].[TermUnit] (
    [TermUnitId]          INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [TermUnitName]        VARCHAR (64)     NOT NULL,
    [TermUnitGUID]        UNIQUEIDENTIFIER NULL,
    [TermUnitCode]        VARCHAR (12)     NULL,
    [TermUnitDescription] VARCHAR (256)    NULL,
    [ActiveInd]           BIT              CONSTRAINT [DF_TermUnit_ActiveInd] DEFAULT ((1)) NOT NULL,
    [CreatedDttm]         DATETIME         CONSTRAINT [DF_TermUnit_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]        DATETIME         NULL,
    [DeletedInd]          BIT              CONSTRAINT [DF_TermUnit_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]         DATETIME         NULL,
    CONSTRAINT [PK_TermUnit] PRIMARY KEY CLUSTERED ([TermUnitId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subscription term duration incl. (Days, Weeks, Months, Years, Unlimited)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TermUnit', @level2type = N'COLUMN', @level2name = N'TermUnitName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies TermUnit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TermUnit', @level2type = N'COLUMN', @level2name = N'TermUnitId';

