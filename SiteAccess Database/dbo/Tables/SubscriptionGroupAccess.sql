﻿CREATE TABLE [dbo].[SubscriptionGroupAccess] (
    [SubscriptionGroupAccessId] BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrganizationGroupId]       BIGINT         NOT NULL,
    [SubscriptionId]            BIGINT         NOT NULL,
    [TermUnitId]                INT            CONSTRAINT [DF_SubscriptionGroupAccess_TermUnitId] DEFAULT ((0)) NOT NULL,
    [TermDuration]              INT            NOT NULL,
    [Version]                   VARCHAR (36)   CONSTRAINT [DF_SubscriptionGroupAccess_Version] DEFAULT ((CONVERT([varchar](4),datepart(year,getdate()),(0))+'-')+CONVERT([varchar](2),datepart(month,getdate()),(0))) NOT NULL,
    [CanConveyInd]              BIT            CONSTRAINT [DF_SubscriptionGroupAccess_CanConveyInd] DEFAULT ((1)) NOT NULL,
    [IPBasedInd]                BIT            NOT NULL,
    [AccessCode]                NVARCHAR (256) NOT NULL,
    [PromotionName]             NVARCHAR (256) NULL,
    [AccessMessage]             NVARCHAR (256) NULL,
    [SeatsAllocated]            INT            CONSTRAINT [DF_SubscriptionGroupAccess_SeatsAllocated] DEFAULT ((0)) NOT NULL,
    [SeatsConsumed]             INT            CONSTRAINT [DF_SubscriptionGroupAccess_SeatsConsumed] DEFAULT ((0)) NOT NULL,
    [LaunchDate]                DATETIME       NULL,
    [AvailabilityEndDate]       DATETIME       NULL,
    [ExpirationDate]            DATETIME       NULL,
    [CreatedDttm]               DATETIME       CONSTRAINT [DF_SubscriptionGroupAccess_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]              DATETIME       NULL,
    [DeletedInd]                BIT            CONSTRAINT [DF_SubscriptionGroupAccess_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]               DATETIME       NULL,
    CONSTRAINT [PK_SubscriptionGroupAccess] PRIMARY KEY CLUSTERED ([SubscriptionGroupAccessId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_SubscriptionGroupAccess_OrganizationGroup] FOREIGN KEY ([OrganizationGroupId]) REFERENCES [dbo].[OrganizationGroup] ([OrganizationGroupId]),
    CONSTRAINT [FK_SubscriptionGroupAccess_Subscription] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([SubscriptionId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group access expiration date', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'ExpirationDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last day group access can be issued', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'AvailabilityEndDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Day group access first available for use', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'LaunchDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of user seats used', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'SeatsConsumed';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Number of user seats available', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'SeatsAllocated';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group access message template for MySubscriptions page', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'AccessMessage';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subscription promotion name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'PromotionName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group site access code', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'AccessCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group access uses IP-based authentication', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'IPBasedInd';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Group access can convey to user - this needs to always true', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'CanConveyInd';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Term length integer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'TermDuration';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Descriptions of of subscription length', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'TermUnitId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Subscription link', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'SubscriptionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Organization group link', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionGroupAccess', @level2type = N'COLUMN', @level2name = N'OrganizationGroupId';

