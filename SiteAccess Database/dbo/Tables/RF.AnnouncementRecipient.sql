﻿CREATE TABLE [dbo].[RF.AnnouncementRecipient] (
    [AnnouncementRecipientID] BIGINT           IDENTITY (1, 1) NOT NULL,
    [AnnouncementID]          BIGINT           NOT NULL,
    [ClassID]                 BIGINT           NULL,
    [GroupID]                 BIGINT           NULL,
    [CreatedBy]               UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]             DATETIME         NOT NULL,
    [ModifiedDttm]            DATETIME         NOT NULL,
    [DeletedDttm]             DATETIME         NULL,
    [DeletedInd]              BIT              NOT NULL,
    CONSTRAINT [PK_RF.AnnouncementRecipient] PRIMARY KEY CLUSTERED ([AnnouncementRecipientID] ASC),
    CONSTRAINT [FK_RF.AnnouncementRecipient_RF.Announcement] FOREIGN KEY ([AnnouncementID]) REFERENCES [dbo].[RF.Announcement] ([AnnouncementID]),
    CONSTRAINT [FK_RF.AnnouncementRecipient_RF.Class] FOREIGN KEY ([ClassID]) REFERENCES [dbo].[RF.Class] ([ClassID]),
    CONSTRAINT [FK_RF.AnnouncementRecipient_RF.Group] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[RF.Group] ([GroupID])
);

