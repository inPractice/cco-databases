﻿CREATE TABLE [dbo].[OrganizationGroup] (
    [OrganizationGroupId]          BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OrganizationId]               BIGINT         NOT NULL,
    [OrganizationGroupName]        NVARCHAR (128) NOT NULL,
    [OrganizationGroupGUID]        NVARCHAR (128) CONSTRAINT [DF_OrganizationGroup_OrganizationGroupGUID] DEFAULT (newid()) NOT NULL,
    [OrganizationGroupDescription] VARCHAR (512)  NULL,
    [CreatedDttm]                  DATETIME       CONSTRAINT [DF_OrganizationGroup_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]                 DATETIME       NULL,
    [DeletedInd]                   BIT            CONSTRAINT [DF_OrganizationGroup_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]                  DATETIME       NULL,
    CONSTRAINT [PK_OrganizationGroup] PRIMARY KEY CLUSTERED ([OrganizationGroupId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_OrganizationGroup_Organization] FOREIGN KEY ([OrganizationId]) REFERENCES [dbo].[Organization] ([OrganizationId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Distinct organization group name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationGroup', @level2type = N'COLUMN', @level2name = N'OrganizationGroupName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Organization link', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationGroup', @level2type = N'COLUMN', @level2name = N'OrganizationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies OrganizationGroup', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OrganizationGroup', @level2type = N'COLUMN', @level2name = N'OrganizationGroupId';

