﻿CREATE TABLE [dbo].[RF.Program] (
    [ProgramID]           BIGINT           IDENTITY (1, 1) NOT NULL,
    [ProgramName]         NVARCHAR (256)   NOT NULL,
    [OrganizationID]      BIGINT           NULL,
    [ProgramURLPart]      NVARCHAR (100)   NOT NULL,
    [LicenseStart]        DATETIME         NULL,
    [LicenseEnd]          DATETIME         NULL,
    [ProductGUID]         UNIQUEIDENTIFIER NULL,
    [CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]         DATETIME         NOT NULL,
    [ModifiedDttm]        DATETIME         NOT NULL,
    [DeletedDttm]         DATETIME         NULL,
    [DeletedInd]          BIT              NOT NULL,
    [StatusID]            INT              NULL,
    [MainContactMemberID] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_RF.Program] PRIMARY KEY CLUSTERED ([ProgramID] ASC),
    CONSTRAINT [FK__RF.Progra__Statu__1B9317B3] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[RF.Status] ([StatusID]),
    CONSTRAINT [FK_RF.Program_Organization] FOREIGN KEY ([OrganizationID]) REFERENCES [dbo].[Organization] ([OrganizationId])
);

