﻿CREATE TABLE [dbo].[RF.LibraryDocumentContent] (
    [DocumentID]   BIGINT           NOT NULL,
    [FileContent]  VARBINARY (MAX)  NOT NULL,
    [CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]  DATETIME         NOT NULL,
    [ModifiedDttm] DATETIME         NOT NULL,
    [DeletedDttm]  DATETIME         NULL,
    [DeletedInd]   BIT              NOT NULL,
    CONSTRAINT [PK_RF.LibraryDocumentContent] PRIMARY KEY CLUSTERED ([DocumentID] ASC)
);

