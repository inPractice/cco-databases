﻿CREATE TABLE [dbo].[RF.ParticipantClass] (
    [ParticipantID] BIGINT           NOT NULL,
    [ClassID]       BIGINT           NOT NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedDttm]   DATETIME         NOT NULL,
    [ModifiedDttm]  DATETIME         NOT NULL,
    [DeletedDttm]   DATETIME         NULL,
    [DeletedInd]    BIT              NOT NULL,
    CONSTRAINT [PK_RF.ParticipantClass_1] PRIMARY KEY CLUSTERED ([ParticipantID] ASC, [ClassID] ASC),
    CONSTRAINT [FK_RF.ParticipantClass_RF.Class] FOREIGN KEY ([ClassID]) REFERENCES [dbo].[RF.Class] ([ClassID]),
    CONSTRAINT [FK_RF.ParticipantClass_RF.Participant] FOREIGN KEY ([ParticipantID]) REFERENCES [dbo].[RF.Participant] ([ParticipantID])
);

