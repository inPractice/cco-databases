﻿CREATE TABLE [dbo].[SubscriptionProduct] (
    [SubscriptionProductId] BIGINT   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SubscriptionId]        BIGINT   NOT NULL,
    [ProductId]             BIGINT   NOT NULL,
    [CreatedDttm]           DATETIME CONSTRAINT [DF_SubscriptionProduct_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]          DATETIME NULL,
    [DeletedInd]            BIT      CONSTRAINT [DF_SubscriptionProduct_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]           DATETIME NULL,
    CONSTRAINT [PK_SubscriptionProduct] PRIMARY KEY CLUSTERED ([SubscriptionProductId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_SubscriptionProduct_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId]),
    CONSTRAINT [FK_SubscriptionProduct_Subscription] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([SubscriptionId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies SubscriptionProduct', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SubscriptionProduct', @level2type = N'COLUMN', @level2name = N'SubscriptionProductId';

