﻿


CREATE  PROCEDURE [dbo].[GetMemberProductAccess](
@MemberId uniqueidentifier
)
AS
BEGIN

	SELECT p.[ProductGUID] 
	,[SubscriptionStartDate] = min([SubscriptionStartDate]) 
	,[SubscriptionEndDate]  = max(ms.[SubscriptionEndDate]) 
	FROM [dbo].[MemberSubscription] ms (nolock)
	JOIN [dbo].[SubscriptionProduct] sp
		ON sp.SubscriptionId = ms.SubscriptionId
	JOIN [dbo].[Product] p
			ON p.ProductId = sp.ProductId
	WHERE ms.MemberGUID = @MemberId
	AND ms.DeletedInd = 0
	--AND CAST(ms.[SubscriptionEndDate] as date) >= CAST(GetDate() as date)
	GROUP BY p.[ProductGUID] 

END


