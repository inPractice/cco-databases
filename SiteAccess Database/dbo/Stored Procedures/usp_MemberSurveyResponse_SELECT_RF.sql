﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_RF_SELECT]
AS
	SELECT DISTINCT 
	 [SiteCode]					= 'R&F'
	,[OrganizationName]			= o.[OrganizationName]
	,[OrganizationDescription]	= CONVERT(NVARCHAR(255), ISNULL(o.[OrganizationDescription], ''))
	,[OrganizationImage]		= CONVERT(VARBINARY, o.[OrganizationImage])
	,[ContactName]				= CONVERT(NVARCHAR(255), ISNULL(o.[ContactName],''))
	,[GroupName]				= CONVERT(NVARCHAR(255), ISNULL(g.[GroupName], ''))
	,[ProgramName]				= CONVERT(NVARCHAR(255), ISNULL(p.[ProgramName], ''))
	,[ClassName]				= CONVERT(NVARCHAR(255), ISNULL(c.[ClassName], 'N/A'))
	,[AssignmentName]			= CONVERT(NVARCHAR(255), ISNULL(asg.[Name], ''))
	,[AssignDate]				= asg.[AssignDate]
	,[AssignmentDueDate]		= [DueDate]
	,[MemberGUID]				= pt.[MemberID]
	,[EnrollmentStatusName]		= CONVERT(VARCHAR(36), st.[StatusName])
	,[MemberRoleName]			= CONVERT(VARCHAR(36),ISNULL(r.[RoleName], ''))
	,[Participant_CreatedBy]	= pt.[CreatedBy]
	,[Participant_CreatedDttm]	= pt.[CreatedDttm]
	,[Participant_ModifiedDttm]	= pt.[ModifiedDttm]
	,[Participant_DeletedDttm]	= pt.[DeletedDttm]
	,[Participant_DeletedInd]	= pt.[DeletedInd]
	,[SRC_GroupId]				= pt.[GroupId]
	,[SRC_ProgramId]			= pt.[ProgramId]
	,[SRC_AssignmentId]			= asg.[AssignmentID]
	FROM [SiteAccess].[dbo].[RF.Participant] pt
	JOIN [SiteAccess].[dbo].[RF.Program] p
		ON p.ProgramID = pt.[ProgramId]
	--	AND NOT(p.[ProgramName] IN ('Hep Test'))
	JOIN [SiteAccess].[dbo].[RF.Group] g
		ON g.[GroupID] = pt.[GroupID]
	JOIN [SiteAccess].[dbo].[Organization] o
		ON o.[OrganizationId] = p.[OrganizationId]
	JOIN [SiteAccess].[dbo].[RF.Role] r
		ON r.[RoleId] = pt.[RoleId]
	JOIN [SiteAccess].[dbo].[RF.Status] st
		ON st.[StatusId] = pt.[StatusId]
	JOIN [MemberCentralProd].[dbo].[Member] m
		ON m.[MemberId] = pt.[MemberId]
	LEFT JOIN [SiteAccess].[dbo].[RF.ParticipantClass] pc
		ON pc.[ParticipantID] = pt.[ParticipantID]
	LEFT JOIN [SiteAccess].[dbo].[RF.Class] c
		ON c.[ClassID] = pc.[ClassID]
	LEFT JOIN [SiteAccess].[dbo].[RF.Assignee] ae
		ON ae.[ParticipantID] = pt.[ParticipantID]
	LEFT JOIN [SiteAccess].[dbo].[RF.Assignment] asg
		ON asg.[AssignmentID] = ae.[AssignmentID]
	;
;
