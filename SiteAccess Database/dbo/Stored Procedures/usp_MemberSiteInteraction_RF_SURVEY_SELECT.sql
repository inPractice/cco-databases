﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_RF_SURVEY_SELECT]
AS
SELECT DISTINCT
 [SiteCode]				= 'R&F' 
,[MemberGUID]			= m.MemberId
,[EmailAddress]			= em.[Email]
,[SessionGUID]			= mq.[SessionUniqueId]
,[Points]				= mbq.[Points]
,[AccreditationGUId]	= mbq.[AccreditationId]
,[ProviderGUId]			= mbq.[ProviderId]
,[ProgramName]			= pg.[ProgramName]
,[ProgramStartDate]		= pg.[LicenseStart]
,[ProgramEndDate]		= pg.[LicenseEnd]
,[AccreditingOrganizationGUId] = mbq.[AccreditingOrganization]
,[CertificateGUId]		= mbq.[CertificateId]
,[CertificateTypeGUId]	= mbq.[CertificateType]
, mbq.[CertificateTitle]
, mbq.[CertificateUrl]
, mbq.[HoursEngaged]
, mq.[AnsweredCorrectly]
, mq.[PresentedCount]
, mq.[Deferred]
,mt.[UserScore]
,[InsertedDttm]			= mq.[CreatedDttm]
FROM [INP_Tracking].[dbo].[MemberQuestion] mq
JOIN [INP_Tracking].[dbo].[MemberTest] mt
	ON mq.[MemberTestId]		= mt.[MemberTestId]
JOIN [INP_Tracking].[dbo].[MemberBoardReviewQuiz] mbq
	ON mbq.[MemberTestId] = mt.[MemberTestId]
JOIN [MemberCentralProd].[dbo].[Member] m
	ON m.[Memberint]			= mt.[MemberId]
JOIN [MemberCentralProd].[dbo].[EmailMember] em
	ON em.[MemberId]			= m.[MemberId]
JOIN [SiteAccess].[dbo].[RF.Participant] pt
	ON pt.[MemberID]			= m.[MemberID]
JOIN [SiteAccess]. [dbo].[RF.Program] pg
	On pg.[ProgramID] = pt.[ProgramID]

;