﻿

CREATE  PROCEDURE [dbo].[GetMemberProductAccessWithAccessMessage](
@Memberid uniqueidentifier
)
AS
BEGIN

	CREATE TABLE #tempMemberSubscription
	(
		  [tempMemberSubscriptionId] [bigint] IDENTITY(1,1) NOT NULL
		, [MemberSubscriptionId] bigint
		, [SubscriptionId] bigint
		, [ProductId] bigint
		, [SubscriptionEndDate] datetime
		, [SubscriptionTrialAccessId] bigint
		, [SubscriptionPaidAccessId] bigint
		, [SubscriptionGroupAccessId] bigint
	)

	CREATE TABLE #tempMemberSubscriptionLink
	(
		[MemberSubscriptionId] bigint
		,[ProductId] bigint
	)
	CREATE UNIQUE NONCLUSTERED INDEX [UIX_MemberSubscription] ON #tempMemberSubscriptionLink
	(
		[ProductId] ASC
	)WITH (IGNORE_DUP_KEY = ON)

	INSERT #tempMemberSubscription
	(
		[MemberSubscriptionId] 
		, [SubscriptionId]
		, [ProductId] 
		, [SubscriptionEndDate]
		, [SubscriptionTrialAccessId]
		, [SubscriptionPaidAccessId] 
		, [SubscriptionGroupAccessId] 
	)
	SELECT  DISTINCT 		
		  ms.[MemberSubscriptionId] 
		, ms.[SubscriptionId]
		, sp.[ProductId] 
		, ms.[SubscriptionEndDate]
		, ms.[SubscriptionTrialAccessId]
		, ms.[SubscriptionPaidAccessId] 
		, ms.[SubscriptionGroupAccessId] 
	FROM dbo.MemberSubscription ms (nolock)
	JOIN dbo.SubscriptionProduct sp
		ON sp. [SubscriptionId]  = ms.[SubscriptionId] 
		AND ms.MemberGUID = @MemberId
		AND ms.DeletedInd = 0

	-- Paid Active
	INSERT #tempMemberSubscriptionLink ([MemberSubscriptionId], [ProductId])
	SELECT DISTINCT  t.[MemberSubscriptionId] , t.[ProductId]
	FROM #tempMemberSubscription t
	JOIN
	(
		SELECT sp.[ProductId]
		, [SubscriptionEndDate] = MAX(ms.[SubscriptionEndDate])
		FROM dbo.MemberSubscription ms
		JOIN dbo.SubscriptionProduct sp
			ON sp.SubscriptionId = ms.SubscriptionId
		WHERE 1=1
			AND ms.MemberGUID = @MemberId
			AND ms.DeletedInd = 0
		GROUP BY sp.[ProductId]
	) p
		ON p.ProductId = t.ProductId
		AND p.SubscriptionEndDate = t.SubscriptionEndDate
	WHERE t.[SubscriptionPaidAccessId] <> 0
	AND CAST(t.SubscriptionEndDate as date) >= CAST(GetDate() as date)

	-- Group Active
	INSERT #tempMemberSubscriptionLink ([MemberSubscriptionId], [ProductId])
	SELECT DISTINCT  t.[MemberSubscriptionId] , t.[ProductId]
	FROM #tempMemberSubscription t
	JOIN
	(
		SELECT sp.[ProductId]
		, [SubscriptionEndDate] = MAX(ms.[SubscriptionEndDate])
		FROM dbo.MemberSubscription ms
		JOIN dbo.SubscriptionProduct sp
			ON sp.SubscriptionId = ms.SubscriptionId
		WHERE 1=1
			AND ms.MemberGUID = @MemberId
			AND ms.DeletedInd = 0
		GROUP BY sp.[ProductId]
	) p
		ON p.ProductId = t.ProductId
		AND p.SubscriptionEndDate = t.SubscriptionEndDate
	WHERE t.[SubscriptionGroupAccessId] <> 0
	AND CAST(t.SubscriptionEndDate as date) >= CAST(GetDate() as date)

	-- Trial Active
	INSERT #tempMemberSubscriptionLink ([MemberSubscriptionId], [ProductId])
	SELECT DISTINCT t.[MemberSubscriptionId] , t.[ProductId]
	FROM #tempMemberSubscription t
	JOIN
	(
		SELECT sp.[ProductId]
		, [SubscriptionEndDate] = MAX(ms.[SubscriptionEndDate])
		FROM dbo.MemberSubscription ms
		JOIN dbo.SubscriptionProduct sp
			ON sp.SubscriptionId = ms.SubscriptionId
		WHERE 1=1
			AND ms.MemberGUID = @MemberId
			AND ms.DeletedInd = 0
		GROUP BY sp.[ProductId]
	) p
		ON p.ProductId = t.ProductId
		AND p.SubscriptionEndDate = t.SubscriptionEndDate
	WHERE t.[SubscriptionTrialAccessId] <> 0
	AND CAST(t.SubscriptionEndDate as date) >= CAST(GetDate() as date)

	-- Paid Inactive
	INSERT #tempMemberSubscriptionLink ([MemberSubscriptionId], [ProductId])
	SELECT DISTINCT  t.[MemberSubscriptionId] , t.[ProductId]
	FROM #tempMemberSubscription t
	JOIN
	(
		SELECT sp.[ProductId]
		, [SubscriptionEndDate] = MAX(ms.[SubscriptionEndDate])
		FROM dbo.MemberSubscription ms
		JOIN dbo.SubscriptionProduct sp
			ON sp.SubscriptionId = ms.SubscriptionId
		WHERE 1=1
			AND ms.MemberGUID = @MemberId
			AND ms.DeletedInd = 0
		GROUP BY sp.[ProductId]
	) p
		ON p.ProductId = t.ProductId
		AND p.SubscriptionEndDate = t.SubscriptionEndDate
	WHERE t.[SubscriptionPaidAccessId] <> 0
	AND CAST(t.SubscriptionEndDate as date) < CAST(GetDate() as date)

	-- Group Inactive
	INSERT #tempMemberSubscriptionLink ([MemberSubscriptionId], [ProductId])
	SELECT DISTINCT  t.[MemberSubscriptionId] , t.[ProductId]
	FROM #tempMemberSubscription t
	JOIN
	(
		SELECT sp.[ProductId]
		, [SubscriptionEndDate] = MAX(ms.[SubscriptionEndDate])
		FROM dbo.MemberSubscription ms
		JOIN dbo.SubscriptionProduct sp
			ON sp.SubscriptionId = ms.SubscriptionId
		WHERE 1=1
			AND ms.MemberGUID = @MemberId
			AND ms.DeletedInd = 0
		GROUP BY sp.[ProductId]
	) p
		ON p.ProductId = t.ProductId
		AND p.SubscriptionEndDate = t.SubscriptionEndDate
	WHERE t.[SubscriptionGroupAccessId] <> 0
	AND CAST(t.SubscriptionEndDate as date) < CAST(GetDate() as date)

	-- Trial Inactive
	INSERT #tempMemberSubscriptionLink ([MemberSubscriptionId], [ProductId])
	SELECT DISTINCT t.[MemberSubscriptionId] , t.[ProductId]
	FROM #tempMemberSubscription t
	JOIN
	(
		SELECT sp.[ProductId]
		, [SubscriptionEndDate] = MAX(ms.[SubscriptionEndDate])
		FROM dbo.MemberSubscription ms
		JOIN dbo.SubscriptionProduct sp
			ON sp.SubscriptionId = ms.SubscriptionId
		WHERE 1=1
			AND ms.MemberGUID = @MemberId
			AND ms.DeletedInd = 0
		GROUP BY sp.[ProductId]
	) p
		ON p.ProductId = t.ProductId
		AND p.SubscriptionEndDate = t.SubscriptionEndDate
	WHERE t.[SubscriptionTrialAccessId] <> 0
	AND CAST(t.SubscriptionEndDate as date) < CAST(GetDate() as date)

	SELECT DISTINCT
	  v.[MemberSubscriptionId]
	, v.[MemberId]
	, v.[SubscriptionId]
	, v.[ProductId]
	, v.[ProductGUID]
	, v.[ProductName]
	, v.[SubscriptionName]
	, v.[ProductDescription]
	, v.[SubscriptionTrialAccessId]
	, v.[TrialAccessMessage]
	, v.[SubscriptionPaidAccessId]
	, v.[PaidAccessMessage]
	, v.[SubscriptionGroupAccessId]
	, v.[GroupAccessMessage]
	, v.[SubscriptionStartDate]
	, v.[SubscriptionEndDate]
	, v.[TransactionID]
	, v.[AccessCode]
	, v.[PromotionName]
	FROM [dbo].[vwMemberProduct] v
	JOIN #tempMemberSubscriptionLink tms
		ON tms.MemberSubscriptionId = v.MemberSubscriptionId
	;

	DROP TABLE #tempMemberSubscription;
	DROP TABLE #tempMemberSubscriptionLink;

END

