﻿
CREATE PROC usp_SubscriptionGroupAccess_INSERT
(
	  @OrganizationGroupId bigint 
	, @SubscriptionId bigint 
	, @TermUnitId int 
	, @TermDuration int 
	, @AccessCode nvarchar(256) 
	, @AccessMessage nvarchar(256)
	, @LaunchDate datetime
	, @AvailabilityEndDate datetime
	, @ExpirationDate datetime
	, @Version varchar(36) = ''
	, @CanConveyInd bit = 0
	, @IPBasedInd bit  = 0
	, @PromotionName nvarchar(256)  = ''
	, @SeatsAllocated int = 0
	, @SeatsConsumed int  = 0
)
AS BEGIN
	/*
		-- USAGE EXAMPLE --
			-- The following example creates a new faculty group subscription with a 36 character GUID access code
			DECLARE @AccessCode uniqueidentifier
			SET @AccessCode = NewID()
			SELECT @AccessCode
			EXEC usp_SubscriptionGroupAccess_INSERT  @OrganizationGroupId = -3, @SubscriptionId = -2, @TermUnitId = 4, @TermDuration = 1, @AccessCode = @AccessCode, @AccessMessage = 'One year faculty access'	, @LaunchDate ='2014-01-01'	, @AvailabilityEndDate ='2014-12-30'	, @ExpirationDate ='2014-12-31'	, @Version  = '2014-1'
	*/
	SET NOCOUNT ON
	DECLARE @SubscriptionGroupAccessId bigint = 0

	SET @AccessCode = LTRIM(RTRIM(@AccessCode))

	IF NOT EXISTS (SELECT 1 
					FROM [dbo].[SubscriptionGroupAccess] 
					WHERE OrganizationGroupId = @OrganizationGroupId 
						AND SubscriptionId = @SubscriptionId
						AND AccessCode = @AccessCode
					)
		INSERT [SiteAccess].[dbo].[SubscriptionGroupAccess]
		(
			 [OrganizationGroupId]
			,[SubscriptionId]
			,[TermUnitId]
			,[TermDuration]
			,[Version]
			,[CanConveyInd]
			,[IPBasedInd]
			,[AccessCode]
			,[PromotionName]
			,[AccessMessage]
			,[SeatsAllocated]
			,[SeatsConsumed]
			,[LaunchDate]
			,[AvailabilityEndDate]
			,[ExpirationDate]
		)
		SELECT
		 [OrganizationGroupId]	= @OrganizationGroupId
		,[SubscriptionId]		= @SubscriptionId 
		,[TermUnitId]			= @TermUnitId 
		,[TermDuration]			= @TermDuration
		,[Version]				= @Version  
		,[CanConveyInd]			= @CanConveyInd
		,[IPBasedInd]			= @IPBasedInd 
		,[AccessCode]			= @AccessCode
		,[PromotionName]		= @PromotionName 
		,[AccessMessage]		= @AccessMessage  
		,[SeatsAllocated]		= @SeatsAllocated
		,[SeatsConsumed]		= @SeatsConsumed 
		,[LaunchDate]			= @LaunchDate
		,[AvailabilityEndDate]	= @AvailabilityEndDate 
		,[ExpirationDate]		= @ExpirationDate 
		SET @SubscriptionGroupAccessId = (SELECT @SubscriptionGroupAccessId FROM [dbo].[SubscriptionGroupAccess] WHERE [AccessCode] = @AccessCode)
		SELECT SubscriptionGroupAccessId = @SubscriptionGroupAccessId	
	END
