﻿CREATE PROCEDURE [dbo].[usp_Program_RF_SELECT]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		 [SiteCode]				= 'R&F'
		,[TherapeuticAreaName]	= pr.[ProductName]
		,[ProgramName]			= CONVERT(NVARCHAR(128), [ProgramName])
		,[ReportingName]		= CONVERT(NVARCHAR(128), [ProgramName])
		,[ProgramTypeName]		= 'Fellowship Program'
		,[SRC_ProgramID]		= rp.[ProgramID]
		,[ProgramURLPart]
		,[ProgramStart]			= rp.[LicenseStart]
		,[ProgramEnd]			= rp.[LicenseEnd]
		,[InstitutionName]		= o.[OrganizationName]
		,[ProductName]			= pr.[ProductName]
		,[StatusName]			= rs.[StatusName]
		,[ProductGUID]			= rp.[ProductGUID]
		,[ProgramCreatedBy]		= rp.[CreatedBy]
		,[InsertedDttm]			= rp.[CreatedDttm]
		,[LastModifiedDttm]		= rp.[ModifiedDttm]
		,[MainContactMemberID]
	FROM [SiteAccess].[dbo].[RF.Program] rp
	JOIN [SiteAccess].[dbo].[Organization] o
	ON o.[OrganizationId] = rp.[OrganizationId]
	JOIN [SiteAccess].[dbo].[Product] pr
		ON pr.[ProductGUID] = rp.[ProductGUID]
	LEFT JOIN [SiteAccess].[dbo].[RF.Status] rs
		ON rs.[StatusID] = rp.[StatusID]
	; 
END
GO
