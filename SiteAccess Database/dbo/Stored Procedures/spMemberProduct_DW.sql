﻿CREATE PROCEDURE [dbo].[spMemberProduct_DW]
AS
BEGIN

	CREATE TABLE #TmpRole
	(
		  RoleId varchar(96)
		, RoleName varchar(128)
	)
	;

	INSERT INTO #TmpRole(RoleId, RoleName) 
	SELECT Id, [Name] 
	FROM [MemberCentralProd].dbo.[Role]
	;


	SELECT 
	  MemberId				= ms.MemberGUID
	, EmailAddress			= em.Email
	, mem.MemberType
	, p.ProductName
	, s.SubscriptionName
	, p.ProductDescription
	, TrialAccessMessage	= sta.AccessMessage
	, PaidAccessMessage		= spa.AccessMessage
	, GroupAccessMessage	= sga.AccessMessage
	, ms.SubscriptionStartDate
	, ms.SubscriptionEndDate
	, ms.TransactionID
	, sga.AccessCode
	, sga.PromotionName
	, sp.ProductId
	, p.ProductGUID
	, sp.SubscriptionId
	, ms.SubscriptionTrialAccessId
	, ms.[SubscriptionPaidAccessId]
	, ms.SubscriptionGroupAccessId
	FROM [SiteAccess].dbo.MemberSubscription ms
	JOIN [SiteAccess].dbo.Subscription s
		ON s.SubscriptionId = ms.SubscriptionId
	JOIN [SiteAccess].dbo.SubscriptionProduct sp
		ON sp.SubscriptionId = ms.SubscriptionId
	JOIN [SiteAccess].dbo.Product p
		ON p.ProductId = sp.ProductId
	JOIN [MemberCentralProd].dbo.[Member] m
		ON m.MemberID = ms.MemberGUID
	JOIN [MemberCentralProd].dbo.[EmailMember] em
		ON em.MemberID = m.MemberId
	LEFT JOIN [SiteAccess].dbo.SubscriptionTrialAccess sta
		ON sta.SubscriptionTrialAccessId = ms.[SubscriptionTrialAccessId]
	LEFT JOIN [SiteAccess].dbo.SubscriptionPaidAccess spa
		ON spa.SubscriptionPaidAccessId = ms.[SubscriptionPaidAccessId]
	LEFT JOIN [SiteAccess].dbo.SubscriptionGroupAccess sga
		ON sga.SubscriptionGroupAccessId = ms.[SubscriptionGroupAccessId]
	LEFT JOIN 
	(
		SELECT m.MemberId
		, em.Email
		, m.FirstName
		, m.LastName
		, MemberType	= CASE 
							WHEN fltr.MemberRole = 'Test'
								THEN 'Test-Account'
							WHEN fltr.MemberRole = 'Employee'
								THEN 'Employee'
							WHEN 		
								   CHARINDEX('clinicaloptions', em.Email) > 0
								OR CHARINDEX('imedoptions.com', em.Email) > 0
								OR CHARINDEX('inpractice.com', em.Email) > 0
								THEN 'Internal'
							WHEN
									CHARINDEX('idea.com', em.Email) > 0
								 OR CHARINDEX('velir.com', em.Email) > 0
								 OR CHARINDEX('tma.com', em.Email) > 0
								 OR CHARINDEX('dynagility.com', em.Email) > 0
								 OR CHARINDEX('openpathproducts.com', em.Email) > 0
								 THEN 'Vendor'
							WHEN 
								   CHARINDEX('@cco.com', em.Email) > 0
								OR CHARINDEX('@test.com', em.Email) > 0
								OR CHARINDEX('testuser.com', em.Email) > 0
	--									OR LEFT(em.Email,3) = 'tst'
	--									OR LEFT(em.Email,4) = 'test'
								OR em.Email = 'sitemonitor'
								THEN 'Flagged-Email'
							WHEN CHARINDEX('account.com'	, ISNULL(m.LastName, '')) > 0
								OR (ISNULL(m.LastName, '')) = 'test'
								OR CHARINDEX('test'			, ISNULL(m.LastName, '')) > 0 AND CHARINDEX(m.LastName, LEFT(em.Email, CHARINDEX('@', em.Email))) = 0
								OR CHARINDEX('test'			, ISNULL(m.FirstName, '')) > 0
								OR LEN(ISNULL(m.FirstName, '')) > 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '') 
								THEN 'Flagged-Name'
							WHEN 	ISNULL(m.FirstName, '') = 'CCO' AND ISNULL(m.LastName, '') = 'User' AND CHARINDEX('clinicaloptions.com', em.Email) = 0 AND CHARINDEX('inpractice.com', em.Email) = 0
								THEN 'Subscriber'
							ELSE 'Subscriber'
						END
			, RegistrationDate = CAST(m.Created as date)
		FROM [MemberCentralProd].[dbo].[Member] m
		JOIN [MemberCentralProd].[dbo].[EmailMember] em
			ON em.MemberId = m.MemberId
	--			JOIN  [inPracticeTracking].[dbo].MobileUser mu				ON mu.MemberId = m.MemberId
		LEFT JOIN 
		(
			SELECT sm.MemberId
			, MemberRole		= tr.RoleName
			FROM [MemberCentralProd].dbo.SiteMember sm
			JOIN #TmpRole tr
				ON CHARINDEX(tr.RoleId, REPLACE(REPLACE(sm.SitecoreGroups, '{', ''), '}', ' ')) > 0
			WHERE 1=1
			AND tr.RoleName <> 'AdminUser'
			GROUP BY sm.MemberId, tr.RoleName
		) fltr
			ON fltr.MemberID = m.MemberID
	) mem
		ON mem.MemberID = ms.MemberGUID
	DROP TABLE #TmpRole;
END

