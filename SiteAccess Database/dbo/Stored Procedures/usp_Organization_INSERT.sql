﻿CREATE PROC usp_Organization_INSERT
(
	  @OrganizationName nvarchar(128) 
	, @OrganizationDescription nvarchar(1000)  = ''
	, @OrganizationGUID uniqueidentifier  = '00000000-0000-0000-0000-000000000000'
	, @ParentOrganizationId bigint = 0
	, @ContactName nvarchar(128)  = ''
	, @ContactMemberId uniqueidentifier  = '00000000-0000-0000-0000-000000000000'
	, @WebSite varchar(256)  = ''
	, @Phone varchar(30)  = ''
	, @Fax varchar(30)  = ''
	, @EmailAddress varchar(128)  = ''
	, @AddressTo nvarchar(128)  = ''
	, @Address1 nvarchar(128)  = ''
	, @Address2 nvarchar(128)  = ''
	, @City nvarchar(96)  = ''
	, @ZipCode varchar(20)  = ''
	, @ZipPlus varchar(5)  = ''
	, @refStateProvinceId int  = 0
	, @CountryId int  = 0
)
AS BEGIN
	SET NOCOUNT ON

	DECLARE @OrganizationId bigint = 0

	SET @OrganizationName = LTRIM(RTRIM(@OrganizationName))

	IF NOT EXISTS (SELECT 1 FROM dbo.Organization WHERE OrganizationName = @OrganizationName)
		INSERT dbo.[Organization]
		(
		   [OrganizationName]
		  ,[OrganizationDescription]
		  ,[OrganizationGUID]
		  ,[ParentOrganizationId]
		  ,[ContactName]
		  ,[ContactMemberId]
		  ,[WebSite]
		  ,[Phone]
		  ,[Fax]
		  ,[EmailAddress]
		  ,[AddressTo]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[ZipCode]
		  ,[ZipPlus]
		  ,[refStateProvinceId]
		  ,[CountryId]
		  )
		SELECT [OrganizationName]	= @OrganizationName
		  ,[OrganizationDescription]=  @OrganizationDescription
		  ,[OrganizationGUID]		=  @OrganizationGUID
		  ,[ParentOrganizationId]	=  @ParentOrganizationId
		  ,[ContactName]			=  @ContactName
		  ,[ContactMemberId]		=  @ContactMemberId
		  ,[WebSite]				=  @WebSite
		  ,[Phone]					=  @Phone
		  ,[Fax]					=  @Fax
		  ,[EmailAddress]			=  @EmailAddress
		  ,[AddressTo]				=  @AddressTo
		  ,[Address1]				=  @Address1
		  ,[Address2]				=  @Address2
		  ,[City]					=  @City
		  ,[ZipCode]				=  @ZipCode
		  ,[ZipPlus]				=  @ZipPlus
		  ,[refStateProvinceId]		=  @refStateProvinceId
		  ,[CountryId]				=  @CountryId

		SET @OrganizationId = (SELECT OrganizationId FROM dbo.Organization WHERE OrganizationName = @OrganizationName)
		SELECT OrganizationId = @OrganizationId	
	END
