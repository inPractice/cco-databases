﻿



CREATE  PROCEDURE [dbo].[GetMemberProductSubscriptionAccess](
	  @MemberId uniqueidentifier	= '00000000-0000-0000-0000-000000000000'
	, @SubscriptionName varchar(64) = '*'
	, @ProductName varchar(64)		= '*'
)
AS
BEGIN

	SELECT DISTINCT
	  EmailAddress				= em.Email
	, [MemberGUID]				= ms.MemberGUID
	, [ProductGUID]				= p.[ProductGUID] 
	, [ProductName]				= p.[ProductName]
	, [SubscriptionName]		= s.[SubscriptionName]
	, [SubscriptionStartDate]	= [SubscriptionStartDate]
	, [SubscriptionEndDate]		= ms.[SubscriptionEndDate] 
	, [AccessCode]				= ISNULL(sga.[AccessCode], '')
	, [PromoCode]				= ISNULL(sga.[PromotionName], '')
	FROM [dbo].[MemberSubscription] ms (nolock)
	JOIN [MemberCentralProd].[dbo].[EmailMember] em (nolock)
		ON em.MemberID = ms.MemberGUID
	JOIN [dbo].[SubscriptionProduct] sp (nolock)
		ON sp.SubscriptionId = ms.SubscriptionId
	JOIN [dbo].[Subscription] s (nolock)
		ON s.SubscriptionId = ms.SubscriptionId
	JOIN [dbo].[Product] p (nolock)
			ON p.ProductId = sp.ProductId
	LEFT JOIN
	(
		SELECT s.[SubscriptionGroupAccessId]
		, s.[PromotionName]
		, s.[AccessCode] 
		FROM [dbo].[SubscriptionGroupAccess] s
		WHERE 1=1
	) sga
		ON sga.[SubscriptionGroupAccessId] = ms.[SubscriptionGroupAccessId]
	WHERE ms.MemberGUID = 
			CASE 
				WHEN @MemberId = '00000000-0000-0000-0000-000000000000' 
					THEN ms.MemberGUID 
				ELSE @MemberId 
			END
	AND s.[SubscriptionName] = 
			CASE 
				WHEN @SubscriptionName = '*' 
					THEN s.[SubscriptionName] 
				ELSE @SubscriptionName 
			END
	AND p.[ProductName] = 
			CASE 
				WHEN @ProductName = '*' 
					THEN p.[ProductName] 
				ELSE @ProductName 
			END
	AND ms.DeletedInd = 0

END



