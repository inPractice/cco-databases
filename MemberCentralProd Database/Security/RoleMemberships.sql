﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'ccouser';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CLINICALOPTIONS\svc_bi';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'listmanager';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'marketing';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CLINICALOPTIONS\Report Users';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'iprdw-report';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'iprdw-report';

