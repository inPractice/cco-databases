﻿CREATE VIEW [marketing].[MemberNonIndustry_Mailing] AS
SELECT
	*
FROM
	[marketing].[MemberNonIndustry]
WHERE
	IsActive = 1
AND
	MailIsActive = 1
AND
	IsBadAddress = 0
AND
	IsDeceased = 0
