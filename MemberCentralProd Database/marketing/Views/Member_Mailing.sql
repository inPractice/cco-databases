﻿CREATE VIEW [marketing].[Member_Mailing] AS
SELECT
	*
FROM
	[marketing].[Member]
WHERE
	IsActive = 1
AND
	MailIsActive = 1
AND
	IsBadAddress = 0
AND
	IsDeceased = 0

