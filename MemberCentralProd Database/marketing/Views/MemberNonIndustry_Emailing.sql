﻿-- members filtered by email address 
CREATE VIEW [marketing].[MemberNonIndustry_Emailing] AS
SELECT
	*
FROM
	[marketing].[MemberNonIndustry]
WHERE
	Email is not null and
	EmailIsActive = 1 and
	IsBadMail = 0 and
	IsActive = 1