﻿
-- listmanager members
CREATE VIEW [marketing].[MemberListmanager] AS
SELECT
	MemberID 		= m.MemberID,
	IsActive 		= m.IsActive,
	Salutation 		= sal.text,
	FirstName 		= m.FirstName, 
	MiddleName 		= m.Middle, 
	LastName 		= m.LastName, 
	Suffix 			= m.Suffix,
	Phone 			= m.Phone, 
	Fax 			= m.Fax, 
	Affiliation 	= m.Affiliation,
	Degree			= deg.Text,  
	DegreeType		= dt.Text,
	Specialty		= sp.Text, 
	Profession		= prof.Text, 
	ProfessionType	= proft.Text,
	Source			= src.Text,
	Address1 		= mm.Address1, 
	Address2 		= mm.Address2, 
	City 			= mm.City, 
	State			= st.Abbreviation,
	ZipCode 		= mm.ZipCode,
	Country			= cnt.Abbreviation,
	CountryName		= cnt.Text,
	IsUS 			= cnt.IsUS,
	IsBadAddress 	= mm.IsBadAddress,
	MailIsActive	= mm.IsActive,
	Email 			= em.Email,
	SSO				= em.SSO,
	EmailFrequency	= ISNULL(ef.ShortName, '?'),
	EmailIsActive	= em.IsActive,
	IsBadMail 		= em.IsBadEmail,
	CASE WHEN emn1.isActive = 1 THEN 'Y' WHEN emn1.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHIV], 
	CASE WHEN emn2.isActive = 1 THEN 'Y' WHEN emn2.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHepB], 
	CASE WHEN emn3.isActive = 1 THEN 'Y' WHEN emn3.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHepC], 
	CASE WHEN emn4.isActive = 1 THEN 'Y' WHEN emn4.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHem], 
	CASE WHEN emn5.isActive = 1 THEN 'Y' WHEN emn5.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsOnc],
	CASE WHEN emn6.isActive = 1 THEN 'Y' WHEN emn6.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsProstate],
	CASE WHEN emn14.isActive = 1 THEN 'Y' WHEN emn14.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsCardio],
	CASE WHEN emn15.isActive = 1 THEN 'Y' WHEN emn15.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsNeuro],
	CASE WHEN emn6.isActive = 1 THEN 'Y' WHEN emn6.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsUro],
	CASE WHEN emn18.isActive = 1 THEN 'Y' WHEN emn18.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsTrans],
	InPracticeDayOfWeek = CASE
						WHEN emn7.IsActive = 1 THEN 'Monday'
						WHEN emn8.IsActive = 1 THEN 'Tuesday'
						WHEN emn9.IsActive = 1 THEN 'Wednesday'
						WHEN emn10.IsActive = 1 THEN 'Thursday'
						WHEN emn11.IsActive = 1 THEN 'Friday'
						WHEN emn12.IsActive = 1 THEN 'Saturday'
						WHEN emn13.IsActive = 1 THEN 'Sunday'
						ELSE 'None'
						END,
	WhereHear		= wh.Abbreviation,
	UserName 		= sm.UserName,
    JoinedMonth		= CAST(CAST(Month(m.Created) AS VarChar(10)) + '/' + '01' + '/' + CAST(Year(m.Created) AS VarChar(10)) AS DATETIME),
	Created 		= m.Created, 
	Updated 		= m.Updated, 
	LegacyMemberID 	= m.LegacyMemberID 
FROM 
	[dbo].[Member] m
LEFT OUTER JOIN Salutation sal	
	ON sal.salutationid = m.salutationid
LEFT OUTER JOIN	Degree deg
	ON deg.DegreeID = m.DegreeID
LEFT OUTER JOIN	DegreeType dt
	ON dt.DegreeTypeID = deg.DegreeTypeID
LEFT OUTER JOIN Specialty sp
	ON sp.SpecialtyID = m.SpecialtyID
LEFT OUTER JOIN Profession prof
	ON prof.professionid = m.professionid
LEFT OUTER JOIN ProfessionType proft
	ON proft.professiontypeid = prof.professiontypeid
INNER JOIN Source src
	ON src.sourceid = m.sourceid
LEFT OUTER JOIN mailmember mm
	ON mm.memberid = m.memberid
LEFT OUTER JOIN State st
	ON st.StateID = mm.StateID
LEFT OUTER JOIN Country cnt
	ON cnt.CountryID = mm.CountryID
LEFT OUTER JOIN emailmember em
	ON em.memberid = m.memberid
LEFT OUTER JOIN emailfrequency ef
	ON em.emailfrequencyid = ef.emailfrequencyid
LEFT OUTER JOIN emailmemberNewsletter emn1
	ON  emn1.memberid = em.memberid
	AND emn1.emailNewsletterID = 1
LEFT OUTER JOIN emailmemberNewsletter emn2
	ON  emn2.memberid = em.memberid
	AND emn2.emailNewsletterID = 2
LEFT OUTER JOIN emailmemberNewsletter emn3
	ON  emn3.memberid = em.memberid
	AND emn3.emailNewsletterID = 3
LEFT OUTER JOIN emailmemberNewsletter emn4
	ON  emn4.memberid = em.memberid
	AND emn4.emailNewsletterID = 4
LEFT OUTER JOIN emailmemberNewsletter emn5
	ON  emn5.memberid = em.memberid
	AND emn5.emailNewsletterID = 5
LEFT OUTER JOIN emailmemberNewsletter emn6
	ON  emn6.memberid = em.memberid
	AND emn6.emailNewsletterID = 6	
LEFT OUTER JOIN emailmemberNewsletter emn14
	ON  emn14.memberid = em.memberid
	AND emn14.emailNewsletterID = 14	
LEFT OUTER JOIN emailmemberNewsletter emn15
	ON  emn15.memberid = em.memberid
	AND emn15.emailNewsletterID = 15
LEFT OUTER JOIN emailmemberNewsletter emn18
	ON  emn18.memberid = em.memberid
	AND emn18.emailNewsletterID = 18	
LEFT JOIN [EmailMemberNewsletter] emn7 ON emn7.MemberID = em.MemberID AND emn7.EmailNewsletterID = 7
LEFT JOIN [EmailMemberNewsletter] emn8 ON emn8.MemberID = em.MemberID AND emn8.EmailNewsletterID = 8
LEFT JOIN [EmailMemberNewsletter] emn9 ON emn9.MemberID = em.MemberID AND emn9.EmailNewsletterID = 9
LEFT JOIN [EmailMemberNewsletter] emn10 ON emn10.MemberID = em.MemberID AND emn10.EmailNewsletterID = 10
LEFT JOIN [EmailMemberNewsletter] emn11 ON emn11.MemberID = em.MemberID AND emn11.EmailNewsletterID = 11
LEFT JOIN [EmailMemberNewsletter] emn12 ON emn12.MemberID = em.MemberID AND emn12.EmailNewsletterID = 12
LEFT JOIN [EmailMemberNewsletter] emn13 ON emn13.MemberID = em.MemberID AND emn13.EmailNewsletterID = 13
LEFT OUTER JOIN sitemember sm
	ON sm.memberid = m.memberid
LEFT OUTER JOIN whereHearType wh
	ON wh.whereHearTypeiD = sm.whereHearTypeID
WHERE 
	(sm.SitecoreGroups IS NULL AND m.IsActive = 1 AND em.IsActive = 1)
OR 
	em.Email IN /* seed list */
	(	'plamy@clinicaloptions.com',
		'agoldman@clinicaloptions.com',
		'tbates@clinicaloptions.com',
		'jmortimer@clinicaloptions.com',
		'DPeralta@mtgessentials.com',
		'egallelli@clinicaloptions.com',
		'bmaney@clinicaloptions.com',
		'akeller@clinicaloptions.com',
		'ljbomlitz@hotmail.com'
	)
OR
	em.Email LIKE 'testers[123456789]@clinicaloptions.com'

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Lyris Listmanager synchronization', @level0type = N'SCHEMA', @level0name = N'marketing', @level1type = N'VIEW', @level1name = N'MemberListmanager';

