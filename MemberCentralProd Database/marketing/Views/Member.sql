﻿
-- all members denormalized
CREATE VIEW [marketing].[Member] AS
SELECT
	MemberID 		= m.MemberID,
	IsActive 		= m.IsActive,
	Salutation 		= sal.text,
	FirstName 		= m.FirstName, 
	MiddleName 		= m.Middle, 
	LastName 		= m.LastName, 
	Suffix 			= m.Suffix,
	Phone 			= m.Phone,
	IsPhoneActive	= m.IsPhoneActive,
	IsTextActive	= m.IsTextActive, 
	Fax 			= m.Fax, 
	IsFaxActive		= m.IsFaxActive,
	Affiliation 	= m.Affiliation,
	Degree			= deg.Text,  
	DegreeType		= dt.Text,
	Specialty		= sp.Text, 
	Profession		= prof.Text, 
	ProfessionType	= proft.Text,
	Source			= src.Text,
	Address1 		= mm.Address1, 
	Address2 		= mm.Address2, 
	City 			= mm.City, 
	State			= st.Abbreviation,
	ZipCode 		= mm.ZipCode,
	Country			= cnt.Abbreviation,
	CountryName		= cnt.Text,
	IsUS 			= cnt.IsUS,
	IsIndustry		= m.IsIndustry,
	IsDeceased		= m.IsDeceased,
	IsBadAddress 	= mm.IsBadAddress,
	MailIsActive	= mm.IsActive,
	Email 			= em.Email,
	EmailFrequency	= ISNULL(ef.ShortName, '?'),
	EmailIsActive	= em.IsActive,
	EmailStatus		= CASE 
						WHEN lm.MemberType_ = 'normal' THEN 'Normal'
						WHEN lm.MemberType_ = 'held' THEN 'Held'
						WHEN lm.MemberType_ = 'unsub' THEN 'Unsubscribed'
						ELSE '(none)'
					END,
	IsBadMail 		= em.IsBadEmail,
	CASE WHEN emn1.isActive = 1 THEN 'Y' WHEN emn1.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHIV], 
	CASE WHEN emn2.isActive = 1 THEN 'Y' WHEN emn2.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHepB], 
	CASE WHEN emn3.isActive = 1 THEN 'Y' WHEN emn3.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHepC], 
	CASE WHEN emn4.isActive = 1 THEN 'Y' WHEN emn4.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsHem], 
	CASE WHEN emn5.isActive = 1 THEN 'Y' WHEN emn5.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsOnc],
	CASE WHEN emn6.isActive = 1 THEN 'Y' WHEN emn6.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsProstate],
	CASE WHEN emn14.isActive = 1 THEN 'Y' WHEN emn14.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsCardio],
	CASE WHEN emn15.isActive = 1 THEN 'Y' WHEN emn15.isActive IS NULL THEN '?' ELSE 'N' END AS [WantsNeuro],
	WhereHear		= wh.Abbreviation,
	UserName 		= sm.UserName,
    JoinedMonth		= CAST(CAST(Month(m.Created) AS VarChar(10)) + '/' + '01' + '/' + CAST(Year(m.Created) AS VarChar(10)) AS DATETIME),
	Created 		= m.Created, 
	Updated 		= m.Updated, 
	LegacyMemberID 	= m.LegacyMemberID,
	PreferredMemberID = m.PreferredMemberID
FROM 
	[dbo].[Member] m
LEFT OUTER JOIN Salutation sal	
	ON sal.salutationid = m.salutationid
LEFT OUTER JOIN	Degree deg
	ON deg.DegreeID = m.DegreeID
LEFT OUTER JOIN	DegreeType dt
	ON dt.DegreeTypeID = deg.DegreeTypeID
LEFT OUTER JOIN Specialty sp
	ON sp.SpecialtyID = m.SpecialtyID
LEFT OUTER JOIN Profession prof
	ON prof.professionid = m.professionid
LEFT OUTER JOIN ProfessionType proft
	ON proft.professiontypeid = prof.professiontypeid
INNER JOIN Source src
	ON src.sourceid = m.sourceid
LEFT OUTER JOIN mailmember mm
	ON mm.memberid = m.memberid
LEFT OUTER JOIN State st
	ON st.StateID = mm.StateID
LEFT OUTER JOIN Country cnt
	ON cnt.CountryID = mm.CountryID
LEFT OUTER JOIN emailmember em
	ON em.memberid = m.memberid
LEFT OUTER JOIN emailfrequency ef
	ON em.emailfrequencyid = ef.emailfrequencyid
LEFT OUTER JOIN emailmemberNewsletter emn1
	ON  emn1.memberid = em.memberid
	AND emn1.emailNewsletterID = 1
LEFT OUTER JOIN emailmemberNewsletter emn2
	ON  emn2.memberid = em.memberid
	AND emn2.emailNewsletterID = 2
LEFT OUTER JOIN emailmemberNewsletter emn3
	ON  emn3.memberid = em.memberid
	AND emn3.emailNewsletterID = 3
LEFT OUTER JOIN emailmemberNewsletter emn4
	ON  emn4.memberid = em.memberid
	AND emn4.emailNewsletterID = 4
LEFT OUTER JOIN emailmemberNewsletter emn5
	ON  emn5.memberid = em.memberid
	AND emn5.emailNewsletterID = 5
LEFT OUTER JOIN emailmemberNewsletter emn6
	ON  emn6.memberid = em.memberid
	AND emn6.emailNewsletterID = 6	
LEFT OUTER JOIN emailmemberNewsletter emn14
	ON  emn14.memberid = em.memberid
	AND emn14.emailNewsletterID = 14	
LEFT OUTER JOIN emailmemberNewsletter emn15
	ON  emn15.memberid = em.memberid
	AND emn15.emailNewsletterID = 15
LEFT OUTER JOIN sitemember sm
	ON sm.memberid = m.memberid
LEFT OUTER JOIN whereHearType wh
	ON wh.whereHearTypeiD = sm.whereHearTypeID
LEFT JOIN [ProdListmanager].[dbo].[members_] lm
	ON em.Email = lm.EmailAddr_ AND lm.list_ = 'member_central'
WHERE 
	sm.SitecoreGroups IS NULL
