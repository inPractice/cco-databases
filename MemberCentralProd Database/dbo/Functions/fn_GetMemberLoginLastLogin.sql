﻿CREATE FUNCTION [dbo].[fn_GetMemberLoginLastLogin] 
(
@MemberId bigint = 0
, @MemberGUID uniqueidentifier = '00000000-0000-0000-0000-000000000000'
) 
RETURNS TABLE
 AS
 RETURN
    SELECT	  
	   ml.MemberId
	   , ml.MemberGUID 
	   , ml.LastLoginDttm
  FROM [MemberCentralProd].[dbo].[MemberLogin] ml
WHERE ml.MemberId = CASE WHEN @MemberID = 0 THEN ml.MemberId ELSE @MemberId END
OR ml.MemberGUID = CASE WHEN @MemberGUID = '00000000-0000-0000-0000-000000000000' THEN ml.MemberGUId ELSE @MemberGUId END
