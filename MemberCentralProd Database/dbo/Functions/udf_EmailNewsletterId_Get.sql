﻿
-- DROP FUNCTION udf_EmailNewsletterId_Get;

CREATE FUNCTION [dbo].[udf_EmailNewsletterId_Get] (@inputStr varchar(256))
RETURNS int
AS
BEGIN
	DECLARE @outputInt INT
	
	SET @inputStr = LOWER(LTRIM(RTRIM(@inputStr)))
	
	SET @outputInt = (SELECT TOP 1 ISNULL(a.EmailNewsletterId, 0)
	FROM dbo.EmailNewsletter a
	WHERE LOWER(a.[Text]) = @inputStr)
	
	
	RETURN @outputInt

END

