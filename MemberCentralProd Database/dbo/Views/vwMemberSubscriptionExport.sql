﻿


CREATE VIEW [dbo].[vwMemberSubscriptionExport] AS
	SELECT 
	  OverWriteInd				= CASE WHEN ems.SubscriptionEndDate <= GetDate() THEN 1 ELSE 0 END
	, MemberGUID				= ems.MemberGUId
	, ProductName				= ems.ProductId
	, SubscriptionName			= ems.SubscriptionId
	, SubscriptionStartDate		= REPLACE(convert(varchar(10), ems.SubscriptionStartDate, 110), '-','/')  + ' ' 
									+ CASE WHEN DATEPART(hour,ems.SubscriptionStartDate) = 0
										THEN CAST(DATEADD(hour, 12, cast(ems.SubscriptionStartDate as time)) as varchar(5))
										WHEN DATEPART(hour, ems.SubscriptionStartDate) > 12 
											THEN CAST(DATEADD(hour, -12, cast(ems.SubscriptionStartDate as time)) as varchar(5)) 
											ELSE CAST(cast(ems.SubscriptionStartDate as time)  as varchar(5)) 
										END
									 + CASE WHEN DATEPART(hour,ems.SubscriptionStartDate) >= 12 
											THEN ' PM' 
											ELSE ' AM' 
										END
	, SubscriptionEndDate		= REPLACE(convert(varchar(10), ems.SubscriptionEndDate, 110), '-','/')  + ' ' 
									+ CASE  WHEN DATEPART(hour,ems.SubscriptionEndDate) = 0
										THEN CAST(DATEADD(hour, 12, cast(ems.SubscriptionEndDate as time)) as varchar(5))
										WHEN DATEPART(hour,ems.SubscriptionEndDate) > 12 
											THEN CAST(DATEADD(hour, -12, cast(ems.SubscriptionEndDate as time)) as varchar(5)) 
											ELSE CAST(cast(ems.SubscriptionEndDate as time)  as varchar(5)) 
										END
									 + CASE WHEN DATEPART(hour,ems.SubscriptionEndDate) >= 12 
											THEN ' PM' 
											ELSE ' AM' 
										END
	,[ExportDate]	 			= REPLACE(convert(varchar(10), GetDate(), 110), '-','/')  + ' ' 
									+ CASE WHEN DATEPART(hour,GetDate()) = 0
										THEN CAST(DATEADD(hour, 12, cast(GetDate() as time)) as varchar(5))
										WHEN DATEPART(hour,GetDate()) > 12 
											THEN CAST(DATEADD(hour, -12, cast(GetDate() as time)) as varchar(5)) 
											ELSE CAST(cast(GetDate() as time)  as varchar(5)) 
										END
									 + CASE WHEN DATEPART(hour,GetDate()) >= 12 
											THEN ' PM' 
											ELSE ' AM' 
										END
	, [SubscriptionStartDttm]	= ems.SubscriptionStartDate
	, [SubscriptionEndDttm]		= ems.SubscriptionEndDate
	, [LastModifiedDttm]		= ems.LastModifiedDttm
	, [CreatedDttm]				= ems.CreatedDttm
	FROM 
	(
		SELECT DISTINCT
		  MemberGUID = em.MemberGUID
		, SubscriptionId	= s.SubscriptionName + CASE WHEN em.SubscriptionTrialAccessId <> 0 THEN ' Trial' ELSE '' END
		, ProductId		= p.ProductName
		, em.SubscriptionStartDate
		, em.SubscriptionEndDate
		, [LastModifiedDttm]		= COALESCE(em.ModifiedDttm, em.CreatedDttm)
		, em.CreatedDttm
		FROM SiteAccess.dbo.MemberSubscription em
		JOIN SiteAccess.dbo.Subscription s
			ON s.SubscriptionId = em.SubscriptionId
		JOIN SiteAccess.dbo.SubscriptionProduct sp
			ON sp.SubscriptionId = s.SubscriptionId
		JOIN SiteAccess.dbo.Product p
			ON p.ProductId = sp.ProductId
	) ems
	JOIN MemberCentralProd.[dbo].[vwMemberExport] mv
	ON mv.SourceMemberGUID = ems.MemberGUId
	WHERE 1=1

