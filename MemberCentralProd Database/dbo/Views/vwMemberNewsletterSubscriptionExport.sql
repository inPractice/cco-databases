﻿






CREATE VIEW [dbo].[vwMemberNewsletterSubscriptionExport] AS
	SELECT 
	  OverWriteInd		= 0
	, MemberGUID 		= MemberID
	, EmailAddress
	, NewsLetterName	= mns.NewsLetterName + '_' + mns.SiteCode
	, ActiveInd
	,[RequestedDate]	 		= REPLACE(convert(varchar(10), RequestedDate, 110), '-','/')  + ' ' 
									+ CASE WHEN DATEPART(hour,RequestedDate) = 0
										THEN CAST(DATEADD(hour, 12, cast(RequestedDate as time)) as varchar(5))
										WHEN DATEPART(hour,RequestedDate) > 12 
											THEN CAST(DATEADD(hour, -12, cast(RequestedDate as time)) as varchar(5)) 
											ELSE CAST(cast(RequestedDate as time)  as varchar(5)) 
										END
									 + CASE WHEN DATEPART(hour,RequestedDate) >= 12 
											THEN ' PM' 
											ELSE ' AM' 
										END
	,[CancelledDate]	 		= CASE WHEN CancelledDate = '' THEN ''
									ELSE 
										REPLACE(convert(varchar(10), CancelledDate, 110), '-','/')  + ' ' 
										+ CASE WHEN DATEPART(hour,CancelledDate) = 0
											THEN CAST(DATEADD(hour, 12, cast(CancelledDate as time)) as varchar(5))
											WHEN DATEPART(hour,CancelledDate) > 12 
												THEN CAST(DATEADD(hour, -12, cast(CancelledDate as time)) as varchar(5)) 
												ELSE CAST(cast(CancelledDate as time)  as varchar(5)) 
											END
										 + CASE WHEN DATEPART(hour,CancelledDate) >= 12 
												THEN ' PM' 
												ELSE ' AM' 
											END
									END
	,[ExportDate]	 			= REPLACE(convert(varchar(10), GetDate(), 110), '-','/')  + ' ' 
									+ CASE WHEN DATEPART(hour,GetDate()) = 0
										THEN CAST(DATEADD(hour, 12, cast(GetDate() as time)) as varchar(5))
										WHEN DATEPART(hour,GetDate()) > 12 
											THEN CAST(DATEADD(hour, -12, cast(GetDate() as time)) as varchar(5)) 
											ELSE CAST(cast(GetDate() as time)  as varchar(5)) 
										END
									 + CASE WHEN DATEPART(hour,GetDate()) >= 12 
											THEN ' PM' 
											ELSE ' AM' 
										END	
	, [RequestedDttm]			= [RequestedDate]
	, [CancelledDttm]			= [CancelledDate]
	FROM dbo.vwMemberNewsletterSubscription mns
	JOIN [dbo].[vwMemberExport] mv
		ON mv.[SourceMemberGUID] = mns.MemberId






