﻿










CREATE VIEW [dbo].[vwMemberExport] AS
	SELECT DISTINCT
	[OverWriteInd]			= 0
	,[SourceMemberGUID]		= vm.[MemberID]
	,[PrimaryEmailAddress]	= [EmailAddress]
	,[SecondaryEmailAddress]	= ''
	,[UserName]
	,[Salutation]
	,[FirstName]
	,[MiddleName]
	,[LastName]
	,[Suffix]
	,[Affiliation]
	,[InstitutionName]		= CASE 
								WHEN vm.MemberType IN ('Internal') THEN 'CCO / inPractice' 
								WHEN vm.MemberType IN ('Vendor') THEN 'CCO Vendor'
								WHEN vm.MemberType IN ('Test-Account') THEN 'CCO Test Account'
								ELSE vm.[InstitutionName]
							END
	,[DegreeName]
	,[DegreeTypeName]
	,[SpecialtyName]
	,[ProfessionName]
	,[ProfessionTypeName]
	,[City]
	,[StateName]
	,[ZipCode]
	,[CountryCode]
	,[CountryName]
	,[IndustryInd]
	,[DeceasedInd]
	,[MailActiveInd]			= CASE WHEN [ActiveInd] = 0 OR [MailActiveInd] = 0 OR [DeceasedInd] = 1 THEN 0 ELSE [MailActiveInd] END
	,[EmailActiveInd]			= CASE WHEN [ActiveInd] = 0 OR [EmailActiveInd] = 0  OR [DeceasedInd] = 1 THEN 0 ELSE EmailActiveInd END
	,[EmailFrequencyName]
	,[DayOfWeekPreference]
	,[WhereHear]
	,[JoinedMonth]	 		= REPLACE(convert(varchar(10), [JoinedMonth], 110), '-','/')  + ' ' 
								+ CASE WHEN DATEPART(hour,[JoinedMonth]) = 0
									THEN CAST(DATEADD(hour, 12, cast([JoinedMonth] as time)) as varchar(5))
									WHEN DATEPART(hour,[JoinedMonth]) > 12 
										THEN CAST(DATEADD(hour, -12, cast([JoinedMonth] as time)) as varchar(5)) 
										ELSE CAST(cast([JoinedMonth] as time)  as varchar(5)) 
									END
									+ CASE WHEN DATEPART(hour,[JoinedMonth]) >= 12 
										THEN ' PM' 
										ELSE ' AM' 
									END
	,[H1BCountryName]
	,[NABPePIDNumber]
	,[NPIRegistryNumber]
	,[FPDMemberNumber]
	,[NursingIDNumber]
	,[SourceName]
	,[LastModified]	 		= REPLACE(convert(varchar(10), lstUpdt.[LastModified], 110), '-','/')  + ' ' 
								+ CASE WHEN DATEPART(hour,lstUpdt.[LastModified]) = 0
									THEN CAST(DATEADD(hour, 12, cast(lstUpdt.[LastModified] as time)) as varchar(5))
									WHEN DATEPART(hour,lstUpdt.[LastModified]) > 12 
										THEN CAST(DATEADD(hour, -12, cast(lstUpdt.[LastModified] as time)) as varchar(5)) 
										ELSE CAST(cast(lstUpdt.[LastModified] as time)  as varchar(5)) 
									END
									+ CASE WHEN DATEPART(hour,lstUpdt.[LastModified]) >= 12 
										THEN ' PM' 
										ELSE ' AM' 
									END
	,[ExportDate]	 		= REPLACE(convert(varchar(10), GetDate(), 110), '-','/')  + ' ' 
								+ CASE WHEN DATEPART(hour,GetDate()) = 0
									THEN CAST(DATEADD(hour, 12, cast(GetDate() as time)) as varchar(5))
									WHEN DATEPART(hour,GetDate()) > 12 
										THEN CAST(DATEADD(hour, -12, cast(GetDate() as time)) as varchar(5)) 
										ELSE CAST(cast(GetDate() as time)  as varchar(5)) 
									END
									+ CASE WHEN DATEPART(hour,GetDate()) >= 12 
										THEN ' PM' 
										ELSE ' AM' 
									END	
	, [LastModifiedDttm]	= lstUpdt.[LastModified]
	FROM [MemberCentralProd].[dbo].[vwMember] vm
	JOIN
	(
	SELECT DISTINCT
	 LastModified = COALESCE(y.Updated, y.Created)
	, y.Created
	, y.Updated
	, y.MemberID
	FROM
	(
			SELECT Created = MAX(x.Created)
			, Updated = MAX(x.Updated)
			, x.MemberId
			FROM
			(
				SELECT MemberId
				, Created = MAX(Created)
				, Updated = MAX(Updated)
				FROM dbo.Member m
				WHERE COALESCE(m.Updated, m.Created) > DATEADD(day, -730, CONVERT(date, GETDATE()))
				GROUP BY m.MemberId
				UNION ALL
				SELECT MemberId
				, Created = MAX(Created)
				, Updated = MAX(Updated)
				FROM dbo.EmailMember m
				WHERE COALESCE(m.Updated, m.Created) > DATEADD(day, -730, CONVERT(date, GETDATE()))
				GROUP BY m.MemberId
				UNION ALL
				SELECT MemberId
				, Created = MAX(Created)
				, Updated = MAX(Updated)
				FROM dbo.MailMember m
				WHERE COALESCE(m.Updated, m.Created) > DATEADD(day, -730, CONVERT(date, GETDATE()))
				GROUP BY m.MemberId
				UNION ALL
				SELECT MemberId
				, Created = MAX(Created)
				, Updated = MAX(Updated)
				FROM dbo.SiteMember m
				WHERE COALESCE(m.Updated, m.Created) > DATEADD(day, -730, CONVERT(date, GETDATE()))
				GROUP BY m.MemberId
			) x
			GROUP BY x.MemberId
		) y
	) lstUpdt
		ON lstUpdt.MemberID = vm.MemberId		
	WHERE NOT (vm.MemberType IN ('Test-Account'))
	AND vm.[EmailAddress] IS NOT NULL









