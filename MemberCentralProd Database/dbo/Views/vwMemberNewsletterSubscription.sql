﻿CREATE VIEW [dbo].[vwMemberNewsletterSubscription] AS
	SELECT
	  eml.MemberID
	, EmailAddress		= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(LTRIM(em.Email)), '<', ''), '>', ''), ';', ''), '?', ''), '[', ''), ']', ''), '..',''), '`','')
	, SiteCode			= REPLACE(ent.EmailNewsletterTypeName, 'INP', 'IPR')
	, NewsLetterName	= nl.[Text]
	, ActiveInd			= CASE WHEN nl.IsActive = 0 THEN 0 ELSE eml.IsActive END
	, RequestedDate		= convert(varchar(10), eml.Created , 110)  + ' ' + CAST(cast(eml.Created  as time) as varchar(5)) + CASE WHEN DATEPART(hour,eml.Created ) >= 12 THEN ' PM' ELSE ' AM' END 
	, CancelledDate		= CASE 
							WHEN eml.IsActive = 0 
								THEN convert(varchar(10), eml.Updated , 110)  + ' ' + CAST(cast(eml.Updated  as time) as varchar(5)) + CASE WHEN DATEPART(hour,eml.Updated ) >= 12 THEN ' PM' ELSE ' AM' END
							WHEN nl.IsActive = 0 
								THEN convert(varchar(10), ISNULL(nl.Updated, '3000-12-31') , 110)  + ' ' + CAST(cast(ISNULL(nl.Updated, '3000-12-31 00:00:00')  as time) as varchar(5)) + CASE WHEN DATEPART(hour,ISNULL(nl.Updated, '3000-12-31 00:00:00') ) >= 12 THEN ' PM' ELSE ' AM' END
							ELSE '' 
							END
	FROM dbo.EmailMemberNewsletter eml (nolock)
	JOIN dbo.EmailNewsletter nl (nolock)
		ON nl.EmailNewsletterID = eml.EmailNewsletterID
	JOIN dbo.EmailNewsletterType ent (nolock)
		ON ent.EmailNewsletterTypeId = nl.EmailNewsletterTypeId
	JOIN dbo.EmailMember em (nolock)
		ON em.MemberID = eml.MemberID
		AND CHARINDEX('[', em.Email) = 0
		AND CHARINDEX(']', em.Email) = 0	
		AND NOT(nl.[Text] IN ('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'))
	WHERE 1=1
	-- AND eml.Created <> ISNULL(eml.Updated, '3000-12-31 00:00:00')






