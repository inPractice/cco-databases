﻿
CREATE VIEW [dbo].[vwMedicalBoardCertification] AS
    SELECT	  
	 mb.[MedicalBoardName]
	, [MedicalCertificateName] = ISNULL(mc.[MedicalCertificateName], '')
	, [MedicalCertificateSubspecialtyName] = ISNULL(mcs.[MedicalCertificateSubspecialtyName], '')
	, mb.MedicalBoardId
	, MedicalCertificateId = ISNULL(mc.MedicalCertificateId, 0)
	, MedicalCertificateSubspecialtyId = ISNULL(mcs.MedicalCertificateSubspecialtyId, 0)
	, r.[MedicalBoardCertificationId]
	, SortOrder = ROW_NUMBER() OVER (PARTITION BY mb.[MedicalBoardName]  ORDER BY mb.[MedicalBoardName],mc.[MedicalCertificateName],mcs.[MedicalCertificateSubspecialtyName])
  FROM [MemberCentralProd].[dbo].[MedicalBoardCertification] r
  JOIN [MemberCentralProd].dbo.MedicalBoard mb
	ON mb.MedicalBoardId = r.MedicalBoardId
	AND mb.ActiveInd = 1
	AND mb.DeletedInd = 0
LEFT JOIN [MemberCentralProd].dbo.MedicalCertificate mc
	ON mc.MedicalCertificateId = r.MedicalCertificateId
	AND mc.ActiveInd = 1
	AND mc.DeletedInd = 0
LEFT JOIN [MemberCentralProd].dbo.MedicalCertificateSubspecialty mcs
	ON mcs.MedicalCertificateSubspecialtyId = r.MedicalCertificateSubspecialtyId
	AND mcs.ActiveInd = 1
	AND mcs.DeletedInd = 0
WHERE 1=1
	AND r.ActiveInd = 1 
	AND r.DeletedInd = 0
