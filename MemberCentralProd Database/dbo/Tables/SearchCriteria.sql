﻿CREATE TABLE [dbo].[SearchCriteria] (
    [SearchCriteriaID] UNIQUEIDENTIFIER NOT NULL,
    [SessionID]        UNIQUEIDENTIFIER NOT NULL,
    [SearchTerm]       VARCHAR (255)    NOT NULL,
    [Parameters]       VARCHAR (255)    NULL,
    [StartIndex]       INT              NULL,
    [Collection]       VARCHAR (255)    NULL,
    [Source]           VARCHAR (50)     NULL,
    [Created]          DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__SearchCriteria__7F2BE32F] PRIMARY KEY CLUSTERED ([SearchCriteriaID] ASC),
    CONSTRAINT [FK__SearchCri__Sessi__2739D489] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID]),
    CONSTRAINT [UQ__SearchCriteria__00200768] UNIQUE NONCLUSTERED ([SearchCriteriaID] ASC)
);

