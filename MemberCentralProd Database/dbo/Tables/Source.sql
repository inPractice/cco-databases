﻿CREATE TABLE [dbo].[Source] (
    [SourceID] INT          IDENTITY (2000, 1) NOT FOR REPLICATION NOT NULL,
    [Text]     VARCHAR (50) NOT NULL,
    [Sequence] INT          DEFAULT (10) NOT NULL,
    [Created]  DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]  DATETIME     NULL,
    CONSTRAINT [PK__Source__5070F446] PRIMARY KEY CLUSTERED ([SourceID] ASC),
    CONSTRAINT [UQ__Source__5165187F] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__Source__52593CB8] UNIQUE NONCLUSTERED ([SourceID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_Source
ON Source
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT SourceID FROM Inserted)

UPDATE Source
SET Updated = GETDATE()
WHERE SourceID = @ID

