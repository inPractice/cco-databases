﻿CREATE TABLE [dbo].[GlobalAlternateId] (
    [Id]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [LocalId]  UNIQUEIDENTIFIER NOT NULL,
    [RemoteId] VARCHAR (255)    NOT NULL,
    [Source]   VARCHAR (255)    NOT NULL,
    [IdType]   VARCHAR (20)     NOT NULL,
    [Created]  DATETIME         CONSTRAINT [DF_GlobalAternateID_Created] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_GlobalAlternateId] PRIMARY KEY CLUSTERED ([Id] ASC)
);

