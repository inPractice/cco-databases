﻿CREATE TABLE [dbo].[SurveyType] (
    [SurveyTypeId] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Text]         VARCHAR (20)  NOT NULL,
    [Created]      DATETIME2 (7) CONSTRAINT [DF_SurveyType_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME2 (7) NULL,
    CONSTRAINT [PK_SurveyType] PRIMARY KEY CLUSTERED ([SurveyTypeId] ASC)
);

