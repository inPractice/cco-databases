﻿CREATE TABLE [dbo].[ToolPageView] (
    [ToolPageViewId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ToolSessionId]  INT                NOT NULL,
    [PageUID]        UNIQUEIDENTIFIER   NOT NULL,
    [PageName]       NVARCHAR (256)     NOT NULL,
    [ViewFormat]     NVARCHAR (50)      NOT NULL,
    [CreatedDate]    DATETIMEOFFSET (7) NOT NULL,
    [InsertedDate]   DATETIMEOFFSET (7) CONSTRAINT [DF_ToolPageView_InsertedDate] DEFAULT (sysdatetimeoffset()) NOT NULL,
    CONSTRAINT [PK_ToolPageView] PRIMARY KEY CLUSTERED ([ToolPageViewId] ASC),
    CONSTRAINT [FK_ToolPageView_ToolSession] FOREIGN KEY ([ToolSessionId]) REFERENCES [dbo].[ToolSession] ([ToolSessionID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Surrogate key for the ToolPageView table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageView', @level2type = N'COLUMN', @level2name = N'ToolPageViewId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The session in which the view occurred', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageView', @level2type = N'COLUMN', @level2name = N'ToolSessionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The GUID of the page viewed', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageView', @level2type = N'COLUMN', @level2name = N'PageUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name or label for the page', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageView', @level2type = N'COLUMN', @level2name = N'PageName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The format of the view: screen, dialog, etc.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageView', @level2type = N'COLUMN', @level2name = N'ViewFormat';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the page was viewed on the client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageView', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the record was added to the database', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageView', @level2type = N'COLUMN', @level2name = N'InsertedDate';

