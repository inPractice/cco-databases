﻿CREATE TABLE [dbo].[MemberProductAccess] (
    [ID]         INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [EntityId]   VARCHAR (50) NOT NULL,
    [ProductId]  VARCHAR (50) NOT NULL,
    [StartDate]  DATETIME     NULL,
    [EndDate]    DATETIME     NULL,
    [Permission] VARCHAR (8)  NOT NULL,
    [EntityType] VARCHAR (50) NULL,
    CONSTRAINT [PK_MemberProductAccess] PRIMARY KEY CLUSTERED ([ID] ASC)
);

