﻿CREATE TABLE [dbo].[ExternalCaseLink] (
    [SessionID]          UNIQUEIDENTIFIER NOT NULL,
    [QuestionAnswerID]   UNIQUEIDENTIFIER NOT NULL,
    [Created]            DATETIME         DEFAULT (getdate()) NOT NULL,
    [ExternalCaseLinkID] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    CONSTRAINT [PK_ExternalCaseLink] PRIMARY KEY NONCLUSTERED ([ExternalCaseLinkID] ASC)
);


GO
CREATE CLUSTERED INDEX [ExternalCaseLink_Created]
    ON [dbo].[ExternalCaseLink]([Created] ASC);

