﻿CREATE TABLE [dbo].[TestBasedInteraction] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [CmeInteractionId]          UNIQUEIDENTIFIER NOT NULL,
    [ActivityId]                UNIQUEIDENTIFIER NOT NULL,
    [TestQuestionnaireId]       UNIQUEIDENTIFIER NOT NULL,
    [EvaluationQuestionnaireId] UNIQUEIDENTIFIER NULL,
    [PassedTest]                BIT              NOT NULL,
    [DeliveryMethod]            VARCHAR (20)     NULL,
    [Created]                   DATETIME         NOT NULL,
    [Updated]                   DATETIME         NULL,
    CONSTRAINT [PK_CmeActivity] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CmeActivity_Questionnaire] FOREIGN KEY ([TestQuestionnaireId]) REFERENCES [dbo].[Questionnaire] ([Id]),
    CONSTRAINT [FK_CmeActivity_Questionnaire1] FOREIGN KEY ([EvaluationQuestionnaireId]) REFERENCES [dbo].[Questionnaire] ([Id]),
    CONSTRAINT [FK_TestBasedInteraction_CmeInteraction] FOREIGN KEY ([CmeInteractionId]) REFERENCES [dbo].[CmeInteraction] ([Id])
);

