﻿CREATE TABLE [dbo].[ImportFile] (
    [ImportFileID]     UNIQUEIDENTIFIER NOT NULL,
    [MemberID]         UNIQUEIDENTIFIER NOT NULL,
    [FileName]         VARCHAR (255)    NOT NULL,
    [Description]      VARCHAR (255)    NOT NULL,
    [EventID]          UNIQUEIDENTIFIER NULL,
    [Password]         NVARCHAR (50)    NULL,
    [WhereHearTypeID]  INT              NULL,
    [IgnoreDuplicates] BIT              DEFAULT (0) NOT NULL,
    [Created]          DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]          DATETIME         NULL,
    CONSTRAINT [PK__ImportFile__4D9F7493] PRIMARY KEY NONCLUSTERED ([ImportFileID] ASC),
    CONSTRAINT [FK__ImportFil__Event__4F87BD05] FOREIGN KEY ([EventID]) REFERENCES [dbo].[datCom_CampaignEvent] ([EventID]),
    CONSTRAINT [FK__ImportFil__Membe__4E9398CC] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID])
);


GO
CREATE CLUSTERED INDEX [Created]
    ON [dbo].[ImportFile]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [MemberID]
    ON [dbo].[ImportFile]([MemberID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [FileName]
    ON [dbo].[ImportFile]([FileName] ASC);


GO
CREATE NONCLUSTERED INDEX [Updated]
    ON [dbo].[ImportFile]([Updated] ASC);

