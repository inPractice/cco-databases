﻿CREATE TABLE [dbo].[MemberLogin] (
    [MemberLoginId]  BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberId]       BIGINT           NOT NULL,
    [MemberGUId]     UNIQUEIDENTIFIER NOT NULL,
    [SourceId]       INT              CONSTRAINT [DF_MemberLogin_SourceId] DEFAULT ((0)) NOT NULL,
    [FirstLoginDttm] DATETIME         NULL,
    [LastLoginDttm]  DATETIME         NULL,
    [VisitCount]     INT              NULL,
    [CreatedDttm]    DATETIME         CONSTRAINT [DF_MemberLogin_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]   DATETIME         NULL,
    [DeletedInd]     BIT              CONSTRAINT [DF_MemberLogin_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]    DATETIME         NULL,
    CONSTRAINT [PK_MemberLogin] PRIMARY KEY CLUSTERED ([MemberLoginId] ASC),
    CONSTRAINT [FK_MemberLogin_Member] FOREIGN KEY ([MemberGUId]) REFERENCES [dbo].[Member] ([MemberID]),
    CONSTRAINT [FK_MemberLogin_Source] FOREIGN KEY ([SourceId]) REFERENCES [dbo].[Source] ([SourceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MemberLogin]
    ON [dbo].[MemberLogin]([MemberGUId] ASC, [SourceId] ASC) WITH (IGNORE_DUP_KEY = ON);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies MemberLogin', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MemberLogin', @level2type = N'COLUMN', @level2name = N'MemberLoginId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Member referring source (CCO, IPR, IPA, Reglite, Resident and Fellows, etc...)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MemberLogin', @level2type = N'COLUMN', @level2name = N'SourceId';

