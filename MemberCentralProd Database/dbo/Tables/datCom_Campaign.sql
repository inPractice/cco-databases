﻿CREATE TABLE [dbo].[datCom_Campaign] (
    [CampaignID]    UNIQUEIDENTIFIER CONSTRAINT [DF_datCom_Campaign_campaign_gid] DEFAULT (newid()) NOT NULL,
    [created]       DATETIME         CONSTRAINT [DF_datCom_Campaign_created] DEFAULT (getdate()) NOT NULL,
    [campaign_code] VARCHAR (50)     NOT NULL,
    [name]          VARCHAR (50)     NOT NULL,
    [description]   VARCHAR (128)    NULL,
    [speciality]    VARCHAR (50)     NOT NULL,
    CONSTRAINT [PK_datCom_Campaign] PRIMARY KEY NONCLUSTERED ([CampaignID] ASC),
    CONSTRAINT [CK__datCom_Ca__speci__0D2FE9C3] CHECK ([Speciality] = 'none' or ([Speciality] = 'Prostate Cancer' or ([Speciality] = 'Oncology' or ([Speciality] = 'Hematology' or ([Speciality] = 'Hepatitis' or [Speciality] = 'HIV')))))
);


GO
CREATE NONCLUSTERED INDEX [created]
    ON [dbo].[datCom_Campaign]([created] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [campaign_code]
    ON [dbo].[datCom_Campaign]([campaign_code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [name]
    ON [dbo].[datCom_Campaign]([name] ASC);


GO
CREATE NONCLUSTERED INDEX [speciality]
    ON [dbo].[datCom_Campaign]([speciality] ASC);

