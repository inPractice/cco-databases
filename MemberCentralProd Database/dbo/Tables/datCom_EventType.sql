﻿CREATE TABLE [dbo].[datCom_EventType] (
    [EventTypeID]   UNIQUEIDENTIFIER CONSTRAINT [DF_datOds_EventType_EventTypeID] DEFAULT (newid()) NOT NULL,
    [EventType]     VARCHAR (50)     NOT NULL,
    [EventTypeDesc] VARCHAR (128)    NULL,
    [create_date]   DATETIME         CONSTRAINT [DF_datCom_EventType_create_date] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_datCom_EventType] PRIMARY KEY NONCLUSTERED ([EventTypeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [EventType]
    ON [dbo].[datCom_EventType]([EventType] ASC);


GO
CREATE NONCLUSTERED INDEX [create_date]
    ON [dbo].[datCom_EventType]([create_date] ASC);

