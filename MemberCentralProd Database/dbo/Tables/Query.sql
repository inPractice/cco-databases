﻿CREATE TABLE [dbo].[Query] (
    [QueryID]    UNIQUEIDENTIFIER NOT NULL,
    [MemberID]   UNIQUEIDENTIFIER NOT NULL,
    [CategoryID] SMALLINT         NOT NULL,
    [QueryName]  VARCHAR (255)    NOT NULL,
    [SqlText]    NTEXT            NOT NULL,
    [ZipCode]    CHAR (5)         NULL,
    [Radius]     SMALLINT         NULL,
    [Created]    DATETIME         NOT NULL,
    [Updated]    DATETIME         NULL,
    CONSTRAINT [PK__Query__41648637] PRIMARY KEY NONCLUSTERED ([QueryID] ASC),
    CONSTRAINT [FK__Query__CategoryI__434CCEA9] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[QueryCategory] ([CategoryID]),
    CONSTRAINT [FK__Query__MemberID__4258AA70] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID])
);


GO
CREATE NONCLUSTERED INDEX [MemberID]
    ON [dbo].[Query]([MemberID] ASC);


GO
CREATE NONCLUSTERED INDEX [CategoryID]
    ON [dbo].[Query]([CategoryID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [QueryName]
    ON [dbo].[Query]([QueryName] ASC);


GO
CREATE NONCLUSTERED INDEX [Created]
    ON [dbo].[Query]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [Updated]
    ON [dbo].[Query]([Updated] ASC);

