﻿CREATE TABLE [dbo].[Cluster] (
    [ClusterID]   UNIQUEIDENTIFIER NOT NULL,
    [ClusterName] VARCHAR (255)    NOT NULL,
    [ClusterType] VARCHAR (50)     NOT NULL,
    [Specialty]   VARCHAR (50)     NOT NULL,
    [Created]     DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]     DATETIME         NULL,
    [Site]        VARCHAR (25)     NULL,
    CONSTRAINT [PK_Cluster] PRIMARY KEY CLUSTERED ([ClusterID] ASC)
);

