﻿CREATE TABLE [dbo].[datCom_CampaignEvent] (
    [EventID]          UNIQUEIDENTIFIER CONSTRAINT [DF_datCom_CampaignEvent_EventID] DEFAULT (newid()) NOT NULL,
    [CampaignID]       UNIQUEIDENTIFIER NOT NULL,
    [event_id]         INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [EventCode]        VARCHAR (50)     NOT NULL,
    [EventType]        VARCHAR (50)     NULL,
    [EventName]        VARCHAR (1024)   NOT NULL,
    [EventDescription] VARCHAR (8000)   NULL,
    [EventDate]        DATETIME         CONSTRAINT [DF_datCom_Events_EventDate] DEFAULT (getdate()) NOT NULL,
    [EventTypeID]      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_datCom_CampaignEvent] PRIMARY KEY NONCLUSTERED ([EventID] ASC),
    CONSTRAINT [FK_datCom_CampaignEvent_CampaignID] FOREIGN KEY ([CampaignID]) REFERENCES [dbo].[datCom_Campaign] ([CampaignID]),
    CONSTRAINT [FK_datCom_CampaignEvent_EventTypeID] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[datCom_EventType] ([EventTypeID])
);


GO
CREATE NONCLUSTERED INDEX [CampaignID]
    ON [dbo].[datCom_CampaignEvent]([CampaignID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [EventCode]
    ON [dbo].[datCom_CampaignEvent]([EventCode] ASC);


GO
CREATE NONCLUSTERED INDEX [EventDate]
    ON [dbo].[datCom_CampaignEvent]([EventDate] ASC);


GO
CREATE NONCLUSTERED INDEX [EventTypeID]
    ON [dbo].[datCom_CampaignEvent]([EventTypeID] ASC);

