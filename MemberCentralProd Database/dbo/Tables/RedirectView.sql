﻿CREATE TABLE [dbo].[RedirectView] (
    [RedirectViewID] UNIQUEIDENTIFIER NOT NULL,
    [SessionID]      UNIQUEIDENTIFIER NOT NULL,
    [Redirect]       VARCHAR (255)    NOT NULL,
    [DestinationURL] VARCHAR (1024)   NOT NULL,
    [Created]        DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__RedirectView__787EE5A0] PRIMARY KEY NONCLUSTERED ([RedirectViewID] ASC),
    CONSTRAINT [FK__RedirectV__Sessi__25518C17] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID]),
    CONSTRAINT [UQ__RedirectView__797309D9] UNIQUE NONCLUSTERED ([RedirectViewID] ASC)
);


GO
CREATE CLUSTERED INDEX [RedirectView_Created]
    ON [dbo].[RedirectView]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [idx_Redirect]
    ON [dbo].[RedirectView]([Redirect] ASC);


GO
CREATE NONCLUSTERED INDEX [RedirectView_SessionID]
    ON [dbo].[RedirectView]([SessionID] ASC);

