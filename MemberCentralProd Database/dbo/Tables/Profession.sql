﻿CREATE TABLE [dbo].[Profession] (
    [ProfessionID]     INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Text]             VARCHAR (100) NOT NULL,
    [Sequence]         INT           NOT NULL,
    [ProfessionTypeID] INT           NULL,
    [Created]          DATETIME      DEFAULT (getdate()) NOT NULL,
    [Updated]          DATETIME      NULL,
    CONSTRAINT [PK__Profession__1920BF5C] PRIMARY KEY CLUSTERED ([ProfessionID] ASC),
    CONSTRAINT [FK__Professio__Profe__17F790F9] FOREIGN KEY ([ProfessionTypeID]) REFERENCES [dbo].[ProfessionType] ([ProfessionTypeID]),
    CONSTRAINT [UQ__Profession__1A14E395] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__Profession__1B0907CE] UNIQUE NONCLUSTERED ([ProfessionID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_Profession
ON Profession
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT ProfessionID FROM Inserted)

UPDATE Profession
SET Updated = GETDATE()
WHERE ProfessionID = @ID

