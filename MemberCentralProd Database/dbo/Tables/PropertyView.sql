﻿CREATE TABLE [dbo].[PropertyView] (
    [PropertyViewID]    UNIQUEIDENTIFIER NOT NULL,
    [SessionID]         UNIQUEIDENTIFIER NOT NULL,
    [TrackedPropertyID] UNIQUEIDENTIFIER NOT NULL,
    [ViewFormat]        VARCHAR (25)     NOT NULL,
    [Referrer]          VARCHAR (1024)   NULL,
    [Created]           DATETIME         CONSTRAINT [DF__PropertyV__Creat__71D1E811] DEFAULT (getdate()) NOT NULL,
    [ProxySpecialty]    VARCHAR (25)     NULL,
    CONSTRAINT [PK__PropertyView__6FE99F9F] PRIMARY KEY NONCLUSTERED ([PropertyViewID] ASC),
    CONSTRAINT [FK__PropertyV__Sessi__245D67DE] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID]),
    CONSTRAINT [FK_PropertyView_TrackedProperty] FOREIGN KEY ([TrackedPropertyID]) REFERENCES [dbo].[TrackedProperty] ([TrackedPropertyID]),
    CONSTRAINT [UQ__PropertyView__70DDC3D8] UNIQUE NONCLUSTERED ([PropertyViewID] ASC)
);


GO
CREATE CLUSTERED INDEX [PropertyView_Created]
    ON [dbo].[PropertyView]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [PropertyView_SessionID]
    ON [dbo].[PropertyView]([SessionID] ASC);


GO
CREATE NONCLUSTERED INDEX [PropertyView_TrackedPropertyID]
    ON [dbo].[PropertyView]([TrackedPropertyID] ASC);


GO
CREATE NONCLUSTERED INDEX [PropertyView_ViewFormat]
    ON [dbo].[PropertyView]([ViewFormat] ASC);

