﻿CREATE TABLE [dbo].[TestResultQuestionAnswer] (
    [TestResultID]     UNIQUEIDENTIFIER NOT NULL,
    [QuestionID]       UNIQUEIDENTIFIER NOT NULL,
    [QuestionAnswerID] UNIQUEIDENTIFIER NOT NULL,
    [Created]          DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]          DATETIME         NULL,
    CONSTRAINT [PK__TestResultQuesti__5629CD9C] PRIMARY KEY NONCLUSTERED ([TestResultID] ASC, [QuestionID] ASC),
    CONSTRAINT [FK__TestResul__TestR__1F98B2C1] FOREIGN KEY ([TestResultID]) REFERENCES [dbo].[TestResult] ([TestResultID])
);


GO
CREATE CLUSTERED INDEX [TestResultQuestionAnswer_Created]
    ON [dbo].[TestResultQuestionAnswer]([Created] ASC);


GO

CREATE  TRIGGER T_UPDATE_TestResultQuestionAnswer
ON TestResultQuestionAnswer
FOR UPDATE
AS

DECLARE @testResultID uniqueidentifier
DECLARE @questionID uniqueidentifier
DECLARE @questionAnswerID uniqueidentifier

SELECT @testResultID = (SELECT TestResultID FROM Inserted)SELECT @questionID = (SELECT QuestionID FROM Inserted)
SELECT @questionAnswerID = (SELECT QuestionAnswerID FROM Inserted)

UPDATE TestResultQuestionAnswer
SET Updated = GETDATE()
WHERE TestResultID = @testResultID
AND QuestionID = @questionID
AND QuestionAnswerID = @questionAnswerID



