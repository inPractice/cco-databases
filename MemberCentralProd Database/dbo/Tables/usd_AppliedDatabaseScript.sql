﻿CREATE TABLE [dbo].[usd_AppliedDatabaseScript] (
    [ScriptFile]  NVARCHAR (255) NOT NULL,
    [DateApplied] DATETIME       NOT NULL,
    [Version]     INT            NULL,
    CONSTRAINT [PK_usd_AppliedDatabaseScript] PRIMARY KEY NONCLUSTERED ([ScriptFile] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_usd_DateApplied]
    ON [dbo].[usd_AppliedDatabaseScript]([DateApplied] ASC);

