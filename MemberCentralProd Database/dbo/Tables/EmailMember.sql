﻿CREATE TABLE [dbo].[EmailMember] (
    [MemberID]         UNIQUEIDENTIFIER NOT NULL,
    [Email]            NVARCHAR (100)   NOT NULL,
    [EmailFrequencyID] INT              NULL,
    [IsActive]         BIT              DEFAULT (1) NOT NULL,
    [IsBadEmail]       BIT              CONSTRAINT [DF__EmailMemb__IsBad__3D5E1FD2] DEFAULT (0) NULL,
    [Created]          DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]          DATETIME         NULL,
    [SSO]              VARCHAR (256)    NULL,
    CONSTRAINT [PK__EmailMember__3D5E1FD2] PRIMARY KEY CLUSTERED ([MemberID] ASC),
    CONSTRAINT [FK__EmailMemb__Email__19DFD96B] FOREIGN KEY ([EmailFrequencyID]) REFERENCES [dbo].[EmailFrequency] ([EmailFrequencyID]),
    CONSTRAINT [FK__EmailMemb__Membe__08B54D69] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
    CONSTRAINT [UQ__EmailMember__3E52440B] UNIQUE NONCLUSTERED ([Email] ASC)
);


GO
CREATE  TRIGGER T_UPDATE_EmailMember
ON dbo.EmailMember
FOR UPDATE
AS



UPDATE EmailMember
SET Updated = GETDATE()
WHERE MemberID  IN (SELECT MemberID FROM Inserted)
