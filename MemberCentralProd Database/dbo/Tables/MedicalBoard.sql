﻿CREATE TABLE [dbo].[MedicalBoard] (
    [MedicalBoardId]          INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MedicalBoardName]        VARCHAR (64)     NOT NULL,
    [MedicalBoardGUID]        UNIQUEIDENTIFIER NULL,
    [MedicalBoardCode]        VARCHAR (12)     NULL,
    [MedicalBoardDescription] VARCHAR (256)    NULL,
    [ActiveInd]               BIT              CONSTRAINT [DF_MedicalBoard_ActiveInd] DEFAULT ((1)) NOT NULL,
    [CreatedDttm]             DATETIME         CONSTRAINT [DF_MedicalBoard_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]            DATETIME         NULL,
    [DeletedInd]              BIT              CONSTRAINT [DF_MedicalBoard_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]             DATETIME         NULL,
    CONSTRAINT [PK_MedicalBoard] PRIMARY KEY CLUSTERED ([MedicalBoardId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MedicalBoard_MedicalBoardName]
    ON [dbo].[MedicalBoard]([MedicalBoardName] ASC) WITH (IGNORE_DUP_KEY = ON);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies MedicalBoard', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalBoard', @level2type = N'COLUMN', @level2name = N'MedicalBoardId';

