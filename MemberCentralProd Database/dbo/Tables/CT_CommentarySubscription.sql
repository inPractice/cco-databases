﻿CREATE TABLE [dbo].[CT_CommentarySubscription] (
    [CommentarySubscriptionID] UNIQUEIDENTIFIER NOT NULL,
    [MemberID]                 UNIQUEIDENTIFIER NOT NULL,
    [ClinicalThoughtID]        UNIQUEIDENTIFIER NOT NULL,
    [SubscriptionType]         SMALLINT         NOT NULL,
    [LastEmailSent]            DATETIME         NULL,
    [IsActive]                 BIT              DEFAULT ((1)) NOT NULL,
    [Created]                  DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]                  DATETIME         NULL,
    CONSTRAINT [PK_CT_CommentarySubscription] PRIMARY KEY NONCLUSTERED ([CommentarySubscriptionID] ASC),
    CONSTRAINT [FK_CT_CommentarySubscription_SiteMember] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID]),
    CONSTRAINT [FK_CT_CommentarySubscription_TrackedProperty] FOREIGN KEY ([ClinicalThoughtID]) REFERENCES [dbo].[TrackedProperty] ([PropertyID])
);


GO
CREATE CLUSTERED INDEX [IX_CT_CommentarySubscription_Created]
    ON [dbo].[CT_CommentarySubscription]([Created] ASC);

