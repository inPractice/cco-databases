﻿CREATE TABLE [dbo].[ToolResponseGroup] (
    [ToolResponseGroupId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ToolPageViewId]      INT                NOT NULL,
    [GroupUID]            UNIQUEIDENTIFIER   NOT NULL,
    [GroupVersion]        INT                NOT NULL,
    [GroupName]           NVARCHAR (256)     NOT NULL,
    [CreatedDate]         DATETIMEOFFSET (7) NOT NULL,
    [InsertedDate]        DATETIMEOFFSET (7) CONSTRAINT [DF_ToolResponseGroup_InsertedDate] DEFAULT (sysdatetimeoffset()) NOT NULL,
    CONSTRAINT [PK_ToolResponseGroup] PRIMARY KEY CLUSTERED ([ToolResponseGroupId] ASC),
    CONSTRAINT [FK_ToolResponseGroup_ToolPageView] FOREIGN KEY ([ToolPageViewId]) REFERENCES [dbo].[ToolPageView] ([ToolPageViewId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Surrogate key for the ToolResponseGroup table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseGroup', @level2type = N'COLUMN', @level2name = N'ToolResponseGroupId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The page on which the batch appeared', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseGroup', @level2type = N'COLUMN', @level2name = N'ToolPageViewId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The GUID for the group or batch of questions', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseGroup', @level2type = N'COLUMN', @level2name = N'GroupUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The version of the group', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseGroup', @level2type = N'COLUMN', @level2name = N'GroupVersion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name or label for the group', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseGroup', @level2type = N'COLUMN', @level2name = N'GroupName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time this group was submitted from the client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseGroup', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the record was added to the database', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseGroup', @level2type = N'COLUMN', @level2name = N'InsertedDate';

