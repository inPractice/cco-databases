﻿CREATE TABLE [dbo].[ImportEmailNewsletter] (
    [ImportFileID]      UNIQUEIDENTIFIER NOT NULL,
    [EmailNewsletterID] INT              NOT NULL,
    [Created]           DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ImportEmailNewsletter] PRIMARY KEY CLUSTERED ([ImportFileID] ASC, [EmailNewsletterID] ASC),
    CONSTRAINT [FK__ImportEma__Email__09B45E9A] FOREIGN KEY ([EmailNewsletterID]) REFERENCES [dbo].[EmailNewsletter] ([EmailNewsletterID]),
    CONSTRAINT [FK__ImportEma__Impor__08C03A61] FOREIGN KEY ([ImportFileID]) REFERENCES [dbo].[ImportFile] ([ImportFileID])
);

