﻿CREATE TABLE [dbo].[CaseItem] (
    [ItemID]    UNIQUEIDENTIFIER NOT NULL,
    [IsCorrect] BIT              NULL,
    [IsPrimary] BIT              NULL,
    [Sequence]  INT              NULL,
    CONSTRAINT [PK_CaseItem] PRIMARY KEY NONCLUSTERED ([ItemID] ASC)
);

