﻿CREATE TABLE [dbo].[datOds_MemberEvent] (
    [EventID]     UNIQUEIDENTIFIER NOT NULL,
    [MemberID]    UNIQUEIDENTIFIER NOT NULL,
    [create_date] DATETIME         CONSTRAINT [DF_datOds_MemberCampaignSource_create_date] DEFAULT (getdate()) NOT NULL,
    [Token1]      NVARCHAR (255)   NULL,
    [Token2]      NVARCHAR (255)   NULL,
    [Token3]      NVARCHAR (255)   NULL,
    CONSTRAINT [PK_datOds_MemberEvent] PRIMARY KEY NONCLUSTERED ([EventID] ASC, [MemberID] ASC),
    CONSTRAINT [FK_datOds_MemberEvent_EventID] FOREIGN KEY ([EventID]) REFERENCES [dbo].[datCom_CampaignEvent] ([EventID]),
    CONSTRAINT [FK_datOds_MemberEvent_MemberID] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]) ON DELETE CASCADE
);


GO
CREATE CLUSTERED INDEX [datOds_MemberEvent_Created]
    ON [dbo].[datOds_MemberEvent]([create_date] ASC);


GO
CREATE NONCLUSTERED INDEX [create_date]
    ON [dbo].[datOds_MemberEvent]([create_date] ASC);


GO
CREATE NONCLUSTERED INDEX [datOds_MemberEvent_MemberID]
    ON [dbo].[datOds_MemberEvent]([MemberID] ASC);

