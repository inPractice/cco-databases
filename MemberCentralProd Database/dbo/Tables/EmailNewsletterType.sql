﻿CREATE TABLE [dbo].[EmailNewsletterType] (
    [EmailNewsletterTypeId]   INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [EmailNewsletterTypeName] VARCHAR (50) NOT NULL,
    [Created]                 DATETIME     NOT NULL,
    [Updated]                 DATETIME     NULL,
    CONSTRAINT [PK_EmailNewsletterType] PRIMARY KEY CLUSTERED ([EmailNewsletterTypeId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EmailNewsletterType]
    ON [dbo].[EmailNewsletterType]([EmailNewsletterTypeName] ASC) WITH (IGNORE_DUP_KEY = ON);

