﻿CREATE TABLE [dbo].[LyrisSpecialEmailList] (
    [Id]          INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberEmail] VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_LyrisSpecialEmailList] PRIMARY KEY CLUSTERED ([Id] ASC)
);

