﻿CREATE TABLE [dbo].[FeatureClick] (
    [FeatureClickID]      INT              IDENTITY (500, 1) NOT FOR REPLICATION NOT NULL,
    [SessionID]           UNIQUEIDENTIFIER NOT NULL,
    [FeatureID]           UNIQUEIDENTIFIER NOT NULL,
    [Target]              VARCHAR (1024)   NOT NULL,
    [Created]             DATETIME         DEFAULT (getdate()) NOT NULL,
    [ClickAction]         VARCHAR (50)     CONSTRAINT [DF_FeatureClick_ClickAction] DEFAULT ('Navigate') NOT NULL,
    [FeatureImpressionID] INT              CONSTRAINT [FeatureClick_FeatureImpressionId] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FeatureClick] PRIMARY KEY CLUSTERED ([FeatureClickID] ASC),
    CONSTRAINT [FK_FeatureClick_FeatureImpression] FOREIGN KEY ([FeatureImpressionID]) REFERENCES [dbo].[FeatureImpression] ([FeatureImpressionID]),
    CONSTRAINT [FK_FeatureClick_Session] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key. Identifies a unique click on a Feature.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureClick', @level2type = N'COLUMN', @level2name = N'FeatureClickID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The Session in which the click occurred.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureClick', @level2type = N'COLUMN', @level2name = N'SessionID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The Feature on which the user clicked.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureClick', @level2type = N'COLUMN', @level2name = N'FeatureID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The target URL for the click, if the ClickAction was to navigate. For other actions, this field may be blank.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureClick', @level2type = N'COLUMN', @level2name = N'Target';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date and time when the click occurred.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureClick', @level2type = N'COLUMN', @level2name = N'Created';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The action performed. This is usually "Navigate" but could be "Close" or "Register" or something else.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureClick', @level2type = N'COLUMN', @level2name = N'ClickAction';

