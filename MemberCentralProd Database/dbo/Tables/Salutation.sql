﻿CREATE TABLE [dbo].[Salutation] (
    [SalutationID] INT          IDENTITY (1000, 1) NOT FOR REPLICATION NOT NULL,
    [Text]         VARCHAR (10) NOT NULL,
    [Created]      DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME     NULL,
    CONSTRAINT [PK__Salutation__7A9C383C] PRIMARY KEY CLUSTERED ([SalutationID] ASC),
    CONSTRAINT [UQ__Salutation__7B905C75] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__Salutation__7C8480AE] UNIQUE NONCLUSTERED ([SalutationID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_Salutation
ON Salutation
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT SalutationID FROM Inserted)

UPDATE Salutation
SET Updated = GETDATE()
WHERE SalutationID = @ID

