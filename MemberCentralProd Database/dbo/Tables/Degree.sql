﻿CREATE TABLE [dbo].[Degree] (
    [DegreeID]     INT          IDENTITY (200, 1) NOT FOR REPLICATION NOT NULL,
    [Text]         VARCHAR (50) NOT NULL,
    [Sequence]     INT          NOT NULL,
    [DegreeTypeID] INT          NOT NULL,
    [Created]      DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME     NULL,
    CONSTRAINT [PK__Degree__08EA5793] PRIMARY KEY CLUSTERED ([DegreeID] ASC),
    CONSTRAINT [FK__Degree__DegreeTy__0C85DE4D] FOREIGN KEY ([DegreeTypeID]) REFERENCES [dbo].[DegreeType] ([DegreeTypeID]),
    CONSTRAINT [UQ__Degree__09DE7BCC] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__Degree__0AD2A005] UNIQUE NONCLUSTERED ([DegreeID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_Degree
ON Degree
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT DegreeID FROM Inserted)

UPDATE Degree
SET Updated = GETDATE()
WHERE DegreeID = @ID

