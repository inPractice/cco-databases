﻿CREATE TABLE [dbo].[EvaluationResult] (
    [EvaluationResultID]       UNIQUEIDENTIFIER NOT NULL,
    [TestResultID]             UNIQUEIDENTIFIER NOT NULL,
    [EvaluationID]             UNIQUEIDENTIFIER NOT NULL,
    [Created]                  DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]                  DATETIME         NULL,
    [LegacyEvaluationresultID] INT              NULL,
    CONSTRAINT [PK__EvaluationResult__5DCAEF64] PRIMARY KEY NONCLUSTERED ([EvaluationResultID] ASC),
    CONSTRAINT [FK__Evaluatio__TestR__208CD6FA] FOREIGN KEY ([TestResultID]) REFERENCES [dbo].[TestResult] ([TestResultID]),
    CONSTRAINT [UQ__EvaluationResult__5EBF139D] UNIQUE NONCLUSTERED ([TestResultID] ASC),
    CONSTRAINT [UQ__EvaluationResult__5FB337D6] UNIQUE NONCLUSTERED ([EvaluationResultID] ASC)
);


GO

CREATE  TRIGGER T_UPDATE_EvaluationResult
ON EvaluationResult
FOR UPDATE
AS

DECLARE @ID uniqueidentifier

SELECT @ID = (SELECT EvaluationResultID FROM Inserted)

UPDATE EvaluationResult
SET Updated = GETDATE()
WHERE EvaluationResultID = @ID


