﻿CREATE TABLE [dbo].[MedicalCertificate] (
    [MedicalCertificateId]          INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MedicalCertificateName]        VARCHAR (64)     NOT NULL,
    [MedicalCertificateGUID]        UNIQUEIDENTIFIER NULL,
    [MedicalCertificateCode]        VARCHAR (12)     NULL,
    [MedicalCertificateDescription] VARCHAR (256)    NULL,
    [ActiveInd]                     BIT              CONSTRAINT [DF_MedicalCertificate_ActiveInd] DEFAULT ((1)) NOT NULL,
    [CreatedDttm]                   DATETIME         CONSTRAINT [DF_MedicalCertificate_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]                  DATETIME         NULL,
    [DeletedInd]                    BIT              CONSTRAINT [DF_MedicalCertificate_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]                   DATETIME         NULL,
    CONSTRAINT [PK_MedicalCertificate] PRIMARY KEY CLUSTERED ([MedicalCertificateId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MedicalCertificate_MedicalCertificateName]
    ON [dbo].[MedicalCertificate]([MedicalCertificateName] ASC) WITH (IGNORE_DUP_KEY = ON);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies MedicalCertificate', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalCertificate', @level2type = N'COLUMN', @level2name = N'MedicalCertificateId';

