﻿CREATE TABLE [dbo].[Response] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [QuestionnaireId]  UNIQUEIDENTIFIER NOT NULL,
    [QuestionTextId]   UNIQUEIDENTIFIER NOT NULL,
    [ResponseTextId]   UNIQUEIDENTIFIER NULL,
    [FreeTextResponse] VARCHAR (1024)   NULL,
    [Created]          DATETIME         NOT NULL,
    [Updated]          DATETIME         NULL,
    CONSTRAINT [PK_Response] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Questionnaire_Response] FOREIGN KEY ([QuestionnaireId]) REFERENCES [dbo].[Questionnaire] ([Id]),
    CONSTRAINT [FK_Response_SitecoreString] FOREIGN KEY ([QuestionTextId]) REFERENCES [dbo].[SitecoreString] ([Id]),
    CONSTRAINT [FK_Response_SitecoreString1] FOREIGN KEY ([ResponseTextId]) REFERENCES [dbo].[SitecoreString] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Response_Questionnaire]
    ON [dbo].[Response]([QuestionnaireId] ASC);

