﻿CREATE TABLE [dbo].[NonCCOCMECredits] (
    [ID]             INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberID]       UNIQUEIDENTIFIER NULL,
    [ActivityName]   VARCHAR (128)    NULL,
    [Provider]       VARCHAR (128)    NULL,
    [CompletionDate] DATETIME         NULL,
    [Credits]        FLOAT (53)       NULL,
    [Notes]          TEXT             NULL,
    [Created]        DATETIME         CONSTRAINT [DF_NonCCOCMECredits_Created] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_NonCCOCMECredits] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonCCOCMECredits_CompletionDate]
    ON [dbo].[NonCCOCMECredits]([CompletionDate] ASC);

