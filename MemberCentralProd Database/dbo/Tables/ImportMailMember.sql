﻿CREATE TABLE [dbo].[ImportMailMember] (
    [ImportFileID]   UNIQUEIDENTIFIER NOT NULL,
    [ImportMemberID] UNIQUEIDENTIFIER NOT NULL,
    [Address1]       NVARCHAR (100)   NULL,
    [Address2]       NVARCHAR (100)   NULL,
    [City]           NVARCHAR (50)    NULL,
    [StateID]        INT              NULL,
    [ZipCode]        NVARCHAR (50)    NULL,
    [CountryID]      INT              NULL,
    [IsActive]       BIT              DEFAULT (1) NOT NULL,
    [IsBadAddress]   BIT              DEFAULT (0) NULL,
    [Created]        DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ImportMailMember] PRIMARY KEY CLUSTERED ([ImportFileID] ASC, [ImportMemberID] ASC),
    CONSTRAINT [FK__ImportMai__Count__7795AE5F] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID]),
    CONSTRAINT [FK__ImportMai__State__76A18A26] FOREIGN KEY ([StateID]) REFERENCES [dbo].[State] ([StateID]),
    CONSTRAINT [FK_ImportMailMember] FOREIGN KEY ([ImportFileID], [ImportMemberID]) REFERENCES [dbo].[ImportMember] ([ImportFileID], [ImportMemberID])
);

