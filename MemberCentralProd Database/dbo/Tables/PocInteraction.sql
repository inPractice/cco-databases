﻿CREATE TABLE [dbo].[PocInteraction] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CmeInteractionId] UNIQUEIDENTIFIER NULL,
    [QuestionnaireId]  UNIQUEIDENTIFIER NULL,
    [Hidden]           BIT              NULL,
    [Created]          DATETIME         NOT NULL,
    [Updated]          DATETIME         NULL,
    [SearchCriteriaId] UNIQUEIDENTIFIER NULL,
    [PocTerm]          VARCHAR (100)    NULL,
    CONSTRAINT [PK_PocActivity] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PocActivity_Questionnaire] FOREIGN KEY ([QuestionnaireId]) REFERENCES [dbo].[Questionnaire] ([Id]),
    CONSTRAINT [FK_PocInteraction_CmeInteraction] FOREIGN KEY ([CmeInteractionId]) REFERENCES [dbo].[CmeInteraction] ([Id]),
    CONSTRAINT [FK_PocInteraction_SearchCriteria] FOREIGN KEY ([SearchCriteriaId]) REFERENCES [dbo].[SearchCriteria] ([SearchCriteriaID])
);


GO
CREATE NONCLUSTERED INDEX [IX_PocInteraction_CmeIntraction]
    ON [dbo].[PocInteraction]([CmeInteractionId] ASC);

