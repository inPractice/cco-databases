﻿CREATE TABLE [dbo].[CT_Commentary] (
    [CommentaryID]      UNIQUEIDENTIFIER NOT NULL,
    [ResponseParentID]  UNIQUEIDENTIFIER NULL,
    [MemberID]          UNIQUEIDENTIFIER NOT NULL,
    [ClinicalThoughtID] UNIQUEIDENTIFIER NOT NULL,
    [Commentary]        NVARCHAR (MAX)   NOT NULL,
    [IsApproved]        BIT              DEFAULT ((0)) NOT NULL,
    [Created]           DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]           DATETIME         NULL,
    CONSTRAINT [PK_CT_Commentary] PRIMARY KEY NONCLUSTERED ([CommentaryID] ASC),
    CONSTRAINT [FK_CT_Commentary_SiteMember] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID]),
    CONSTRAINT [FK_CT_Commentary_TrackedProperty] FOREIGN KEY ([ClinicalThoughtID]) REFERENCES [dbo].[TrackedProperty] ([PropertyID])
);


GO
CREATE CLUSTERED INDEX [IX_CT_Commentary_Created]
    ON [dbo].[CT_Commentary]([Created] ASC);

