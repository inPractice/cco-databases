﻿CREATE TABLE [dbo].[ToolInteraction] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [QuestionnaireId] UNIQUEIDENTIFIER NOT NULL,
    [IsComplete]      BIT              NOT NULL,
    [Created]         DATETIME         NOT NULL,
    [Updated]         DATETIME         NULL,
    CONSTRAINT [PK_ToolInteraction] PRIMARY KEY NONCLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ToolInteraction_Questionnaire] FOREIGN KEY ([QuestionnaireId]) REFERENCES [dbo].[Questionnaire] ([Id])
);


GO
CREATE CLUSTERED INDEX [ToolInteraction_Created]
    ON [dbo].[ToolInteraction]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [ToolInteraction_QuestionnaireId]
    ON [dbo].[ToolInteraction]([QuestionnaireId] ASC);

