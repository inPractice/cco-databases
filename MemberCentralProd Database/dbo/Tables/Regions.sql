﻿CREATE TABLE [dbo].[Regions] (
    [RegionId]   INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RegionName] VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED ([RegionId] ASC)
);

