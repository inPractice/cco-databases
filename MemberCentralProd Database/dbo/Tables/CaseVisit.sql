﻿CREATE TABLE [dbo].[CaseVisit] (
    [CaseVisitID]       UNIQUEIDENTIFIER NOT NULL,
    [SessionID]         UNIQUEIDENTIFIER NOT NULL,
    [CaseID]            UNIQUEIDENTIFIER NOT NULL,
    [Created]           DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]           DATETIME         NULL,
    [LegacyCaseVisitID] INT              NULL,
    CONSTRAINT [PK__CaseVisit__656C112C] PRIMARY KEY CLUSTERED ([CaseVisitID] ASC),
    CONSTRAINT [FK__CaseVisit__Sessi__2645B050] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID]),
    CONSTRAINT [UQ__CaseVisit__66603565] UNIQUE NONCLUSTERED ([CaseVisitID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_CaseVisit
ON CaseVisit
FOR UPDATE
AS

DECLARE @ID uniqueidentifier

SELECT @ID = (SELECT CaseVisitID FROM Inserted)

UPDATE CaseVisit
SET Updated = GETDATE()
WHERE CaseVisitID = @ID
