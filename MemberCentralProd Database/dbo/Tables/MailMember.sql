﻿CREATE TABLE [dbo].[MailMember] (
    [MemberID]     UNIQUEIDENTIFIER NOT NULL,
    [Address1]     NVARCHAR (100)   NULL,
    [Address2]     NVARCHAR (100)   NULL,
    [City]         NVARCHAR (50)    NULL,
    [StateID]      INT              NULL,
    [ZipCode]      NVARCHAR (50)    NULL,
    [CountryID]    INT              NULL,
    [IsActive]     BIT              NOT NULL,
    [IsBadAddress] BIT              NOT NULL,
    [Created]      DATETIME         CONSTRAINT [DF__MailMembe__Creat__0CBAE877] DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME         NULL,
    CONSTRAINT [PK__MailMember__47DBAE45] PRIMARY KEY CLUSTERED ([MemberID] ASC),
    CONSTRAINT [FK__MailMembe__Count__10566F31] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID]),
    CONSTRAINT [FK__MailMembe__Membe__09A971A2] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
    CONSTRAINT [FK__MailMembe__State__0D7A0286] FOREIGN KEY ([StateID]) REFERENCES [dbo].[State] ([StateID])
);


GO
CREATE NONCLUSTERED INDEX [MailMember_CountryID]
    ON [dbo].[MailMember]([CountryID] ASC);


GO
CREATE NONCLUSTERED INDEX [MailMember_ZipCode]
    ON [dbo].[MailMember]([ZipCode] ASC);


GO

CREATE  TRIGGER T_UPDATE_MailMember
ON dbo.MailMember
FOR UPDATE
AS



UPDATE MailMember
SET Updated = GETDATE()
WHERE MemberID IN (SELECT MemberID FROM Inserted)

