﻿CREATE TABLE [dbo].[TestResult] (
    [TestResultID]       UNIQUEIDENTIFIER NOT NULL,
    [MemberID]           UNIQUEIDENTIFIER NOT NULL,
    [TestID]             UNIQUEIDENTIFIER NOT NULL,
    [PassedTest]         BIT              DEFAULT (0) NOT NULL,
    [GotCredit]          BIT              DEFAULT (0) NOT NULL,
    [CreditCompletion]   DATETIME         NULL,
    [CertificateCreated] DATETIME         NULL,
    [CertificateViews]   INT              DEFAULT (0) NOT NULL,
    [HoursClaimed]       FLOAT (53)       NULL,
    [DeliveryMethodID]   UNIQUEIDENTIFIER NULL,
    [Comment]            TEXT             NULL,
    [CreditTypeID]       UNIQUEIDENTIFIER NULL,
    [Created]            DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]            DATETIME         NULL,
    [LegacyTestResultID] INT              NULL,
    [CountryID]          INT              NULL,
    CONSTRAINT [PK__TestResult__59063A47] PRIMARY KEY NONCLUSTERED ([TestResultID] ASC),
    CONSTRAINT [FK__TestResul__Membe__114A936A] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID]),
    CONSTRAINT [FK_TestResult_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID]),
    CONSTRAINT [UQ__TestResult__59FA5E80] UNIQUE NONCLUSTERED ([TestResultID] ASC)
);


GO
CREATE CLUSTERED INDEX [TestResult_Created]
    ON [dbo].[TestResult]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [TestResult_MemberIDTestIDPassedTest]
    ON [dbo].[TestResult]([MemberID] ASC, [TestID] ASC, [PassedTest] ASC);


GO

CREATE TRIGGER [T_UPDATE_TestResult]
ON [dbo].[TestResult]
FOR UPDATE
AS
DECLARE @testResultID uniqueidentifier

--SELECT @testResultID = (SELECT TestResultID FROM Inserted)

UPDATE TestResult
SET Updated = GETDATE()
WHERE TestResultID in (SELECT TestResultID FROM Inserted)
