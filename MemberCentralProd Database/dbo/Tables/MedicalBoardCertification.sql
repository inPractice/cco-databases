﻿CREATE TABLE [dbo].[MedicalBoardCertification] (
    [MedicalBoardCertificationId]      BIGINT   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MedicalBoardId]                   INT      NOT NULL,
    [MedicalCertificateId]             INT      CONSTRAINT [DF_MedicalBoardCertification_MedicalCertificateId] DEFAULT ((0)) NOT NULL,
    [MedicalCertificateSubspecialtyId] INT      CONSTRAINT [DF_MedicalBoardCertification_MedicalCertificateSubspecialtyId] DEFAULT ((0)) NOT NULL,
    [SortOrder]                        INT      CONSTRAINT [DF_MedicalBoardCertification_SortOrder] DEFAULT ((1)) NOT NULL,
    [ActiveInd]                        BIT      CONSTRAINT [DF_MedicalBoardCertification_ActiveInd] DEFAULT ((1)) NOT NULL,
    [CreatedDttm]                      DATETIME CONSTRAINT [DF_MedicalBoardCertification_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]                     DATETIME NULL,
    [DeletedInd]                       BIT      CONSTRAINT [DF_MedicalBoardCertification_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]                      DATETIME NULL,
    CONSTRAINT [PK_MedicalBoardCertification] PRIMARY KEY CLUSTERED ([MedicalBoardCertificationId] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK_MedicalBoardCertification_MedicalBoard] FOREIGN KEY ([MedicalBoardId]) REFERENCES [dbo].[MedicalBoard] ([MedicalBoardId]),
    CONSTRAINT [FK_MedicalBoardCertification_MedicalCertificate] FOREIGN KEY ([MedicalCertificateId]) REFERENCES [dbo].[MedicalCertificate] ([MedicalCertificateId]),
    CONSTRAINT [FK_MedicalBoardCertification_MedicalCertificateSubspecialty] FOREIGN KEY ([MedicalCertificateSubspecialtyId]) REFERENCES [dbo].[MedicalCertificateSubspecialty] ([MedicalCertificateSubspecialtyId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MedicalBoardCertification]
    ON [dbo].[MedicalBoardCertification]([MedicalBoardId] ASC, [MedicalCertificateId] ASC, [MedicalCertificateSubspecialtyId] ASC) WITH (IGNORE_DUP_KEY = ON);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies MedicalBoardCertification', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalBoardCertification', @level2type = N'COLUMN', @level2name = N'MedicalBoardCertificationId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Medical board name record', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalBoardCertification', @level2type = N'COLUMN', @level2name = N'MedicalBoardId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Medical board general medical specialty certificate name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalBoardCertification', @level2type = N'COLUMN', @level2name = N'MedicalCertificateId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Medical board subspecialty certificate name', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalBoardCertification', @level2type = N'COLUMN', @level2name = N'MedicalCertificateSubspecialtyId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Currently offered indicator', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalBoardCertification', @level2type = N'COLUMN', @level2name = N'ActiveInd';

