﻿CREATE TABLE [dbo].[Bookmark] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [MemberId]    UNIQUEIDENTIFIER NOT NULL,
    [PropertyId]  UNIQUEIDENTIFIER NOT NULL,
    [Created]     DATETIME         NOT NULL,
    [HasRead]     BIT              NOT NULL,
    [IsCmeLayout] BIT              NULL,
    CONSTRAINT [PK_Bookmark] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Bookmark_Member] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Member] ([MemberID]),
    CONSTRAINT [FK_Bookmark_TrackedProperty] FOREIGN KEY ([PropertyId]) REFERENCES [dbo].[TrackedProperty] ([TrackedPropertyID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Bookmark_Member]
    ON [dbo].[Bookmark]([MemberId] ASC);

