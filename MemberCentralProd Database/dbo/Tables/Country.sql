﻿CREATE TABLE [dbo].[Country] (
    [CountryID]    INT          IDENTITY (300, 1) NOT FOR REPLICATION NOT NULL,
    [Abbreviation] CHAR (2)     NOT NULL,
    [Text]         VARCHAR (50) NOT NULL,
    [Sequence]     INT          DEFAULT (10) NOT NULL,
    [IsUS]         BIT          NULL,
    [Created]      DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME     NULL,
    CONSTRAINT [PK__Country__0DAF0CB0] PRIMARY KEY CLUSTERED ([CountryID] ASC),
    CONSTRAINT [UQ__Country__0EA330E9] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__Country__0F975522] UNIQUE NONCLUSTERED ([Abbreviation] ASC),
    CONSTRAINT [UQ__Country__108B795B] UNIQUE NONCLUSTERED ([CountryID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_Country
ON Country
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT CountryID FROM Inserted)

UPDATE Country
SET Updated = GETDATE()
WHERE CountryID = @ID

