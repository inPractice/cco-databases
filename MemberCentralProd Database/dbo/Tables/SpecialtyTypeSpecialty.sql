﻿CREATE TABLE [dbo].[SpecialtyTypeSpecialty] (
    [SpecialtyTypeID] INT      NOT NULL,
    [SpecialtyID]     INT      NOT NULL,
    [Created]         DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SpecialtyTypeSpecialty] PRIMARY KEY CLUSTERED ([SpecialtyTypeID] ASC, [SpecialtyID] ASC),
    CONSTRAINT [FK_SpecialtyTypeSpecialty_SpecialtyID] FOREIGN KEY ([SpecialtyID]) REFERENCES [dbo].[Specialty] ([SpecialtyID]),
    CONSTRAINT [FK_SpecialtyTypeSpecialty_SpecialtyTypeID] FOREIGN KEY ([SpecialtyTypeID]) REFERENCES [dbo].[SpecialtyType] ([SpecialtyTypeID])
);

