﻿CREATE TABLE [dbo].[EmailMemberNewsletter] (
    [MemberID]          UNIQUEIDENTIFIER NOT NULL,
    [EmailNewsletterID] INT              NOT NULL,
    [IsActive]          BIT              CONSTRAINT [DF_EmailMemberNewsletter_IsActive] DEFAULT ((1)) NOT NULL,
    [Created]           DATETIME         CONSTRAINT [DF_EmailMemberNewsletter_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]           DATETIME         NULL,
    CONSTRAINT [PK_EmailMemberNewsletter] PRIMARY KEY CLUSTERED ([MemberID] ASC, [EmailNewsletterID] ASC) WITH (IGNORE_DUP_KEY = ON),
    CONSTRAINT [FK__EmailMemb__Email__18EBB532] FOREIGN KEY ([EmailNewsletterID]) REFERENCES [dbo].[EmailNewsletter] ([EmailNewsletterID]),
    CONSTRAINT [FK__EmailMemb__Membe__1BC821DD] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[EmailMember] ([MemberID]) ON DELETE CASCADE
);

