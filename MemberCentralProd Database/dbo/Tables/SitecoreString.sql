﻿CREATE TABLE [dbo].[SitecoreString] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [SitecoreId] UNIQUEIDENTIFIER NOT NULL,
    [Text]       VARCHAR (4096)   NOT NULL,
    [Created]    DATETIME         NOT NULL,
    CONSTRAINT [PK_SitecoreString] PRIMARY KEY CLUSTERED ([Id] ASC)
);

