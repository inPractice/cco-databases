﻿CREATE TABLE [dbo].[ProfessionType] (
    [ProfessionTypeID] INT          IDENTITY (100, 1) NOT FOR REPLICATION NOT NULL,
    [Text]             VARCHAR (20) NOT NULL,
    [Created]          DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]          DATETIME     NULL,
    CONSTRAINT [PK__ProfessionType__2C3393D0] PRIMARY KEY CLUSTERED ([ProfessionTypeID] ASC),
    CONSTRAINT [UQ__ProfessionType__2D27B809] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__ProfessionType__2E1BDC42] UNIQUE NONCLUSTERED ([ProfessionTypeID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_ProfessionType
ON ProfessionType
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT ProfessionTypeID FROM Inserted)

UPDATE ProfessionType
SET Updated = GETDATE()
WHERE ProfessionTypeID = @ID

