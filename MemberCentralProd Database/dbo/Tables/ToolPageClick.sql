﻿CREATE TABLE [dbo].[ToolPageClick] (
    [ToolPageClickId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ToolPageViewId]  INT                NOT NULL,
    [ControlUID]      UNIQUEIDENTIFIER   NOT NULL,
    [ControlType]     NVARCHAR (50)      NOT NULL,
    [ControlName]     NVARCHAR (256)     NOT NULL,
    [CreatedDate]     DATETIMEOFFSET (7) NOT NULL,
    [InsertedDate]    DATETIMEOFFSET (7) CONSTRAINT [DF_ToolPageClick_InsertedDate] DEFAULT (sysdatetimeoffset()) NOT NULL,
    CONSTRAINT [PK_ToolPageClick] PRIMARY KEY CLUSTERED ([ToolPageClickId] ASC),
    CONSTRAINT [FK_ToolPageClick_ToolPageView] FOREIGN KEY ([ToolPageViewId]) REFERENCES [dbo].[ToolPageView] ([ToolPageViewId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Surrogate key for the ToolPageClick table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageClick', @level2type = N'COLUMN', @level2name = N'ToolPageClickId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The page where the click occurred', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageClick', @level2type = N'COLUMN', @level2name = N'ToolPageViewId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The GUID of the control clicked', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageClick', @level2type = N'COLUMN', @level2name = N'ControlUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The type of control: button, menu, etc.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageClick', @level2type = N'COLUMN', @level2name = N'ControlType';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name or label of the control', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageClick', @level2type = N'COLUMN', @level2name = N'ControlName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the click occured on the client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageClick', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the record was added to the database', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolPageClick', @level2type = N'COLUMN', @level2name = N'InsertedDate';

