﻿CREATE TABLE [dbo].[EulaAccept] (
    [EulaAcceptID] UNIQUEIDENTIFIER NOT NULL,
    [MemberID]     UNIQUEIDENTIFIER NOT NULL,
    [ActivityID]   UNIQUEIDENTIFIER NOT NULL,
    [Created]      DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME         NOT NULL,
    CONSTRAINT [PK_EulaAccept] PRIMARY KEY NONCLUSTERED ([EulaAcceptID] ASC),
    CONSTRAINT [FK_CT_EulaAccept_SiteMember] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID])
);

