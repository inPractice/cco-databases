﻿CREATE TABLE [dbo].[SpecialtyType] (
    [SpecialtyTypeID] INT          IDENTITY (100, 1) NOT FOR REPLICATION NOT NULL,
    [Text]            VARCHAR (20) NOT NULL,
    [Created]         DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]         DATETIME     NULL,
    CONSTRAINT [PK__SpecialtyType__276EDEB3] PRIMARY KEY CLUSTERED ([SpecialtyTypeID] ASC),
    CONSTRAINT [UQ__SpecialtyType__286302EC] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__SpecialtyType__29572725] UNIQUE NONCLUSTERED ([SpecialtyTypeID] ASC)
);


GO
CREATE TRIGGER [dbo].[T_UPDATE_SpecialtyType]
ON [dbo].[SpecialtyType]
FOR UPDATE
AS

UPDATE SpecialtyType
SET Updated = GETDATE()
WHERE SpecialtyTypeID in (SELECT SpecialtyTypeID FROM Inserted)




