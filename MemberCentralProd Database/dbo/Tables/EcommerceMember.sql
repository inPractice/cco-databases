﻿CREATE TABLE [dbo].[EcommerceMember] (
    [MemberID]            UNIQUEIDENTIFIER NOT NULL,
    [EcommerceInternalId] INT              NOT NULL,
    [Created]             DATETIME         CONSTRAINT [DF__EcommerceMember__Creat__0CBAE877] DEFAULT (getdate()) NOT NULL,
    [Email]               NVARCHAR (100)   NOT NULL,
    [Password]            NVARCHAR (172)   NOT NULL,
    [Updated]             DATETIME         NULL,
    [OtherInfo]           NVARCHAR (4000)  NOT NULL,
    CONSTRAINT [PK__EcommerceMember__47DBAE45] PRIMARY KEY CLUSTERED ([MemberID] ASC),
    CONSTRAINT [FK__EcommerceMember__Membe__09A971A2] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
);

