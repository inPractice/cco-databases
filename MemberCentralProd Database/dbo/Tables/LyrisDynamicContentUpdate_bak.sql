﻿CREATE TABLE [dbo].[LyrisDynamicContentUpdate_bak] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [Email]                  VARCHAR (250)  NOT NULL,
    [InPracticeDayOfWeek]    VARCHAR (25)   NOT NULL,
    [inPracticeEmailContent] VARCHAR (MAX)  NULL,
    [LyrisResponse]          VARCHAR (2000) NULL,
    [CodeException]          VARCHAR (MAX)  NULL,
    [Success]                BIT            NULL,
    [Created]                DATETIME       NOT NULL,
    [Updated]                DATETIME       NULL,
    [CreatedDttm]            DATETIME       NULL
);

