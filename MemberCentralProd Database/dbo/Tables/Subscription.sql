﻿CREATE TABLE [dbo].[Subscription] (
    [SubscriptionId]               INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [SubscriptionUniqueSitecoreId] VARCHAR (100)  NOT NULL,
    [SubscriptionName]             VARCHAR (500)  NOT NULL,
    [SubscriptionDescription]      VARCHAR (2000) CONSTRAINT [DF_Subscription_SubscriptionDescription] DEFAULT ('') NOT NULL,
    [SiteCoreProductId]            VARCHAR (2000) NOT NULL,
    [ProductUniqueId]              VARCHAR (2000) NOT NULL,
    [AccessType]                   VARCHAR (500)  NOT NULL,
    [AccessMessage]                VARCHAR (MAX)  CONSTRAINT [DF_Subscription_AccessMessage] DEFAULT ('') NOT NULL,
    [Cost]                         VARCHAR (500)  CONSTRAINT [DF_Subscription_Cost] DEFAULT ('') NOT NULL,
    [TermDuration ]                VARCHAR (500)  CONSTRAINT [DF_Subscription_TermDuration] DEFAULT ('') NOT NULL,
    [IsActive]                     BIT            NULL,
    [OfferExpiration]              DATETIME       NULL,
    [StartDate]                    DATETIME       NULL,
    [EndDate]                      DATETIME       NULL,
    [Created]                      DATETIME       NOT NULL,
    [Updated]                      DATETIME       NOT NULL,
    CONSTRAINT [PK_Subscription] PRIMARY KEY CLUSTERED ([SubscriptionId] ASC)
);

