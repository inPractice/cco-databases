﻿CREATE TABLE [dbo].[CT_DiscussionEmails] (
    [DiscussionEmailID]        UNIQUEIDENTIFIER NOT NULL,
    [CommentarySubscriptionID] UNIQUEIDENTIFIER NOT NULL,
    [DiscussionID]             UNIQUEIDENTIFIER NOT NULL,
    [DatePosted]               DATETIME         NOT NULL,
    [Created]                  DATETIME         NOT NULL,
    [Updated]                  DATETIME         NULL,
    CONSTRAINT [PK_CT_DiscussionEmails] PRIMARY KEY CLUSTERED ([DiscussionEmailID] ASC),
    CONSTRAINT [FK_CT_DiscussionEmails_CT_CommentarySubscription] FOREIGN KEY ([CommentarySubscriptionID]) REFERENCES [dbo].[CT_CommentarySubscription] ([CommentarySubscriptionID])
);

