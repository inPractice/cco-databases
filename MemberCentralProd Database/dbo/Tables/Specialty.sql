﻿CREATE TABLE [dbo].[Specialty] (
    [SpecialtyID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Text]        VARCHAR (100) NOT NULL,
    [Sequence]    INT           NOT NULL,
    [Created]     DATETIME      DEFAULT (getdate()) NOT NULL,
    [Updated]     DATETIME      NULL,
    CONSTRAINT [PK__Specialty__1DE57479] PRIMARY KEY CLUSTERED ([SpecialtyID] ASC),
    CONSTRAINT [UQ__Specialty__1ED998B2] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__Specialty__1FCDBCEB] UNIQUE NONCLUSTERED ([SpecialtyID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_Specialty
ON Specialty
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT SpecialtyID FROM Inserted)

UPDATE Specialty
SET Updated = GETDATE()
WHERE SpecialtyID = @ID

