﻿CREATE TABLE [dbo].[Questionnaire] (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [MemberId] UNIQUEIDENTIFIER NOT NULL,
    [SurveyId] UNIQUEIDENTIFIER NOT NULL,
    [Created]  DATETIME         NOT NULL,
    [Updated]  DATETIME         NULL,
    CONSTRAINT [PK_Questionnaire] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Questionnaire_Member] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Member] ([MemberID])
);

