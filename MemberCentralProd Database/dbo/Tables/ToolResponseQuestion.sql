﻿CREATE TABLE [dbo].[ToolResponseQuestion] (
    [ToolResponseQuestionId] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ToolResponseGroupId]    INT                NOT NULL,
    [QuestionUID]            UNIQUEIDENTIFIER   NOT NULL,
    [QuestionVersion]        INT                NOT NULL,
    [QuestionName]           NVARCHAR (256)     NOT NULL,
    [CreatedDate]            DATETIMEOFFSET (7) NOT NULL,
    [InsertedDate]           DATETIMEOFFSET (7) CONSTRAINT [DF_ToolResponseQuestion_InsertedDate] DEFAULT (sysdatetimeoffset()) NOT NULL,
    CONSTRAINT [PK_ToolResponseQuestion] PRIMARY KEY CLUSTERED ([ToolResponseQuestionId] ASC),
    CONSTRAINT [FK_ToolResponseQuestion_ToolResponseGroup] FOREIGN KEY ([ToolResponseGroupId]) REFERENCES [dbo].[ToolResponseGroup] ([ToolResponseGroupId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Surrogate key for the ToolResponseQuestion table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseQuestion', @level2type = N'COLUMN', @level2name = N'ToolResponseQuestionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The group to which this question response belongs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseQuestion', @level2type = N'COLUMN', @level2name = N'ToolResponseGroupId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The GUID for the question', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseQuestion', @level2type = N'COLUMN', @level2name = N'QuestionUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The version of the question', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseQuestion', @level2type = N'COLUMN', @level2name = N'QuestionVersion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name or label for the question', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseQuestion', @level2type = N'COLUMN', @level2name = N'QuestionName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time this question was answered on the client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseQuestion', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the record was added to the database', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseQuestion', @level2type = N'COLUMN', @level2name = N'InsertedDate';

