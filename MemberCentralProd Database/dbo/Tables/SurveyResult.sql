﻿CREATE TABLE [dbo].[SurveyResult] (
    [SurveyResultID]           UNIQUEIDENTIFIER NOT NULL,
    [MemberID]                 UNIQUEIDENTIFIER NULL,
    [SurveyID]                 UNIQUEIDENTIFIER NOT NULL,
    [IsComplete]               BIT              DEFAULT ((0)) NOT NULL,
    [Created]                  DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]                  DATETIME         NULL,
    [ShownOnTrackedPropertyId] UNIQUEIDENTIFIER NULL,
    [SurveyTypeId]             INT              CONSTRAINT [DF_SurveyResult_SurveyTypeId] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SurveyResult] PRIMARY KEY NONCLUSTERED ([SurveyResultID] ASC),
    CONSTRAINT [FK_SurveyResult_MemberID] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID]),
    CONSTRAINT [FK_SurveyResult_SurveyType] FOREIGN KEY ([SurveyTypeId]) REFERENCES [dbo].[SurveyType] ([SurveyTypeId]),
    CONSTRAINT [FK_SurveyResult_TrackedProperty] FOREIGN KEY ([ShownOnTrackedPropertyId]) REFERENCES [dbo].[TrackedProperty] ([TrackedPropertyID])
);


GO
CREATE CLUSTERED INDEX [SurveyResult_Created]
    ON [dbo].[SurveyResult]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [SurveyResult_MemberID]
    ON [dbo].[SurveyResult]([MemberID] ASC);


GO
CREATE TRIGGER [dbo].[T_UPDATE_SurveyResult]
ON dbo.SurveyResult
FOR UPDATE
AS

DECLARE @SurveyResultID uniqueidentifier

SELECT @SurveyResultID = (SELECT SurveyResultID FROM Inserted)

UPDATE SurveyResult
SET Updated = GETDATE()
WHERE SurveyResultID = @SurveyResultID
