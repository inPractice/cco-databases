﻿CREATE TABLE [dbo].[MemberRole] (
    [Id]       INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberId] UNIQUEIDENTIFIER NOT NULL,
    [RoleId]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_MemberRole] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MemberRole_Role] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Role] ([Id]),
    CONSTRAINT [FK_MemberRole_SiteMember] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[SiteMember] ([MemberID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IDX_MemberRole]
    ON [dbo].[MemberRole]([MemberId] ASC, [RoleId] ASC);

