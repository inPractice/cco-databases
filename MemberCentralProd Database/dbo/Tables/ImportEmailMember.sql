﻿CREATE TABLE [dbo].[ImportEmailMember] (
    [ImportFileID]     UNIQUEIDENTIFIER NOT NULL,
    [ImportMemberID]   UNIQUEIDENTIFIER NOT NULL,
    [Email]            NVARCHAR (100)   NULL,
    [EmailFrequencyID] INT              CONSTRAINT [DF__ImportEma__Email__4DD47EBD] DEFAULT (3720) NULL,
    [IsActive]         BIT              DEFAULT (1) NOT NULL,
    [IsBadEmail]       BIT              DEFAULT (0) NULL,
    [Created]          DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ImportEmailMember] PRIMARY KEY CLUSTERED ([ImportFileID] ASC, [ImportMemberID] ASC),
    CONSTRAINT [FK__ImportEma__Email__6FF48C97] FOREIGN KEY ([EmailFrequencyID]) REFERENCES [dbo].[EmailFrequency] ([EmailFrequencyID]),
    CONSTRAINT [FK_ImportEmailMember] FOREIGN KEY ([ImportFileID], [ImportMemberID]) REFERENCES [dbo].[ImportMember] ([ImportFileID], [ImportMemberID])
);

