﻿CREATE TABLE [dbo].[ImportColumnMap] (
    [ImportFileID]   UNIQUEIDENTIFIER NOT NULL,
    [ImportColumnID] SMALLINT         NOT NULL,
    [FileColumnName] VARCHAR (50)     NOT NULL,
    [Created]        DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ImportColumnMap] PRIMARY KEY NONCLUSTERED ([ImportFileID] ASC, [ImportColumnID] ASC),
    CONSTRAINT [FK__ImportCol__Impor__5A054B78] FOREIGN KEY ([ImportFileID]) REFERENCES [dbo].[ImportFile] ([ImportFileID]),
    CONSTRAINT [FK__ImportCol__Impor__5AF96FB1] FOREIGN KEY ([ImportColumnID]) REFERENCES [dbo].[ImportColumn] ([ImportColumnID])
);


GO
CREATE CLUSTERED INDEX [Created]
    ON [dbo].[ImportColumnMap]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [FileColumnName]
    ON [dbo].[ImportColumnMap]([FileColumnName] ASC);

