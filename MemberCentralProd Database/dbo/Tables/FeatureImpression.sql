﻿CREATE TABLE [dbo].[FeatureImpression] (
    [FeatureImpressionID]       INT              IDENTITY (100, 1) NOT FOR REPLICATION NOT NULL,
    [SessionID]                 UNIQUEIDENTIFIER NOT NULL,
    [FeatureID]                 UNIQUEIDENTIFIER NOT NULL,
    [Created]                   DATETIME         DEFAULT (getdate()) NOT NULL,
    [ShownOnTrackedPropertyId]  UNIQUEIDENTIFIER NULL,
    [PromotedTrackedPropertyId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_FeatureImpression] PRIMARY KEY CLUSTERED ([FeatureImpressionID] ASC),
    CONSTRAINT [FK_FeatureImpression_TrackedProperty] FOREIGN KEY ([ShownOnTrackedPropertyId]) REFERENCES [dbo].[TrackedProperty] ([TrackedPropertyID]),
    CONSTRAINT [FK_FeatureImpression_TrackedProperty2] FOREIGN KEY ([PromotedTrackedPropertyId]) REFERENCES [dbo].[TrackedProperty] ([TrackedPropertyID]),
    CONSTRAINT [FK_Session_FeatureImpression] FOREIGN KEY ([SessionID]) REFERENCES [dbo].[Session] ([SessionID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Primary key. Identifies a unique viewing of a Feature during a Session.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureImpression', @level2type = N'COLUMN', @level2name = N'FeatureImpressionID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The Session in which the feature was shown.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureImpression', @level2type = N'COLUMN', @level2name = N'SessionID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The feature shown.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureImpression', @level2type = N'COLUMN', @level2name = N'FeatureID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date and time on which the feature was shown.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureImpression', @level2type = N'COLUMN', @level2name = N'Created';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Records on which TrackedProperty the feature appeared.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FeatureImpression', @level2type = N'COLUMN', @level2name = N'ShownOnTrackedPropertyId';

