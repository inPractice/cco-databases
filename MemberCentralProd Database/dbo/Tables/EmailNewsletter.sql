﻿CREATE TABLE [dbo].[EmailNewsletter] (
    [EmailNewsletterID]     INT          IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Text]                  VARCHAR (50) NOT NULL,
    [Sequence]              INT          CONSTRAINT [DF_EmailNewsletter_Sequence] DEFAULT ((10)) NOT NULL,
    [IsActive]              BIT          CONSTRAINT [DF_EmailNewsletter_IsActive] DEFAULT ((1)) NOT NULL,
    [Created]               DATETIME     CONSTRAINT [DF_EmailNewsletter_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]               DATETIME     NULL,
    [SourceID]              INT          CONSTRAINT [DF_EmailNewsletter_SourceID] DEFAULT ((2000)) NOT NULL,
    [EmailNewsletterTypeId] INT          NOT NULL,
    CONSTRAINT [PK__EmailNewsletter__30F848ED] PRIMARY KEY CLUSTERED ([EmailNewsletterID] ASC),
    CONSTRAINT [FK_EmailNewsletter_Source] FOREIGN KEY ([SourceID]) REFERENCES [dbo].[Source] ([SourceID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_EmailNewsletter]
    ON [dbo].[EmailNewsletter]([Text] ASC, [EmailNewsletterTypeId] ASC) WITH (IGNORE_DUP_KEY = ON);

