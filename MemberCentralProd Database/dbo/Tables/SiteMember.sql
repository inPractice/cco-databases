﻿CREATE TABLE [dbo].[SiteMember] (
    [MemberID]                  UNIQUEIDENTIFIER NOT NULL,
    [UserName]                  NVARCHAR (128)   NOT NULL,
    [SitecoreGroups]            VARCHAR (1024)   NULL,
    [WhereHearTypeID]           INT              NULL,
    [IsActive]                  BIT              CONSTRAINT [DF__SiteMembe__IsAct__49C3F6B7] DEFAULT (1) NOT NULL,
    [Created]                   DATETIME         CONSTRAINT [DF__SiteMembe__Creat__4AB81AF0] DEFAULT (getdate()) NOT NULL,
    [Updated]                   DATETIME         NULL,
    [DefaultHomePage]           UNIQUEIDENTIFIER NULL,
    [EncryptedPassword]         VARCHAR (172)    NULL,
    [InPracticePreferences]     VARCHAR (255)    NULL,
    [NickName]                  NVARCHAR (100)   NULL,
    [IsASHM]                    BIT              NULL,
    [IsASHMReporting]           BIT              NULL,
    [IsTermsAndConditionAgreed] BIT              DEFAULT ((1)) NULL,
    CONSTRAINT [PK__SiteMember__145C0A3F] PRIMARY KEY CLUSTERED ([MemberID] ASC),
    CONSTRAINT [FK__SiteMembe__Membe__1CBC4616] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[EmailMember] ([MemberID]),
    CONSTRAINT [FK__SiteMembe__Where__1DB06A4F] FOREIGN KEY ([WhereHearTypeID]) REFERENCES [dbo].[WhereHearType] ([WhereHearTypeID]),
    CONSTRAINT [UQ__SiteMember__15502E78] UNIQUE NONCLUSTERED ([UserName] ASC)
);


GO

CREATE  TRIGGER T_UPDATE_SiteMember
ON dbo.SiteMember
FOR UPDATE
AS

UPDATE SiteMember
SET Updated = GETDATE()
WHERE MemberID  IN (SELECT MemberID FROM Inserted)

