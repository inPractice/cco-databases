﻿CREATE TABLE [dbo].[WhereHearType] (
    [WhereHearTypeID] INT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Abbreviation]    VARCHAR (20)  NOT NULL,
    [Text]            VARCHAR (200) NOT NULL,
    [Sequence]        INT           DEFAULT (10) NOT NULL,
    [IsActive]        BIT           DEFAULT (1) NOT NULL,
    [Created]         DATETIME      DEFAULT (getdate()) NOT NULL,
    [Updated]         DATETIME      NULL,
    CONSTRAINT [PK__WhereHearType__49C3F6B7] PRIMARY KEY CLUSTERED ([WhereHearTypeID] ASC),
    CONSTRAINT [UQ__WhereHearType__4AB81AF0] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__WhereHearType__4BAC3F29] UNIQUE NONCLUSTERED ([WhereHearTypeID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_WhereHearType
ON WhereHearType
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT WhereHearTypeID FROM Inserted)

UPDATE WhereHearType
SET Updated = GETDATE()
WHERE WhereHearTypeID = @ID

