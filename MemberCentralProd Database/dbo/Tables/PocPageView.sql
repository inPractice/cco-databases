﻿CREATE TABLE [dbo].[PocPageView] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [PocInteractionId]  UNIQUEIDENTIFIER NOT NULL,
    [TrackedPropertyId] UNIQUEIDENTIFIER NOT NULL,
    [Relevant]          BIT              NULL,
    [Created]           DATETIME         NULL,
    CONSTRAINT [PK_PocActivityPageView] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PocPageView_PocInteraction] FOREIGN KEY ([PocInteractionId]) REFERENCES [dbo].[PocInteraction] ([Id]),
    CONSTRAINT [FK_PocPageView_TrackedProperty] FOREIGN KEY ([TrackedPropertyId]) REFERENCES [dbo].[TrackedProperty] ([TrackedPropertyID])
);


GO
CREATE NONCLUSTERED INDEX [IX_PocPageView_PocInteraction]
    ON [dbo].[PocPageView]([PocInteractionId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PocPageView_TrackedProperty]
    ON [dbo].[PocPageView]([TrackedPropertyId] ASC);

