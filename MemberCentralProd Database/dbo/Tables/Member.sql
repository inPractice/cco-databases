﻿CREATE TABLE [dbo].[Member] (
    [MemberID]                         UNIQUEIDENTIFIER CONSTRAINT [DF__Member__MemberID__37A5467C] DEFAULT (newid()) NOT NULL,
    [SalutationID]                     INT              NULL,
    [FirstName]                        NVARCHAR (50)    NULL,
    [Middle]                           NVARCHAR (10)    NULL,
    [LastName]                         NVARCHAR (50)    NULL,
    [Suffix]                           NVARCHAR (50)    NULL,
    [Phone]                            NVARCHAR (50)    NULL,
    [Fax]                              NVARCHAR (50)    NULL,
    [Affiliation]                      NVARCHAR (100)   NULL,
    [DegreeID]                         INT              NULL,
    [SpecialtyID]                      INT              NULL,
    [ProfessionID]                     INT              NULL,
    [StateLicensedID]                  INT              NULL,
    [NursingIDNumber]                  NVARCHAR (50)    NULL,
    [SourceID]                         INT              NOT NULL,
    [IsActive]                         BIT              CONSTRAINT [DF__Member__IsActive__38996AB5] DEFAULT (1) NOT NULL,
    [Created]                          DATETIME         CONSTRAINT [DF__Member__Created__398D8EEE] DEFAULT (getdate()) NOT NULL,
    [Updated]                          DATETIME         NULL,
    [LegacyMemberID]                   INT              NULL,
    [IsDeceased]                       BIT              CONSTRAINT [DF_Member_IsDeceased] DEFAULT ((0)) NOT NULL,
    [IsPhoneActive]                    BIT              CONSTRAINT [DF_Member_IsPhoneActive] DEFAULT ((1)) NOT NULL,
    [IsTextActive]                     BIT              CONSTRAINT [DF_Member_IsTextActive] DEFAULT ((0)) NOT NULL,
    [IsFaxActive]                      BIT              CONSTRAINT [DF_Member_IsFaxActive] DEFAULT ((1)) NOT NULL,
    [IsIndustry]                       BIT              CONSTRAINT [DF_Member_IsIndustry] DEFAULT ((0)) NOT NULL,
    [PreferredMemberID]                UNIQUEIDENTIFIER NULL,
    [DateOfBirth]                      DATETIME         NULL,
    [NABPePIDNumber]                   NVARCHAR (50)    NULL,
    [NPIRegistryNumber]                NVARCHAR (50)    NULL,
    [FPDMemberNumber]                  NVARCHAR (50)    NULL,
    [MedicalCertificateId]             INT              NULL,
    [MedicalCertificateSubspecialtyId] INT              NULL,
    [GraduationYear]                   INT              NULL,
    [MemberInt]                        BIGINT           IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    CONSTRAINT [PK__Member__76CBA758] PRIMARY KEY CLUSTERED ([MemberID] ASC),
    CONSTRAINT [FK__Member__DegreeID__0F624AF8] FOREIGN KEY ([DegreeID]) REFERENCES [dbo].[Degree] ([DegreeID]),
    CONSTRAINT [FK__Member__Professi__14270015] FOREIGN KEY ([ProfessionID]) REFERENCES [dbo].[Profession] ([ProfessionID]),
    CONSTRAINT [FK__Member__Salutati__0B91BA14] FOREIGN KEY ([SalutationID]) REFERENCES [dbo].[Salutation] ([SalutationID]),
    CONSTRAINT [FK__Member__SourceID__1EA48E88] FOREIGN KEY ([SourceID]) REFERENCES [dbo].[Source] ([SourceID]),
    CONSTRAINT [FK__Member__Specialt__151B244E] FOREIGN KEY ([SpecialtyID]) REFERENCES [dbo].[Specialty] ([SpecialtyID]),
    CONSTRAINT [FK__Member__StateLic__0E6E26BF] FOREIGN KEY ([StateLicensedID]) REFERENCES [dbo].[State] ([StateID]),
    CONSTRAINT [FK_Member_PreferredMember] FOREIGN KEY ([PreferredMemberID]) REFERENCES [dbo].[Member] ([MemberID]),
    CONSTRAINT [UQ__Member__77BFCB91] UNIQUE NONCLUSTERED ([MemberID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PreferredMemberID]
    ON [dbo].[Member]([PreferredMemberID] ASC);


GO
CREATE NONCLUSTERED INDEX [Member_Created]
    ON [dbo].[Member]([Created] ASC);


GO
CREATE  TRIGGER T_UPDATE_Member
ON dbo.Member
FOR UPDATE
AS


UPDATE Member
SET Updated = GETDATE()
WHERE MemberID IN (SELECT MemberID FROM Inserted)
