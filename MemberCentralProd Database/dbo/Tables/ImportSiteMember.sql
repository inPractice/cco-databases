﻿CREATE TABLE [dbo].[ImportSiteMember] (
    [ImportFileID]    UNIQUEIDENTIFIER NOT NULL,
    [ImportMemberID]  UNIQUEIDENTIFIER NOT NULL,
    [UserName]        NVARCHAR (128)   NULL,
    [SitecoreGroups]  VARCHAR (1024)   NULL,
    [WhereHearTypeID] INT              NULL,
    [IsActive]        BIT              DEFAULT (1) NOT NULL,
    [Created]         DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ImportSiteMember] PRIMARY KEY CLUSTERED ([ImportFileID] ASC, [ImportMemberID] ASC),
    CONSTRAINT [FK__ImportSit__Where__7E42ABEE] FOREIGN KEY ([WhereHearTypeID]) REFERENCES [dbo].[WhereHearType] ([WhereHearTypeID]),
    CONSTRAINT [FK_ImportSiteMember] FOREIGN KEY ([ImportFileID], [ImportMemberID]) REFERENCES [dbo].[ImportMember] ([ImportFileID], [ImportMemberID])
);

