﻿CREATE TABLE [dbo].[ResponseFixes] (
    [QuestionsetId]  UNIQUEIDENTIFIER NULL,
    [QuestionTextId] UNIQUEIDENTIFIER NULL,
    [SortOrder]      INT              NOT NULL
);

