﻿CREATE TABLE [dbo].[CmeInteraction] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [MemberId]                UNIQUEIDENTIFIER NOT NULL,
    [Status]                  VARCHAR (15)     NOT NULL,
    [HoursEngaged]            FLOAT (53)       NULL,
    [ProviderId]              UNIQUEIDENTIFIER NOT NULL,
    [AccreditingOrganization] VARCHAR (10)     NOT NULL,
    [CertificateType]         VARCHAR (20)     NOT NULL,
    [CountryId]               INT              NULL,
    [RegisteredOn]            DATETIME         NOT NULL,
    [CompletedOn]             DATETIME         NULL,
    [Created]                 DATETIME         NOT NULL,
    [Updated]                 DATETIME         NULL,
    CONSTRAINT [PK_CmeInteraction] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CmeInteraction_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryID]),
    CONSTRAINT [FK_CmeInteraction_Member] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Member] ([MemberID])
);


GO
CREATE NONCLUSTERED INDEX [IX_CmeInteraction]
    ON [dbo].[CmeInteraction]([MemberId] ASC);

