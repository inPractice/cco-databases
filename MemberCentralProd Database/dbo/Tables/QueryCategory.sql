﻿CREATE TABLE [dbo].[QueryCategory] (
    [CategoryID]   SMALLINT      NOT NULL,
    [CategoryName] VARCHAR (255) NOT NULL,
    [Created]      DATETIME      NOT NULL,
    [Updated]      DATETIME      NULL,
    CONSTRAINT [PK__QueryCategory__3F7C3DC5] PRIMARY KEY CLUSTERED ([CategoryID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [CategoryName]
    ON [dbo].[QueryCategory]([CategoryName] ASC);


GO
CREATE NONCLUSTERED INDEX [Created]
    ON [dbo].[QueryCategory]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [Updated]
    ON [dbo].[QueryCategory]([Updated] ASC);

