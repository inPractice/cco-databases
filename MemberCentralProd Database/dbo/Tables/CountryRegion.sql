﻿CREATE TABLE [dbo].[CountryRegion] (
    [CountryRegionId] INT IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [RegionId]        INT NOT NULL,
    [CountryId]       INT NOT NULL,
    CONSTRAINT [PK_CountryRegionId] PRIMARY KEY CLUSTERED ([CountryRegionId] ASC),
    CONSTRAINT [FK_CountryRegion_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryID]),
    CONSTRAINT [FK_CountryRegion_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Regions] ([RegionId])
);

