﻿CREATE TABLE [dbo].[EcommerceMemberSubscription] (
    [ID]                  INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberID]            UNIQUEIDENTIFIER NOT NULL,
    [EcommerceInternalId] INT              NOT NULL,
    [PromotationCode]     NVARCHAR (200)   NULL,
    [SubscriptionId]      NVARCHAR (2000)  NULL,
    [ProductId]           NVARCHAR (200)   NOT NULL,
    [ProductType]         NVARCHAR (200)   NULL,
    [StartDate]           DATETIME         NULL,
    [EndDate]             DATETIME         NULL,
    [Email]               NVARCHAR (100)   NULL,
    [OtherInfo]           NVARCHAR (4000)  NULL,
    [IsAutoRenewable]     BIT              DEFAULT ((0)) NOT NULL,
    [IsActive]            BIT              DEFAULT ((0)) NOT NULL,
    [Created]             DATETIME         CONSTRAINT [DF__EcommerceMemberSubscription__Creat__0CBAE877] DEFAULT (getdate()) NOT NULL,
    [Updated]             DATETIME         DEFAULT (getdate()) NULL,
    [SubscriptionType]    INT              CONSTRAINT [EcommerceMemberSubscription_SubscriptionType] DEFAULT ((2)) NULL,
    [ProductUniqueId]     NVARCHAR (2000)  NULL,
    CONSTRAINT [PK__EcommerceMemberSubscription__47DBAE45] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK__EcommerceMemberSubscription__Membe__09A971A2] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
);

