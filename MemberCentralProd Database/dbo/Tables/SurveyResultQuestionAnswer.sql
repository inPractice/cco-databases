﻿CREATE TABLE [dbo].[SurveyResultQuestionAnswer] (
    [SurveyResultID]   UNIQUEIDENTIFIER NOT NULL,
    [QuestionID]       UNIQUEIDENTIFIER NOT NULL,
    [QuestionAnswerID] UNIQUEIDENTIFIER DEFAULT ('00000000-0000-0000-0000-000000000000') NOT NULL,
    [FreeTextAnswer]   NTEXT            NULL,
    [Created]          DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]          DATETIME         NULL,
    CONSTRAINT [PK_SurveyResultQuestionAnswer] PRIMARY KEY NONCLUSTERED ([SurveyResultID] ASC, [QuestionID] ASC, [QuestionAnswerID] ASC),
    CONSTRAINT [FK_SurveyResultQuestionAnswer_SurveyResultID] FOREIGN KEY ([SurveyResultID]) REFERENCES [dbo].[SurveyResult] ([SurveyResultID])
);


GO
CREATE CLUSTERED INDEX [SurveyResultQuestionAnswer_Created]
    ON [dbo].[SurveyResultQuestionAnswer]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [SurveyResultQuestionAnswer_QuestionID]
    ON [dbo].[SurveyResultQuestionAnswer]([QuestionID] ASC);


GO
CREATE TRIGGER [dbo].[T_UPDATE_SurveyResultQuestionAnswer]
ON [dbo].[SurveyResultQuestionAnswer]
FOR UPDATE
AS

DECLARE @SurveyResultID uniqueidentifier
DECLARE @QuestionID uniqueidentifier
DECLARE @QuestionAnswerID uniqueidentifier

SELECT @SurveyResultID = (SELECT SurveyResultID FROM Inserted)
SELECT @QuestionID = (SELECT QuestionID FROM Inserted)
SELECT @QuestionAnswerID = (SELECT QuestionAnswerID FROM Inserted)

UPDATE SurveyResultQuestionAnswer
SET Updated = GETDATE()
WHERE SurveyResultID = @SurveyResultID
AND QuestionID = @QuestionID
AND QuestionAnswerID = @QuestionAnswerID


