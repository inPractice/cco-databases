﻿CREATE TABLE [dbo].[CaseFollowupResponse] (
    [CasePrimaryResponseID]    UNIQUEIDENTIFIER NOT NULL,
    [FollowupQuestionID]       UNIQUEIDENTIFIER NOT NULL,
    [FollowupQuestionAnswerID] UNIQUEIDENTIFIER NOT NULL,
    [Created]                  DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__CaseFollowupResp__7C4F7684] PRIMARY KEY CLUSTERED ([CasePrimaryResponseID] ASC),
    CONSTRAINT [FK__CaseFollo__CaseP__236943A5] FOREIGN KEY ([CasePrimaryResponseID]) REFERENCES [dbo].[CasePrimaryResponse] ([CasePrimaryResponseID])
);

