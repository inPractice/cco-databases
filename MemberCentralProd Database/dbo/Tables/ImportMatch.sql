﻿CREATE TABLE [dbo].[ImportMatch] (
    [ImportFileID]   UNIQUEIDENTIFIER NOT NULL,
    [ImportMemberID] UNIQUEIDENTIFIER NOT NULL,
    [MemberID]       UNIQUEIDENTIFIER NOT NULL,
    [MatchPoints]    SMALLINT         NOT NULL,
    [Created]        DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ImportMatch] PRIMARY KEY CLUSTERED ([ImportFileID] ASC, [ImportMemberID] ASC, [MemberID] ASC),
    CONSTRAINT [FK__ImportMat__Membe__03FB8544] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
    CONSTRAINT [FK_ImportMatch] FOREIGN KEY ([ImportFileID], [ImportMemberID]) REFERENCES [dbo].[ImportMember] ([ImportFileID], [ImportMemberID])
);

