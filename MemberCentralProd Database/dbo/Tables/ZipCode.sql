﻿CREATE TABLE [dbo].[ZipCode] (
    [ZIPCode]    CHAR (5)       NOT NULL,
    [ZIPType]    CHAR (1)       NULL,
    [CityName]   VARCHAR (64)   NOT NULL,
    [CityType]   CHAR (1)       NULL,
    [CountyName] VARCHAR (64)   NULL,
    [CountyFIPS] CHAR (5)       NULL,
    [StateName]  VARCHAR (64)   NULL,
    [StateAbbr]  CHAR (2)       NULL,
    [StateFips]  CHAR (2)       NULL,
    [MSACode]    CHAR (4)       NULL,
    [AreaCode]   VARCHAR (16)   NULL,
    [TimeZone]   VARCHAR (16)   NULL,
    [UTC]        DECIMAL (3, 1) NULL,
    [DST]        CHAR (1)       NULL,
    [Latitude]   DECIMAL (9, 6) NULL,
    [Longitude]  DECIMAL (9, 6) NULL,
    CONSTRAINT [PK_ZipCode] PRIMARY KEY CLUSTERED ([ZIPCode] ASC, [CityName] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ZipCode]
    ON [dbo].[ZipCode]([ZIPCode] ASC);


GO
CREATE NONCLUSTERED INDEX [CityName]
    ON [dbo].[ZipCode]([CityName] ASC);


GO
CREATE NONCLUSTERED INDEX [CountyName]
    ON [dbo].[ZipCode]([CountyName] ASC);


GO
CREATE NONCLUSTERED INDEX [StateName]
    ON [dbo].[ZipCode]([StateName] ASC);


GO
CREATE NONCLUSTERED INDEX [StateAbbr]
    ON [dbo].[ZipCode]([StateAbbr] ASC);


GO
CREATE NONCLUSTERED INDEX [Latitude]
    ON [dbo].[ZipCode]([Latitude] ASC);


GO
CREATE NONCLUSTERED INDEX [Longitude]
    ON [dbo].[ZipCode]([Longitude] ASC);

