﻿CREATE TABLE [dbo].[TrackedProperty] (
    [TrackedPropertyID] UNIQUEIDENTIFIER NOT NULL,
    [PropertyID]        UNIQUEIDENTIFIER NOT NULL,
    [PropertyName]      VARCHAR (255)    NOT NULL,
    [ClusterID]         UNIQUEIDENTIFIER NOT NULL,
    [TemplateName]      VARCHAR (255)    NOT NULL,
    [Created]           DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]           DATETIME         NULL,
    CONSTRAINT [PK_TrackedProperty] PRIMARY KEY CLUSTERED ([TrackedPropertyID] ASC),
    CONSTRAINT [FK_TrackedProperty_Cluster] FOREIGN KEY ([ClusterID]) REFERENCES [dbo].[Cluster] ([ClusterID]),
    CONSTRAINT [UQ_TrackedProperty_PropertyID] UNIQUE NONCLUSTERED ([PropertyID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [TrackedProperty_Created]
    ON [dbo].[TrackedProperty]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [TrackedProperty_TemplateName]
    ON [dbo].[TrackedProperty]([TemplateName] ASC);

