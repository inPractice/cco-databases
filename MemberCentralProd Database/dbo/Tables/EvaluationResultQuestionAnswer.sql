﻿CREATE TABLE [dbo].[EvaluationResultQuestionAnswer] (
    [EvaluationResultID] UNIQUEIDENTIFIER NOT NULL,
    [QuestionID]         UNIQUEIDENTIFIER NOT NULL,
    [QuestionAnswerID]   UNIQUEIDENTIFIER NOT NULL,
    [Created]            DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]            DATETIME         NULL,
    [FreeTextAnswer]     NTEXT            NULL,
    CONSTRAINT [PK__EvaluationResult__628FA481] PRIMARY KEY NONCLUSTERED ([EvaluationResultID] ASC, [QuestionID] ASC),
    CONSTRAINT [FK__Evaluatio__Evalu__2180FB33] FOREIGN KEY ([EvaluationResultID]) REFERENCES [dbo].[EvaluationResult] ([EvaluationResultID])
);


GO
CREATE CLUSTERED INDEX [EvaluationResultQuestionAnswer_Created]
    ON [dbo].[EvaluationResultQuestionAnswer]([Created] ASC);


GO

CREATE  TRIGGER T_UPDATE_EvaluationResultQuestionAnswer
ON EvaluationResultQuestionAnswer
FOR UPDATE
AS

DECLARE @evaluationResultID uniqueidentifier
DECLARE @questionID uniqueidentifier
DECLARE @questionAnswerID uniqueidentifier

SELECT @evaluationResultID = (SELECT EvaluationResultID FROM Inserted)
SELECT @questionID = (SELECT QuestionID FROM Inserted)
SELECT @questionAnswerID = (SELECT QuestionAnswerID FROM Inserted)

UPDATE EvaluationResultQuestionAnswer
SET Updated = GETDATE()
WHERE EvaluationResultID = @evaluationResultID
AND QuestionID = @questionID
AND QuestionAnswerID = @questionAnswerID


