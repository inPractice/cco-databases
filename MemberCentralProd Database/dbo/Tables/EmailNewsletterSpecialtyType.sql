﻿CREATE TABLE [dbo].[EmailNewsletterSpecialtyType] (
    [EmailNewsletterID] INT      NOT NULL,
    [SpecialtyTypeID]   INT      NOT NULL,
    [Created]           DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EmailNewsletterSpecialtyType] PRIMARY KEY CLUSTERED ([EmailNewsletterID] ASC, [SpecialtyTypeID] ASC),
    CONSTRAINT [FK_EmailNewsletterSpecialtyType_EmailNewsletterID] FOREIGN KEY ([EmailNewsletterID]) REFERENCES [dbo].[EmailNewsletter] ([EmailNewsletterID]),
    CONSTRAINT [FK_EmailNewsletterSpecialtyType_SpecialtyTypeID] FOREIGN KEY ([SpecialtyTypeID]) REFERENCES [dbo].[SpecialtyType] ([SpecialtyTypeID])
);

