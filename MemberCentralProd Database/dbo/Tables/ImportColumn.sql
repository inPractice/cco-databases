﻿CREATE TABLE [dbo].[ImportColumn] (
    [ImportColumnID] SMALLINT       NOT NULL,
    [NameVariants]   VARCHAR (1000) NOT NULL,
    [TableName]      VARCHAR (50)   NOT NULL,
    [ColumnName]     VARCHAR (50)   NOT NULL,
    [KeyTableName]   VARCHAR (50)   NULL,
    [KeyColumnName]  VARCHAR (50)   NULL,
    [Created]        DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__ImportColumn__5634BA94] PRIMARY KEY CLUSTERED ([ImportColumnID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [TableName]
    ON [dbo].[ImportColumn]([TableName] ASC);


GO
CREATE NONCLUSTERED INDEX [ColumnName]
    ON [dbo].[ImportColumn]([ColumnName] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyTableName]
    ON [dbo].[ImportColumn]([KeyTableName] ASC);


GO
CREATE NONCLUSTERED INDEX [KeyColumnName]
    ON [dbo].[ImportColumn]([KeyColumnName] ASC);


GO
CREATE NONCLUSTERED INDEX [Created]
    ON [dbo].[ImportColumn]([Created] ASC);

