﻿CREATE TABLE [dbo].[CertificatesIssuance] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [MemberId]      UNIQUEIDENTIFIER NOT NULL,
    [ActivityId]    UNIQUEIDENTIFIER NOT NULL,
    [CertificateId] UNIQUEIDENTIFIER NOT NULL,
    [Created]       DATE             CONSTRAINT [DF_CertificatesIssuance_Created] DEFAULT (getdate()) NULL
);

