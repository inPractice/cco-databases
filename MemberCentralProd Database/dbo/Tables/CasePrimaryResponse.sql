﻿CREATE TABLE [dbo].[CasePrimaryResponse] (
    [CasePrimaryResponseID]       UNIQUEIDENTIFIER NOT NULL,
    [CaseVisitID]                 UNIQUEIDENTIFIER NOT NULL,
    [QuestionID]                  UNIQUEIDENTIFIER NOT NULL,
    [QuestionAnswerID]            UNIQUEIDENTIFIER NOT NULL,
    [Created]                     DATETIME         DEFAULT (getdate()) NOT NULL,
    [LegacyCasePrimaryResponseID] INT              NULL,
    CONSTRAINT [PK__CasePrimaryRespo__693CA210] PRIMARY KEY CLUSTERED ([CasePrimaryResponseID] ASC),
    CONSTRAINT [FK__CasePrima__CaseV__22751F6C] FOREIGN KEY ([CaseVisitID]) REFERENCES [dbo].[CaseVisit] ([CaseVisitID]),
    CONSTRAINT [UQ__CasePrimaryRespo__6A30C649] UNIQUE NONCLUSTERED ([CasePrimaryResponseID] ASC)
);

