﻿CREATE TABLE [dbo].[EmailFrequency] (
    [EmailFrequencyID] INT          IDENTITY (3710, 10) NOT FOR REPLICATION NOT NULL,
    [ShortName]        VARCHAR (20) NOT NULL,
    [Text]             VARCHAR (50) NOT NULL,
    [Sequence]         INT          NOT NULL,
    [Created]          DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]          DATETIME     NULL,
    CONSTRAINT [PK__EmailFrequency__37A5467C] PRIMARY KEY CLUSTERED ([EmailFrequencyID] ASC),
    CONSTRAINT [UQ__EmailFrequency__38996AB5] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__EmailFrequency__398D8EEE] UNIQUE NONCLUSTERED ([ShortName] ASC),
    CONSTRAINT [UQ__EmailFrequency__3A81B327] UNIQUE NONCLUSTERED ([EmailFrequencyID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_EmailFrequency
ON EmailFrequency
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT EmailFrequencyID FROM Inserted)

UPDATE EmailFrequency
SET Updated = GETDATE()
WHERE EmailFrequencyID = @ID

