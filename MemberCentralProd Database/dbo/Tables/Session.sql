﻿CREATE TABLE [dbo].[Session] (
    [SessionID]          UNIQUEIDENTIFIER NOT NULL,
    [MemberID]           UNIQUEIDENTIFIER NULL,
    [Referrer]           VARCHAR (1024)   NULL,
    [Created]            DATETIME         DEFAULT (getdate()) NOT NULL,
    [Updated]            DATETIME         DEFAULT (getdate()) NOT NULL,
    [Site]               VARCHAR (25)     NULL,
    [UserAgent]          VARCHAR (255)    NULL,
    [HostName]           VARCHAR (255)    NULL,
    [HostAddress]        VARCHAR (25)     NULL,
    [Platform]           VARCHAR (50)     NULL,
    [Location]           INT              NULL,
    [OriginatingPartner] VARCHAR (50)     NULL,
    CONSTRAINT [PK__Session__73BA3083] PRIMARY KEY NONCLUSTERED ([SessionID] ASC),
    CONSTRAINT [FK__Session__MemberI__123EB7A3] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID]),
    CONSTRAINT [UQ__Session__74AE54BC] UNIQUE NONCLUSTERED ([SessionID] ASC)
);


GO
CREATE CLUSTERED INDEX [Session_Created]
    ON [dbo].[Session]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [Session_MemberID]
    ON [dbo].[Session]([MemberID] ASC);

