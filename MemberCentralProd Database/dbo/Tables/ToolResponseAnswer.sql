﻿CREATE TABLE [dbo].[ToolResponseAnswer] (
    [ToolResponseAnswerId]   INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ToolResponseQuestionId] INT                NOT NULL,
    [AnswerUID]              UNIQUEIDENTIFIER   NOT NULL,
    [AnswerVersion]          INT                NOT NULL,
    [AnswerName]             NVARCHAR (256)     NOT NULL,
    [FreeFormResponse]       NVARCHAR (2000)    NULL,
    [CreatedDate]            DATETIMEOFFSET (7) NOT NULL,
    [InsertedDate]           DATETIMEOFFSET (7) CONSTRAINT [DF_ToolResponseAnswer_InsertedDate] DEFAULT (sysdatetimeoffset()) NOT NULL,
    CONSTRAINT [PK_ToolResponseAnswer] PRIMARY KEY CLUSTERED ([ToolResponseAnswerId] ASC),
    CONSTRAINT [FK_ToolResponseAnswer_ToolResponseQuestion] FOREIGN KEY ([ToolResponseQuestionId]) REFERENCES [dbo].[ToolResponseQuestion] ([ToolResponseQuestionId])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Surrogate key for the ToolResponseAnswer table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'ToolResponseAnswerId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The question to which this answer response belongs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'ToolResponseQuestionId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The GUID for the answer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'AnswerUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The version of the answer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'AnswerVersion';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The name or label for the answer', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'AnswerName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Stores the free text response given by the user, if any', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'FreeFormResponse';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time this answered was submitted on the client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the record was added to the database', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolResponseAnswer', @level2type = N'COLUMN', @level2name = N'InsertedDate';

