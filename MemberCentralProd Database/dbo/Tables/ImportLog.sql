﻿CREATE TABLE [dbo].[ImportLog] (
    [ImportFileID] UNIQUEIDENTIFIER NOT NULL,
    [MemberID]     UNIQUEIDENTIFIER NOT NULL,
    [Type]         VARCHAR (50)     NOT NULL,
    [Source]       VARCHAR (50)     NOT NULL,
    [Event]        VARCHAR (50)     NOT NULL,
    [Description]  VARCHAR (255)    NULL,
    [Created]      DATETIME         DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [FK__ImportLog__Membe__53584DE9] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[SiteMember] ([MemberID])
);


GO
CREATE CLUSTERED INDEX [Created]
    ON [dbo].[ImportLog]([Created] ASC);


GO
CREATE NONCLUSTERED INDEX [ImportFileID]
    ON [dbo].[ImportLog]([ImportFileID] ASC);


GO
CREATE NONCLUSTERED INDEX [MemberID]
    ON [dbo].[ImportLog]([MemberID] ASC);


GO
CREATE NONCLUSTERED INDEX [Type]
    ON [dbo].[ImportLog]([Type] ASC);


GO
CREATE NONCLUSTERED INDEX [Source]
    ON [dbo].[ImportLog]([Source] ASC);


GO
CREATE NONCLUSTERED INDEX [Event]
    ON [dbo].[ImportLog]([Event] ASC);

