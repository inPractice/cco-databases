﻿CREATE TABLE [dbo].[DegreeType] (
    [DegreeTypeID] INT          IDENTITY (100, 1) NOT FOR REPLICATION NOT NULL,
    [Text]         VARCHAR (20) NOT NULL,
    [Created]      DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME     NULL,
    CONSTRAINT [PK__DegreeType__7F60ED59] PRIMARY KEY CLUSTERED ([DegreeTypeID] ASC),
    CONSTRAINT [UQ__DegreeType__00551192] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__DegreeType__014935CB] UNIQUE NONCLUSTERED ([DegreeTypeID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_DegreeType
ON DegreeType
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT DegreeTypeID FROM Inserted)

UPDATE DegreeType
SET Updated = GETDATE()
WHERE DegreeTypeID = @ID


