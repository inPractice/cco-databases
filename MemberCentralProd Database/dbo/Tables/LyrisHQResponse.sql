﻿CREATE TABLE [dbo].[LyrisHQResponse] (
    [LyrisHQResponseId] INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MemberId]          UNIQUEIDENTIFIER NULL,
    [LyrisUserId]       VARCHAR (50)     NULL,
    [RequestType]       VARCHAR (50)     NOT NULL,
    [ResponseXML]       VARCHAR (2000)   NULL,
    [Success]           BIT              NOT NULL,
    [Created]           DATETIME         NOT NULL,
    CONSTRAINT [PK_LyrisHQResponse] PRIMARY KEY CLUSTERED ([LyrisHQResponseId] ASC),
    CONSTRAINT [FK_LyrisHQResponse_LyrisHQResponse] FOREIGN KEY ([LyrisHQResponseId]) REFERENCES [dbo].[LyrisHQResponse] ([LyrisHQResponseId])
);

