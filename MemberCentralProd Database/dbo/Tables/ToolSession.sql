﻿CREATE TABLE [dbo].[ToolSession] (
    [ToolSessionID] INT                IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ToolUID]       UNIQUEIDENTIFIER   NOT NULL,
    [SessionUID]    UNIQUEIDENTIFIER   NOT NULL,
    [CreatedDate]   DATETIMEOFFSET (7) NOT NULL,
    [InsertedDate]  DATETIMEOFFSET (7) CONSTRAINT [DF_ToolSession_InsertedDate] DEFAULT (sysdatetimeoffset()) NOT NULL,
    CONSTRAINT [PK_ToolSession] PRIMARY KEY CLUSTERED ([ToolSessionID] ASC),
    CONSTRAINT [FK_ToolSession_Session] FOREIGN KEY ([SessionUID]) REFERENCES [dbo].[Session] ([SessionID])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Surrogate key for the ToolSession table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolSession', @level2type = N'COLUMN', @level2name = N'ToolSessionID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The GUID of the tool with which the user interacted', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolSession', @level2type = N'COLUMN', @level2name = N'ToolUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user session in which the interaction happened', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolSession', @level2type = N'COLUMN', @level2name = N'SessionUID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the session began on the client', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolSession', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The time the record was added to the database', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ToolSession', @level2type = N'COLUMN', @level2name = N'InsertedDate';

