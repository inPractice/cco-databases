﻿CREATE TABLE [dbo].[MedicalCertificateSubspecialty] (
    [MedicalCertificateSubspecialtyId]          INT              IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MedicalCertificateSubspecialtyName]        VARCHAR (64)     NOT NULL,
    [MedicalCertificateSubspecialtyGUID]        UNIQUEIDENTIFIER NULL,
    [MedicalCertificateSubspecialtyCode]        VARCHAR (12)     NULL,
    [MedicalCertificateSubspecialtyDescription] VARCHAR (256)    NULL,
    [ActiveInd]                                 BIT              CONSTRAINT [DF_MedicalCertificateSubspecialty_ActiveInd] DEFAULT ((1)) NOT NULL,
    [CreatedDttm]                               DATETIME         CONSTRAINT [DF_MedicalCertificateSubspecialty_CreatedDttm] DEFAULT (getdate()) NOT NULL,
    [ModifiedDttm]                              DATETIME         NULL,
    [DeletedInd]                                BIT              CONSTRAINT [DF_MedicalCertificateSubspecialty_DeletedInd] DEFAULT ((0)) NOT NULL,
    [DeletedDttm]                               DATETIME         NULL,
    CONSTRAINT [PK_MedicalCertificateSubspecialty] PRIMARY KEY CLUSTERED ([MedicalCertificateSubspecialtyId] ASC) WITH (IGNORE_DUP_KEY = ON)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_MedicalCertificateSubspecialty_MedicalCertificateSubspecialtyName]
    ON [dbo].[MedicalCertificateSubspecialty]([MedicalCertificateSubspecialtyName] ASC) WITH (IGNORE_DUP_KEY = ON);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifies MedicalCertificateSubspecialty', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MedicalCertificateSubspecialty', @level2type = N'COLUMN', @level2name = N'MedicalCertificateSubspecialtyId';

