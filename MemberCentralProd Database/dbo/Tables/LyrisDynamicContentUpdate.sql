﻿CREATE TABLE [dbo].[LyrisDynamicContentUpdate] (
    [Id]                     INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Email]                  VARCHAR (250)  NOT NULL,
    [InPracticeDayOfWeek]    VARCHAR (25)   CONSTRAINT [DF_LyrisDynamicContentUpdate_InPracticeDayOfWeek] DEFAULT ('Monday') NULL,
    [inPracticeEmailContent] VARCHAR (MAX)  NULL,
    [LyrisResponse]          VARCHAR (2000) NULL,
    [CodeException]          VARCHAR (MAX)  NULL,
    [Success]                BIT            NULL,
    [Created]                DATETIME       CONSTRAINT [DF_LyrisDynamicContentUpdate_Created] DEFAULT (getdate()) NOT NULL,
    [Updated]                DATETIME       NULL,
    CONSTRAINT [PK_LyrisDynamicContentUpdate] PRIMARY KEY CLUSTERED ([Id] ASC)
);

