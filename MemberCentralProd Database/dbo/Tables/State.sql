﻿CREATE TABLE [dbo].[State] (
    [StateID]      INT          IDENTITY (200, 1) NOT FOR REPLICATION NOT NULL,
    [Abbreviation] CHAR (2)     NOT NULL,
    [Text]         VARCHAR (50) NOT NULL,
    [Sequence]     INT          NOT NULL,
    [Created]      DATETIME     DEFAULT (getdate()) NOT NULL,
    [Updated]      DATETIME     NULL,
    CONSTRAINT [PK__State__0425A276] PRIMARY KEY CLUSTERED ([StateID] ASC),
    CONSTRAINT [UQ__State__0519C6AF] UNIQUE NONCLUSTERED ([Text] ASC),
    CONSTRAINT [UQ__State__060DEAE8] UNIQUE NONCLUSTERED ([StateID] ASC)
);


GO
CREATE TRIGGER T_UPDATE_State
ON State
FOR UPDATE
AS

DECLARE @ID integer

SELECT @ID = (SELECT StateID FROM Inserted)

UPDATE State
SET Updated = GETDATE()
WHERE StateID = @ID

