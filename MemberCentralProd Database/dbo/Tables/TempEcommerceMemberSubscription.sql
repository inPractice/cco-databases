﻿CREATE TABLE [dbo].[TempEcommerceMemberSubscription] (
    [ID]                  INT              IDENTITY (1, 1) NOT NULL,
    [MemberID]            UNIQUEIDENTIFIER NOT NULL,
    [EcommerceInternalId] INT              NOT NULL,
    [PromotationCode]     NVARCHAR (200)   NULL,
    [SubscriptionId]      NVARCHAR (2000)  NULL,
    [ProductId]           NVARCHAR (200)   NOT NULL,
    [ProductType]         NVARCHAR (200)   NULL,
    [StartDate]           DATETIME         NULL,
    [EndDate]             DATETIME         NULL,
    [Email]               NVARCHAR (100)   NULL,
    [OtherInfo]           NVARCHAR (4000)  NULL,
    [IsAutoRenewable]     BIT              NOT NULL,
    [IsActive]            BIT              NOT NULL,
    [Created]             DATETIME         NOT NULL,
    [Updated]             DATETIME         NULL,
    [SubscriptionType]    INT              NULL,
    [ProductUniqueId]     NVARCHAR (2000)  NULL
);

