﻿CREATE TABLE [dbo].[ImportMember] (
    [ImportFileID]                     UNIQUEIDENTIFIER NOT NULL,
    [ImportMemberID]                   UNIQUEIDENTIFIER NOT NULL,
    [SalutationID]                     INT              NULL,
    [FirstName]                        NVARCHAR (50)    NULL,
    [Middle]                           NCHAR (10)       NULL,
    [LastName]                         NVARCHAR (50)    NULL,
    [Suffix]                           NVARCHAR (50)    NULL,
    [Phone]                            NVARCHAR (50)    NULL,
    [Fax]                              NVARCHAR (50)    NULL,
    [Affiliation]                      NVARCHAR (100)   NULL,
    [DegreeID]                         INT              NULL,
    [SpecialtyID]                      INT              NULL,
    [ProfessionID]                     INT              NULL,
    [StateLicensedID]                  INT              NULL,
    [NursingIDNumber]                  NVARCHAR (50)    NULL,
    [SourceID]                         INT              DEFAULT (2001) NOT NULL,
    [IsActive]                         BIT              DEFAULT (1) NOT NULL,
    [Created]                          DATETIME         DEFAULT (getdate()) NOT NULL,
    [Token1]                           NVARCHAR (255)   NULL,
    [Token2]                           NVARCHAR (255)   NULL,
    [Token3]                           NVARCHAR (255)   NULL,
    [IsDuplicate]                      BIT              DEFAULT (0) NOT NULL,
    [IsReviewed]                       BIT              DEFAULT (1) NOT NULL,
    [IsIgnored]                        BIT              DEFAULT (0) NOT NULL,
    [IsCopied]                         BIT              DEFAULT (0) NOT NULL,
    [IsSynchronized]                   BIT              DEFAULT (0) NOT NULL,
    [MemberID]                         UNIQUEIDENTIFIER NULL,
    [MedicalCertificateId]             INT              NULL,
    [MedicalCertificateSubspecialtyId] INT              NULL,
    [GraduationYear]                   INT              NULL,
    CONSTRAINT [PK_ImportMember] PRIMARY KEY CLUSTERED ([ImportFileID] ASC, [ImportMemberID] ASC),
    CONSTRAINT [FK__ImportMem__Degre__60B24907] FOREIGN KEY ([DegreeID]) REFERENCES [dbo].[Degree] ([DegreeID]),
    CONSTRAINT [FK__ImportMem__Impor__5ECA0095] FOREIGN KEY ([ImportFileID]) REFERENCES [dbo].[ImportFile] ([ImportFileID]),
    CONSTRAINT [FK__ImportMem__Membe__6D181FEC] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
    CONSTRAINT [FK__ImportMem__Profe__629A9179] FOREIGN KEY ([ProfessionID]) REFERENCES [dbo].[Profession] ([ProfessionID]),
    CONSTRAINT [FK__ImportMem__Salut__5FBE24CE] FOREIGN KEY ([SalutationID]) REFERENCES [dbo].[Salutation] ([SalutationID]),
    CONSTRAINT [FK__ImportMem__Sourc__6482D9EB] FOREIGN KEY ([SourceID]) REFERENCES [dbo].[Source] ([SourceID]),
    CONSTRAINT [FK__ImportMem__Speci__61A66D40] FOREIGN KEY ([SpecialtyID]) REFERENCES [dbo].[Specialty] ([SpecialtyID]),
    CONSTRAINT [FK__ImportMem__State__638EB5B2] FOREIGN KEY ([StateLicensedID]) REFERENCES [dbo].[State] ([StateID])
);

