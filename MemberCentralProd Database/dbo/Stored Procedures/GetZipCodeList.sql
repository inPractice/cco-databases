﻿
-- ========================================================
-- Create GetZipCodeList
-- ========================================================

CREATE PROCEDURE [dbo].[GetZipCodeList] 
(
	@ZipCode 	char(5),
	@Radius		smallint
)
AS

SET NOCOUNT ON

DECLARE @MilesDegree AS numeric(4,2)

DECLARE @Latitude  AS numeric(9,6)
DECLARE @Longitude AS numeric(9,6)

DECLARE @LatitudeRange  AS numeric(9,6)
DECLARE @LongitudeRange AS numeric(9,6)

-- approximate number of miles per degree latitude is constant
SET @MilesDegree = 69.11

-- get zip code latitude and longitude
SET @Latitude 	= (SELECT TOP 1 Latitude  FROM [dbo].[ZipCode] WHERE ZipCode = @ZipCode)
SET @Longitude 	= (SELECT TOP 1 Longitude FROM [dbo].[ZipCode] WHERE ZipCode = @ZipCode)

-- set ranges
SET @LatitudeRange = @Radius / @MilesDegree
SET @LongitudeRange = @Radius / (COS(RADIANS(@Latitude)) * @MilesDegree)

-- first pass: insert 'bounding box' into temporary table
SELECT 
	ZipCode 	= ZipCode,
	City		= CityName,
	County		= CountyName,
	State		= StateAbbr,
	Distance = CAST(DEGREES(ACOS(CAST((SIN(RADIANS(@Latitude)) * SIN(RADIANS(Latitude))) + (COS(RADIANS(@Latitude)) * COS(RADIANS(Latitude)) * COS(RADIANS(ABS(@Longitude - Longitude)))) AS numeric(16, 15)))) * @MilesDegree AS numeric(4,1))
INTO
	#ZipCode
FROM 
	ZipCode
WHERE 
	Latitude >= @Latitude - @LatitudeRange
AND	Latitude <= @Latitude + @LatitudeRange
AND 	Longitude >= @Longitude - @LongitudeRange
AND 	Longitude <= @Longitude + @LongitudeRange

SET NOCOUNT OFF

-- second pass: assuming earth is flat for short distances, get all zip codes within radius from previous results
SELECT
	*
FROM
	#ZipCode
WHERE
	Distance <= @Radius
ORDER BY
	Distance, ZipCode, City

DROP TABLE #ZipCode
