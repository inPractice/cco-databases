﻿CREATE PROCEDURE [dbo].[UpdateSessionOriginatingPartner]
(
	@SessionID uniqueidentifier,
	@OriginatingPartner varchar(50)
)
AS

UPDATE [Session]
SET
	OriginatingPartner = @OriginatingPartner,
	Updated = GETDATE()
WHERE SessionID = @SessionID


