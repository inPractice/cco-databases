﻿
CREATE PROCEDURE dbo.usp_SyncronizeMemberIsActive 
AS BEGIN
	SET NOCOUNT ON
	DECLARE @LastModifiedDttm datetime = GETDATE()

	UPDATE em
	SET em.IsActive = m.IsActive
	, em.Updated = @LastModifiedDttm
	-- SELECT m.MemberId, em.IsActive, m.IsActive
	FROM [MemberCentralProd].[dbo].[EmailMember] em (nolock)
	JOIN [MemberCentralProd].[dbo].[Member] m (nolock)
		ON m.[MemberID] = em.[MemberID]
	WHERE em.IsActive = 1 AND m.IsActive = 0
	;

	UPDATE sm
	SET sm.IsActive = m.IsActive
	, sm.Updated = @LastModifiedDttm
	-- SELECT m.MemberId, sm.IsActive, m.IsActive
	FROM [MemberCentralProd].[dbo].[SiteMember] sm (nolock)
	JOIN [MemberCentralProd].[dbo].[Member] m (nolock)
		ON m.[MemberID] = sm.[MemberID]
	WHERE sm.IsActive = 1 AND m.IsActive = 0
	;

	UPDATE mm
	SET mm.IsActive = m.IsActive
	, mm.Updated = @LastModifiedDttm
	-- SELECT m.MemberId, mm.IsActive, m.IsActive
	FROM [MemberCentralProd].[dbo].[MailMember] mm (nolock)
	JOIN [MemberCentralProd].[dbo].[Member] m (nolock)
		ON m.[MemberID] = mm.[MemberID]
	WHERE mm.IsActive = 1 AND m.IsActive = 0
	;

	UPDATE m
	SET m.IsActive = sm.IsActive
	, m.Updated = @LastModifiedDttm
	-- SELECT m.MemberId, m.IsActive, sm.IsActive
	FROM [MemberCentralProd].[dbo].[Member] m (nolock)
	JOIN [MemberCentralProd].[dbo].[SiteMember] sm (nolock)
		ON sm.[MemberID] = m.[MemberID]
	WHERE sm.IsActive = 0 AND m.IsActive = 1
	;

END
