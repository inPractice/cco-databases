﻿CREATE PROCEDURE [dbo].[UpdateSessionMemberID]
(
	@SessionID uniqueidentifier,
	@MemberID uniqueidentifier
)
AS

UPDATE [Session]
SET
	MemberID = @MemberID,
	Updated = GETDATE()
WHERE SessionID = @SessionID


