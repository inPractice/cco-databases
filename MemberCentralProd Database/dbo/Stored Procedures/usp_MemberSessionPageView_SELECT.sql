﻿CREATE PROCEDURE [dbo].[usp_MemberSessionPageView_SELECT]
(
@YearsBack INT = -3
)
AS
BEGIN

	SELECT
	 [SiteCode]		
	,[MemberGUId] 	
	,[SessionGUId] 	
	,[PageViewGUID]	
	,[URL]			
	,[SessionStartDttm]
	,[SessionYear] 	
	,[SessionMonth] 
	,[SessionWeek]
	FROM
	(
		SELECT DISTINCT
			[SiteCode]		= 'CCO'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionID]
		,   [PageViewGUID]		= tp.[PropertyID]
		,   [URL]				= pv.[Referrer]
		,	[SessionStartDttm] 	= tp.[Created]
		,	[SessionStartDttm] 	= s.[Created]
		,	[SessionYear] 		= YEAR(tp.[Created])
		,	[SessionMonth] 		= MONTH(tp.[Created])
		,	[SessionWeek]		= DATEPART(wk ,tp.[Created])
		FROM [MemberCentralProd].[dbo].[Session] s
		JOIN [MemberCentralProd].[dbo].[PropertyView] pv
			ON pv.[SessionID] = s.[SessionID]
		  JOIN [MemberCentralProd].[dbo].[TrackedProperty] tp
			ON tp.[TrackedPropertyID] = pv.[TrackedPropertyID]
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])

		UNION ALL
		SELECT DISTINCT
			[SiteCode]		= 'CCO'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionID]
		,   [PageViewGUID]		= tp.[FeatureID]
		,   [URL]				= tp.[Target]
		,	[SessionStartDttm] 	= tp.[Created]
		,	[SessionStartDttm] 	= s.[Created]
		,	[SessionYear] 		= YEAR(tp.[Created])
		,	[SessionMonth] 		= MONTH(tp.[Created])
		,	[SessionWeek]		= DATEPART(wk ,tp.[Created])
		FROM [MemberCentralProd].[dbo].[Session] s
		JOIN [MemberCentralProd].[dbo].[FeatureClick] tp
			ON tp.[SessionID] = s.[SessionID]
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])

	UNION ALL
		SELECT DISTINCT
			[SiteCode]		= 'CCO'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionID]
		,   [PageViewGUID]		= tp.[RedirectViewID]
		,   [URL]				= tp.[Redirect]
		,	[SessionStartDttm] 	= tp.[Created]
		,	[SessionStartDttm] 	= s.[Created]
		,	[SessionYear] 		= YEAR(tp.[Created])
		,	[SessionMonth] 		= MONTH(tp.[Created])
		,	[SessionWeek]		= DATEPART(wk ,tp.[Created])
		FROM [MemberCentralProd].[dbo].[Session] s
		JOIN [MemberCentralProd].[dbo].[RedirectView] tp
			ON tp.[SessionID] = s.[SessionID]
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])

	UNION ALL
		SELECT DISTINCT
			[SiteCode]			= 'IPR'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionUniqueID]
		,   [PageViewGUID]		= pv.[PropertyID]
		,   [URL]				= ''
		,	[SessionStartDttm] 	= pv.[Created]
		,	[SessionStartDttm] 	= s.[Created]
		,	[SessionYear] 		= YEAR(pv.[Created])
		,	[SessionMonth] 		= MONTH(pv.[Created])
		,	[SessionWeek]		= DATEPART(wk ,pv.[Created])
		FROM [INP_Tracking].[dbo].[Session] s
		JOIN [INP_Tracking].[dbo].[PageView] pv
			ON pv.[SessionUniqueID] = s.[SessionUniqueID]
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])

	UNION ALL
		SELECT DISTINCT
			[SiteCode]			= 'IPA'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionUniqueID]
		,   [PageViewGUID]		= pv.[PropertyID]
		,   [URL]				= ''
		,	[SessionStartDttm] 	= pv.[Created]
		,	[SessionStartDttm] 	= s.[Created]
		,	[SessionYear] 		= YEAR(pv.[Created])
		,	[SessionMonth] 		= MONTH(pv.[Created])
		,	[SessionWeek]		= DATEPART(wk ,pv.[Created])
		FROM [IPA_Tracking].[dbo].[Session] s
		JOIN [IPA_Tracking].[dbo].[PageView] pv
			ON pv.[SessionUniqueID] = s.[SessionUniqueID]
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])
	) pv

END
;