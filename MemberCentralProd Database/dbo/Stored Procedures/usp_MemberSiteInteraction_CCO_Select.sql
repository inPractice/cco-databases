﻿CREATE  PROCEDURE [dbo].[usp_MemberSiteInteraction_CCO_Select]
(
	 @LastImportDate datetime
)
AS
BEGIN
	SET NOCOUNT ON
 
	SELECT DISTINCT
	  [SourceMemberGUID]
	, [SourceSessionGUID]
	, [ViewFormatName]
	, [SiteActionCode]
	, [ContentPageObjectGUID]
	, [SessionStartDttm]
	, [InteractionDttm]	
	FROM
	(
		SELECT DISTINCT 
		  [SourceMemberGUID]		= ISNULL(ss.[MemberID], '00000000-0000-0000-0000-000000000000')
		, [SourceSessionGUID]		= ss.[SessionID]
		, [ContentPageObjectGUID]	= tp.[PropertyID]
		, [ViewFormatName]		= [ViewFormat]
		, [SiteActionCode]		= [IPRDW_SRC].[dbo].[udf_SiteActionCode_Get]([ViewFormat], [TemplateName])	
		, [SessionStartDttm]		= ss.[Created]
		, [InteractionDttm]			= pv.[Created]
		, [TherapeuticAreaName]	= cl.[Specialty]
		, [ContentTypeName]		= cl.[ClusterType]
		, [ContentPageTitle]	= cl.[ClusterName]
		, [TemplateName]		= tp.[TemplateName]
		FROM [dbo].[Session] ss (nolock)
		JOIN [dbo].[PropertyView] pv (nolock)
			ON pv.[SessionID] = ss.[SessionID]
			AND ss.[Created] >= @LastImportDate
			AND ss.[Created] < DATEADD(hour, 1, GETDATE())
		LEFT JOIN [dbo].[TrackedProperty] tp (nolock)
			ON tp.[TrackedPropertyID] = pv.[TrackedPropertyID]
		LEFT JOIN [dbo].[Cluster] cl (nolock)
			ON cl.[ClusterID] = tp.[ClusterID]
		WHERE 1=1
			AND ss.[Created] >= @LastImportDate
			AND ss.[Created] < DATEADD(hour, 1, GETDATE())
	--	AND CHARINDEX('launch', pv.[ViewFormat]) > 0
	) vw
	WHERE 1=1
	;
		
END

