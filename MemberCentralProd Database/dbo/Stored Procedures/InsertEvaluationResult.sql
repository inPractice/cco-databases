﻿CREATE PROCEDURE InsertEvaluationResult (
  @evaluationResultID uniqueidentifier,
  @testResultID uniqueidentifier,
  @evaluationID uniqueidentifier,
  @hoursClaimed float = NULL,
  @deliveryMethodID uniqueidentifier = NULL,
  @comment text = NULL
)
AS

INSERT INTO EvaluationResult
(EvaluationResultID, TestResultID, EvaluationID)
VALUES
(@evaluationResultID, @testResultID, @evaluationID)

UPDATE TestResult
SET
HoursClaimed = @hoursClaimed,
DeliveryMethodID = @deliveryMethodID,
Comment = @comment
WHERE TestResultID = @testResultID