﻿

CREATE PROCEDURE [dbo].[usp_MemberSubscription_SELECT]
(
		@ProductName varchar(128) = ''
		, @SubscriptionName varchar(128) = ''
		, @SpecialtyName varchar(128) = ''
		, @EmailAddress varchar(128) = ''
		, @MemberTypeCode varchar(1) = 'S'
		, @MemberRole varchar(12) = 'Registered'
		, @SubDate date = '2010-01-01'
		, @SubActiveInd bit = 1
)
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #TmpRole
	(
	RoleId varchar(96)
	, RoleName varchar(128)
	)
	;
	INSERT INTO #TmpRole(RoleId, RoleName) SELECT Id, [Name] FROM dbo.[Role];

	SELECT 
	  result.MemberID
	, result.FirstName
	, result.LastName
	, result.Email
	, result.SpecialtyName
	, result.Product
	, result.SubscriptionType
	, result.Subscription
	, result.SubscriptionStartDate
	, result.SubscriptionEndDate
	, result.MemberType
	, result.MemberRole
	, result.SubscriptionActiveInd
	FROM
	(		
		SELECT DISTINCT 
		em.MemberID
		, FirstName					= ISNULL(m.FirstName, '')
		, LastName					= ISNULL(m.LastName, '')
		, em.Email
		, SpecialtyName				= sp.[Text]
		, SubscriptionType			= ISNULL(st.SubscriptionType, 'N/A')
		, Product					= REPLACE(ms.ProductId, '-Trial', '')
		, Subscription				= SubscriptionId
		, SubscriptionStartDate		= CONVERT(datetime, ms.StartDate, 131)
		, SubscriptionEndDate		= CONVERT(datetime, ms.EndDate, 131)
		, MemberType				= CASE 
										WHEN 		
												CHARINDEX('clinicaloptions', em.Email) > 0
											OR CHARINDEX('imedoptions.com', em.Email) > 0
											OR CHARINDEX('inpractice.com', em.Email) > 0
											THEN 'Internal'
										WHEN
											  CHARINDEX('idea.com', em.Email) > 0
											 OR CHARINDEX('velir.com', em.Email) > 0
											 OR CHARINDEX('tma.com', em.Email) > 0
											 THEN 'Vendor'
										WHEN 
											   ISNULL(m.FirstName, '') = 'CCO'
											OR ISNULL(m.LastName, '') = 'CCO'
											OR ISNULL(m.LastName, '') = 'User'
											OR CHARINDEX('account.com'	, ISNULL(m.LastName, '')) > 0
											OR CHARINDEX('test'			, ISNULL(m.LastName, '')) > 0
											OR CHARINDEX('test'			, ISNULL(m.FirstName, '')) > 0
											OR CHARINDEX('cco.com', em.Email) > 0
											OR CHARINDEX('test.com', em.Email) > 0
											OR CHARINDEX('testuser.com', em.Email) > 0
											OR LEFT(em.Email,3) = 'tst'
											OR LEFT(em.Email,4) = 'test'
											OR em.Email = 'sitemonitor'
											THEN 'Test-Account'
										WHEN LEN(ISNULL(m.FirstName, '')) > 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '')
											THEN 'Test-Account'
										WHEN LEN(ISNULL(m.FirstName, '')) = 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '') AND CHARINDEX('trial', ms.ProductId) > 0
											THEN 'Subscriber-RegLite'
										ELSE 'Subscriber'
										END
			, MemberRole				= ISNULL(fltr.MemberRole, 'Registered')
			, SubscriptionActiveInd		= CASE WHEN ms.EndDate > CAST(GetDate() as date) THEN 1 ELSE 0 END
		FROM [MemberCentralProd].[dbo].[SiteMember] sm (nolock)
--		JOIN MemberCentralProd.dbo.EcommerceMemberSubscription ms (nolock)
		JOIN 
		(
			SELECT ems.MemberId
			, SubscriptionId	= ISNULL(s.SubscriptionName, ems.SubscriptionId)
			, ProductId			= ISNULL(s.ProductUniqueId, ems.ProductId)
			, ems.StartDate
			, ems.EndDate
			, ems.SubscriptionType
			FROM MemberCentralProd.dbo.EcommerceMemberSubscription ems (nolock)
			LEFT JOIN MemberCentralProd.dbo.Subscription s (nolock)
				ON s.SubscriptionUniqueSitecoreId = ems.SubscriptionId
			WHERE 1=1
--				AND ems.EndDate >= CAST(GetDate() as DATE)
				AND ems.IsActive = 1
		) ms
			ON ms.MemberID = sm.MemberID
		LEFT JOIN MemberCentralProd.dbo.EmailMember em (nolock)
			ON em.MemberID = sm.MemberID
			AND em.IsActive = 1
			AND em.IsBadEmail = 0
		LEFT JOIN  MemberCentralProd.dbo.Member m (nolock)
			ON m.MemberId = sm.MemberId
			AND m.IsActive = 1
		LEFT JOIN MemberCentralProd.dbo.Specialty sp (nolock)
			On sp.SpecialtyID = m.SpecialtyID
		LEFT JOIN	MemberCentralProd.dbo.[Source] s (nolock)
			ON s.SourceID = m.SourceID
		-- Filter Test and Internal Accounts
		LEFT JOIN 
			(
				SELECT sm.MemberId
				, MemberRole		= tr.RoleName
				FROM dbo.SiteMember sm
				JOIN #TmpRole tr
					ON CHARINDEX(tr.RoleId, REPLACE(REPLACE(sm.SitecoreGroups, '{', ''), '}', ' ')) > 0
				WHERE 1=1
				GROUP BY sm.MemberId, tr.RoleName
			) fltr
			ON fltr.MemberID = sm.MemberID
		LEFT JOIN MemberCentralProd.dbo.SubscriptionType st (nolock)
			ON st.Id = ms.SubscriptionType
--			AND st.SubscriptionType <> 'Free'
		WHERE 1=1
--			AND ms.ProductId = @ProductName
			AND CHARINDEX(CASE WHEN @ProductName = '' THEN ms.ProductId ELSE @ProductName END, ms.ProductId) > 0 
			AND CHARINDEX(CASE WHEN @SpecialtyName = '' THEN sp.[Text] ELSE @SpecialtyName END, sp.[Text]) > 0 
			AND CHARINDEX(CASE WHEN @SubscriptionName = '' THEN ms.SubscriptionId ELSE @SubscriptionName END, ms.SubscriptionId) > 0 
			AND CHARINDEX(CASE WHEN @EmailAddress = '' THEN em.Email ELSE @EmailAddress END, em.Email) > 0 
			AND StartDate > @SubDate	
	) result
		WHERE 1=1
		ANd LEFT(result.MemberType, 1) = CASE WHEN @MemberTypeCode = '' THEN LEFT(result.MemberType, 1) ELSE @MemberTypeCode END
		AND result.MemberRole =  CASE WHEN @MemberRole = '' THEN result.MemberRole ELSE @MemberRole END
		AND result.SubscriptionActiveInd = @SubActiveInd
	ORDER BY Email, Product, Subscription
	
	DROP TABLE #TmpRole;
		
END


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[usp_MemberSubscription_SELECT] TO [iprdw-report]
    AS [dbo];

