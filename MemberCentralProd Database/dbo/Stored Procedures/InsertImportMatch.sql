﻿
-- ========================================================
-- Create InsertImportMatch
-- ========================================================

CREATE PROCEDURE [dbo].[InsertImportMatch] 
(
	@ImportFileID uniqueidentifier,
	@DuplicateCount smallint OUTPUT
)
AS

SET NOCOUNT ON

DECLARE @MatchColumns varchar(255), @MatchPoints smallint

CREATE TABLE [#ImportMatchColumns] (
	[ImportFileID] 		[uniqueidentifier],
	[ImportMemberID] 	[uniqueidentifier],
	[MemberID] 		[uniqueidentifier],
	[MatchColumns] 		[varchar] (255),
	[MatchValues]		[nvarchar] (255),
	[ImportValues]		[nvarchar] (255),
	[MatchPoints]		[smallint]
)

------------------------------------------------------------------------------------------------
-- first name, last name (lower possibility of match)
------------------------------------------------------------------------------------------------

SET @MatchColumns  = 'LastName;FirstName'
SET @MatchPoints = 10

INSERT INTO
	[#ImportMatchColumns]
SELECT 
	ImportFileID 	= im.ImportFileID, 
	ImportMemberID 	= im.ImportMemberID, 
	MemberID	= m.MemberID,
	MatchColumns 	= @MatchColumns,
	MatchValues 	= m.LastName + ';' + m.FirstName,
	ImportValues 	= im.LastName + ';' + im.FirstName,
	MatchPoints	= @MatchPoints
FROM 
	[ImportMember] im
INNER JOIN 
	[Member] m
ON 
	SOUNDEX(im.LastName) = SOUNDEX(m.LastName) AND SOUNDEX(im.FirstName) = SOUNDEX(m.FirstName)
WHERE
	im.ImportFileID = @ImportFileID
AND	im.IsDuplicate	= 0
AND	im.IsIgnored	= 0


------------------------------------------------------------------------------------------------
-- first name, last name, zip code (good possibility of match)
------------------------------------------------------------------------------------------------

SET @MatchColumns  = 'LastName;FirstName;ZipCode'
SET @MatchPoints = 30

INSERT INTO
	[#ImportMatchColumns]
SELECT 
	ImportFileID 	= im.ImportFileID, 
	ImportMemberID 	= im.ImportMemberID, 
	MemberID	= m.MemberID,
	MatchColumns 	= @MatchColumns,
	MatchValues 	= m.LastName + ';' + m.FirstName + ';' + mm.ZipCode,
	ImportValues 	= im.LastName + ';' + im.FirstName + ';' + imm.ZipCode,
	MatchPoints	= @MatchPoints
FROM 
	[ImportMember] im
INNER JOIN 
	[ImportMailMember] imm
ON 
	im.ImportMemberID = imm.ImportMemberID
INNER JOIN 
	[Member] m
ON 
	SOUNDEX(im.LastName)  = SOUNDEX(m.LastName) AND SOUNDEX(im.FirstName) = SOUNDEX(m.FirstName)
INNER JOIN 
	[MailMember] mm
ON 
	m.MemberID = mm.MemberID AND LEFT(imm.ZipCode, 5) = LEFT(mm.ZipCode, 5)
WHERE
	im.ImportFileID = @ImportFileID
AND	im.IsDuplicate	= 0
AND	im.IsIgnored	= 0


------------------------------------------------------------------------------------------------
-- username (good possibility of match)
------------------------------------------------------------------------------------------------

SET @MatchColumns  = 'UserName'
SET @MatchPoints = 30

INSERT INTO
	[#ImportMatchColumns]
SELECT 
	ImportFileID 	= ism.ImportFileID, 
	ImportMemberID 	= ism.ImportMemberID, 
	MemberID		= sm.MemberID,
	MatchColumns 	= @MatchColumns,
	MatchValues 	= sm.UserName,
	ImportValues 	= ism.UserName,
	MatchPoints		= @MatchPoints
FROM 
	[ImportMember] im
INNER JOIN
	[ImportSiteMember] ism
ON 
	im.ImportMemberID = ism.ImportMemberID
INNER JOIN 
	[SiteMember] sm
ON 
	ism.UserName = sm.UserName
WHERE
	ism.ImportFileID = @ImportFileID
AND	im.IsDuplicate	= 0
AND	im.IsIgnored	= 0


------------------------------------------------------------------------------------------------
-- email address (almost certain match)
------------------------------------------------------------------------------------------------

SET @MatchColumns  = 'Email'
SET @MatchPoints = 60

INSERT INTO
	[#ImportMatchColumns]
SELECT 
	ImportFileID 	= iem.ImportFileID, 
	ImportMemberID 	= iem.ImportMemberID, 
	MemberID	= em.MemberID,
	MatchColumns 	= @MatchColumns,
	MatchValues 	= em.Email,
	ImportValues 	= iem.Email,
	MatchPoints	= @MatchPoints
FROM 
	[ImportMember] im
INNER JOIN
	[ImportEmailMember] iem
ON 
	im.ImportMemberID = iem.ImportMemberID
INNER JOIN 
	[EmailMember] em
ON 
	iem.Email = em.Email
WHERE
	iem.ImportFileID = @ImportFileID
AND	im.IsDuplicate	= 0
AND	im.IsIgnored	= 0

-- for testing
--SELECT * FROM [#ImportMatchColumns] ORDER BY ImportFileID, ImportMemberID, MemberID, MatchColumns

-- insert new import match records
INSERT INTO
	[ImportMatch]
SELECT 
	ImportFileID 	= imc.ImportFileID,
	ImportMemberID	= imc.ImportMemberID,
	MemberID	= imc.MemberID,
	MatchPoints	= SUM(imc.MatchPoints),
	Created		= GETDATE()
FROM 
	[#ImportMatchColumns]  imc
WHERE NOT EXISTS	-- records can exist if re-match
(	SELECT
		* 
	FROM 
		[ImportMatch] im 
	WHERE 
		im.ImportFileID = imc.ImportFileID
	AND
		im.ImportMemberID = imc.ImportMemberID
	AND
		im.MemberID = imc.MemberID
)
GROUP BY 
	imc.ImportFileID, imc.ImportMemberID, imc.MemberID

-- update existing import match records (records can exist if re-match)
UPDATE 
	im
SET
	MatchPoints = x.MatchPoints
FROM 
	[ImportMatch] im INNER JOIN
(
SELECT 
	ImportFileID	= imc.ImportFileID,
	ImportMemberID	= imc.ImportMemberID,
	MemberID	= imc.MemberID,
	MatchPoints	= SUM(imc.MatchPoints)
FROM 
	[#ImportMatchColumns]  imc
GROUP BY 
	imc.ImportFileID, imc.ImportMemberID, imc.MemberID
) AS x
ON 
	im.ImportFileID = x.ImportFileID AND im.ImportMemberID = x.ImportMemberID AND im.MemberID = x.MemberID
WHERE
	im.ImportFileID = @ImportFileID

-- drop temporary table
DROP TABLE [#ImportMatchColumns]

-- flag potential duplicates
UPDATE 
	im
SET
	IsDuplicate = 1,
	IsReviewed = 0
FROM 
	[ImportMember] im 
INNER JOIN
(
SELECT 
	ImportFileID,
	ImportMemberID,
	MatchPoints
FROM
	ImportMatch
WHERE 
	MatchPoints > 30
AND
	ImportFileID = @ImportFileID
) AS x
ON 
	im.ImportFileID = x.ImportFileID AND im.ImportMemberID = x.ImportMemberID
WHERE
	im.ImportFileID 	= @ImportFileID
AND	im.IsDuplicate		= 0
AND	im.IsIgnored		= 0
AND	im.IsCopied		= 0

-- ignore 'Test' sitecore group members
UPDATE 
	im
SET
	IsDuplicate = 0,
	IsReviewed = 1,
	IsIgnored = 1
FROM 
	[ImportMember] im 
INNER JOIN
	[ImportEmailMember] iem
ON 
	im.ImportFileID = iem.ImportFileID AND im.ImportMemberID = iem.ImportMemberID
WHERE
	im.ImportFileID = @ImportFileID
AND
	iem.Email IN (
		SELECT
			em.Email
		FROM
			[EmailMember] em
		INNER JOIN
			[SiteMember] sm
		ON
			em.MemberID = sm.MemberID
		WHERE
			sm.SitecoreGroups LIKE '%056E3BA7-474A-435B-B9FA-271EDCE95A77%')

-- get number of potential duplicates
SET @DuplicateCount = (SELECT COUNT(*) FROM ImportMember WHERE ImportFileID = @ImportFileID AND IsReviewed = 0 AND IsIgnored = 0 AND IsDuplicate = 1)

SET NOCOUNT OFF


