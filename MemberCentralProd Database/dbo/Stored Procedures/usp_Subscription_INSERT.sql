﻿

CREATE PROCEDURE [dbo].[usp_Subscription_INSERT] 
AS 
	BEGIN
	SET NOCOUNT ON;
	

	-- SELECT * FROM [MemberCentralProd].[dbo].[Subscription] s

	CREATE TABLE #NewSubscriptions
	(
	  [SubscriptionUniqueSitecoreId] varchar(256)
	, [SubscriptionName] varchar(256)
	, [SiteCoreProductId] varchar(256)
	, [ProductUniqueId] varchar(256)
	)

	INSERT INTO #NewSubscriptions
	(
	SubscriptionUniqueSitecoreId
	, SubscriptionName
	, SiteCoreProductId
	, ProductUniqueId
	) 
	SELECT
		  [SubscriptionUniqueSitecoreId] = NewID()
		, [SubscriptionName] =  ss.[SubscriptionName]
		, [SiteCoreProductId] = NewID()
		, [ProductUniqueId] =  ss.[ProductUniqueId]
	 FROM
	 (
		SELECT DISTINCT
		[SubscriptionName] = ms.SubscriptionId
		, [ProductUniqueId] = ISNULL(ms.ProductUniqueId, ms.ProductId)
		FROM MemberCentralProd.dbo.EcommerceMemberSubscription ms
		LEFT JOIN [MemberCentralProd].[dbo].[Subscription] s
			ON s.SubscriptionUniqueSitecoreId = ms.SubscriptionId
			OR s.SubscriptionName = ms.SubscriptionId
		WHERE s.SubscriptionId IS NULL
	) ss


	INSERT INTO [MemberCentralProd].[dbo].[Subscription]
	(
	  SubscriptionUniqueSitecoreId
	, SubscriptionName
	, SiteCoreProductId
	, ProductUniqueId
	,[AccessType]
	, [Created]
	, [Updated]
	)
	SELECT 
	SubscriptionUniqueSitecoreId
	, SubscriptionName
	, SiteCoreProductId
	, ProductUniqueId
	, [AccessType] = 'Paid'
	, [Created] = CAST(GetDate() as date)
	, [Updated] = CAST(GetDate() as date)
	FROM #NewSubscriptions


	UPDATE s SET [AccessType] = 'Trial'
	-- SELECT SubscriptionName, [AccessType]
	FROM [MemberCentralProd].[dbo].[Subscription] s
	WHERE CHARINDEX('Trial', s.SubscriptionName) > 0
	AND [AccessType] <> 'Trial'

	-- SELECT * FROM [MemberCentralProd].[dbo].[Subscription] s

	DROP TABLE #NewSubscriptions
END
