﻿
CREATE PROCEDURE [dbo].[usp_MemberNewsletterSubscriptionImport_SELECT] AS
BEGIN
	CREATE TABLE #vwMemberNewsletterSubscriptionHeader
	 (
	 [OverWriteInd]		varchar(256)
	,[MemberGUID]		nvarchar(36)
	,[EmailAddress]		varchar(256)
	,[NewsLetterName]	varchar(256)
	,[ActiveInd]		varchar(256)
	,[RequestedDate]	varchar(256)
	,[CancelledDate]	varchar(256)
	,[ExportDate]		varchar(256)
	)

	CREATE TABLE #vwMemberNewsletterSubscriptionImport
	 (
	 [OverWriteInd]		varchar(256)
	,[MemberGUID]		varchar(36)
	,[EmailAddress]		varchar(256)
	,[NewsLetterName]	varchar(256)
	,[ActiveInd]		varchar(256)
	,[RequestedDate]	varchar(256)
	,[CancelledDate]	varchar(256)
	,[ExportDate]		varchar(256)
	)

	INSERT INTO #vwMemberNewsletterSubscriptionHeader
	( [OverWriteInd]	
	,[MemberGUID]		
	,[EmailAddress]		
	,[NewsLetterName]	
	,[ActiveInd]
	,[RequestedDate]
	,[CancelledDate]
	,[ExportDate]
	)
	SELECT
	 [OverWriteInd]		= 'OverWriteInd'
	,[MemberGUID]		= 'MemberGUID'
	,[EmailAddress]		= 'EmailAddress'
	,[NewsLetterName]	= 'NewsLetterName'
	,[ActiveInd]		= 'ActiveInd'
	,[RequestedDate]	= 'RequestedDate'
	,[CancelledDate]	= 'CancelledDate'
	,[ExportDate]		= 'ExportDate'
	;

	INSERT INTO #vwMemberNewsletterSubscriptionImport
	( [OverWriteInd]	
	,[MemberGUID]		
	,[EmailAddress]		
	,[NewsLetterName]	
	,[ActiveInd]
	,[RequestedDate]
	,[CancelledDate]
	,[ExportDate]
	)
	SELECT
	  OverWriteInd		=CAST(CASE WHEN RequestedDate > DATEADD(day, -1, CAST(GetDate() as date)) THEN 1 ELSE  0 END AS varchar(256))
	, MemberID
	, EmailAddress		= CAST(EmailAddress AS varchar(256))
	, NewsLetterName	= CAST(NewsLetterName AS varchar(256))
	, ActiveInd			= CAST(ActiveInd AS varchar(256))
	, RequestedDate		= CAST(REPLACE(convert(varchar(10), RequestedDate, 110), '-','/')  + ' ' + CAST(cast(RequestedDate as time) as varchar(5)) + CASE WHEN DATEPART(hour,RequestedDate) >= 12 THEN ' PM' ELSE ' AM' END AS varchar(256))
	, CancelledDate		= CAST(REPLACE(convert(varchar(10), CancelledDate, 110), '-','/')  + ' ' + CAST(cast(CancelledDate as time) as varchar(5)) + CASE WHEN DATEPART(hour,CancelledDate) >= 12 THEN ' PM' ELSE ' AM' END AS varchar(256))
	, [ExportDate]		= CAST(REPLACE(convert(varchar(10), GETDATE(), 110), '-','/')  + ' ' + CAST(cast(GETDATE() as time) as varchar(5)) + CASE WHEN DATEPART(hour,GETDATE()) >= 12 THEN ' PM' ELSE ' AM' END	AS varchar(256))
	FROM dbo.vwMemberNewsletterSubscription mns
	JOIN [dbo].[vwMemberImport] mv
		ON mv.[SourceMemberGUID] = mns.MemberId

	SELECT * FROM #vwMemberNewsletterSubscriptionHeader
	UNION ALL
	SELECT * FROM #vwMemberNewsletterSubscriptionImport;
	DROP TABLE #vwMemberNewsletterSubscriptionHeader;
	DROP TABLE #vwMemberNewsletterSubscriptionImport;
END
