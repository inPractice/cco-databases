﻿CREATE PROCEDURE [dbo].[usp_Member_Demographics_SELECT]
(
	@YearsBackInt	 int = -3
)
AS 
BEGIN
	SELECT 
		  [MemberId]				= m.[MemberId]
		, [EmailAddress]			= em.[Email]
		, [Specialty]				= ISNULL(rs.[Text], '')
		, [Degree]					= ISNULL(rdt.[Text], '')
		, [US/Non-US]				= CASE 
										WHEN ISNULL(c.[Abbreviation], '') = '' THEN 'Unknown' 
										WHEN c.[Abbreviation] = 'USA' THEN 'US' 
										ElSE 'Non-US' 
									END
		, [Country]					= ISNULL(c.[Text], '')
		, [USInd]				= CASE WHEN c.[Abbreviation] = 'USA' THEN 1 ELSE 0 END
		, [City/State]				= REPLACE(REPLACE(
										LTRIM(RTRIM(
										CASE WHEN LTRIM(ISNULL(mm.[City], ''))  = '' THEN 'Locale' ELSE RTRIM(mm.[City]) END
										+ CASE 
											WHEN LTRIM(ISNULL(mm.[City], ''))  = '' AND ISNULL(rsp.[Text],'') = '' THEN 'Locale' 
											WHEN ISNULL(rsp.[Text],'') = '' THEN ' / State' 
											ELSE ' / ' + LTRIM(rsp.[Text]) 
										END))
									, ' / State', ''), 'Locale', '')
		, [Age]						= DATEDIFF(year, [DateOfBirth], CONVERT(datetime, CONVERT(Date, GETDATE())))
		, [DateOfBirth]				= CONVERT(date, [DateOfBirth])
		, [SiteCode]					= LEFT(UPPER(rrs.[Text]), 3)
		, [ReferringSource]			= rrs.[Text]
		, [MemberType]				= CASE 
										WHEN 		
												CHARINDEX('clinicaloptions', em.[Email]) > 0
											OR CHARINDEX('imedoptions.com', em.[Email]) > 0
											OR CHARINDEX('inpractice.com', em.[Email]) > 0
											THEN 'Internal'
										WHEN
												CHARINDEX('idea.com', em.[Email]) > 0
												OR CHARINDEX('velir.com', em.[Email]) > 0
												OR CHARINDEX('tma.com', em.[Email]) > 0
												OR CHARINDEX('openpath.com', em.[Email]) > 0
												THEN 'Vendor'
										WHEN 
												CHARINDEX('account.com'	, ISNULL(m.LastName, '')) > 0
											OR ISNULL(m.[LastName], '') = 'test'
											OR ISNULL(m.[FirstName], '') = 'test'
											OR CHARINDEX('cco.com', em.[Email]) > 0
											OR CHARINDEX('test.com', em.[Email]) > 0
											OR CHARINDEX('testuser.com', em.[Email]) > 0
											OR em.[Email] = 'sitemonitor'
											THEN 'Test-Account'
										WHEN LEN(ISNULL(m.FirstName, '')) > 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '')
											THEN 'Test-Account'
		--									WHEN LEN(ISNULL(m.FirstName, '')) = 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '') AND CHARINDEX('trial', ms.ProductId) > 0
		--										THEN 'Subscriber-RegLite'
										ELSE 'Subscriber'
										END
		, [LastLoginDttm]			= ISNULL(s.[LastLoginDttm], '1901-01-01 00:00:00')
		, [InsertDt]				= CONVERT(date,m.[Created])
		, [InsertYear]				= YEAR(m.[Created])
		, [InsertMonth]				= MONTH(m.[Created])
		, [InsertWeek]				= DATEPART(wk ,m.[Created])
		FROM [dbo].[Member] m 
		JOIN
		(
			SELECT maxs.[MemberId]
			, [LastLoginDttm] = MAX(maxs.[LastLoginDttm])
			FROM
			(
				SELECT ss.[MemberId]
				, [LastLoginDttm] = MAX(ss.[Created])
				FROM [dbo].[Session] ss
				WHERE 1=1
					AND ss.[Created] >= DATEADD(year, @YearsBackInt, CONVERT(date, GetDate()))
				GROUP BY ss.[MemberId]
				UNION
				SELECT ss.[MemberId]
				, [LastLoginDttm] = MAX(ss.[Created])
				FROM [INP_Tracking].[dbo].[Session] ss
				WHERE 1=1
					AND ss.[Created] >= DATEADD(year, @YearsBackInt, CONVERT(date, GetDate()))
				GROUP BY ss.[MemberId]
				UNION
				SELECT ss.[MemberId]
				, [LastLoginDttm] = MAX(ss.[Created])
				FROM [IPA_Tracking].[dbo].[Session] ss
				WHERE 1=1
					AND ss.[Created] >= DATEADD(year, @YearsBackInt, CONVERT(date, GetDate()))
				GROUP BY ss.[MemberId]
			) maxs
			GROUP BY maxs.[MemberId]
		) s
			ON s.MemberId = m.MemberId
		JOIN [dbo].[EmailMember] em (nolock)
			ON em.[MemberId] = m.[MemberId]
		JOIN [dbo].[MailMember] mm (nolock)
			ON mm.[MemberId] = m.[MemberId]
		LEFT JOIN [dbo].[Specialty] rs (nolock)
			ON rs.[SpecialtyId] = m.[SpecialtyId]
		LEFT JOIN [dbo].[Degree] td (nolock)
			ON td.[DegreeId] = m.[DegreeId]
		LEFT JOIN [dbo].[DegreeType] rdt (nolock)
			ON rdt.[DegreeTypeId] = td.[DegreeTypeId]
		LEFT JOIN [dbo].[State] rsp (nolock)
			ON rsp.[StateId] = mm.[StateId]
		LEFT JOIN [dbo].[Country] c (nolock)
			ON c.[CountryId] = mm.[CountryId]
		LEFT JOIN [dbo].[Source] rrs (nolock)
			ON rrs.[SourceID] = m.[SourceID]
		WHERE 1=1
			AND s.[LastLoginDttm] >= DATEADD(year, @YearsBackInt, CONVERT(date, GetDate()))
		;
END