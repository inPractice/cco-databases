﻿CREATE PROCEDURE [dbo].[UpdateSessionLocation]
(
	@SessionID uniqueidentifier,
	@Location int
)
AS

UPDATE [Session]
SET
	Location = @Location,
	Updated = GETDATE()
WHERE SessionID = @SessionID


