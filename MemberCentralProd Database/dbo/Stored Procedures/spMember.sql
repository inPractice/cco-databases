﻿CREATE PROCEDURE [dbo].[spMember] AS
	BEGIN
		DECLARE @FileName varchar(128) = 'MemberSubscriptionImportAdd';
		DECLARE @LastExportDate datetime;
		SET @LastExportDate =
				(
				SELECT LastExportDate = MAX(EndDttm) 
				FROM IPRDW_SRC.dbo.tblDataFileTransferTracking 
				WHERE CHARINDEX(@FileName, [FileName] ) > 0
				GROUP BY [FileName]
				);
						
		CREATE TABLE #TmpRole
		(
			  RoleId varchar(96)
			, RoleName varchar(128)
		)
		;

		INSERT INTO #TmpRole(RoleId, RoleName) 
		SELECT Id, [Name] 
		FROM [MemberCentralProd].dbo.[Role]
		;

		SELECT
		  MemberID 				= m.MemberID
		, ActiveInd 			= m.IsActive
		, EmailAddress			= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(em.Email), '<', ''), '>', ''), ';', ''), '?', ''), '[', ''), ']', ''), '..',''), '`','')
		, EmailActiveInd		= em.IsActive
		, BadEmailInd 			= em.IsBadEmail
		, UserName 				= sm.UserName
		, Salutation 			= sal.[Text]
		, FirstName 			= m.FirstName
		, MiddleName 			= m.Middle
		, LastName 				= m.LastName
		, Suffix 				= m.Suffix
		, Phone 				= m.Phone
		, PhoneActiveInd		= m.IsPhoneActive
		, TextActiveInd			= m.IsTextActive
		, Fax 					= m.Fax
		, FaxActiveInd			= m.IsFaxActive
		, Affiliation 			= m.Affiliation
		, InstitutionName		= CAST('Unknown' as nvarchar(128))
		, DegreeName			= deg.[Text]  
		, DegreeTypeName		= dt.[Text]
		, SpecialtyName			= sp.[Text]
		, ProfessionName		= prof.[Text]
		, ProfessionTypeName	= proft.[Text]
		, SourceName			= src.[Text]
		, Address1 				= mm.Address1
		, Address2 				= mm.Address2
		, City 					= mm.City
		, StateName				= st.Abbreviation
		, ZipCode 				= mm.ZipCode
		, CountryCode			= cnt.Abbreviation
		, CountryName			= cnt.[Text]
		, USInd 				= cnt.IsUS
		, IndustryInd			= m.IsIndustry
		, DeceasedInd			= m.IsDeceased
		, BadAddressInd 		= mm.IsBadAddress
		, MailActiveInd			= mm.IsActive
		, EmailFrequencyName	= ISNULL(ef.ShortName, '?')
		, [DayOfWeekPreference]	= ISNULL(dow.[DayOfWeekPreference], 'Monday')
		, WhereHear				= wh.Abbreviation
		, JoinedMonth			= CAST(CAST(Year(m.Created) as varchar(4)) + '-' + CAST(Month(m.Created) as varchar(2)) + '-01' AS DATETIME)
		, H1BCountryName		= ''
		, m.[NABPePIDNumber]
		, m.[NPIRegistryNumber]
		, m.[FPDMemberNumber]
		, m.[NursingIDNumber]
		-- Company News Updates
		, ltr.[WantsCorpInd]	
		, ltr.[WantsINPInd] 	
		-- Specialty Newsletters
		, ltr.[WantsCardioInd] 
		, ltr.[WantsHemInd] 	
		, ltr.[WantsHepBInd]  
		, ltr.[WantsHepCInd]  
		, ltr.[WantsHIVInd]  	
		, ltr.[WantsNeuroInd] 
		, ltr.[WantsOncInd] 
		, ltr.[WantsRheumInd]
		, ltr.[WantsTransInd]
		, ltr.[WantsUroInd]  
		-- Daily Newsletters
		, ltr.[WantsMonInd]  
		, ltr.[WantsTueInd]  
		, ltr.[WantsWedInd]  
		, ltr.[WantsThuInd]  
		, ltr.[WantsFriInd]  
		, ltr.[WantsSatInd]  
		, ltr.[WantsSunInd]  
		, LastModified 		= ISNULL(m.Updated, m.Created)
		, MemberType	= CASE 
							WHEN fltr.MemberRole = 'Test'
								THEN 'Test-Account'
							WHEN fltr.MemberRole = 'Employee'
								THEN 'Employee'
							WHEN 		
								   CHARINDEX('clinicaloptions', em.Email) > 0
								OR CHARINDEX('imedoptions.com', em.Email) > 0
								OR CHARINDEX('inpractice.com', em.Email) > 0
								THEN 'Internal'
							WHEN
									CHARINDEX('idea.com', em.Email) > 0
								 OR CHARINDEX('velir.com', em.Email) > 0
								 OR CHARINDEX('tma.com', em.Email) > 0
								 OR CHARINDEX('dynagility.com', em.Email) > 0
								 OR CHARINDEX('openpathproducts.com', em.Email) > 0
								 THEN 'Vendor'
							WHEN 
								   CHARINDEX('@cco.com', em.Email) > 0
								OR CHARINDEX('@test.com', em.Email) > 0
								OR CHARINDEX('testuser.com', em.Email) > 0
	--									OR LEFT(em.Email,3) = 'tst'
	--									OR LEFT(em.Email,4) = 'test'
								OR em.Email = 'sitemonitor'
								THEN 'Flagged-Email'
							WHEN CHARINDEX('account.com'	, ISNULL(m.LastName, '')) > 0
								OR (ISNULL(m.LastName, '')) = 'test'
								OR CHARINDEX('test'			, ISNULL(m.LastName, '')) > 0 AND CHARINDEX(m.LastName, LEFT(em.Email, CHARINDEX('@', em.Email))) = 0
								OR CHARINDEX('test'			, ISNULL(m.FirstName, '')) > 0
								OR LEN(ISNULL(m.FirstName, '')) > 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '') 
								THEN 'Flagged-Name'
							WHEN 	ISNULL(m.FirstName, '') = 'CCO' AND ISNULL(m.LastName, '') = 'User' AND CHARINDEX('clinicaloptions.com', em.Email) = 0 AND CHARINDEX('inpractice.com', em.Email) = 0
								THEN 'Subscriber'
							ELSE 'Subscriber'
						END
		, Created 		= m.Created 
		, Updated 		= m.Updated
		FROM [dbo].[Member] m (nolock)
		LEFT JOIN 
		(
			SELECT sm.MemberId
			, MemberRole		= tr.RoleName
			FROM [MemberCentralProd].dbo.SiteMember sm
			JOIN #TmpRole tr
				ON CHARINDEX(tr.RoleId, REPLACE(REPLACE(sm.SitecoreGroups, '{', ''), '}', ' ')) > 0
			WHERE 1=1
			AND tr.RoleName <> 'AdminUser'
			GROUP BY sm.MemberId, tr.RoleName
		) fltr
			ON fltr.MemberID = m.MemberID
		LEFT  JOIN Salutation sal (nolock)
			ON sal.salutationid = m.salutationid
		LEFT  JOIN	Degree deg (nolock)
			ON deg.DegreeID = m.DegreeID
		LEFT  JOIN	DegreeType dt (nolock)
			ON dt.DegreeTypeID = deg.DegreeTypeID
		LEFT  JOIN Specialty sp (nolock)
			ON sp.SpecialtyID = m.SpecialtyID
		LEFT  JOIN Profession prof (nolock)
			ON prof.professionid = m.professionid
		LEFT  JOIN ProfessionType proft (nolock)
			ON proft.professiontypeid = prof.professiontypeid
		JOIN Source src (nolock)
			ON src.sourceid = m.sourceid
		LEFT  JOIN MailMember mm (nolock)
			ON mm.memberid = m.memberid
		LEFT  JOIN State st (nolock)
			ON st.StateID = mm.StateID
		LEFT  JOIN Country cnt (nolock)
			ON cnt.CountryID = mm.CountryID
		LEFT  JOIN EmailMember em (nolock)
			ON em.memberid = m.memberid
			AND CHARINDEX('[', em.Email) = 0
			AND CHARINDEX(']', em.Email) = 0
		LEFT  JOIN Emailfrequency ef (nolock)
			ON em.emailfrequencyid = ef.emailfrequencyid
		LEFT  JOIN SiteMember sm (nolock)
			ON sm.memberid = m.memberid
			AND sm.SitecoreGroups IS NULL
		LEFT  JOIN whereHearType wh (nolock)
			ON wh.whereHearTypeiD = sm.whereHearTypeID
		LEFT JOIN
		(
			SELECT MemberGUID			= emnl.MemberID
			, [DayOfWeekPreference]		= enl.[Text]
			FROM dbo.EmailNewsletter enl (nolock)
			JOIN dbo.EmailMemberNewsletter emnl (nolock)
				ON emnl.EmailNewsletterID = enl.EmailNewsletterID
			WHERE emnl.IsActive = 1
				AND LEFT(enl.[Text] , 3) IN ('Mon','Tue','Wed','Thu','Fri','Sat','Sun') 
		) dow
			ON dow.MemberGUID	= m.MemberID
		LEFT JOIN
		(
			SELECT 
			  MemberGUID		= NL.MemberID
			-- Company News
			, [WantsCorpInd]		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsCorp' THEN 1 ELSE 0 END) as bit)
			, [WantsINPInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsINP' THEN 1 ELSE 0 END) as bit)
			---- Specialty Newsletters
			, [WantsCardioInd]  	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsCardio' THEN 1 ELSE 0 END) as bit)
			, [WantsHemInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHem' THEN 1 ELSE 0 END) as bit)
			, [WantsHepBInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHepB' THEN 1 ELSE 0 END) as bit)
			, [WantsHepCInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHepC' THEN 1 ELSE 0 END) as bit)
			, [WantsHIVInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHIV' THEN 1 ELSE 0 END) as bit)
			, [WantsNeuroInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsNeuro' THEN 1 ELSE 0 END) as bit)
			, [WantsOncInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsOnc' THEN 1 ELSE 0 END) as bit)
			, [WantsOncNursInd] 	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsOncNurs' THEN 1 ELSE 0 END) as bit)
			, [WantsRheumInd]  	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsOnc' THEN 1 ELSE 0 END) as bit)
			, [WantsTransInd]  	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsTrans' THEN 1 ELSE 0 END) as bit)
			, [WantsUroInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsUro' THEN 1 ELSE 0 END) as bit)
			---- Daily Newsletters
			, [WantsMonInd]  		= 0
			, [WantsTueInd]  		= 0
			, [WantsWedInd]  		= 0
			, [WantsThuInd]  		= 0
			, [WantsFriInd]  		= 0
			, [WantsSatInd]  		= 0
			, [WantsSunInd]  		= 0

			FROM
			(
			SELECT emnl.MemberID
			, NewsLetterName		= enl.[Text]
			, NewsLetterInicator	=	'Wants' + CASE LEFT(enl.[Text] , 3)
											WHEN 'Car' THEN 'Cardio'
											WHEN 'Cor' THEN 'Corp'
											WHEN 'Hem' THEN 'Hem'
											WHEN 'Onc' THEN 
												CASE WHEN CHARINDEX('Nurse', enl.[Text]) > 0
													THEN 'OncNurse'
													ELSE 'Onc'
												END
											WHEN 'Hep' 
												THEN 
												CASE 
													WHEN Right(enl.[Text], 1) = 'B' THEN 'HepB'
													WHEN Right(enl.[Text], 1) = 'C' THEN 'HepC'
													ELSE '??'
												END
											WHEN 'HIV' THEN 'HIV'
											WHEN 'inP' THEN 'inPracticeUpdate'
											WHEN 'Neu' THEN 'Neuro'
											WHEN 'Pro' THEN 'Prostate'
											WHEN 'Rhe' THEN 'Rheum'
											WHEN 'Tra' THEN 'Trans'
											WHEN 'Uro' THEN 'Uro'
										ELSE LEFT(enl.[Text], 3)
										END
			, ActiveInd				= 1
			FROM dbo.EmailNewsletter enl (nolock)
			JOIN dbo.EmailMemberNewsletter emnl (nolock)
				ON emnl.EmailNewsletterID = enl.EmailNewsletterID
			WHERE emnl.IsActive = 1
			) NL
			GROUP BY NL.MemberID
		) ltr
			ON ltr.MemberGUID	= m.MemberID
		WHERE 1=1
END


