﻿CREATE PROCEDURE [dbo].[InsertEvaluationResultQuestionAnswer] (
  @evaluationResultID uniqueidentifier,
  @questionID uniqueidentifier,
  @questionAnswerID uniqueidentifier,
  @freeTextAnswer ntext = NULL
)
AS

INSERT INTO EvaluationResultQuestionAnswer
(EvaluationResultID, QuestionID, QuestionAnswerID, FreeTextAnswer)
VALUES
(@evaluationResultID, @questionID, @questionAnswerID, @freeTextAnswer)