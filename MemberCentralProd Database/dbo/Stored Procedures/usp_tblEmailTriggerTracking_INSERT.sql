﻿
CREATE PROCEDURE [dbo].[usp_tblEmailTriggerTracking_INSERT]
(
	@RunTime time = '12:58:00'
) 
AS 
	BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CurrentTime time = CAST(GetDate() AS TIME);
	DECLARE @CurrentDate datetime = CASE WHEN @CurrentTime > @RunTime THEN DAteAdd(day, 1, CAST(GetDate() as date)) ELSE CAST(GetDate() as date) END;

	
	INSERT INTO inPracticeMarketing.dbo.tblEmailTriggerTracking
	(
	  MemberId
	, EmailAddress
	, TriggerId
	, MemberSubscriptionId
	, FireOnDttm
	, CreatedDttm
	)
	
	SELECT DISTINCT
	  MemberId				= ems.MemberId
	, EmailAddress			= em.Email
	, TriggerId				= et.TriggerId
	, MemberSubscriptionId	= ems.SubscriptionId
	, FireOnDttm			= CASE 
								WHEN et.[PostSubscriptionInd] = 1 OR et.PostRegistrationInd = 1
									THEN (DATEADD(Day, et.TriggerDateDayOffset, ems.StartDate))
								WHEN et.PreExpirationInd = 1 OR et.PostExpirationInd = 1
									THEN DATEADD(Day, et.TriggerDateDayOffset, ems.EndDate)
							END
	, CreatedDttm			= GetDate()
-- 		FROM MemberCentralProd.dbo.EcommerceMemberSubscription ms (nolock)
	FROM 
	(
		SELECT ms.MemberId
		, SubscriptionId	= ISNULL(s.SubscriptionName, ms.SubscriptionId)
		, ProductId			= ISNULL(s.ProductUniqueId, ms.ProductId)
		, ms.StartDate
		, ms.EndDate
		FROM MemberCentralProd.dbo.EcommerceMemberSubscription ms (nolock)
		LEFT JOIN MemberCentralProd.dbo.Subscription s (nolock)
			ON s.SubscriptionUniqueSitecoreId = ms.SubscriptionId
		WHERE ms.EndDate >= CAST(GetDate() as DATE)
	) ems
	JOIN inPracticeMarketing.dbo.tblEmailTrigger et (nolock)
		ON ems.ProductId		= et.ProductId
		AND ems.SubscriptionId	= et.SubscriptionId
		AND et.ActiveInd		= 1
		AND (
			CASE 
				WHEN et.[PostSubscriptionInd] = 1 OR et.PostRegistrationInd = 1
					THEN (DATEADD(Day, et.TriggerDateDayOffset, ems.StartDate))
				WHEN et.PreExpirationInd = 1 OR et.PostExpirationInd = 1
					THEN DATEADD(Day, et.TriggerDateDayOffset, ems.EndDate)
			END >= @CurrentDate
			)
	JOIN MemberCentralProd.dbo.Member m (nolock)
		ON m.MemberID			= ems.MemberID
		AND m.IsActive			= 1
		AND et.ActiveInd		= 1
	JOIN MemberCentralProd.dbo.EmailMember em (nolock)
		ON em.MemberID			= m.MemberID
		AND em.IsActive			= 1
		AND em.IsBadEmail		= 0
	LEFT JOIN 
	(
		SELECT
		  s.MemberId
		, VisitCount	= Count(s.SessionId)
		FROM MemberCentralProd.dbo.[Session] s (nolock)  
		JOIN MemberCentralProd.dbo.EcommerceMemberSubscription ems (nolock)
			ON ems.MemberId		= s.MemberId
			AND ems.IsActive	= 1
		GROUP BY s.MemberID
	) visit
		ON visit.MemberId		= ems.MemberId
		AND ISNULL(et.VisitCount, 0) <= ISNULL(visit.VisitCount, 0)
		AND et.ActiveInd		= 1
	LEFT JOIN inPracticeMarketing.dbo.tblEmailTriggerTracking ett
		ON  ett.MemberId			= ems.MemberID
		AND ett.TriggerId		= et.TriggerId
		AND ett.FireOnDttm		= CASE 
								WHEN et.[PostSubscriptionInd] = 1 OR et.PostRegistrationInd = 1
									THEN (DATEADD(Day, et.TriggerDateDayOffset, ems.StartDate))
								WHEN et.PreExpirationInd = 1 OR et.PostExpirationInd = 1
									THEN DATEADD(Day, et.TriggerDateDayOffset, ems.EndDate)
							END
	WHERE 1=1
		AND ett.EmailTriggerTrackingID IS NULL
		AND et.ActiveInd		= 1
	;
END

