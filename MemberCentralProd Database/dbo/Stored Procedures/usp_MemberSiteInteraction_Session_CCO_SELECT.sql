﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_Session_CCO_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN
	SELECT 
	 [SiteCode]
	,[MemberGUId] 		
	,[SessionGUId] 		
	,[URL]              
	,[SessionStartDttm] 	
	,[SessionYear] 		
	,[SessionMonth] 	
	,[SessionWeek]
	FROM
	(
		SELECT [SiteCode]		= 'CCO'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionID]
		,	[DeviceTypeName]    = ''
		,	[OSVersionName]     = ''
		,	[URL]               = s.[Referrer]
		,	[SessionStartDttm] 	= s.[Created]
		,	[SessionYear] 		= YEAR(s.[Created])
		,	[SessionMonth] 		= MONTH(s.[Created])
		,	[SessionWeek]		= DATEPART(wk ,s.[Created])
		FROM [dbo].[Session] s
		WHERE s.[Created] >= @Dttm
	) ss
	;
END
;