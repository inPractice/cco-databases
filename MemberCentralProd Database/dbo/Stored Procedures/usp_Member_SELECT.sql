﻿
CREATE PROCEDURE [dbo].[usp_Member_SELECT] 
(
	@MemberId uniqueidentifier = '00000000-0000-0000-0000-000000000000'
	, @UserName varchar(128) = ''
	, @EmailAddress varchar(128) = ''
)
AS 
BEGIN
	CREATE TABLE #TmpRole
	(
	RoleId varchar(96)
	, RoleName varchar(128)
	)
	;
	CREATE TABLE #TmpMember
	(
		MemberId uniqueidentifier 
	);

	INSERT INTO #TmpRole(RoleId, RoleName) SELECT Id, [Name] FROM dbo.[Role];

	INSERT #TmpMember(MemberId) 
	SELECT DISTINCT m.MemberId 
	FROM dbo.Member m
	LEFT JOIN dbo.SiteMember sm
		ON sm.MemberID = m.MemberId
	LEFT JOIN dbo.EmailMember em
		ON em.MemberId = m.MemberId
	WHERE 1=1
		AND m.MemberId = CASE WHEN @MemberId <> '00000000-0000-0000-0000-000000000000' THEN @MemberId ELSE m.MemberID END
		AND ISNULL(sm.UserName, '') = CASE WHEN @UserName <> '' THEN @UserName ELSE ISNULL(sm.UserName, '') END
		AND ISNULL(em.Email, '') = CASE WHEN @EmailAddress <> '' THEN @EmailAddress ELSE ISNULL(em.Email, '') END
	;


	SELECT
	  MemberID 				= m.MemberID
	, ActiveInd 			= m.IsActive
	, EmailAddress			= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(em.Email), '<', ''), '>', ''), ';', ''), '?', ''), '[', ''), ']', ''), '..',''), '`','')
	, EmailActiveInd		= ISNULL(em.IsActive  , '')
	, BadEmailInd 			= ISNULL(em.IsBadEmail  , '')
	, UserName 				= ISNULL(sm.UserName  , '')
	, Salutation 			= ISNULL(sal.[Text]  , '')
	, FirstName 			= ISNULL(m.FirstName  , '')
	, MiddleName 			= ISNULL(m.Middle  , '')
	, LastName 				= ISNULL(m.LastName  , '')
	, Suffix 				= ISNULL(m.Suffix  , '')
	, Phone 				= ISNULL(m.Phone  , '')
	, PhoneActiveInd		= ISNULL(m.IsPhoneActive  , '')
	, TextActiveInd			= ISNULL(m.IsTextActive  , '')
	, Fax 					= ISNULL(m.Fax  , '')
	, FaxActiveInd			= ISNULL(m.IsFaxActive  , '')
	, Affiliation 			= ISNULL(m.Affiliation  , '')
	, InstitutionName		= CAST('Unknown' as nvarchar(128))
	, DegreeName			= ISNULL(deg.[Text]  , '')
	, DegreeTypeName		= ISNULL(dt.[Text]  , '')
	, SpecialtyName			= ISNULL(sp.[Text]  , '')
	, ProfessionName		= ISNULL(prof.[Text]  , '')
	, ProfessionTypeName	= ISNULL(proft.[Text]  , '')
	, SourceName			= ISNULL(src.[Text]  , '')
	, Address1 				= ISNULL(mm.Address1  , '')
	, Address2 				= ISNULL(mm.Address2  , '')
	, City 					= ISNULL(mm.City  , '')
	, StateName				= ISNULL(st.Abbreviation  , '')
	, ZipCode 				= ISNULL(mm.ZipCode  , '')
	, CountryCode			= ISNULL(cnt.Abbreviation  , '')
	, CountryName			= ISNULL(cnt.[Text]  , '')
	, USInd 				= ISNULL(cnt.IsUS  , '')
	, IndustryInd			= m.IsIndustry
	, DeceasedInd			= m.IsDeceased
	, BadAddressInd 		= mm.IsBadAddress
	, MailActiveInd			= mm.IsActive
	, EmailFrequencyName	= ISNULL(ef.ShortName, '?')
	, [DayOfWeekPreference]	= ISNULL(dow.[DayOfWeekPreference], 'Monday')
	, WhereHear				= wh.Abbreviation
	, JoinedMonth			= CAST(CAST(Year(m.Created) as varchar(4)) + '-' + CAST(Month(m.Created) as varchar(2)) + '-01' AS DATETIME)
	, H1BCountryName		= ''
	, m.[NABPePIDNumber]
	, m.[NPIRegistryNumber]
	, m.[FPDMemberNumber]
	, m.[NursingIDNumber]
	-- Company News Updates
	, ltr.[WantsCorpInd]	
	, ltr.[WantsINPInd] 	
	-- Specialty Newsletters
	, ltr.[WantsCardioInd] 
	, ltr.[WantsHemInd] 	
	, ltr.[WantsHepBInd]  
	, ltr.[WantsHepCInd]  
	, ltr.[WantsHIVInd]  	
	, ltr.[WantsNeuroInd] 
	, ltr.[WantsOncInd] 
	, ltr.[WantsRheumInd]
	, ltr.[WantsTransInd]
	, ltr.[WantsUroInd]  
	-- Daily Newsletters
	, ltr.[WantsMonInd]  
	, ltr.[WantsTueInd]  
	, ltr.[WantsWedInd]  
	, ltr.[WantsThuInd]  
	, ltr.[WantsFriInd]  
	, ltr.[WantsSatInd]  
	, ltr.[WantsSunInd]  
	, LastModified 		= ISNULL(m.Updated, m.Created)
	, MemberType				= CASE 
									WHEN 		
											CHARINDEX('clinicaloptions', em.Email) > 0
										OR CHARINDEX('imedoptions.com', em.Email) > 0
										OR CHARINDEX('inpractice.com', em.Email) > 0
										THEN 'Internal'
									WHEN
										  CHARINDEX('idea.com', em.Email) > 0
										 OR CHARINDEX('velir.com', em.Email) > 0
										 OR CHARINDEX('tma.com', em.Email) > 0
										 THEN 'Vendor'
									WHEN 
										   ISNULL(m.FirstName, '') = 'CCO'
										OR ISNULL(m.LastName, '') = 'CCO'
										OR ISNULL(m.LastName, '') = 'User'
										OR CHARINDEX('account.com'	, ISNULL(m.LastName, '')) > 0
										OR CHARINDEX('test'			, ISNULL(m.LastName, '')) > 0
										OR CHARINDEX('test'			, ISNULL(m.FirstName, '')) > 0
										OR CHARINDEX('cco.com', em.Email) > 0
										OR CHARINDEX('test.com', em.Email) > 0
										OR CHARINDEX('testuser.com', em.Email) > 0
										OR LEFT(em.Email,3) = 'tst'
										OR LEFT(em.Email,4) = 'test'
										OR em.Email = 'sitemonitor'
										THEN 'Test-Account'
									WHEN LEN(ISNULL(m.FirstName, '')) > 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '')
										THEN 'Test-Account'
--									WHEN LEN(ISNULL(m.FirstName, '')) = 0 AND ISNULL(m.FirstName, '') = ISNULL(m.Lastname, '') AND CHARINDEX('trial', ms.ProductId) > 0
--										THEN 'Subscriber-RegLite'
									ELSE 'Subscriber'
									END
	, MemberRole	= ISNULL(fltr.MemberRole, '')
	, Created 		= m.Created 
	, Updated 		= m.Updated
	, LastModified	= CASE 
						WHEN ISNULL( m.Updated, '1901-01-01') > ISNULL(sm.Updated, '1901-01-01') AND ISNULL( m.Updated, '1901-01-01') > ISNULL(em.Updated, '1901-01-01') AND ISNULL( m.Updated, '1901-01-01') > ISNULL(mm.Updated, '1901-01-01') THEN ISNULL( m.Updated, '1901-01-01')
						WHEN ISNULL(em.Updated, '1901-01-01') > ISNULL( m.Updated, '1901-01-01') AND ISNULL(em.Updated, '1901-01-01') > ISNULL(sm.Updated, '1901-01-01') AND ISNULL(em.Updated, '1901-01-01') > ISNULL(mm.Updated, '1901-01-01') THEN ISNULL(em.Updated, '1901-01-01')
						WHEN ISNULL(sm.Updated, '1901-01-01') > ISNULL( m.Updated, '1901-01-01') AND ISNULL(sm.Updated, '1901-01-01') > ISNULL(em.Updated, '1901-01-01') AND ISNULL(sm.Updated, '1901-01-01') > ISNULL(mm.Updated, '1901-01-01') THEN ISNULL(sm.Updated, '1901-01-01')
						WHEN ISNULL(mm.Updated, '1901-01-01') > ISNULL( m.Updated, '1901-01-01') AND ISNULL(mm.Updated, '1901-01-01') > ISNULL(em.Updated, '1901-01-01') AND ISNULL(mm.Updated, '1901-01-01') > ISNULL(sm.Updated, '1901-01-01') THEN ISNULL(mm.Updated, '1901-01-01')
						ELSE ISNULL(m.Created, '1901-01-01')
					END
	FROM #TmpMember tm
	JOIN [dbo].[Member] m (nolock)
		ON m.MemberID = tm.MemberId
	LEFT  JOIN Salutation sal (nolock)
		ON sal.salutationid = m.salutationid
	LEFT  JOIN	Degree deg (nolock)
		ON deg.DegreeID = m.DegreeID
	LEFT  JOIN	DegreeType dt (nolock)
		ON dt.DegreeTypeID = deg.DegreeTypeID
	LEFT  JOIN Specialty sp (nolock)
		ON sp.SpecialtyID = m.SpecialtyID
	LEFT  JOIN Profession prof (nolock)
		ON prof.professionid = m.professionid
	LEFT  JOIN ProfessionType proft (nolock)
		ON proft.professiontypeid = prof.professiontypeid
	JOIN Source src (nolock)
		ON src.sourceid = m.sourceid
	LEFT  JOIN MailMember mm (nolock)
		ON mm.memberid = m.memberid
	LEFT  JOIN State st (nolock)
		ON st.StateID = mm.StateID
	LEFT  JOIN Country cnt (nolock)
		ON cnt.CountryID = mm.CountryID
	LEFT  JOIN EmailMember em (nolock)
		ON em.memberid = m.memberid
		AND CHARINDEX('[', em.Email) = 0
		AND CHARINDEX(']', em.Email) = 0
	LEFT  JOIN Emailfrequency ef (nolock)
		ON em.emailfrequencyid = ef.emailfrequencyid
	LEFT  JOIN SiteMember sm (nolock)
		ON sm.memberid = m.memberid
		AND sm.SitecoreGroups IS NULL
	LEFT  JOIN whereHearType wh (nolock)
		ON wh.whereHearTypeiD = sm.whereHearTypeID
	-- Member Email Day Of Week Preference
	LEFT JOIN
	(
		SELECT MemberGUID			= emnl.MemberID
		, [DayOfWeekPreference]		= enl.[Text]
		FROM dbo.EmailNewsletter enl (nolock)
		JOIN dbo.EmailMemberNewsletter emnl (nolock)
			ON emnl.EmailNewsletterID = enl.EmailNewsletterID
		WHERE emnl.IsActive = 1
			AND LEFT(enl.[Text] , 3) IN ('Mon','Tue','Wed','Thu','Fri','Sat','Sun') 
	) dow
		ON dow.MemberGUID	= m.MemberID
	--Member Newsletter Subscriptions
	LEFT JOIN
	(
		SELECT 
		  MemberGUID		= NL.MemberID
		-- Company News
		, [WantsCorpInd]		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsCorp' THEN 1 ELSE 0 END) as bit)
		, [WantsINPInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsINP' THEN 1 ELSE 0 END) as bit)
		---- Specialty Newsletters
		, [WantsCardioInd]  	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsCardio' THEN 1 ELSE 0 END) as bit)
		, [WantsHemInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHem' THEN 1 ELSE 0 END) as bit)
		, [WantsHepBInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHepB' THEN 1 ELSE 0 END) as bit)
		, [WantsHepCInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHepC' THEN 1 ELSE 0 END) as bit)
		, [WantsHIVInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsHIV' THEN 1 ELSE 0 END) as bit)
		, [WantsNeuroInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsNeuro' THEN 1 ELSE 0 END) as bit)
		, [WantsOncInd] 		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsOnc' THEN 1 ELSE 0 END) as bit)
		, [WantsOncNursInd] 	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsOncNurs' THEN 1 ELSE 0 END) as bit)
		, [WantsRheumInd]  	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsOnc' THEN 1 ELSE 0 END) as bit)
		, [WantsTransInd]  	= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsTrans' THEN 1 ELSE 0 END) as bit)
		, [WantsUroInd]  		= CAST(SUM(CASE WHEN NL.NewsLetterInicator = 'WantsUro' THEN 1 ELSE 0 END) as bit)
		---- Daily Newsletters
		, [WantsMonInd]  		= 0
		, [WantsTueInd]  		= 0
		, [WantsWedInd]  		= 0
		, [WantsThuInd]  		= 0
		, [WantsFriInd]  		= 0
		, [WantsSatInd]  		= 0
		, [WantsSunInd]  		= 0

		FROM
		(
		SELECT emnl.MemberID
		, NewsLetterName		= enl.[Text]
		, NewsLetterInicator	=	'Wants' + CASE LEFT(enl.[Text] , 3)
										WHEN 'Car' THEN 'Cardio'
										WHEN 'Cor' THEN 'Corp'
										WHEN 'Hem' THEN 'Hem'
										WHEN 'Onc' THEN 
											CASE WHEN CHARINDEX('Nurse', enl.[Text]) > 0
												THEN 'OncNurse'
												ELSE 'Onc'
											END
										WHEN 'Hep' 
											THEN 
											CASE 
												WHEN Right(enl.[Text], 1) = 'B' THEN 'HepB'
												WHEN Right(enl.[Text], 1) = 'C' THEN 'HepC'
												ELSE '??'
											END
										WHEN 'HIV' THEN 'HIV'
										WHEN 'inP' THEN 'inPracticeUpdate'
										WHEN 'Neu' THEN 'Neuro'
										WHEN 'Pro' THEN 'Prostate'
										WHEN 'Rhe' THEN 'Rheum'
										WHEN 'Tra' THEN 'Trans'
										WHEN 'Uro' THEN 'Uro'
									ELSE LEFT(enl.[Text], 3)
									END
		, ActiveInd				= 1
		FROM dbo.EmailNewsletter enl (nolock)
		JOIN dbo.EmailMemberNewsletter emnl (nolock)
			ON emnl.EmailNewsletterID = enl.EmailNewsletterID
		WHERE emnl.IsActive = 1
		) NL
		GROUP BY NL.MemberID
	) ltr
		ON ltr.MemberGUID	= m.MemberID
		-- Filter Test and Internal Accounts
	LEFT JOIN 
		(
			SELECT sm.MemberId
			, MemberRole		= ISNULL(tr.RoleName, '')
			FROM dbo.SiteMember sm
			JOIN #TmpRole tr
				ON CHARINDEX(tr.RoleId, REPLACE(REPLACE(sm.SitecoreGroups, '{', ''), '}', ' ')) > 0
			WHERE 1=1
			AND tr.RoleName IS NOT NULL
			GROUP BY sm.MemberId, tr.RoleName
		) fltr
		ON fltr.MemberID = sm.MemberID
	WHERE 1=1
	;

	DROP TABLE #TmpRole
	DROP TABLE #TmpMember

END

