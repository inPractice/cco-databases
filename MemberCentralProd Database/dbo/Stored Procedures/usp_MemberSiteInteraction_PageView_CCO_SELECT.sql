﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_PageView_CCO_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
	 [SiteCode]		
	,[MemberGUId] 	
	,[SessionGUId] 	
	,[PageViewGUID]	
	,[PageItemGUID]	
	,[URL]			
	,[ActivityStartDttm] 
	,[SessionStartDttm]
	,[ActivityYear] 	
	,[ActivityMonth] 
	,[ActivityWeek]
	FROM
	(
		SELECT DISTINCT
		 [SiteCode]		= 'CCO'
		,[MemberGUId] 		= s.[MemberID]
		,[SessionGUId] 		= s.[SessionID]
		,[PageViewGUID]		= tp.[PropertyID]
		,[PageItemGUID]		= tp.[PropertyID]
		,[URL]				= pv.[Referrer]
		,[ActivityStartDttm] 	= tp.[Created]
		,[SessionStartDttm] 	= s.[Created]
		,[ActivityYear] 		= YEAR(tp.[Created])
		,[ActivityMonth] 		= MONTH(tp.[Created])
		,[ActivityWeek]		= DATEPART(wk ,tp.[Created])
		FROM [dbo].[Session] s
		JOIN [dbo].[PropertyView] pv
			ON pv.[SessionID] = s.[SessionID]
			AND s.[MemberID] IS NOT NULL
		  JOIN [dbo].[TrackedProperty] tp
			ON tp.[TrackedPropertyID] = pv.[TrackedPropertyID]
		WHERE s.[Created] >= @Dttm
			AND s.[MemberID] IS NOT NULL

		UNION ALL
		SELECT DISTINCT
		 [SiteCode]		= 'CCO'
		,[MemberGUId] 		= s.[MemberID]
		,[SessionGUId] 		= s.[SessionID]
		,[PageViewGUID]		= tp.[FeatureID]
		,[PageItemGUID]		= tp.[FeatureID]
		,[URL]				= tp.[Target]
		,[ActivityStartDttm] 	= tp.[Created]
		,[SessionStartDttm] 	= s.[Created]
		,[ActivityYear] 		= YEAR(tp.[Created])
		,[ActivityMonth] 		= MONTH(tp.[Created])
		,[ActivityWeek]		= DATEPART(wk ,tp.[Created])
		FROM [dbo].[Session] s
		JOIN [dbo].[FeatureClick] tp
			ON tp.[SessionID] = s.[SessionID]
			AND s.[MemberID] IS NOT NULL
		WHERE s.[Created] >= @Dttm
			AND s.[MemberID] IS NOT NULL

	UNION ALL
		SELECT DISTINCT
		 [SiteCode]		= 'CCO'
		,[MemberGUId] 		= s.[MemberID]
		,[SessionGUId] 		= s.[SessionID]
		,[PageViewGUID]		= tp.[RedirectViewID]
		,[PageItemGUID]		= tp.[RedirectViewID]
		,[URL]				= tp.[Redirect]
		,[ActivityStartDttm] 	= tp.[Created]
		,[SessionStartDttm] 	= s.[Created]
		,[ActivityYear] 		= YEAR(tp.[Created])
		,[ActivityMonth] 		= MONTH(tp.[Created])
		,[ActivityWeek]		= DATEPART(wk ,tp.[Created])
		FROM [dbo].[Session] s
		JOIN [dbo].[RedirectView] tp
			ON tp.[SessionID] = s.[SessionID]
			AND s.[MemberID] IS NOT NULL
		WHERE s.[Created] >= @Dttm
			AND s.[MemberID] IS NOT NULL

	) pv

END
;