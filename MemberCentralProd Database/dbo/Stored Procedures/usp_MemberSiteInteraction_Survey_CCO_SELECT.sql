﻿CREATE PROCEDURE [dbo].[usp_MemberSiteInteraction_Survey_CCO_SELECT]
(
 @Dttm DATETIME = '2000-01-01'
)
AS
BEGIN

	SELECT
		 [SiteCode] 		
		,[SourceCode]		
		,[SRC_MemberSurveyGUID]	
		,[SRC_MemberGUID]	
		,[SRC_SurveyGUID]	
		,[SRC_QuestionGUID] 
		,[SRC_ResponseGUID] 
		,[ResponseText] 
		,[QuestionTypeName]	
		,[ResponseTypeName]	
		,[SurveyType]		
		,[CountryCode]		
		,[ActivityDttm]		
		,[CompletedInd]		
		,[PassedInd]		
		,[InsertedDttm]		
		,[LastModifiedDttm]
	FROM
	(
		SELECT 
		 [SiteCode] 		= 'CCO'
		,[SourceCode]		= 'CCO'
		,[SRC_MemberSurveyGUID]	= tr.[TestResultID]
		,[SRC_MemberGUID]	= tr.[MemberID]
		,[SRC_SurveyGUID]	= tr.[TestID]
		,[SRC_QuestionGUID] = trqa.[QuestionID]
		,[SRC_ResponseGUID] = trqa.[QuestionAnswerID]
		,[ResponseText] 		= CONVERT(nvarchar(1024), '')
		,[QuestionTypeName]	= CASE WHEN trqa.[QuestionAnswerID] IS NULL AND ISNULL(CONVERT(nvarchar(1024),  ''), '') <> '' THEN 'Free-form Text' ELSE 'QuestionType Lookup' END
		,[ResponseTypeName]	= 'ResponseType Lookup'
		,[SurveyType]		= CONVERT(varchar(36), 'Test')
		,[CountryCode]		= co.[Abbreviation]
		,[ActivityDttm]		= tr.[Created]
		,[CompletedInd]		= CONVERT(bit, 1)
		,[PassedInd]		= CONVERT(bit, tr.[PassedTest])
		,[InsertedDttm]		= tr.Created
		,[LastModifiedDttm]	= tr.Updated
		FROM [dbo].[TestResult] tr
		LEFT JOIN [dbo].[TestResultQuestionAnswer] trqa
			ON trqa.[TestResultID] = tr.[TestResultID]
		LEFT JOIN [dbo].[Country] co
			ON co.[CountryId] = tr.[CountryID]
		WHERE 1=1
			AND COALESCE (tr.[Updated], tr.[Created]) >= @Dttm

		UNION ALL

		SELECT 
		 [SiteCode] 		= 'CCO'
		,[SourceCode]		= 'CCO'
		,[SRC_MemberSurveyGUID]	= er.[EvaluationResultID]
		,[SRC_MemberGUID]	= tr.[MemberID]
		,[SRC_SurveyGUID]	= er.[EvaluationID]
		,[SRC_QuestionGUID] = trqa.[QuestionID]
		,[SRC_ResponseGUID] = trqa.[QuestionAnswerID]
		,[ResponseText] 	= CONVERT(nvarchar(1024), trqa.[FreeTextAnswer])
		,[QuestionTypeName]	= CASE WHEN trqa.[QuestionAnswerID] IS NULL AND ISNULL(CONVERT(nvarchar(1024),  trqa.[FreeTextAnswer]), '') <> '' THEN 'Free-form Text' ELSE 'QuestionType Lookup' END
		,[ResponseTypeName]	= 'ResponseType Lookup'
		,[SurveyType]		=  CONVERT(varchar(36), 'Evaluation')
		,[CountryCode]		= co.[Abbreviation]
		,[ActivityDttm]		= er.[Created]
		,[CompletedInd]		= CONVERT(bit, 1)
		,[PassedInd]		= CONVERT(bit, tr.[PassedTest])
		,[InsertedDttm]		= er.Created
		,[LastModifiedDttm]	= er.Updated
		FROM [dbo].[EvaluationResult] er
		JOIN [dbo].[TestResult] tr
			ON tr.[TestResultID] = er.[TestResultID]
		LEFT JOIN [dbo].[EvaluationResultQuestionAnswer] trqa
			ON trqa.[EvaluationResultID] = er.[EvaluationResultID]
		LEFT JOIN [dbo].[Country] co
			ON co.[CountryId] = tr.[CountryID]
		WHERE COALESCE (tr.[Updated], tr.[Created]) >= @Dttm

		UNION ALL

		SELECT 
		 [SiteCode] 		= 'CCO'
		,[SourceCode]		= 'CCO'
		,[SRC_MemberSurveyGUID]	= cv.[CaseVisitID]
		,[SRC_MemberGUID]	= s.[MemberID]
		,[SRC_SurveyGUID]	= cv.[CaseID]
		,[SRC_QuestionGUID] = trqa.[QuestionID]
		,[SRC_ResponseGUID] = trqa.[QuestionAnswerID]
		,[ResponseText] 	= CONVERT(nvarchar(1024), '')
		,[QuestionTypeName]	= CASE WHEN trqa.[QuestionAnswerID] IS NULL AND ISNULL(CONVERT(nvarchar(1024),  ''), '') <> '' THEN 'Free-form Text' ELSE 'QuestionType Lookup' END
		,[ResponseTypeName]	= 'ResponseType Lookup'
		,[SurveyType]		=  CONVERT(varchar(36), 'Case Challenge')
		,[CountryCode]		= ''
		,[ActivityDttm]		= cv.[Created]
		,[CompletedInd]		= CONVERT(bit,1)
		,[PassedInd]		= CONVERT(bit,NULL)
		,[InsertedDttm]		= cv.Created
		,[LastModifiedDttm]	= cv.Updated
		FROM [dbo].[CaseVisit] cv
		JOIN [dbo].[Session] s
			ON s.[SessionId] = cv.[SessionId]
		LEFT JOIN [dbo].[CasePrimaryResponse] trqa
			ON trqa.[CaseVisitID] = cv.[CaseVisitID]
		WHERE COALESCE (cv.[Updated], cv.[Created]) >= @Dttm

		UNION ALL

		SELECT 
		 [SiteCode] 		= 'CCO'
		,[SourceCode]		= 'CCO'
		,[SRC_MemberSurveyGUID]	= sr.[SurveyResultID]
		,[SRC_MemberGUID]	= sr.[MemberID]
		,[SRC_SurveyGUID]	= sr.[SurveyID]
		,[SRC_QuestionGUID] = trqa.[QuestionID]
		,[SRC_ResponseGUID] = trqa.[QuestionAnswerID]
		,[ResponseText] 	= CONVERT(nvarchar(1024), trqa.[FreeTextAnswer])
		,[QuestionTypeName]	= CASE WHEN trqa.[QuestionAnswerID] IS NULL AND ISNULL(CONVERT(nvarchar(1024),  trqa.[FreeTextAnswer]), '') <> '' THEN 'Free-form Text' ELSE 'QuestionType Lookup' END
		,[ResponseTypeName]	= 'ResponseType Lookup'
		,[SurveyType]		= st.[Text]
		,[CountryCode]		= ''
		,[ActivityDttm]		= sr.[Created]
		,[CompletedInd]		= sr.[IsComplete]
		,[PassedInd]		= CONVERT(bit,NULL)
		,[InsertedDttm]		= sr.Created
		,[LastModifiedDttm]	= sr.Updated
		FROM [dbo].[SurveyResult] sr
		JOIN [dbo].[SurveyType] st
			ON st.[SurveyTypeId] = sr.[SurveyTypeId]
		LEFT JOIN [dbo].[SurveyResultQuestionAnswer] trqa
			ON trqa.[SurveyResultID] = sr.[SurveyResultID]
		WHERE COALESCE (sr.[Updated], sr.[Created]) >= @Dttm

		UNION ALL

		SELECT 
		 [SiteCode] 		= 'CCO'
		,[SourceCode]		= 'CCO'
		,[SRC_MemberSurveyGUID]	= qr.[Id]
		,[SRC_MemberGUID]	= qr.[MemberID]
		,[SRC_SurveyGUID]	= qr.[SurveyID]
		,[SRC_QuestionGUID] = trqa.[QuestionTextId]
		,[SRC_ResponseGUID] = trqa.[ResponseTextId]
		,[ResponseText] 	= CONVERT(nvarchar(1024), trqa.[FreeTextResponse])
		,[QuestionTypeName]	= CASE WHEN trqa.[QuestionTextId] IS NULL AND ISNULL(CONVERT(nvarchar(1024),  trqa.[FreeTextResponse]), '') <> '' THEN 'Free-form Text' ELSE 'QuestionType Lookup' END
		,[ResponseTypeName]	= 'ResponseType Lookup'
		,[SurveyType]		=  CONVERT(varchar(36), 'Site Survey')
		,[CountryCode]		= ''
		,[ActivityDttm]		= qr.[Created]
		,[CompletedInd]		= CONVERT(bit,1)
		,[PassedInd]		= CONVERT(bit,NULL)
		,[InsertedDttm]		= qr.Created
		,[LastModifiedDttm]	= qr.Updated
		FROM [dbo].[Questionnaire] qr
		LEFT JOIN [dbo].[Response] trqa
			ON trqa.[QuestionnaireId] = qr.[Id]
		WHERE COALESCE (qr.[Updated], qr.[Created]) >= @Dttm

	) pv

END
;
GO