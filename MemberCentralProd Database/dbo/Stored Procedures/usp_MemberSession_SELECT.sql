﻿CREATE PROCEDURE [dbo].[usp_MemberSession_SELECT]
(
@YearsBack INT = -3
)
AS
BEGIN
	SELECT 
	 [SiteCode]
	,[MemberGUId] 		
	,[SessionGUId] 		
	,[URL]              
	,[SessionStartDt] 	
	,[SessionYear] 		
	,[SessionMonth] 	
	,[SessionWeek]
	FROM
	(
		SELECT [SiteCode]		= 'CCO'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionID]
		,	[DeviceTypeName]    = ''
		,	[OSVersionName]     = ''
		,	[URL]               = s.[Referrer]
		,	[SessionStartDt] 	= s.[Created]
		,	[SessionYear] 		= YEAR(s.[Created])
		,	[SessionMonth] 		= MONTH(s.[Created])
		,	[SessionWeek]		= DATEPART(wk ,s.[Created])
		FROM [MemberCentralProd].[dbo].[Session] s
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])

		UNION ALL
		SELECT [SiteCode]		= 'IPR'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionUniqueId]
		,	[DeviceTypeName]    = ''
		,	[OSVersionName]     = ''
		,	[URL]               = sd.[Referrer]
		,	[SessionStartDt] 	= s.[Created]
		,	[SessionYear] 		= YEAR(s.[Created])
		,	[SessionMonth] 		= MONTH(s.[Created])
		,	[SessionWeek]		= DATEPART(wk ,s.[Created])
		FROM [INP_Tracking].[dbo].[Session] s
		LEFT JOIN [INP_Tracking].[dbo].[SessionDetails] sd
			ON sd.[SessionId] = s.[SessionId]
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])

		UNION ALL
		SELECT [SiteCode]		= 'IPA'
		,	[MemberGUId] 		= s.[MemberID]
		,	[SessionGUId] 		= s.[SessionUniqueId]
		,	[DeviceTypeName]    = ''
		,	[OSVersionName]     = ''
		,	[URL]               = sd.[Referrer]
		,	[SessionStartDt] 	= s.[Created]
		,	[SessionYear] 		= YEAR(s.[Created])
		,	[SessionMonth] 		= MONTH(s.[Created])
		,	[SessionWeek]		= DATEPART(wk ,s.[Created])
		FROM [IPA_Tracking].[dbo].[Session] s
		LEFT JOIN [INP_Tracking].[dbo].[SessionDetails] sd
			ON sd.[SessionId] = s.[SessionId]
		WHERE s.[Created] >= DATEADD(YEAR, @YearsBack, s.[Created])
	) ss
	;
END
;