﻿
CREATE PROCEDURE [dbo].[FixEvals]
	@questionSetId uniqueidentifier,
	@questionnaireId uniqueidentifier,
    @questionTextId  uniqueidentifier
AS
WITH CTE1
     AS (SELECT *,
                Row_number() OVER (ORDER BY SortOrder) AS RN
         FROM   ResponseFixes 
		 Where QuestionSetId = @questionSetId
		 ),
     CTE2
     AS (SELECT *,
                Row_number() OVER (ORDER BY Created) AS RN
         FROM   Response
         Where QuestionnaireId = @questionnaireId
         and QuestionTextId = @questionTextId
         )
UPDATE CTE2
SET		QuestionTextId = CTE1.QuestionTextId,
		Updated = GETDATE()
		
FROM   CTE2
       JOIN CTE1
         ON CTE1.RN = CTE2.RN  

