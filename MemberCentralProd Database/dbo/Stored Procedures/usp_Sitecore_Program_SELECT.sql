﻿CREATE PROCEDURE [dbo].[usp_Sitecore_Program_SELECT]
AS
BEGIN

	SELECT DISTINCT 
	  [ProgramGUID]			= CONVERT(UNIQUEIDENTIFIER, I.[ID])
	, [ProgramName]			= CONVERT(VARCHAR(512), I.[Name]) COLLATE SQL_Latin1_General_CP1_CI_AS
	, [ProgramStartDate]	= CONVERT(DATE,(CASE WHEN CHARINDEX('T', fv.[Value]) > 0 THEN LEFT(fv.[Value], 8) ELSE fv.[Value] END))
	--, [SurveyTextType]		= fvi.[Name]
	FROM [CCO_Sitecore_Web].[dbo].[Items] i
	LEFT JOIN [CCO_Sitecore_Web].[dbo].[VersionedFields] fv
	ON fv.[ItemId] = i.[ID]
	LEFT JOIN [CCO_Sitecore_Web].[dbo].[Items] fvi
		ON fvi.[ID] = fv.[FieldId]
	WHERE  1=1
		AND fvi.[Name] IN('StartDate')
	--	AND NOT(i.[Name] IN ('__Standard Values'))

END
;
GO