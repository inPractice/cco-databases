﻿CREATE PROCEDURE [dbo].[usp_MemberLastLoginDate_SELECT]
(
	@Dttm datetime = '2000-01-01'
	, @MemberGUID uniqueidentifier = '99999999-9999-9999-9999-999999999999'
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	SiteCode = 'CCO'
	, MemberId
	, [LoginDttm] = MAX(s.Created)
	FROM dbo.Session s
	WHERE 1=1
	AND  ISNULL(s.MemberID, '00000000-0000-0000-0000-000000000000') <> '00000000-0000-0000-0000-000000000000'
	AND Year(s.Created) <= YEAR(GetDate())
	AND s.[Created] >= @Dttm
	AND s.[MemberId] = CASE WHEN @MemberGUID <> '99999999-9999-9999-9999-999999999999' THEN @MemberGUID ELSE s.[MemberId] END
	GROUP BY s.MemberId
	;

END
GO
